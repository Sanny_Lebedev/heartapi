package main

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"path/filepath"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	myreq "bitbucket.org/Sanny_Lebedev/heartapi/utils/requests"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// UsersController implements the users resource.
type UsersController struct {
	*goa.Controller
	privateKey *rsa.PrivateKey
	DB         *pgx.ConnPool
	log        logger.Logger
	picsURL    string
}

// Answer ...
type Answer struct {
	TotalCount string
	Data       int
}

// NewUsersController creates a users controller.
func NewUsersController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, pics string) (*UsersController, error) {
	b, err := ioutil.ReadFile(filepath.Join(binpath.BinaryPath(), "keys/rs256-4096-private.rsa"))
	if err != nil {
		return nil, err
	}
	privKey, err := jwtgo.ParseRSAPrivateKeyFromPEM(b)
	if err != nil {
		return nil, fmt.Errorf("jwt: failed to load private key: %s", err) // bug
	}
	return &UsersController{
		Controller: service.NewController("AuthController"),
		privateKey: privKey,
		DB:         pg,
		log:        log,
		picsURL:    pics,
	}, nil
	//return &UsersController{Controller: service.NewController("UsersController")}
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *UsersController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			// res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		// res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *UsersController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Getlist runs the getlist action.
func (c *UsersController) Getlist(ctx *app.GetlistUsersContext) error {
	// UsersController_Getlist: start_implement

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	res, uid := c.Preset(token, claims)
	if !res {
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		c.log.Error().Err(err).Msg("Faild to load users information")
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only for ADMINS !!!!
	if MyUser.Is(user.RoleAdmin) {

		req := new(myreq.Req)

		Search := new(myreq.SearchPayload)
		Search.Search = *ctx.Payload.Search
		Search.CurrentPage = *ctx.Payload.CurrentPage
		Search.ItemsOnPage = *ctx.Payload.ItemsOnPage
		Search.Types = *ctx.Payload.Types
		Search.Actives = *ctx.Payload.Actives

		req.Search = ""
		outlist, _ := req.GetUserList(*Search, c.DB, c.picsURL)
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Users", "List of users", &outlist))
		return ctx.OK(&res)
	}

	return ctx.BadRequest(goa.ErrBadRequest("Information is available only for administrators"))

}

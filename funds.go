package main

import (
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"
	"sync"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	Funds "bitbucket.org/Sanny_Lebedev/heartapi/utils/funds"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/ibase"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// FundsController implements the funds resource.
type FundsController struct {
	*goa.Controller
	DB   *pgx.ConnPool
	log  logger.Logger
	pics string
	*sync.Mutex
	images []*ImageData
}

// NewFundsController creates a image controller.
func NewFundsController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, pics string) (*FundsController, error) {
	return &FundsController{
		Controller: service.NewController("FundsController"),
		DB:         pg,
		log:        log,
		pics:       pics,
		Mutex:      &sync.Mutex{},
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *FundsController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			// res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		// res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *FundsController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Changeactive runs the changeactive action.
func (c *FundsController) Changeactive(ctx *app.ChangeactiveFundsContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) {

		if err := Funds.ChangeActive(c.DB, *ctx.Payload.UID); err != nil {
			answer.Success = false
			c.log.Error().Err(err).Msg("Error with change active status of fund")
			answer.Action = "Ошибка изменения статуса фонда"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		answer.Success = true
		answer.Action = "Статус фонда успешно изменен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Deleteitems runs the deleteitems action.
func (c *FundsController) Deleteitems(ctx *app.DeleteitemsFundsContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) {

		// Check by uid
		if Funds.Exist(c.DB, *ctx.Payload.UID) == false {
			answer.Success = false
			answer.Action = "Фонд не найден"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		pathtofile := filepath.Join(binpath.BinaryPath(), "images/funds")

		if _, err := os.Stat(filepath.Join(pathtofile, *ctx.Payload.UID+".jpg")); !os.IsNotExist(err) {
			err := os.Remove(filepath.Join(pathtofile, *ctx.Payload.UID+".jpg"))
			if err != nil {
				fmt.Println(err)
				c.log.Error().Err(err).Msg("Error with delete fund image from disk")
				answer.Success = false
				answer.Action = "Ошибка удаления изображения"
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
		}

		errDel := Funds.Delete(c.DB, *ctx.Payload.UID)
		if errDel != nil {
			c.log.Error().Err(errDel).Msg("Error with delete fund from DB")
			answer.Success = false
			answer.Action = "Ошибка удаления фонда"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		errDel = Funds.DeletePhoto(c.DB, *ctx.Payload.UID)
		if errDel != nil {
			c.log.Error().Err(errDel).Msg("Error with delete funds photo from DB")
			answer.Success = false
			answer.Action = "Ошибка удаления фонда"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}
		answer.Success = true
		answer.Action = "Фонд успешно удален"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Deletephoto runs the deletephoto action.
func (c *FundsController) Deletephoto(ctx *app.DeletephotoFundsContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) {

		// Check by uid
		if Funds.Exist(c.DB, *ctx.Payload.UID) == false {
			answer.Success = false
			answer.Action = "Фонд не найден"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		pathtofile := filepath.Join(binpath.BinaryPath(), "images/funds")

		if _, err := os.Stat(filepath.Join(pathtofile, *ctx.Payload.UID+".jpg")); !os.IsNotExist(err) {
			err := os.Remove(filepath.Join(pathtofile, *ctx.Payload.UID+".jpg"))
			if err != nil {
				fmt.Println(err)
				c.log.Error().Err(err).Msg("Error with delete fund image from disk")
				answer.Success = false
				answer.Action = "Ошибка удаления изображения"
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
		}

		errDel := Funds.DeletePhoto(c.DB, *ctx.Payload.UID)
		if errDel != nil {
			c.log.Error().Err(errDel).Msg("Error with delete funds photo from DB")
			answer.Success = false
			answer.Action = "Ошибка удаления изображения"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}
		answer.Success = true
		answer.Action = "Изображение успешно удалено"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Item runs the item action.
func (c *FundsController) Item(ctx *app.ItemFundsContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(Funds.AnswerOne)
	var err error
	answer.Info, err = Funds.GetOne(c.DB, c.pics, ctx.UID)
	if err != nil && err != pgx.ErrNoRows {
		c.log.Error().Err(err).Msg("Error with getting fund")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования данных о фонде"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Action = "Информация о фонде"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)

}

// Items runs the items action.
func (c *FundsController) Items(ctx *app.ItemsFundsContext) error {
	var res app.MyservAuthToapp
	// token := jwt.ContextJWT(ctx)
	// claims := token.Claims.(jwtgo.MapClaims)
	// result, uid := c.Preset(token, claims)
	// if !result {
	// 	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
	// 	return ctx.OK(&res)
	// }

	// MyUser := user.NewUser(c.DB, c.log)
	// if err := c.LoadUserInfo(MyUser, uid); err != nil {
	// 	return ctx.BadRequest(goa.ErrBadRequest(err))
	// }

	answer := new(Funds.Answer)
	search := new(Funds.Search)
	var err error
	search.Paginator = false

	if ctx.Search != nil {
		search.Search = *ctx.Search
	}

	search.Onpage = 0
	search.Current = 0
	search.Paginator = false

	if ctx.OnPage != nil && ctx.CurrentPage != nil {
		if *ctx.OnPage != "" && *ctx.CurrentPage != "" {
			search.Onpage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Current, err = strconv.Atoi(*ctx.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Paginator = true
		}
	}

	answer.List, answer.TotalCount, err = Funds.Get(c.DB, c.pics, *search)

	if err != nil && err != pgx.ErrNoRows {
		c.log.Error().Err(err).Msg("Error with getting fund")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования данных "
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.HasNext = false
	if search.Onpage*search.Current < answer.TotalCount {
		answer.HasNext = true
	}
	answer.Count = len(answer.List)
	answer.Success = true
	answer.Action = "Список фондов"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Senditems runs the senditems action.
func (c *FundsController) Senditems(ctx *app.SenditemsFundsContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	reader, _ := ctx.MultipartReader()

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(SavingAnswer)

	if MyUser.Is(user.RoleAdmin) {

		// Saving section information
		myFund := new(Funds.Fund)

		myFund.Title = ctx.Payload.Title
		myFund.OwnerUID = uid
		if ctx.Payload.UID != nil {
			myFund.UID = *ctx.Payload.UID
		}
		if ctx.Payload.Inn != nil {
			myFund.Inn = *ctx.Payload.Inn
		}

		if ctx.Payload.Description != nil {
			myFund.Description = *ctx.Payload.Description
		}
		if ctx.Payload.Status != nil {
			myFund.Status = *ctx.Payload.Status
		}
		if ctx.Payload.Active != nil {
			myFund.Active = *ctx.Payload.Active
		}

		fund := Funds.New(c.DB, *myFund, c.log)
		newUID, saveErr := fund.Save()
		if saveErr != nil {
			return ctx.BadRequest(goa.ErrBadRequest(saveErr))
		}

		if ctx.Payload.Contact != nil {
			contact := models.Contacts{}

			if ctx.Payload.Contact.Website != nil {
				contact.Website = *ctx.Payload.Contact.Website
			}
			if ctx.Payload.Contact.Skype != nil {
				contact.Skype = *ctx.Payload.Contact.Skype
			}

			contact.Email = ctx.Payload.Contact.Email
			contact.Phone = ctx.Payload.Contact.Phone
			contact.Person = ctx.Payload.Contact.Person
			fund.Fund.Contacts = contact
			saveErr := fund.SaveContacts()
			if saveErr != nil {
				return ctx.BadRequest(goa.ErrBadRequest(saveErr))
			}

		}

		if ctx.Payload.Address != nil {
			address := models.Address{}
			if ctx.Payload.Address.Postal != nil {
				address.Postal = *ctx.Payload.Address.Postal
			}
			address.City = ctx.Payload.Address.City
			address.Address = ctx.Payload.Address.Address
			address.Country = ctx.Payload.Address.Country
			fund.Fund.Address = address
			saveErr := fund.SaveAddress()
			if saveErr != nil {
				return ctx.BadRequest(goa.ErrBadRequest(saveErr))
			}
		}

		conflag := 0
		if reader != nil {
			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/funds")

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				// if conflag > 1 {
				// 	c.log.Error().Msg("wrong numbers of files")
				// 	return ctx.BadRequest(goa.ErrBadRequest("wrong numbers of files"))
				// }

				if err == io.EOF {
					break
				}
				if err != nil {
					break
					// c.log.Error().Err(err).Msg("failed to load part")
					// return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))
				}

				if p.Header["Content-Type"][0] != contentTypeImage {
					return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
				}
				// nres, list, err := uploads.SaveinProfileFiles(c.DB, *ctx.UID)
				// if err != nil {
				// 	return ctx.InternalServerError()
				// }
				f, err := os.OpenFile(filepath.Join(pathtofile, newUID+".jpg"), os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					c.log.Error().Err(err).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()
				io.Copy(f, p)
				conflag = conflag + 1
			}
		}
		answer.UID = newUID
		answer.Success = true
		answer.Action = "Раздел сохранен"

		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)

}

// Sendphoto runs the sendphoto action.
func (c *FundsController) Sendphoto(ctx *app.SendphotoFundsContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	reader, _ := ctx.MultipartReader()

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(ActionAnswer)

	// conf := ibase.Conf{c.DB, c.log}
	// myFunds := Funds.Fund{}
	// myFunds.UID = *ctx.UID
	// iFunds := ibase.Fund{conf, myFunds}
	// isExist := iFunds.IsExist()

	iFunds := ibase.Fund{}
	iFunds = iFunds.SetStruct(c.DB, c.log, *ctx.UID)
	isExist := iFunds.IsExist()
	// isExist, errChk := Funds.FundCheck(c.DB, *ctx.UID)

	// if errChk != nil {
	// 	c.log.Error().Err(errChk).Msg("Error with funds checking")
	// 	return ctx.BadRequest(goa.ErrBadRequest(errChk))
	// }

	if !isExist {
		answer.Action = "Фонд не найден"
		answer.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) {

		conflag := 0
		if reader != nil {
			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/funds")

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				if conflag > 1 {
					c.log.Error().Msg("wrong numbers of files")
					return ctx.BadRequest(goa.ErrBadRequest("wrong numbers of files"))
				}

				if err == io.EOF {
					break
				}
				if err != nil {
					break
					// c.log.Error().Err(err).Msg("failed to load part")
					// return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))
				}

				if p.Header["Content-Type"][0] != contentTypeImage {
					return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
				}

				// Check line in DB about photo
				existPic, errPic := Funds.ExistPhotoinDB(c.DB, *ctx.UID)
				if errPic != nil {
					c.log.Error().Err(errPic).Msg("Error of check DB for pics for fund")
					answer.Success = false
					answer.Action = "Ошибка сохранения данных"
					res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
					return ctx.OK(&res)
				}

				err = Funds.SavePhotoFile(c.DB, *ctx.UID, uid, existPic)
				if err != nil {
					c.log.Error().Err(errPic).Msg("Error of check DB for pics for fund")
					answer.Success = false
					answer.Action = "Ошибка сохранения данных"
					res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
					return ctx.OK(&res)
				}

				f, errFile := os.OpenFile(filepath.Join(pathtofile, *ctx.UID+".jpg"), os.O_WRONLY|os.O_CREATE, 0666)
				if errFile != nil {
					c.log.Error().Err(errFile).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()
				io.Copy(f, p)
				conflag = conflag + 1
			}
		}

		answer.Success = true
		answer.Action = "Изображение успешно сохранено"

		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

package main

import (
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/ibase"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/sections"
	"github.com/goadesign/goa"
	"github.com/jackc/pgx"

	"github.com/pkg/errors"

	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa/middleware/security/jwt"
)

const (
	contentTypeImage      = "image/jpeg"
	contentTypeVideo      = "video/mp4"
	contentTypeVideo_mp4  = "video/mp4"
	contentTypeVideo_flv  = "video/x-flv"
	contentTypeVideo_m3u8 = "application/x-mpegURL"
	contentTypeVideo_3gp  = "video/3gpp"
	contentTypeVideo_mov  = "video/quicktime"
	contentTypeVideo_avi  = "video/x-msvideo"
	contentTypeVideo_wmv  = "video/x-ms-wmv"
)

// SectionsController implements the sections resource.
type SectionsController struct {
	*goa.Controller
	DB   *pgx.ConnPool
	log  logger.Logger
	Conf configMain
}

// NewPollsController creates a polls controller.
func NewSectionsController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, configMain configMain) (*SectionsController, error) {
	return &SectionsController{
		Controller: service.NewController("SectionsController"),
		DB:         pg,
		log:        log,
		Conf:       configMain,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *SectionsController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *SectionsController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Items runs the items action.
func (c *SectionsController) Items(ctx *app.ItemsSectionsContext) error {
	// SectionsController_Items: start_implement
	var err error
	answer := new(sections.Answer)

	//	List := new(sections.[]*List, 0)
	answer.List, err = sections.Get(c.DB, c.Conf.Picsurl.URL)
	if err != nil {
		c.log.Error().Err(err).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка раздела"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Count = len(answer.List)
	answer.Action = "Список разделов"
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
	// SectionsController_Items: end_implement
}

// Deletephoto - delete photo
func (c *SectionsController) Deletephoto(ctx *app.DeletephotoSectionsContext) error {

	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) {

		if ctx.Sectionuid == nil {
			answer.Success = false
			answer.Action = "Неверный формат запроса"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		// Check by uid
		if sections.Exist(c.DB, *ctx.Sectionuid) == false {
			answer.Success = false
			answer.Action = "Запись не найдена"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}
		pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")
		err := os.Remove(filepath.Join(pathtofile, *ctx.Sectionuid+".jpg"))
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}

		answer.Success = true
		answer.Action = "Изображение успешно удалено"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Deleteitems - delete items
func (c *SectionsController) Deleteitems(ctx *app.DeleteitemsSectionsContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) {

		if ctx.Sectionuid == nil {
			answer.Success = false
			answer.Action = "Неверный формат запроса"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		iSection := ibase.Sections{}
		iSection = iSection.SetStruct(c.DB, c.log, *ctx.Sectionuid)
		isExist := iSection.IsExist()

		if isExist == false {
			answer.Success = false
			answer.Action = "Запись не найдена"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}
		pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")

		if _, err := os.Stat(filepath.Join(pathtofile, *ctx.Sectionuid+".jpg")); !os.IsNotExist(err) {
			err := os.Remove(filepath.Join(pathtofile, *ctx.Sectionuid+".jpg"))
			if err != nil {
				fmt.Println(err)
				answer.Success = false
				answer.Action = "Ошибка удаления изображения"
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
		}

		errDel := sections.Delete(c.DB, *ctx.Sectionuid)
		if errDel != nil {
			answer.Success = false
			answer.Action = "Ошибка удаления записи"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		answer.Success = true
		answer.Action = "Раздел успешно удален"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Sendphoto - sending photo for section
func (c *SectionsController) Sendphoto(ctx *app.SendphotoSectionsContext) error {
	var res app.MyservAuthToapp
	reader, err := ctx.MultipartReader()
	if err != nil && reader != nil {
		return goa.ErrBadRequest("failed to load multipart request: %s", err)
	}

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)

	if MyUser.Is(user.RoleAdmin) {

		if reader == nil {
			answer.Success = false
			answer.Action = "Отсутствует файл в запросе"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		if ctx.Sectionuid == nil {
			answer.Success = false
			answer.Action = "Неверный формат запроса"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		// Check by uid
		if sections.Exist(c.DB, *ctx.Sectionuid) == false {
			answer.Success = false
			answer.Action = "Запись не найдена"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		conflag := 0

		if reader != nil {
			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				if err == io.EOF {
					break
				}
				if err != nil {
					answer.Success = false
					answer.Action = "Файл не найден"
					res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
					return ctx.OK(&res)
				}

				if p.Header["Content-Type"][0] != contentTypeImage {
					answer.Success = false
					answer.Action = "Неверный формат изображения. Допускается только jpg"
					res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
					return ctx.OK(&res)
				}
				// nres, list, err := uploads.SaveinProfileFiles(c.DB, *ctx.UID)
				// if err != nil {
				// 	return ctx.InternalServerError()
				// }
				f, err := os.OpenFile(filepath.Join(pathtofile, *ctx.Sectionuid+".jpg"), os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					c.log.Error().Err(err).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()
				io.Copy(f, p)
				conflag = conflag + 1
			}
		}

		answer.Success = true
		answer.Action = "Фото успешно сохранено"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Senditems runs the senditems action.
func (c *SectionsController) Senditems(ctx *app.SenditemsSectionsContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	reader, _ := ctx.MultipartReader()
	// if err != nil && reader != nil {
	// 	return goa.ErrBadRequest("failed to load multipart request: %s", err)
	// }

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(ActionAnswer)

	if MyUser.Is(user.RoleAdmin) {

		// Determine the type of action. Either editing or adding

		//		if ctx.Sectionuid == nil {

		// Saving section information
		Section := sections.New(c.DB, ctx, MyUser.UID, c.log)
		newUID, saveErr := Section.Save()
		if saveErr != nil {
			return ctx.BadRequest(goa.ErrBadRequest(saveErr))
		}

		// } else {

		// }

		conflag := 0

		if reader != nil {
			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				// if conflag > 1 {
				// 	c.log.Error().Msg("wrong numbers of files")
				// 	return ctx.BadRequest(goa.ErrBadRequest("wrong numbers of files"))
				// }

				if err == io.EOF {
					break
				}
				if err != nil {
					break
					// c.log.Error().Err(err).Msg("failed to load part")
					// return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))
				}

				if p.Header["Content-Type"][0] != contentTypeImage {
					return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
				}
				// nres, list, err := uploads.SaveinProfileFiles(c.DB, *ctx.UID)
				// if err != nil {
				// 	return ctx.InternalServerError()
				// }
				f, err := os.OpenFile(filepath.Join(pathtofile, newUID+".jpg"), os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					c.log.Error().Err(err).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()
				io.Copy(f, p)
				conflag = conflag + 1
			}
		}

		answer.Success = true
		answer.Action = "Раздел сохранен"

		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}

	answer.Success = false
	answer.Action = "Раздел доступен только администатору"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
	// SectionsController_Senditems: end_implement

	// SectionsController_Senditems: end_implement
}

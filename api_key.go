package main

import (
	"context"
	"net/http"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"github.com/goadesign/goa"
)

// NewAPIKeyMiddleware creates a middleware that checks for the presence of an authorization header
// and validates its content.
func NewAPIKeyMiddleware() goa.Middleware {
	// Instantiate API Key security scheme details generated from design
	scheme := app.NewAPIKeySecurity()

	// Middleware
	return func(h goa.Handler) goa.Handler {
		return func(ctx context.Context, rw http.ResponseWriter, req *http.Request) error {
			// Retrieve and log header specified by scheme
			key := req.Header.Get(scheme.Name)
			// A real app would do something more interesting here
			if len(key) == 0 || key == "Bearer" {
				goa.LogInfo(ctx, "failed api key auth")
				return ErrUnauthorized("missing auth")
			}
			// Proceed.
			goa.LogInfo(ctx, "auth", "apikey", "key", key)
			return h(ctx, rw, req)
		}
	}
}

// APIKeyController implements the api_key resource.
type APIKeyController struct {
	*goa.Controller
}

// NewAPIKeyController creates a api_key controller.
func NewAPIKeyController(service *goa.Service) *APIKeyController {
	return &APIKeyController{Controller: service.NewController("APIKeyController")}
}

// Secure runs the secure action.
func (c *APIKeyController) Secure(ctx *app.SecureAPIKeyContext) error {
	// APIKeyController_Secure: start_implement

	// Put your logic here

	res := &app.Success{}
	return ctx.OK(res)
	// APIKeyController_Secure: end_implement
}

// Unsecure runs the unsecure action.
func (c *APIKeyController) Unsecure(ctx *app.UnsecureAPIKeyContext) error {
	// APIKeyController_Unsecure: start_implement

	// Put your logic here

	res := &app.Success{}
	return ctx.OK(res)
	// APIKeyController_Unsecure: end_implement
}

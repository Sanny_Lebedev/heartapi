package main

import (
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"github.com/goadesign/goa"
	"github.com/jackc/pgx"

	"github.com/pkg/errors"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/fundraising"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/ibase"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"

	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa/middleware/security/jwt"
)

// FundraisingController implements the sections resource.
type FundraisingController struct {
	*goa.Controller
	DB   *pgx.ConnPool
	log  logger.Logger
	Conf configMain
}

// NewFundraisingController creates a polls controller.
func NewFundraisingController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, configMain configMain) (*FundraisingController, error) {
	return &FundraisingController{
		Controller: service.NewController("FundraisingController"),
		DB:         pg,
		log:        log,
		Conf:       configMain,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *FundraisingController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *FundraisingController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Deleteitems runs the deleteitems action.
func (c *FundraisingController) Deleteitems(ctx *app.DeleteitemsFundraisingContext) error {
	var err error
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)

	if !MyUser.Is(user.RoleAdmin) {
		answer.Success = false
		answer.Action = "Ошибка удаления сбора"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	err = fundraising.Delete(c.DB, c.log, ctx.Payload.UID)
	if err != nil {
		c.log.Error().Err(err).Msg("Error forming fundraising info")
		answer.Success = false
		answer.Action = "Ошибка удаления сбора"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Action = "Сбор удален"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Deletephoto runs the deletephoto action.
func (c *FundraisingController) Deletephoto(ctx *app.DeletephotoFundraisingContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) {
		var err error
		pathtofile := filepath.Join(binpath.BinaryPath(), "images/fundraising")
		myfilename := *ctx.UID + ".jpg"

		err = os.Remove(filepath.Join(pathtofile, myfilename))
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}
		Fundraising := fundraising.Initial(c.DB, MyUser.UID, "", c.log)

		err = Fundraising.DeleteImageByUID(*ctx.UID)
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}

		answer.Success = true
		answer.Action = "Изображение успешно удалено"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}
	answer.Success = false
	answer.Action = "Доступ к разделу закрыт"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

//Getusers - get information about current fundraising
func (c *FundraisingController) Getusers(ctx *app.GetusersFundraisingContext) error {
	var err error

	// Get a list of users for fundraising
	actusers := &models.ActusersAnswer{}
	actusers.HasNext = false

	search := &models.Paginator{}
	search.Search = ""

	if ctx.Search != nil {
		search.Search = *ctx.Search
	}

	search.OnPage = 0
	search.CurrentPage = 0
	search.HasPaginator = false

	if ctx.OnPage != nil && ctx.CurrentPage != nil {
		if *ctx.OnPage != "" && *ctx.CurrentPage != "" {
			search.OnPage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.CurrentPage, err = strconv.Atoi(*ctx.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.HasPaginator = true
		}
	}

	actusers.Actusers, actusers.Total, err = fundraising.Getlistusers(c.DB, c.Conf.Picsurl.URL, *ctx.Fundraisinguid, true, *search)
	if len(actusers.Actusers) < actusers.Total {
		actusers.HasNext = true
	}
	actusers.Count = len(actusers.Actusers)
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &actusers))
	return ctx.OK(&res)
}

//Item - get information about current fundraising
func (c *FundraisingController) Item(ctx *app.ItemFundraisingContext) error {
	var err error
	answer := new(fundraising.AnswerOne)
	MyUserUID := ""
	if ctx.UsersUID != nil {
		MyUserUID = *ctx.UsersUID
	}
	answer.Info, err = fundraising.Getone(c.DB, c.Conf.Picsurl.URL, ctx.UID, MyUserUID, c.Conf.Options.Domain)
	if err != nil {
		c.log.Error().Err(err).Msg("Error forming fundraising info")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования данных"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Action = "Информация о сборе"
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Itemsearch ...
func (c *FundraisingController) Itemsearch(ctx *app.ItemsearchFundraisingContext) error {
	// SectionsController_Items: start_implement
	var res app.MyservAuthToapp
	var err error
	Sectionuid := ""
	answer := new(fundraising.Answer)
	search := new(fundraising.Search)
	filter := new(fundraising.Filters)

	search.Paginator = false

	if ctx.Payload.Search != nil {
		search.Search = *ctx.Payload.Search
	}

	search.Onpage = 0
	search.Current = 0
	search.Paginator = false
	search.NotActive = false
	search.OnlyActive = false

	if ctx.Payload.Filter != nil {
		sections := make([]fundraising.SectionFilter, 0)
		flt := make([]fundraising.OtherFilter, 0)
		for _, item := range ctx.Payload.Filter.Sections {
			if *item.Active {
				sections = append(sections, fundraising.SectionFilter{Name: *item.Title, Active: *item.Active, UID: *item.UID})
			}
		}
		for _, item := range ctx.Payload.Filter.Filters {
			flt = append(flt, fundraising.OtherFilter{Name: *item.Title, Active: *item.Active, UID: *item.UID})
		}
		filter.Sections = sections
		filter.Other = flt
	}

	if ctx.Payload.NotActive != nil {
		search.NotActive = *ctx.Payload.NotActive
	}

	if ctx.Payload.OnlyActive != nil {
		search.OnlyActive = *ctx.Payload.OnlyActive
	}

	if ctx.Payload.OnPage != nil && ctx.Payload.CurrentPage != nil {
		if *ctx.Payload.OnPage != "" && *ctx.Payload.CurrentPage != "" {
			search.Onpage, err = strconv.Atoi(*ctx.Payload.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Current, err = strconv.Atoi(*ctx.Payload.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Paginator = true
		}
	}
	MyUserUID := ""
	if ctx.Payload.UsersUID != nil {
		MyUserUID = *ctx.Payload.UsersUID
	}

	answer.List, answer.TotalCount, err = fundraising.Get(c.DB, c.Conf.Picsurl.URL, Sectionuid, *search, MyUserUID, *filter)

	if err != nil {
		c.log.Error().Err(err).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.HasNext = false
	// if search.Onpage*search.Current < (answer.TotalCount) {
	// 	answer.HasNext = true
	// }
	// answer.Count = len(answer.List)

	NewList, NewTotalCount, err1 := fundraising.GetFromActions(c.DB, c.Conf.Picsurl.URL, Sectionuid, *search, MyUserUID, *filter)

	if err1 != nil {
		c.log.Error().Err(err1).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.TotalCount = answer.TotalCount + NewTotalCount
	answer.List = append(answer.List, NewList...)

	if search.Onpage*2*search.Current < (answer.TotalCount + NewTotalCount) {
		answer.HasNext = true
	}

	answer.Count = len(answer.List)
	answer.Success = true

	answer.Action = "Список сборов"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
	// SectionsController_Items: end_implement
}

// Items runs the items action.
func (c *FundraisingController) Items(ctx *app.ItemsFundraisingContext) error {
	// SectionsController_Items: start_implement
	var res app.MyservAuthToapp
	var err error
	Sectionuid := ""
	answer := new(fundraising.Answer)

	search := new(fundraising.Search)

	search.Paginator = false

	if ctx.Search != nil {
		search.Search = *ctx.Search
	}
	if ctx.Sectionuid != nil {
		Sectionuid = *ctx.Sectionuid
	}

	search.Onpage = 0
	search.Current = 0
	search.Paginator = false
	search.NotActive = false
	search.OnlyActive = false

	if ctx.NotActive != nil {
		search.NotActive = *ctx.NotActive
	}

	if ctx.OnlyActive != nil {
		search.OnlyActive = *ctx.OnlyActive
	}

	if ctx.OnPage != nil && ctx.CurrentPage != nil {
		if *ctx.OnPage != "" && *ctx.CurrentPage != "" {
			search.Onpage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Current, err = strconv.Atoi(*ctx.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Paginator = true
		}
	}
	MyUserUID := ""
	if ctx.UsersUID != nil {
		MyUserUID = *ctx.UsersUID
	}

	answer.List, answer.TotalCount, err = fundraising.Get(c.DB, c.Conf.Picsurl.URL, Sectionuid, *search, MyUserUID, fundraising.Filters{})

	if err != nil {
		c.log.Error().Err(err).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.HasNext = false
	if search.Onpage*search.Current < (answer.TotalCount) {
		answer.HasNext = true
	}
	answer.Count = len(answer.List)

	if ctx.FullSearch != nil && *ctx.FullSearch {
		NewList, NewTotalCount, err1 := fundraising.GetFromActions(c.DB, c.Conf.Picsurl.URL, Sectionuid, *search, MyUserUID, fundraising.Filters{})

		if err1 != nil {
			c.log.Error().Err(err1).Msg("Error forming section list")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формирования списка"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		answer.TotalCount = answer.TotalCount + NewTotalCount
		answer.List = append(answer.List, NewList...)

		if search.Onpage*2*search.Current < (answer.TotalCount + NewTotalCount) {
			answer.HasNext = true
		}

	}
	answer.Count = len(answer.List)
	answer.Success = true

	answer.Action = "Список сборов"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
	// SectionsController_Items: end_implement
}

// Getfilters ...
func (c *FundraisingController) Getfilters(ctx *app.GetfiltersFundraisingContext) error {
	var res app.MyservAuthToapp
	var err error
	answer := new(fundraising.FilterAnswer)
	answer.List.Sections, answer.List.Other, err = fundraising.GetFilter(c.DB)
	if err != nil {
		c.log.Error().Err(err).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Actions = "Набор фильтров для поиска"
	answer.Success = true
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Senditems runs the senditems action.
func (c *FundraisingController) Senditems(ctx *app.SenditemsFundraisingContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)

	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(SavingAnswer)
	if MyUser.Is(user.RoleAdmin) {
		var donated int
		var donatedsumm int
		donated = 0
		donatedsumm = 0

		if ctx.Payload.DonatedSumm != nil {
			donatedsumm = *ctx.Payload.DonatedSumm
		}

		if ctx.Payload.Donated != nil {
			donated = *ctx.Payload.Donated
		}

		// Saving section information
		Fundraising := fundraising.New(c.DB, ctx, donatedsumm, donated, MyUser.UID, c.log)

		isSection, errChk := Fundraising.SectionCheck()
		if errChk != nil {
			c.log.Error().Err(errChk).Msg("Error with section check")
			return ctx.BadRequest(goa.ErrBadRequest(errChk))
		}

		if !isSection {
			answer := new(ActionAnswer)
			answer.Action = "Раздел не найден"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		var saveErr error
		answer.UID, saveErr = Fundraising.Save()
		if saveErr != nil {
			c.log.Error().Err(saveErr).Msg("Error with saving fundraising")
			return ctx.BadRequest(goa.ErrBadRequest(saveErr))
		}

		if ctx.Payload.Fund != nil {
			saveErr := Fundraising.SaveFund(ctx.Payload.Fund.Funduid)
			if saveErr != nil {
				c.log.Error().Err(saveErr).Msg("Error with saving fund for fundraising")
				return ctx.BadRequest(goa.ErrBadRequest(saveErr))
			}
		}

		// } else {

		// }

	}
	answer.Success = true
	answer.Action = "Сбор сохранен"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Sendphoto runs the sendphoto action.
func (c *FundraisingController) Sendphoto(ctx *app.SendphotoFundraisingContext) error {

	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	reader, _ := ctx.MultipartReader()
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) {
		// Saving section information
		Fundraising := fundraising.Initial(c.DB, MyUser.UID, *ctx.UID, c.log)

		iFundraising := ibase.Fundraising{}
		iFundraising = iFundraising.SetStruct(c.DB, c.log, *ctx.UID)
		isExist := iFundraising.IsExist()

		if !isExist {
			answer.Action = "Сбор не найден"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		conflag := 0

		if reader != nil {
			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/fundraising")

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				if err == io.EOF {
					break
				}
				if err != nil {
					break
					// c.log.Error().Err(err).Msg("failed to load part")
					// return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))
				}

				if p.Header["Content-Type"][0] != contentTypeImage {
					return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
				}

				im, saveErr := Fundraising.SaveImage(uid, *ctx.UID)
				if saveErr != nil {
					return ctx.BadRequest(goa.ErrBadRequest(saveErr))
				}
				f, err := os.OpenFile(filepath.Join(pathtofile, im.Image), os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					c.log.Error().Err(err).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()
				io.Copy(f, p)
				conflag = conflag + 1
			}
		}

		answer.Success = true
		answer.Action = "Изображение загружено"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.Success = false
	answer.Action = "Ошибка доступа к разделу"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

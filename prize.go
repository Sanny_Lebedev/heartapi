package main

import (
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"image/jpeg"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/ibase"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/prize"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/disintegration/imaging"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// PrizeController implements the sections resource.
type PrizeController struct {
	*goa.Controller
	DB      *pgx.ConnPool
	log     logger.Logger
	PicsURL string
	Options Options
}

// NewPrizeController creates a prizes controller.
func NewPrizeController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, picsUrl string, options Options) (*PrizeController, error) {
	return &PrizeController{
		Controller: service.NewController("PrizeController"),
		DB:         pg,
		log:        log,
		PicsURL:    picsUrl,
		Options:    options,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *PrizeController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *PrizeController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Cansend runs the cansend action.
func (c *PrizeController) Cansend(ctx *app.CansendPrizeContext) error {
	// PrizeController_Cansend: start_implement

	// Put your logic here

	res := &app.MyservAuthToapp{}
	return ctx.OK(res)
	// PrizeController_Cansend: end_implement
}

// Deleteitems runs the deleteitems action.
func (c *PrizeController) Deleteitems(ctx *app.DeleteitemsPrizeContext) error {
	var err error
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)

	iPrize := ibase.Prize{}
	iPrize = iPrize.SetStruct(c.DB, c.log, *ctx.Payload.UID)
	isExist := iPrize.IsExist()

	if !isExist {
		answer.Action = "Приз не найден"
		answer.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
		ThinkOwner, err := prize.GetPhotoOwner(c.DB, *ctx.Payload.UID)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with checking prize owner")
		}

		if err != nil || ThinkOwner != MyUser.UID {
			answer.Action = "Ошибка - ограничение доступа."
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
			return ctx.OK(&res)
		}
	}

	if !MyUser.Is(user.RoleAdmin) && !MyUser.Is(user.RoleSponsor) {
		answer.Success = false
		answer.Action = "Ошибка удаления приза"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}

	err = prize.Delete(c.DB, c.log, *ctx.Payload.UID)
	if err != nil {
		c.log.Error().Err(err).Msg("Error with deleting prize")
		answer.Success = false
		answer.Action = "Ошибка удаления приза"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Action = "Приз успешно удален"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
	return ctx.OK(&res)
}

// Deletephoto runs the deletephoto action.
func (c *PrizeController) Deletephoto(ctx *app.DeletephotoPrizeContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(ActionAnswer)

	if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
		ThinkOwner, err := prize.GetPhotoOwner(c.DB, *ctx.Payload.UID)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with checking prizrs owner")
		}

		if err != nil || ThinkOwner != MyUser.UID {
			answer.Action = "Доступ ограничен"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
			return ctx.OK(&res)
		}
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {

		var err error
		pathtofile := filepath.Join(binpath.BinaryPath(), "images/"+prize.Area)
		myfilename := *ctx.Payload.UID + ".jpg"
		err = os.Remove(filepath.Join(pathtofile, myfilename))
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}
		Prize := prize.Initial(c.DB, MyUser.UID, "", c.log)
		err = Prize.DeleteImageByUID(*ctx.Payload.UID)
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}

		answer.Success = true
		answer.Action = "Изображение успешно удалено"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)

	}
	answer.Success = false
	answer.Action = "Доступ к разделу ограничен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
	return ctx.OK(&res)
}

// Item runs the item action.
func (c *PrizeController) Item(ctx *app.ItemPrizeContext) error {
	var err error
	answer := new(prize.AnswerOne)

	answer.Info, err = prize.Getone(c.DB, c.PicsURL, ctx.UID)
	if err != nil {
		c.log.Error().Err(err).Msg("Error forming prize info")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования данных"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Action = "Информация о призе"
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
	return ctx.OK(&res)
}

// Items runs the items action.
func (c *PrizeController) Items(ctx *app.ItemsPrizeContext) error {
	var res app.MyservAuthToapp
	var err error
	answer := new(prize.Answer)
	search := new(prize.Search)
	search.Paginator = false

	if ctx.Search != nil {
		search.Search = *ctx.Search

	}

	search.Onpage = 0
	search.Current = 0
	search.Paginator = false

	if ctx.OnPage != nil && ctx.CurrentPage != nil {
		if *ctx.OnPage != "" && *ctx.CurrentPage != "" {
			search.Onpage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Current, err = strconv.Atoi(*ctx.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Paginator = true
		}
	}

	answer.List, answer.TotalCount, err = prize.Get(c.DB, c.PicsURL, *ctx.UID, *search)

	if err != nil {
		c.log.Error().Err(err).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Count = len(answer.List)
	answer.HasNext = false
	if search.Onpage*search.Current < answer.TotalCount {
		answer.HasNext = true
	}
	answer.Action = "Список призов акции"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
	return ctx.OK(&res)
}

// Senditems runs the senditems action.
func (c *PrizeController) Senditems(ctx *app.SenditemsPrizeContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if MyUser.Is(user.RoleSponsor) && uid != ctx.Payload.MaecenasUID && !MyUser.Is(user.RoleAdmin) {
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Вы указали некорректного мецената"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}

	iMaecenas := ibase.Maecenas{}
	iMaecenas = iMaecenas.SetStruct(c.DB, c.log, ctx.Payload.MaecenasUID)
	if iMaecenas.IsExist() == false {
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Вы указали некорректного мецената"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {
		var err error

		iActions := ibase.Actions{}
		iActions = iActions.SetStruct(c.DB, c.log, ctx.Payload.ActionsUID)
		isExist := iActions.IsExist()

		if !isExist {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Акция не найдена. Ошибка сохранения"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
			return ctx.OK(&res)
		}

		answer := new(prize.AnswerWithUID)
		Prize := prize.New(c.DB, ctx, uid, c.log)

		// Verification of the right to edit information by the patron
		if ctx.Payload.UID != nil {
			if MyUser.Is(user.RoleSponsor) && *ctx.Payload.UID != "" && !MyUser.Is(user.RoleAdmin) {
				isOk, err := Prize.CheckSender()
				if err != nil {
					c.log.Error().Err(err).Msg("Error with checking prizes owner")
				}

				if err != nil || !isOk {
					answer.Action = "Ошибка - ограничение доступа."
					answer.Success = false
					res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
					return ctx.OK(&res)
				}
			}

		} else {
			isPlaceExist, err1 := Prize.CheckSamePlace()
			if err1 != nil {
				c.log.Error().Err(err1).Msg("Error with checking prizes owner")
			}

			if err1 != nil || isPlaceExist {
				answer.Action = "Приз для этого места уже назначен"
				answer.Success = false
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
				return ctx.OK(&res)
			}
		}

		answer.UID, err = Prize.Save()
		if err != nil && err != pgx.ErrNoRows {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка сохранения данных"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
			return ctx.OK(&res)
		}

		answer.Success = true
		answer.Action = "Приз успешно сохранен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Ошибка доступа к разделу"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
	return ctx.OK(&res)
}

// Sendphoto runs the sendphoto action.
func (c *PrizeController) Sendphoto(ctx *app.SendphotoPrizeContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	reader, _ := ctx.MultipartReader()
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {

		Prize := prize.Initial(c.DB, MyUser.UID, *ctx.UID, c.log)

		// Verification of the right to edit information by the patron
		if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
			isOk, err := Prize.CheckSender()
			if err != nil {
				c.log.Error().Err(err).Msg("Error with checking prizes owner")
			}

			if err != nil || !isOk {
				answer.Action = "Ошибка сохранения изображения."
				answer.Success = false
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
				return ctx.OK(&res)
			}
		}

		iPrize := ibase.Prize{}
		iPrize = iPrize.SetStruct(c.DB, c.log, *ctx.UID)
		isExist := iPrize.IsExist()

		if !isExist {
			answer.Action = "Приз не найден"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
			return ctx.OK(&res)
		}

		conflag := 0

		if reader != nil {
			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/"+prize.Area)

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				if err == io.EOF {
					break
				}
				if err != nil {
					break
					// c.log.Error().Err(err).Msg("failed to load part")
					// return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))
				}

				if p.Header["Content-Type"][0] != contentTypeImage {
					return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
				}

				im, saveErr := Prize.SaveImage(uid, *ctx.UID)
				if saveErr != nil {
					return ctx.BadRequest(goa.ErrBadRequest(saveErr))
				}
				f, err := os.OpenFile(filepath.Join(pathtofile, im.Image), os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					c.log.Error().Err(err).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()

				img, err := jpeg.Decode(p)
				if err != nil {

				}

				// resize to width 1000 using Lanczos resampling
				// and preserve aspect ratio
				// m := resize.Resize(300, 300, img, resize.Lanczos3)
				m := imaging.Fill(img, 300, 300, imaging.Center, imaging.Lanczos)
				// io.Copy(f, pt)
				jpeg.Encode(f, m, nil)
				conflag = conflag + 1
			}
		}

		answer.Success = true
		answer.Action = "Изображение загружено"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.Success = false
	answer.Action = "Ошибка доступа к разделу"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Prize", "Status", &answer))
	return ctx.OK(&res)
}

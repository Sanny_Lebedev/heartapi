package main

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/likes"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"

	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// LikesController implements the likes resource.
type LikesController struct {
	*goa.Controller
	DB      *pgx.ConnPool
	log     logger.Logger
	PicsURL string
	Options Options
}

// NewLikesController creates a polls controller.
func NewLikesController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, picsUrl string, options Options) (*LikesController, error) {
	return &LikesController{
		Controller: service.NewController("LikesController"),
		DB:         pg,
		log:        log,
		PicsURL:    picsUrl,
		Options:    options,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *LikesController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *LikesController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Getlike runs the getlike action.
func (c *LikesController) Getlike(ctx *app.GetlikeLikesContext) error {
	// LikesController_Getlike: start_implement
	var res app.MyservAuthToapp
	var err error
	OwnerType := ""
	if ctx.OwnerType != nil {
		OwnerType = *ctx.OwnerType
	}

	SenderUID := ""
	if ctx.SenderUID != nil {
		SenderUID = *ctx.SenderUID
	}

	if ctx.OwnerUID == nil {
		return ctx.NotFound()
	}

	Likes := likes.Initial(c.DB, *ctx.OwnerUID, OwnerType, SenderUID, c.log)

	AnswerOne := likes.AnswerOne{}
	AnswerOne.Success = true
	AnswerOne.Action = "Счетчика лайков"
	AnswerOne.MyLike = Likes.MyLike
	AnswerOne.MyLike.Count, err = Likes.Getcount()
	if err != nil {
		c.log.Error().Err(err).Msg("Error with counting likes")
		AnswerOne.Action = "Ошибка вычисления количества лайков"
		AnswerOne.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Likes", "Status", &AnswerOne))
		return ctx.OK(&res)
	}
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Likes", "Status", &AnswerOne))
	return ctx.OK(&res)

}

// Sendlike runs the sendlike action.
func (c *LikesController) Sendlike(ctx *app.SendlikeLikesContext) error {
	// LikesController_Sendlike: start_implement
	var res app.MyservAuthToapp
	OwnerType := ""
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if &ctx.Payload.OwnerType != nil {
		OwnerType = ctx.Payload.OwnerType
	}

	if &ctx.Payload.SenderUID == nil || &ctx.Payload.OwnerUID == nil {
		return ctx.NotFound()
	}

	if !MyUser.Is(user.RoleAdmin) && ctx.Payload.SenderUID != MyUser.UID {
		return ctx.NotFound()
	}

	AnswerOne := likes.AnswerOne{}

	Likes := likes.Initial(c.DB, ctx.Payload.OwnerUID, OwnerType, ctx.Payload.SenderUID, c.log)
	isExist, err := Likes.IsExist()
	if err != nil {
		c.log.Error().Err(err).Msg("Error with getting likes")
		AnswerOne.Action = "Ошибка доступа к данным"
		AnswerOne.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Likes", "Status", &AnswerOne))
		return ctx.OK(&res)
	}

	if isExist {
		// If like exist - delete it from DB
		err = Likes.Delete()

	} else {
		err = Likes.Insert()
	}

	if err != nil {
		c.log.Error().Err(err).Msg("Error with deleting like")
		AnswerOne.Action = "Невозможно отправить like"
		AnswerOne.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Likes", "Status", &AnswerOne))
		return ctx.OK(&res)
	}

	AnswerOne.Success = true
	AnswerOne.Action = "Лайк успешно добавлен"
	AnswerOne.MyLike = Likes.MyLike
	AnswerOne.MyLike.Count, err = Likes.Getcount()
	if err != nil {
		c.log.Error().Err(err).Msg("Error with counting likes")
		AnswerOne.Action = "Ошибка вычисления количества лайков"
		AnswerOne.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Likes", "Status", &AnswerOne))
		return ctx.OK(&res)
	}
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Likes", "Status", &AnswerOne))
	return ctx.OK(&res)
}

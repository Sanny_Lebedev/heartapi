package main

import (
	"fmt"
	"image/jpeg"
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/uploads"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/disintegration/imaging"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// ImageData ...
type ImageData struct {
	// Unique image id
	ID int
	// Filename of image
	Filename string
	// UploadedAt is the upload timestamp
	UploadedAt time.Time
}

// ImageController implements the image resource.
type ImageController struct {
	*goa.Controller
	DB   *pgx.ConnPool
	log  logger.Logger
	pics string
	*sync.Mutex
	images []*ImageData
}

// const (
// 	contentTypeImage = "image/jpeg"
// )

// NewImageController creates a image controller.
func NewImageController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, pics string) (*ImageController, error) {
	return &ImageController{
		Controller: service.NewController("ImageController"),
		DB:         pg,
		log:        log,
		pics:       pics,
		Mutex:      &sync.Mutex{},
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *ImageController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			// res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		// res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *ImageController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Delimageforid runs the delimage action.
func (c *ImageController) Delimageforid(ctx *app.DelimageforidImageContext) error {
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	res, uid := c.Preset(token, claims)
	if !res {
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if (MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser)) && uid != *ctx.UID && !MyUser.Is(user.RoleAdmin) {
		return ctx.NotFound()
	}

	pathtofile := filepath.Join(binpath.BinaryPath(), "images/profiles")

	//getting profiles image
	myfilename, myerr := uploads.GetPhotoNameByUID(c.DB, *ctx.UID)
	if myerr != nil && myerr != pgx.ErrNoRows {
		return ctx.BadRequest(goa.ErrBadRequest(myerr))
	}

	err := os.Remove(filepath.Join(pathtofile, myfilename))
	if err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}
	err = uploads.DeleteFromProfile(c.DB, *ctx.UID)
	if err != nil {
		c.log.Error().Err(err).Msg("Error with delete photo from users profile")
		answer := new(uploads.Answer)
		answer.Success = false
		answer.Count = 0
		answer.Message = "Ошибка удаления файла"
		result := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Images", "Status", &answer))
		return ctx.OK(&result)
	}
	answer := new(uploads.Answer)
	answer.Success = true
	answer.Count = 0
	answer.Message = "Файл успешно удален"
	result := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Images", "Status", &answer))
	return ctx.OK(&result)
}

// Delimage runs the delimage action.
func (c *ImageController) Delimage(ctx *app.DelimageImageContext) error {
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	res, uid := c.Preset(token, claims)
	if !res {
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	pathtofile := filepath.Join(binpath.BinaryPath(), "images/profiles")

	//getting profiles image
	myfilename, myerr := uploads.GetPhotoNameByUID(c.DB, uid)
	if myerr != nil && myerr != pgx.ErrNoRows {
		c.log.Error().Err(myerr).Msg("Error with delete photo from users profile")
		answer := new(uploads.Answer)
		answer.Success = false
		answer.Count = 0
		answer.Message = "Ошибка удаления файла"
		result := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Images", "Status", &answer))
		return ctx.OK(&result)
	}
	fmt.Println(pathtofile)
	fmt.Println(myfilename)

	err := os.Remove(filepath.Join(pathtofile, myfilename))
	if err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}
	err = uploads.DeleteFromProfile(c.DB, uid)
	if err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}
	answer := new(uploads.Answer)
	answer.Success = true
	answer.Count = 0
	answer.Message = "Файл успешно удален"
	result := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Images", "Status", &answer))
	return ctx.OK(&result)
}

// Putimage runs the putimage action.
func (c *ImageController) Putimage(ctx *app.PutimageImageContext) error {
	reader, err := ctx.MultipartReader()

	if err != nil {
		return goa.ErrBadRequest("failed to load multipart request: %s", err)
	}
	if reader == nil {
		return goa.ErrBadRequest("not a multipart request")
	}

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	res, uid := c.Preset(token, claims)
	if !res {
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// var myfilename string
	pathtofile := filepath.Join(binpath.BinaryPath(), "images/profiles")
	// myfilename = uid + ".jpg"

	conflag := 0

	for {
		p, err := reader.NextPart()
		conflag = conflag + 1
		//if (*ctx.Typeimage == "profile") && (conflag > 1) {
		if conflag > 1 {
			return ctx.BadRequest(goa.ErrBadRequest("wrong numbers of files"))
		}

		if err == io.EOF {
			break
		}
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))

		}

		if p.Header["Content-Type"][0] != contentTypeImage {
			return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
		}

		nres, list, err := uploads.SaveinProfileFiles(c.DB, uid)
		if err != nil {
			return ctx.InternalServerError()
		}
		//f, err := os.OpenFile(pathtofile+p.FileName(), os.O_WRONLY|os.O_CREATE, 0666)
		f, err := os.OpenFile(filepath.Join(pathtofile, nres.Name), os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
		}
		defer f.Close()

		img, err := jpeg.Decode(p)
		if err != nil {

		}

		// resize to width 1000 using Lanczos resampling
		// and preserve aspect ratio
		// m := resize.Resize(300, 300, img, resize.Lanczos3)
		m := imaging.Fill(img, 300, 300, imaging.Center, imaging.Lanczos)
		// io.Copy(f, pt)
		jpeg.Encode(f, m, nil)

		// io.Copy(f, p)

		answer := new(uploads.Answer)
		answer.Success = true
		answer.Count = len(list)
		answer.List = list
		answer.Message = "Файл успешно загружен"
		result := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Images", "Status", &answer))
		return ctx.OK(&result)

		//}
	}
	return ctx.BadRequest(goa.ErrBadRequest("wrong datas"))
}

// Putimageforid runs the putimageforid action.
func (c *ImageController) Putimageforid(ctx *app.PutimageforidImageContext) error {
	reader, err := ctx.MultipartReader()

	if err != nil {
		return goa.ErrBadRequest("failed to load multipart request: %s", err)
	}
	if reader == nil {
		return goa.ErrBadRequest("not a multipart request")
	}

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	res, uid := c.Preset(token, claims)
	if !res {
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if (MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser)) && uid != *ctx.UID && !MyUser.Is(user.RoleAdmin) {
		return ctx.NotFound()
	}

	// var myfilename string
	pathtofile := filepath.Join(binpath.BinaryPath(), "images/profiles")

	conflag := 0

	for {
		p, err := reader.NextPart()
		conflag = conflag + 1

		if conflag > 1 {
			return ctx.BadRequest(goa.ErrBadRequest("wrong numbers of files"))
		}

		if err == io.EOF {
			break
		}
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))

		}

		if p.Header["Content-Type"][0] != contentTypeImage {
			return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
		}

		nres, list, err := uploads.SaveinProfileFiles(c.DB, *ctx.UID)
		if err != nil {
			return ctx.InternalServerError()
		}
		//f, err := os.OpenFile(pathtofile+p.FileName(), os.O_WRONLY|os.O_CREATE, 0666)
		f, err := os.OpenFile(filepath.Join(pathtofile, nres.Name), os.O_WRONLY|os.O_CREATE, 0666)
		if err != nil {
			return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
		}
		defer f.Close()
		io.Copy(f, p)

		answer := new(uploads.Answer)
		answer.Success = true
		answer.Count = len(list)
		answer.List = list
		answer.Message = "Файл успешно загружен"
		result := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Images", "Status", &answer))
		return ctx.OK(&result)

		//}
	}
	return ctx.BadRequest(goa.ErrBadRequest("wrong datas"))
}

// Show runs the show action.
func (c *ImageController) Show(ctx *app.ShowImageContext) error {
	// ImageController_Show: start_implement

	// Put your logic here

	res := &app.ImageMedia{}
	return ctx.OK(res)
	// ImageController_Show: end_implement
}

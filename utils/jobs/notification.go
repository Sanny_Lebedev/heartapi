package jobs

import (
	"context"
	"errors"
	"fmt"
	"path/filepath"
	"sync"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"github.com/google/go-gcm"
	"github.com/jackc/pgx"
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/payload"
	"github.com/sideshow/apns2/token"
	"google.golang.org/api/option"
)

const (
	// AndroidMainToken- main service token for Android
	AndroidMainToken = "AAAAAY7jv2w:APA91bH-Nu9h44FpDqnIwchYp-fBi3FJqKcB7A-Y24x85R7ITO5W1h0QQthxsIkfJXI5UIvERdFGBXWvAmZdsIfiJ76W6M1K4RTXIxk4Sg3p6C21eHZXbsYcHvFbbSnmw2YNeM6bYSKz"
)

//PushGetter - structure for pushing
type PushGetter struct {
	Opersys     string
	Token       string
	Action      string
	Fundraising string
}

// GetActionsSender ...
func GetActionsSender(uid string, db *pgx.ConnPool, log logger.Logger) (string, error) {
	maecenas := ""
	err := db.QueryRow(`SELECT maecenas FROM actions WHERE uid = $1`, uid).Scan(&maecenas)
	if err != nil {
		log.Error().Err(err).Msg("Error with getting maecenas")
		return "", err
	}
	return maecenas, nil
}

//Notification - sending push about new trip
func Notification(db *pgx.ConnPool, log logger.Logger, action, fundraising, KeyID, TeamID string) error {
	maecenas := ""
	if action != "" {
		maecenas, _ = GetActionsSender(action, db, log)
	}

	rows, err := db.Query(`SELECT pushnavigator.token, pushnavigator.opersys, pushnavigator.useruid FROM pushnavigator 
	LEFT JOIN users on users.uid::VARCHAR = pushnavigator.useruid WHERE users.getpush = true
	GROUP BY pushnavigator.token, pushnavigator.opersys`)
	if err != nil && err != pgx.ErrNoRows {
		log.Error().Err(err).Msg("Error query calculating a set of tokens to notify of a new trip")
		return err
	}

	var PushGetters []PushGetter

	for rows.Next() {

		Getter := PushGetter{}
		user := ""
		errScan := rows.Scan(&Getter.Token, &Getter.Opersys, &user)
		Getter.Action = action
		Getter.Fundraising = fundraising
		if errScan != nil {
			log.Error().Err(errScan).Msg("Error calculating a set of tokens to notify of a new trip")
			return errScan
		}

		if (maecenas == "") || (maecenas != "" && maecenas != user) {
			PushGetters = append(PushGetters, Getter)
		}

	}

	var wg sync.WaitGroup
	ch := make(chan PushGetter, 100)
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			err := wait(i, ch, KeyID, TeamID)
			if err != nil {
				//
			}
		}(i)
		//go wait(i, ch, &wg)
	}

	for i := 0; i < len(PushGetters); i++ {
		ch <- PushGetters[i]
	}
	close(ch)
	wg.Wait()

	return nil
}

func wait(id int, ch chan PushGetter, KeyID string, TeamID string) error {
	var err error

	for Getter := range ch {
		switch Getter.Opersys {
		case "ios":
			// Sending to IOS
			err = SendToIOS(Getter, KeyID, TeamID)
		default:
			// Sending to ANDROID
			// err = SendToAndroid(Getter)
			err = SendToAndroidFB(Getter)
		}

	}
	return err
}

// SendToAndroidFB ...
func SendToAndroidFB(Getter PushGetter) error {
	Token := Getter.Token
	var opt option.ClientOption

	opt = option.WithCredentialsFile(
		filepath.Join(binpath.BinaryPath(), "cert/ksetrin-test1-firebase-adminsdk-ir4pu-e08afda473.json"),
	)
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := app.Messaging(ctx)
	var message *messaging.Message
	var notification *messaging.AndroidNotification

	registrationToken := Token

	myTextOne := "Новая акция от МЕЦЕНАТ21ВЕК"
	myTextBody := "Приглашаем принять участие в новой акции"

	notification = &messaging.AndroidNotification{
		Title: myTextOne,
		Body:  myTextBody,
		// 		ClickAction: ClickStr,
		Sound: "default",
	}

	android := &messaging.AndroidConfig{
		Notification: notification,
		Data: map[string]string{
			"action":      Getter.Action,
			"fundraising": Getter.Fundraising,
		},
	}

	message = &messaging.Message{
		Data: map[string]string{
			"action":      Getter.Action,
			"fundraising": Getter.Fundraising,
		},

		// Notification: notification,

		Android: android,
	}

	message.Token = registrationToken
	// Send a message to the device corresponding to the provided
	// registration token.
	_, err = client.Send(ctx, message)
	fmt.Println("Токен:", Token)
	if err != nil {

		fmt.Println("Ошибка", err)

		cancel()
		//continue

	} else {
		fmt.Println("Статус - ОК")
	}

	cancel()
	return err
}

// SendGCMToClient ...
func SendGCMToClient(Getter PushGetter) error {
	pushToken := Getter.Token
	serverKey := AndroidMainToken
	var msg gcm.HttpMessage

	// ClickStr := "org.pureHearts.FCM_ALERT"
	myTextOne := "Новая акция от МЕЦЕНАТ21ВЕК"
	myTextBody := "Приглашаем принять участие в новой акции"

	data := map[string]interface{}{
		"action":      Getter.Action,
		"fundraising": Getter.Fundraising}
	regIDs := []string{pushToken}
	msg.RegistrationIds = regIDs
	msg.Data = data
	msg.Notification = &gcm.Notification{
		Title: myTextOne,
		Body:  myTextBody,
		Sound: "default",
	}

	_, err := gcm.SendHttp(serverKey, msg)
	if err != nil {
		// fmt.Println(err)
		return err
	} else {
		// fmt.Println("Response ", response.Success)
		// fmt.Println("MessageID ", response.MessageId)
		// fmt.Println("Failure ", response.Failure)
		// fmt.Println("Error ", response.Error)
		// fmt.Println("Results ", response.Results)
	}
	return nil
}

//SendToAndroid - sending push notificators to android
func SendToAndroid(Getter PushGetter) error {
	Token := Getter.Token
	var opt option.ClientOption

	opt = option.WithCredentialsFile(
		filepath.Join(binpath.BinaryPath(), "cert/ksetrin-test1-firebase-adminsdk-ir4pu-e08afda473.json"),
	)
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := app.Messaging(ctx)
	var message *messaging.Message
	var notification *messaging.AndroidNotification

	registrationToken := Token

	// ClickStr := "ru.red.choppy.trainer.FCM_ALERT"
	myTextOne := "Новая акция от МЕЦЕНАТ21ВЕК"
	myTextBody := "Приглашаем принять участие в новой акции"

	notification = &messaging.AndroidNotification{
		Title: myTextOne,
		Body:  myTextBody,
		// ClickAction: ClickStr,
		Sound: "default",
	}

	android := &messaging.AndroidConfig{
		Notification: notification,
		Data: map[string]string{
			"action":      Getter.Action,
			"fundraising": Getter.Fundraising,
		},
	}

	message = &messaging.Message{
		Data: map[string]string{
			"action":      Getter.Action,
			"fundraising": Getter.Fundraising,
		},

		// Notification: notification,

		Android: android,
	}

	message.Token = registrationToken
	// Send a message to the device corresponding to the provided
	// registration token.
	_, err = client.Send(ctx, message)

	if err != nil {
		cancel()
		//continue

	}

	cancel()
	return err
}

//SendToIOS - sending push notificators to IOS
func SendToIOS(Getter PushGetter, KeyID string, TeamID string) error {
	Token := Getter.Token
	authKey, err := token.AuthKeyFromFile(filepath.Join(binpath.BinaryPath(), "cert/AuthKey_Y9Y26N382S.p8"))
	if err != nil {
		return err
	}

	token := &token.Token{
		AuthKey: authKey,
		KeyID:   KeyID,
		TeamID:  TeamID,
	}

	notification := &apns2.Notification{}
	notification.DeviceToken = Token

	notification.Topic = "com.choppypro.Choppy"

	payload := payload.NewPayload().Alert("Приглашаем принять участие в новой акции").Sound("default").Custom("action", Getter.Action).Custom("fundraising", Getter.Fundraising)
	// payload := payload.NewPayload().Alert("Приглашаем принять участие в новой акции").Sound("default")
	notification.Payload = payload

	client := apns2.NewTokenClient(token).Production()
	res, err := client.Push(notification)

	if res == nil {

	} else {

		switch res.StatusCode {
		case 400:
			return errors.New("Something wrong")

		case 200:
			return nil
		default:
			return nil

		}

	}

	return nil
}

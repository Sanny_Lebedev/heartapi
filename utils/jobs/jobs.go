package jobs

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	"github.com/jackc/pgx"
)

const (
	//driverarrivedText ...
	driverarrivedText = "driverarrived"
	//driverstartText ...
	driverstartText = "driverstarted"
	//driverstartMessage ...
	driverstartMessage = "Инструктор выехал за Вами"
	//driverarrivedMessage ...
	driverarrivedMessage = "Вас ожидает инструктор"
	//driverfinishedText ...
	driverfinishedText = "tripfinished"
	// driverfinishedMessage ...
	driverfinishedMessage = "Поездка окончена. Пожалуйста, оцените инструктора."
)

type (

	//Message ...
	Message interface {
		send()
		watch()
	}

	//Messtr ...
	Messtr struct {
		Qname     string `json:"qName"`
		Getter    string `json:"getter"`
		Message   string `json:"message"`
		TicketUID string `json:"TicketUID"`
		Role      string `json:"role"`
	}

	//Config ...
	Config struct {
		DB *pgx.ConnPool
	}

	//Letter - main part of message
	Letter struct {
		Sender string `json:"sender"`
		Getter string `json:"getter"`
		Body   string `json:"message"`
	}

	// PushBody ...
	PushBody struct {
		Action      string
		Fundraising string
	}

	//SMS - only for SMS
	SMS struct {
		Letter
		Config
	}

	// PushNotificator ...
	PushNotificator struct {
		Config
		PushBody
	}

	//Email - only mail
	Email struct {
		Letter
		Config
		UsersUID string `json:"usersUID"`
		ToRole   string `json:"role"`
	}

	//Push - only for push notification
	Push struct {
		Letter
		Config
		System    string `json:"system"`
		Token     string `json:"token"`
		Stage     int    `json:"stage"`
		TicketUID string `json:"ticketUID"`
		ToRole    string `json:"role"`
	}

	//Pay - struct for payment
	Pay struct {
		UID       string  `json:"uid"`
		Sender    string  `json:"sender"`
		Amount    float64 `json:"amount"`
		TicketUID string  `json:"ticketUID"`
		DB        *pgx.ConnPool
		Log       logger.Logger
	}

	//Result - result of work
	Result struct {
		UID     string `json:"uid"`
		Success bool   `json:"success"`
		Err     error  `json:"error"`
	}
)

// IsInDB - check for data in DB
func IsInDB(db *pgx.ConnPool, data Messtr, action string) (bool, error) {
	var isInDB bool

	err := db.QueryRow(`SELECT count(*)>0 as count FROM queue_classic_jobs WHERE q_name = $1 AND action = $2 AND getter = $3 AND success = false`, data.Qname, action, data.Getter).Scan(&isInDB)
	if err != nil {
		return false, err
	}
	return isInDB, nil

}

//ActionToDB - saving to queue for different actions
func ActionToDB(db *pgx.ConnPool, data Messtr, action string) Result {
	var uid string
	res := Result{}
	var err error
	if data.Role == "" {
		data.Role = string(user.RoleUser)
	}

	err = db.QueryRow(`INSERT INTO queue_classic_jobs (q_name, action, getter, message) VALUES ($1, $4, $2, $3) RETURNING uid`, data.Qname, data.Getter, data.Message, action).Scan(&uid)

	if err != nil {

		res.Success = false
		res.Err = err
		return res
	}
	res.Err = nil
	res.Success = true
	res.UID = uid
	return res
}

//Saving to DB
func toDB(db *pgx.ConnPool, data Messtr) Result {
	return ActionToDB(db, data, "send")
}

// NewPush ...
func NewPush(db *pgx.ConnPool, action, fundraising string) PushNotificator {
	return PushNotificator{Config: Config{DB: db}, PushBody: PushBody{Action: action, Fundraising: fundraising}}
}

// Send - sending push notification for all users in DB
func (p PushNotificator) Send() error {
	isOk := false
	err := p.Config.DB.QueryRow(`SELECT count(uid)=0 FROM fornotificate WHERE action = $1 AND fundraising=$2 AND state = 'new'`, p.Action, p.Fundraising).Scan(&isOk)

	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	if err == pgx.ErrNoRows {
		isOk = true
	}

	if isOk {
		_, err = p.Config.DB.Exec(`INSERT INTO fornotificate (action, fundraising, state) VALUES($1, $2, 'new')`, p.Action, p.Fundraising)
	}
	return err
}

//Send - sending sms
func (m SMS) Send() Result {
	return toDB(m.DB, Messtr{Qname: "sms", Getter: m.Getter, Message: m.Body})
}

//Send - sending email
func (m Email) Send() Result {
	return toDB(m.DB, Messtr{Qname: "email", Getter: m.Getter, Message: m.Body, Role: string(user.RoleAdmin)})
}

//SendWithUID - sending sms
func (m SMS) SendWithUID() Result {
	return m.Send()
}

//Send - sending PAYMENT
func (p Pay) Send() Result {

	return toDB(p.DB, Messtr{Qname: "pay", Getter: p.Sender, Message: "Payment for ticket", Role: string(user.RoleUser)})
}

//Send - sending PUSH
func (p Push) Send() Result {
	if p.ToRole == "" {
		p.ToRole = string(user.RoleUser)
	}
	return toDB(p.DB, Messtr{Qname: "push", Getter: p.Getter, Message: p.Body, Role: p.ToRole})
}

// //SendWithUID - sending PUSH
// func (p Push) SendWithUID() Result {
// 	if p.ToRole == "" {
// 		p.ToRole = string(user.RoleUser)
// 	}
// 	return toDB(p.DB, Messtr{Qname: "push", Getter: p.Getter, Message: p.Body, Role: p.ToRole})
// }

//
// func (p Push) SavePushFlag(flagname string, flagstatus bool) error {
// 	if flagname == "driverstarted" {
// 		flagname = "driverstart"
// 	}
// 	_, err := p.DB.Exec(`UPDATE pushflags SET `+flagname+`= $1 WHERE TicketUID = $2`, flagstatus, p.TicketUID)
// 	return err
// }

//GetRecipient - getting the recipient of push notification by TicketUID
func (p Push) GetRecipient() (string, error) {
	Recipient := ""
	var err error
	if p.ToRole == string(user.RoleSponsor) {
		err = p.DB.QueryRow(`SELECT usersuid FROM tickets WHERE uid = $1`, p.TicketUID).Scan(&Recipient)

	} else {
		err = p.DB.QueryRow(`SELECT actions.maecenas FROM tickets
		LEFT JOIN actions on actions.uid::VARCHAR = tickets.actionsuid WHERE tickets.uid = $1`, p.TicketUID).Scan(&Recipient)
	}
	return Recipient, err
}

//SendToSponsor - sending push to sponsor
func (p Push) SendToSponsor() error {
	var err error
	if p.Getter == "" {
		p.Getter, err = p.GetRecipient()
		if err != nil {
			return err

		}
	}

	return nil
}

// //SetPush - prepating push notification
// func (p Push) SetPush(stage string) error {

// 	tripstarted := false
// 	driverstart := false
// 	tripfinished := false
// 	perhour := false
// 	forhalfhour := false
// 	infifteenmin := false
// 	perkone := false
// 	driverarrived := false
// 	driverfinished := false

// 	err := p.DB.QueryRow(`SELECT tripstarted, driverstart, tripfinished, perhour, forhalfhour, infifteenmin, perkone, driverarrived, driverfinished
// 			FROM pushflags WHERE TicketUID = $1`, p.TicketUID).Scan(&tripstarted, &driverstart, &tripfinished, &perhour, &forhalfhour, &infifteenmin, &perkone, &driverarrived, &driverfinished)

// 	if err != nil {
// 		return err
// 	}

// 	switch stage {

// 	case driverfinishedText:
// 		if !driverfinished {
// 			var err error
// 			p.Body = driverfinishedMessage
// 			p.Getter, err = p.GetRecipient()
// 			if err != nil {
// 				return err
// 			}
// 			p.TicketUID = ""
// 			resM := p.SendWithUID()

// 			if resM.Err != nil {
// 				return resM.Err
// 			}

// 			err = p.SavePushFlag(driverfinishedText, true)
// 			if err != nil {
// 				return err
// 			}
// 		}

// 	case driverarrivedText:
// 		if !driverarrived {
// 			var err error
// 			p.Body = driverarrivedMessage
// 			p.Getter, err = p.GetRecipient()
// 			if err != nil {
// 				return err
// 			}

// 			resM := p.SendWithUID()

// 			if resM.Err != nil {
// 				return resM.Err
// 			}

// 			err = p.SavePushFlag(driverarrivedText, true)
// 			if err != nil {
// 				return err
// 			}
// 		}

// 	case driverstartText:
// 		if !driverstart {
// 			p.Body = driverstartMessage

// 			p.Getter, err = p.GetRecipient()
// 			if err != nil {
// 				return err
// 			}

// 			resM := p.SendWithUID()

// 			if resM.Err != nil {
// 				return resM.Err
// 			}

// 			err := p.SavePushFlag(driverstartText, true)
// 			if err != nil {
// 				return err
// 			}
// 		}

// 	}

// 	//_ = push.SendWithUID(ctx.Payload.UID)
// 	return nil
// }

package uploads

import (
	"path/filepath"
	// Imports the Google Cloud Storage client package.
	"os"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	// 	"cloud.google.com/go/storage"
)

type (
	Ans struct {
		Name string `db:"url" json:"url"`
		UID  string `db:"uid" json:"uid"`
	}

	Answer struct {
		List    []*Ans `db:"list" json:"list"`
		Success bool   `db:"success" json:"success"`
		Message string `db:"message" json:"message"`
		Count   int    `db:"count" json:"count"`
	}
)

//DeleteFromProfile - deleting photos from user's profiles
func DeleteFromProfile(db *pgx.ConnPool, uid string) error {
	sql := `DELETE FROM usersphotos WHERE usersuid=$1`
	_, err := db.Exec(sql, &uid)
	if err != nil {
		return errors.Wrap(err, "query failed")
	}
	return nil

}

// DeletePhotoFile - deleting file from disk
func DeletePhotoFile(filename string, part string) error {
	pathtofile := filepath.Join(binpath.BinaryPath(), "images/"+part)
	_ = os.Remove(filepath.Join(pathtofile, filename))
	return nil

}

//DelProfileFiles - deleting files from profile
func DelProfileFiles(db *pgx.ConnPool, usersUID string) error {

	rows, err := db.Query(`SELECT filename FROM usersphotos WHERE usersuid=$1`, usersUID)
	// if no photos in DB
	if err == pgx.ErrNoRows {
		return nil
	}

	if err != nil {
		return errors.Wrap(err, "failed to select from usersphotos")
	}
	pathtofile := filepath.Join(binpath.BinaryPath(), "images/profiles")
	for rows.Next() {
		filename := ""
		err = rows.Scan(&filename)
		if err != nil && err != pgx.ErrNoRows {
			return err
		}
		if filename != "" || &filename != nil {
			_ = os.Remove(filepath.Join(pathtofile, filename))
		}

	}

	_, err = db.Exec(`DELETE FROM usersphotos WHERE usersuid=$1`, usersUID)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err

}

//GetPhotoNameByUID ...
func GetPhotoNameByUID(db *pgx.ConnPool, usersUID string) (string, error) {

	filename := ""
	err := db.QueryRow(`SELECT filename FROM usersphotos WHERE usersuid = $1`, usersUID).Scan(&filename)

	return filename, err
}

// SaveinProfileFiles ...
func SaveinProfileFiles(db *pgx.ConnPool, usersUID string) (Ans, []*Ans, error) {
	var uid string
	var err error
	ans := &Ans{}
	answer := make([]*Ans, 0)

	err = DelProfileFiles(db, usersUID)
	if err != nil {
		return *ans, answer, errors.Wrap(err, "query failed")
	}

	sql := `INSERT INTO usersphotos (usersuid, date) VALUES ($1, CURRENT_TIMESTAMP) RETURNING uid`
	err = db.QueryRow(sql, &usersUID).Scan(&uid)

	if err != nil {
		return *ans, answer, errors.Wrap(err, "query failed")
	}
	ans.Name = uid + ".jpg"
	ans.UID = uid
	sql = `UPDATE usersphotos SET filename = $2 WHERE uid = $1`
	_, err = db.Exec(sql, &uid, ans.Name)

	answer = append(answer, ans)
	return *ans, answer, err
}

//SaveinProfile - save new filename to DB
func SaveinProfile(db *pgx.ConnPool, filename string, uid string) ([]*Ans, error) {
	answer := make([]*Ans, 0)
	sql := `INSERT INTO usersphotos (usersuid, filename, date)
		VALUES ($1, $2, CURRENT_TIMESTAMP)
		ON CONFLICT (usersuid)
		DO UPDATE SET
		usersuid=$1,
		filename=$2,
		date=CURRENT_TIMESTAMP`
	_, err := db.Exec(sql, &uid, &filename)

	if err != nil {
		return answer, errors.Wrap(err, "query failed")
	}

	ans := &Ans{}
	ans.Name = filename
	ans.UID = uid
	answer = append(answer, ans)

	return answer, err
}

package funds

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

type (
	// Main - main structure for fundraising
	Main struct {
		Fund Fund
		db   *pgx.ConnPool
		log  logger.Logger
	}

	// Fund  - main structure
	Fund struct {
		UID         string          `json:"uid"`
		OwnerUID    string          `json:"owneruid"`
		Title       string          `json:"title"`
		Description string          `json:"description"`
		Inn         string          `json:"inn"`
		Status      string          `json:"status"`
		Active      bool            `json:"active"`
		Images      models.Images   `json:"images"`
		Contacts    models.Contacts `json:"contacts"`
		Address     models.Address  `json:"address"`
		Date        string          `json:"date"`
	}

	// Search - structure for search and pagination
	Search struct {
		Search    string `json:"search"`
		Current   int    `json:"current"`
		Onpage    int    `json:"onPage"`
		Paginator bool   `json: "paginator"`
	}

	// Sections - structure of sections for current fundraising
	Sections struct {
		SectionUID     string `json:"sectionUID"`
		FundraisingUID string `json:"fundraisingUID"`
		SectionName    string `json:"sectionName"`
	}

	// // Images - structure for pics for fundraising
	// Images struct {
	// 	UID   string `json:"UID"`
	// 	Image string `json:"image"`
	// }

	// // Contacts - structure for contacts
	// Contacts struct {
	// 	UID     string `json:"UID"`
	// 	Person  string `json:"person"`
	// 	Phone   string `json:"phone"`
	// 	Email   string `json:"email"`
	// 	Website string `json:"website"`
	// 	Skype   string `json:"skype"`
	// }

	// // Address - structure for contacts
	// Address struct {
	// 	UID     string `json:"UID"`
	// 	Country string `json:"country"`
	// 	Postal  string `json:"postal"`
	// 	City    string `json:"city"`
	// 	Address string `json:"address"`
	// }

	// Answer - answer for API with list of sections
	Answer struct {
		List       []*Fund `json:"list"`
		Success    bool    `json:"success"`
		Action     string  `json:"message"`
		Count      int     `json:"count,omitempty"`
		TotalCount int     `json:"totalCount,omitempty"`
		HasNext    bool    `json:"hasNext"`
	}

	// AnswerOne - answer for API with info about current fundraising
	AnswerOne struct {
		Info    Fund   `json:"info"`
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}
)

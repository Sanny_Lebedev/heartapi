package funds

import (
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"github.com/jackc/pgx"
)

// New - returns new sections instance
func New(db *pgx.ConnPool, fund Fund, Log logger.Logger) *Main {
	u := &Main{
		db:   db,
		Fund: fund,
		log:  Log,
	}
	return u
}

// Delete - delete fund by UID
func Delete(db *pgx.ConnPool, uid string) error {
	var err error
	_, err = db.Exec(`DELETE FROM fund WHERE uid = $1`, uid)
	if err != nil {
		return err
	}

	_, err = db.Exec(`DELETE FROM address WHERE owneruid = $1 and ownertype = $2`, uid, "fund")
	if err != nil {
		return err
	}

	_, err = db.Exec(`DELETE FROM contacts WHERE owneruid = $1 and ownertype = $2`, uid, "fund")

	return err
}

// DeletePhoto - delete fund by UID
func DeletePhoto(db *pgx.ConnPool, uid string) error {
	var err error
	_, err = db.Exec(`DELETE FROM crossimages WHERE owneruid = $1 AND ownertype = 'fund'`, uid)
	return err
}

// FundCheck - check for exist fund with UID
func FundCheck(db *pgx.ConnPool, uid string) (bool, error) {
	var err error
	exist := false
	err = db.QueryRow(`SELECT count(*)>0 FROM fund WHERE uid = $1`, uid).Scan(&exist)
	if err == pgx.ErrNoRows {
		return false, err
	}
	return exist, err
}

// ExistPhotoinDB - check for info in DB about photo
func ExistPhotoinDB(db *pgx.ConnPool, uid string) (bool, error) {
	var err error
	exist := false
	err = db.QueryRow(`SELECT count(*)>0 FROM crossimages WHERE owneruid = $1 AND ownertype = 'fund' `, uid).Scan(&exist)
	if err == pgx.ErrNoRows {
		return false, err
	}
	return exist, err
}

// ChangeActive - change active status of fund
func ChangeActive(db *pgx.ConnPool, uid string) error {
	_, err := db.Exec(`UPDATE fund SET active = NOT active WHERE uid = $1`, uid)
	return err
}

// SavePhotoFile - save information about photo to db
func SavePhotoFile(db *pgx.ConnPool, owneruid string, senderuid string, state bool) error {
	var err error

	if state {
		_, err = db.Exec(`UPDATE crossimages SET senderuid = $1, date = CURRENT_TIMESTAMP WHERE owneruid = $2 and ownertype = 'fund'`, senderuid, owneruid)
	} else {
		imageName := owneruid + ".jpg"
		_, err = db.Exec(`INSERT INTO crossimages (owneruid, senderuid, ownertype, imagename, imagelink, active, date) 
	VALUES ($1, $2, 'fund', $3, $4, true, CURRENT_TIMESTAMP)`, owneruid, senderuid, imageName, imageName)
	}
	return err
}

// Exist - check for exist of section
func Exist(db *pgx.ConnPool, uid string) bool {
	var exist bool
	err := db.QueryRow(`SELECT COUNT(uid)>0 from fund WHERE uid = $1`, uid).Scan(&exist)
	if err != nil || err == pgx.ErrNoRows {
		return false
	}
	return true
}

// Save - Checking for a similar entry
func (s *Main) Save() (string, error) {
	var newUID string
	var err error

	if s.Fund.UID == "" {
		newUID, err = s.Addnew()
		if err != nil {
			s.log.Error().Err(err).Msg("Error adding section")
			return "", err
		}
		return newUID, nil
	}

	err = s.Edit()
	if err != nil {
		s.log.Error().Err(err).Msg("Error adding section")
		return s.Fund.UID, err
	}

	return s.Fund.UID, nil
}

// CheckForAddress - checking for current address for fund
func (s *Main) CheckForAddress() (bool, error) {
	if s.Fund.UID == "" {
		return false, nil
	}
	answer := false
	err := s.db.QueryRow(`SELECT COUNT(*)>0 as CNT FROM ADDRESS WHERE owneruid = $1 AND ownertype = $2`, s.Fund.UID, "fund").Scan(&answer)
	return answer, err
}

// CheckForContact - checking for current contact for fund
func (s *Main) CheckForContact() (bool, error) {
	if s.Fund.UID == "" {
		return false, nil
	}
	answer := false
	err := s.db.QueryRow(`SELECT COUNT(*)>0 as CNT FROM CONTACTS WHERE owneruid = $1 AND ownertype = $2`, s.Fund.UID, "fund").Scan(&answer)
	return answer, err
}

// SaveContacts - Checking for a similar entry
func (s *Main) SaveContacts() error {

	var err error
	isContactsExist := false
	isContactsExist, err = s.CheckForContact()
	if err != nil {
		s.log.Error().Err(err).Msg("Error with checking existing of contacts")
		return err
	}
	if !isContactsExist {
		err = s.AddNewContact()
		if err != nil {
			s.log.Error().Err(err).Msg("Error adding new contacts for fund")
			return err
		}
		return nil
	}

	err = s.EditContact()
	if err != nil {
		s.log.Error().Err(err).Msg("Error adding address for fund")
		return err
	}

	return nil
}

// AddNewContact - adding new contact for fund
func (s *Main) AddNewContact() error {
	_, err := s.db.Exec(`INSERT INTO contacts (senderuid, owneruid, ownertype, person, phone, email, website, skype, active, date) VALUES 
	($1, $2, $3, $4, $5, $6, $7, $8, true, CURRENT_TIMESTAMP)`, s.Fund.OwnerUID, s.Fund.UID, "fund",
		s.Fund.Contacts.Person, s.Fund.Contacts.Phone, s.Fund.Contacts.Email, s.Fund.Contacts.Website, s.Fund.Contacts.Skype)
	return err
}

// EditContact - Edit contact for fund
func (s *Main) EditContact() error {
	_, err := s.db.Exec(`UPDATE contacts SET senderuid = $1, person = $4, phone = $5, email = $6, 
	website = $7, skype = $8, date = CURRENT_TIMESTAMP WHERE owneruid = $2 AND ownertype = $3`, s.Fund.OwnerUID, s.Fund.UID, "fund",
		s.Fund.Contacts.Person, s.Fund.Contacts.Phone, s.Fund.Contacts.Email, s.Fund.Contacts.Website, s.Fund.Contacts.Skype)
	return err
}

// SaveAddress - Checking for a similar entry
func (s *Main) SaveAddress() error {

	var err error
	isAddressExist := false
	isAddressExist, err = s.CheckForAddress()
	if err != nil {
		s.log.Error().Err(err).Msg("Error with checking existing of address")
		return err
	}
	if !isAddressExist {
		err = s.AddNewAddress()
		if err != nil {
			s.log.Error().Err(err).Msg("Error adding new address for fund")
			return err
		}
		return nil
	}

	err = s.EditAddress()
	if err != nil {
		s.log.Error().Err(err).Msg("Error adding address for fund")
		return err
	}

	return nil
}

// AddNewAddress - adding new address for fund
func (s *Main) AddNewAddress() error {
	_, err := s.db.Exec(`INSERT INTO address (senderuid, owneruid, ownertype, country, postal, city, address, active, date) VALUES 
	($1, $2, $3, $4, $5, $6, $7, true, CURRENT_TIMESTAMP)`, s.Fund.OwnerUID, s.Fund.UID, "fund", s.Fund.Address.Country, s.Fund.Address.Postal, s.Fund.Address.City, s.Fund.Address.Address)
	return err
}

// EditAddress - Edit address for fund
func (s *Main) EditAddress() error {
	_, err := s.db.Exec(`UPDATE address SET senderuid = $1, country = $4, postal = $5, city = $6, 
	address = $7, date = CURRENT_TIMESTAMP WHERE owneruid = $2 AND ownertype = $3`, s.Fund.OwnerUID, s.Fund.UID, "fund",
		s.Fund.Address.Country, s.Fund.Address.Postal, s.Fund.Address.City, s.Fund.Address.Address)
	return err
}

// Addnew - adding new section
func (s *Main) Addnew() (string, error) {
	var newUID string
	var err error

	err = s.db.QueryRow(`INSERT INTO fund (senderuid, title, description, inn, status, active, date) VALUES ($1, $2, $3, $4, $5, true, CURRENT_TIMESTAMP) RETURNING uid`,
		s.Fund.OwnerUID, s.Fund.Title, s.Fund.Description, s.Fund.Inn, s.Fund.Status).Scan(&newUID)

	return newUID, err

}

// Edit - edit section
func (s *Main) Edit() error {
	sql := `UPDATE fund SET title = $1, description = $2, inn=$3, status=$4, active=$5, senderuid = $6 WHERE uid = $7`
	_, err := s.db.Exec(sql, s.Fund.Title, s.Fund.Description, s.Fund.Inn, s.Fund.Status, s.Fund.Active, s.Fund.OwnerUID, s.Fund.UID)
	return err
}

// FileExist - check for file
func FileExist(filename string) bool {

	pathtofile := filepath.Join(binpath.BinaryPath(), "images/funds")

	if _, err := os.Stat(filepath.Join(pathtofile, filename+".jpg")); !os.IsNotExist(err) {
		return true
	}
	return false

}

// GetOne - get information about current fund
func GetOne(db *pgx.ConnPool, picsURL string, UID string) (Fund, error) {
	var err error
	var time time.Time
	item := &Fund{}
	sql := `SELECT fund.uid, fund.title, fund.description, fund.inn, fund.status, fund.date,
	COALESCE(address.country, '') as country, COALESCE(address.city, '') as City, COALESCE(address.postal, '') as postal, 
	COALESCE(address.address, '') as address,
	COALESCE(contact.person,'') as contactPerson, 
	COALESCE(contact.phone,'') as contactPhone, COALESCE(contact.email,'') as contactEmail, COALESCE(contact.website,'') as contactWeb, 
	COALESCE(contact.skype,'') as contactSkype

	from fund 
	LEFT JOIN address on address.owneruid = fund.uid::VARCHAR
	LEFT JOIN (SELECT contacts.uid, contacts.person, contacts.phone, contacts.email, contacts.website, contacts.skype, contacts.owneruid FROM contacts where contacts.ownertype = 'fund') as
			contact ON contact.owneruid = fund.uid::VARCHAR   
	WHERE fund.uid = $1`
	err = db.QueryRow(sql, UID).Scan(&item.UID, &item.Title, &item.Description, &item.Inn, &item.Status, &time, &item.Address.Country, &item.Address.City, &item.Address.Postal, &item.Address.Address,
		&item.Contacts.Person, &item.Contacts.Phone, &item.Contacts.Email,
		&item.Contacts.Website, &item.Contacts.Skype)

	if err != nil {
		return *item, err
	}
	item.Date = time.Format("2006-01-02T15:04:05")
	item.Images.Image = picsURL
	if FileExist(item.UID) {
		item.Images.Image = item.Images.Image + "funds/" + item.UID + ".jpg"
	} else {
		item.Images.Image = item.Images.Image + "nophoto.jpg"
	}
	return *item, err
}

// Get - get list of items
func Get(db *pgx.ConnPool, picsURL string, search Search) ([]*Fund, int, error) {
	var Offset int
	var err error
	var rows *pgx.Rows

	list := make([]*Fund, 0)
	WithSearch := false
	SearchSQL := ` `
	LimitSQL := ` `

	if search.Search != "" {
		WithSearch = true
		SearchSQL = ` WHERE title like $1 `
	}

	if search.Paginator {
		if WithSearch {
			LimitSQL = ` LIMIT $2 OFFSET $3`
		} else {
			LimitSQL = ` LIMIT $1 OFFSET $2`
		}
		Offset = search.Current*search.Onpage - search.Onpage
	}

	sql := `SELECT count(*) OVER() AS total_count, fund.uid, fund.title, fund.description, fund.inn, fund.status, fund.date,
	COALESCE(address.country, '') as country, COALESCE(address.city, '') as City, COALESCE(address.postal, '') as postal, 
	COALESCE(address.address, '') as address,
	COALESCE(contact.person,'') as contactPerson, 
	COALESCE(contact.phone,'') as contactPhone, COALESCE(contact.email,'') as contactEmail, COALESCE(contact.website,'') as contactWeb, 
	COALESCE(contact.skype,'') as contactSkype
	from fund 
	LEFT JOIN address on address.owneruid = fund.uid::VARCHAR 
	LEFT JOIN (SELECT contacts.uid, contacts.person, contacts.phone, contacts.email, contacts.website, contacts.skype, contacts.owneruid FROM contacts where contacts.ownertype = 'fund') as
			contact ON contact.owneruid = fund.uid::VARCHAR 
	` + SearchSQL + `order by title` + LimitSQL

	if WithSearch && search.Paginator {
		rows, err = db.Query(sql, "%"+search.Search+"%", search.Onpage, Offset)
	}

	if WithSearch && !search.Paginator {
		rows, err = db.Query(sql, "%"+search.Search+"%")
	}

	if !WithSearch && !search.Paginator {
		rows, err = db.Query(sql)
	}
	if !WithSearch && search.Paginator {
		rows, err = db.Query(sql, search.Onpage, Offset)
	}

	if err != nil && err != pgx.ErrNoRows {
		return list, 0, err
	}
	totalCount := 0

	for rows.Next() {
		item := &Fund{}

		var time time.Time

		err = rows.Scan(&totalCount, &item.UID, &item.Title, &item.Description, &item.Inn, &item.Status, &time, &item.Address.Country, &item.Address.City, &item.Address.Postal, &item.Address.Address,
			&item.Contacts.Person, &item.Contacts.Phone, &item.Contacts.Email,
			&item.Contacts.Website, &item.Contacts.Skype)

		if err != nil {
			return list, 0, err
		}

		item.Date = time.Format("2006-01-02T15:04:05")
		item.Images.Image = picsURL
		if FileExist(item.UID) {
			item.Images.Image = item.Images.Image + "funds/" + item.UID + ".jpg"
		} else {
			item.Images.Image = item.Images.Image + "nophoto.jpg"
		}

		list = append(list, item)
	}

	return list, totalCount, nil
}

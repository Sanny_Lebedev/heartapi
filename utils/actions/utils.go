package actions

// GetPrices - Formation of payment buttons
func GetPrices(isAuction bool, ticketPrice, minSum, curRate, minBet float64) []float64 {

	out := make([]float64, 0)

	switch isAuction {
	case true:
		if curRate == 0 {
			curRate = minSum
			out = append(out, curRate/100)
		}
		for i := 1; i < 6; i++ {
			curRate = curRate + minBet
			out = append(out, curRate/100)
		}

	default:
		for i := 1; i < 6; i++ {
			out = append(out, ticketPrice)
			ticketPrice = ticketPrice * 2
		}
	}

	return out
}

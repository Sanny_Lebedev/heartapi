package actions

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crossimages"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/prize"
	"github.com/jackc/pgx"
)

type (
	// Main - main structure for fundraising
	Main struct {
		Actions Actions
		ci      crossimages.Main
		db      *pgx.ConnPool
		log     logger.Logger
	}

	// MiniFund ...
	MiniFund struct {
		UID         string `json:"uid"`
		Title       string `json:"title"`
		Image       string `json:"image"`
		Description string `json:"description"`
		Likes       int    `json:"likes"`
		Step        int    `json:"step"`
	}

	// Actions  - main structure
	Actions struct {
		UID            string   `json:"uid"`
		OwnerUID       string   `json:"-"`
		FundraisingUID string   `json:"fundraisingUID"`
		MaecenasUID    string   `json:"maecenasUID"`
		Title          string   `json:"title"`
		LongTitle      string   `json:"longTitle"`
		Description    string   `json:"description"`
		Result         string   `json:"result"`
		NeedSumm       float64  `json:"needSumm"`
		Donated        int64    `json:"donated"`
		Likes          int64    `json:"likes"`
		DonatedSumm    float64  `json:"donatedSumm"`
		TicketPrice    float64  `json:"ticketPrice"`
		TotalWinners   int64    `json:"totalWinners"`
		Status         string   `json:"status"`
		HaveVideo      bool     `json:"haveVideo"`
		VideoURL       string   `json:"videoURL"`
		Fund           MiniFund `json:"fund"`
		Section        MiniFund `json:"section"`
		Flags          Flags    `json:"flags"`
		Auction        Auct     `json:"auction"`
		Foruser
		Dates     Dates                 `json:"dates"`
		Images    []*models.Images      `json:"images"`
		Prizes    []*prize.Prize        `json:"prizes"`
		Users     models.ActusersAnswer `json:"users"`
		Maecenas  Maecenas              `json:"maecenas"`
		ShortLink ShortLink             `json:"shortLink"`
		Prices    []float64             `json:"prices"`

		//	Fundraising    fundraising.Fundraising `json:"fundraising, omitempty"`
	}

	//ShortLink ...
	ShortLink struct {
		URL  string `json:"URL"`
		Link string `json:"link"`
	}

	//Auct - structure of auction
	Auct struct {
		IsAuction   bool    `json:"isAuction"`
		MinSumm     float64 `json:"minSumm"`
		CurrentRate float64 `json:"currentRate"`
		MinBet      float64 `json:"minBet"`
	}

	//Maecenas - sponsor of action
	Maecenas struct {
		UID         string `json:"UID"`
		Name        string `json:"name"`
		Instagram   string `json:"instagram"`
		Description string `json:"description"`
		Photo       string `json:"image"`
	}

	//MaecenasList - list of sponsors
	MaecenasList struct {
		Maecenases []*Maecenas `json:"maecenasList,omitempty"`
	}

	// Foruser - the part of answer just for user
	Foruser struct {
		IsLiked        bool    `json:"isLiked"`
		IsPaid         bool    `json:"isPaid"`
		IsWin          bool    `json:"isWin"`
		TicketAmount   float64 `json:"ticketAmount"`
		TickenUID      string  `json:"ticketUID"`
		WinnersMessage string  `json:"winnersMessage"`
	}

	// Flags - the part of answer just for Flags
	Flags struct {
		Active        bool `json:"active"`
		Flagstart     bool `json:"flagstart"`
		Flagfinish    bool `json:"flagfinish"`
		Criteriasumm  bool `json:"criteriasSumm"`
		Criteriasdate bool `json:"criteriasDate"`
	}

	// Dates - the part of answer just for dates
	Dates struct {
		DateStart  string `json:"dateStart"`
		DateFinish string `json:"dateFinish"`
		DaysLeft   int    `json:"daysLeft"`
		HoursLeft  int    `json:"hoursLeft"`
		Date       int64 `json:"date"`
	}

	// Images - structure for pics for fundraising
	Images struct {
		UID   string `json:"UID"`
		Image string `json:"image"`
	}

	// Search - structure for search and pagination
	Search struct {
		Search     string `json:"search"`
		Current    int    `json:"current"`
		Onpage     int    `json:"onPage"`
		Paginator  bool   `json:"paginator"`
		OnlyActive bool   `json:"onlyActive"`
		NotActive  bool   `json:"notActive"`
	}

	// Answer - answer for API with list of Actions
	Answer struct {
		List       []*Actions `json:"list"`
		Success    bool       `json:"success"`
		Action     string     `json:"message"`
		Count      int        `json:"count,omitempty"`
		TotalCount int        `json:"totalCount,omitempty"`
		HasNext    bool       `json:"hasNext"`
	}

	// AnswerOne - answer for API with info about current Action
	AnswerOne struct {
		Info    Actions `json:"info"`
		Success bool    `json:"success"`
		Action  string  `json:"message"`
	}

	//AnswerWithUID - asnwer with UID of action
	AnswerWithUID struct {
		Info  Actions     `json:"info"`
		Alert ActionAlert `json:"alert"`
		AnsWithUID
	}

	//AnsWithUID ...
	AnsWithUID struct {
		UID     string `json:"UID"`
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}

	//ActionAlert - alert for action with message and short link
	ActionAlert struct {
		Message string `json:"message"`
		Link    string `json:"URL"`
	}
)

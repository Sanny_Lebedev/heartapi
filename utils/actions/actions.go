package actions

import (
	"encoding/hex"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/jobs"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/likes"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/prize"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crossimages"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"

	"github.com/jackc/pgx"
)

const (
	//Area - area name for os
	Area = "actions"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, OwnerUID string, ActionsUID string, Log logger.Logger) *Main {

	u := &Main{
		db:  db,
		log: Log,
		ci:  *crossimages.Initial(db, Log, Area),
		Actions: Actions{
			UID:      ActionsUID,
			OwnerUID: OwnerUID,
		},
	}

	return u
}

// New - returns new sections instance
func New(db *pgx.ConnPool, ctx *app.SenditemsActionsContext, OwnerUID string, Log logger.Logger) *Main {
	uid := ""
	MaecenasUID := OwnerUID
	TotalWinners := 1
	longtitle := ""
	if ctx.Payload.UID != nil {
		uid = *ctx.Payload.UID
	}

	if &ctx.Payload.MaecenasUID != nil {
		MaecenasUID = ctx.Payload.MaecenasUID
	}

	if ctx.Payload.TotalWinners != nil {
		TotalWinners = *ctx.Payload.TotalWinners
	}

	if ctx.Payload.LongTitle != nil {
		longtitle = *ctx.Payload.LongTitle
	}

	u := &Main{
		db:  db,
		log: Log,
		ci:  *crossimages.Initial(db, Log, Area),
		Actions: Actions{
			UID:            uid,
			OwnerUID:       OwnerUID,
			FundraisingUID: ctx.Payload.FundraisingUID,
			MaecenasUID:    MaecenasUID,
			Title:          ctx.Payload.Title,
			LongTitle:      longtitle,
			Description:    *ctx.Payload.Description,
			Result:         *ctx.Payload.Result,
			NeedSumm:       float64(*ctx.Payload.NeedSumm),
			TotalWinners:   int64(TotalWinners),
			Flags: Flags{
				Active:        *ctx.Payload.Flags.Active,
				Flagstart:     *ctx.Payload.Flags.Flagstart,
				Flagfinish:    *ctx.Payload.Flags.Flagfinish,
				Criteriasumm:  *ctx.Payload.Flags.Criteriasumm,
				Criteriasdate: *ctx.Payload.Flags.Criteriasdate,
			},
			Auction: Auct{
				IsAuction:   ctx.Payload.Auction.IsAuction,
				MinSumm:     float64(ctx.Payload.Auction.MinSumm),
				CurrentRate: float64(ctx.Payload.Auction.CurrentRate),
				MinBet:      float64(ctx.Payload.Auction.MinBet),
			},
			Dates: Dates{
				DateStart:  *ctx.Payload.Dates.DateStart,
				DateFinish: *ctx.Payload.Dates.DateFinish,
			},
			Status: *ctx.Payload.Status,
		},
	}

	return u
}

// Stopaction - stop action by maecenas uid
func Stopaction(db *pgx.ConnPool, maecenas string) error {
	var err error
	_, err = db.Exec(`UPDATE actions SET active = false, flagfinish = false WHERE maecenas = $1`, maecenas)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// RemoveMaecenas - remove maecenas from action
func RemoveMaecenas(db *pgx.ConnPool, maecenas string) error {
	var err error
	_, err = db.Exec(`UPDATE actions SET active = false, flagfinish = false, maecenas = '' WHERE maecenas = $1`, maecenas)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// Delete - delete actions from db
func Delete(db *pgx.ConnPool, log logger.Logger, uid string) error {
	var err error

	// delete photos of actions
	err = DeleteFRPhotos(db, log, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	// Delete from links tab
	err = DeleteLinks(db, log, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	// Delete from likes tab
	err = DeleteLikes(db, log, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	// Delete from prizes tab
	err = DeletePrizes(db, log, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	// Delete prizes photo

	// Delete action from table
	_, err = db.Exec(`DELETE from actions WHERE uid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// DeleteLinks - delete links from db
func DeleteLinks(db *pgx.ConnPool, log logger.Logger, uid string) error {
	var err error
	_, err = db.Exec(`DELETE from links WHERE action = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// DeleteLikes - delete likes from db
func DeleteLikes(db *pgx.ConnPool, log logger.Logger, uid string) error {
	var err error
	_, err = db.Exec(`DELETE from likes WHERE owneruid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

func DeletePrizes(db *pgx.ConnPool, log logger.Logger, uid string) error {

	rows, err := db.Query(`SELECT uid FROM prize WHERE owneruid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}

	if err != nil {
		log.Error().Err(err).Msg("Error with selecting prizes for action for deleting")
		return err
	}

	for rows.Next() {
		item := ""
		err = rows.Scan(&item)
		if err != nil {
			log.Error().Err(err).Msg("Error with scan for uid of prize")
			return err
		}
		ci := crossimages.Initial(db, log, "prize")
		err = ci.DeleteFRPhotos(item)
		if err != nil && err != pgx.ErrNoRows {
			return err
		}
	}

	_, err = db.Exec(`DELETE from prize WHERE owneruid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// DeleteFRPhotos - delete all photos of findraising from db
func DeleteFRPhotos(db *pgx.ConnPool, log logger.Logger, uid string) error {
	ci := crossimages.Initial(db, log, Area)
	return ci.DeleteFRPhotos(uid)
}

// DeleteFRPhotos - delete all photos of findraising from db
func (s *Main) DeleteFRPhotos(uid string) error {
	return s.ci.DeleteFRPhotos(uid)
}

// Exist - check for exist of section
func Exist(db *pgx.ConnPool, uid string) (bool, error) {
	var exist bool
	err := db.QueryRow(`SELECT COUNT(uid)>0 from actions WHERE uid = $1`, uid).Scan(&exist)
	if err != nil || err == pgx.ErrNoRows {

		return false, err
	}
	return exist, nil
}

//MakeLink - make link for current action
func MakeLink(db *pgx.ConnPool, uid string) (string, error) {
	link, err := CheckLink(db, uid)
	if err != nil {
		return "", err
	}

	switch len(link) {
	case 0:
		break
	default:
		return link, nil
	}

	b := make([]byte, 3)
	rand.Read(b)
	link = hex.EncodeToString(b)

	_, err = db.Exec(`INSERT INTO links (action, link) VALUES ($1, $2)`, uid, link)
	if err != nil {
		return "", err
	}
	return link, nil

}

// CheckLink - check link for action
func CheckLink(db *pgx.ConnPool, uid string) (string, error) {
	link := ""
	err := db.QueryRow(`SELECT link FROM links WHERE action = $1`, uid).Scan(&link)
	if err == pgx.ErrNoRows {
		return "", nil
	}

	return link, err
}

// GetLink - check link for action
func GetLink(db *pgx.ConnPool, link string) (string, error) {
	action := ""
	err := db.QueryRow(`SELECT action FROM links WHERE link = $1`, link).Scan(&action)
	if err == pgx.ErrNoRows {
		return "", nil
	}

	return action, err
}

// IsExist - check for exist
func (s *Main) IsExist() (bool, error) {
	isExist, err := Exist(s.db, s.Actions.UID)
	if err != nil {
		s.log.Error().Err(err).Msg("Error with checking action for exist")
	}
	return isExist, err
}

// DeleteImageByUID - deleting Image by image UID
func (s *Main) DeleteImageByUID(imageuid string) error {
	return s.ci.DeleteImageByUID(imageuid)
}

// DeleteActionVideo - delete video from action
func (s *Main) DeleteActionVideo(uid string) error {
	_, err := s.db.Exec(`UPDATE actions SET havevideo = false WHERE uid = $1`, uid)
	return err
}

// SaveActionVideo - save video to actions db
func (s *Main) SaveActionVideo(uid string) error {
	_, err := s.db.Exec(`UPDATE actions SET havevideo = true WHERE uid = $1`, uid)
	return err
}

// SaveActionVideoToConvert ...
func (s *Main) SaveActionVideoToConvert(UID, fileName string) error {
	var err error
	_, err = s.db.Exec(`DELETE FROM videoconvert WHERE action=$1 AND converted = false`, UID)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}
	_, err = s.db.Exec(`INSERT INTO videoconvert (action, filein, date) VALUES ($1, $2, CURRENT_TIMESTAMP)`, UID, fileName)
	return err
}

// SaveImage - saving image to DB
func (s *Main) SaveImage(senderuid, owneruid string) (Images, error) {
	img, err := s.ci.SaveImage(senderuid, owneruid)
	return Images(img), err
}

//ActionsCheck - check actions for exist
func (s *Main) ActionsCheck() (bool, error) {
	var ans bool
	var err error
	ans, err = Exist(s.db, s.Actions.UID)

	if err != nil && err != pgx.ErrNoRows {
		s.log.Error().Err(err).Msg("Error with checking exists of action")
		return false, err
	}
	return ans, nil

}

//Save - saving actions data
func (s *Main) Save() (string, error) {

	if s.Actions.UID == "" {
		return s.Insert()
	}

	isExist, err := s.IsExist()
	if err != nil && err != pgx.ErrNoRows {
		return "", err
	}

	if !isExist {
		return s.Insert()
	}

	return s.Update()
}

//Update - updating actions data
func (s *Main) Update() (string, error) {
	var err error

	DateStart, err2 := time.Parse("2006-01-02", s.Actions.Dates.DateStart)
	if err != nil {
		s.log.Error().Err(err2).Msg("Error with parsing from datestart")
		return s.Actions.UID, err2
	}

	DateFinish, err1 := time.Parse("2006-01-02", s.Actions.Dates.DateFinish)
	if err1 != nil {
		s.log.Error().Err(err1).Msg("Error with parsing from datefinish")
		return s.Actions.UID, err1
	}

	_, err = s.db.Exec(`UPDATE actions SET senderuid = $1, maecenas = $2, owneruid = $3, ownertype = $4, title=$5, longtitle=$6, 
	description=$7, result=$8, needsumm=$9, totalwinners=$10, active=$11, status=$12, flagstart=$13, flagfinish=$14, criteriasumm=$15, 
	criteriasdate=$16, datestart=$17, datefinish=$18, date = CURRENT_TIMESTAMP,
	minsumm = $20, currentsumm = $21, minbet = $22, isauction = $23
	WHERE uid = $19`, s.Actions.OwnerUID, s.Actions.MaecenasUID, s.Actions.FundraisingUID, models.OwnerFundraising, s.Actions.Title, s.Actions.LongTitle,
		s.Actions.Description, s.Actions.Result, s.Actions.NeedSumm, s.Actions.TotalWinners, s.Actions.Flags.Active, s.Actions.Status, s.Actions.Flags.Flagstart,
		s.Actions.Flags.Flagfinish, s.Actions.Flags.Criteriasumm,
		s.Actions.Flags.Criteriasdate, DateStart, DateFinish, s.Actions.UID,
		s.Actions.Auction.MinSumm, s.Actions.Auction.CurrentRate, s.Actions.Auction.MinBet, s.Actions.Auction.IsAuction)
	if err != nil {
		s.log.Error().Err(err).Msg("Error with update actions DB")
	}

	return s.Actions.UID, nil
}

// Insert - insert actions data
func (s *Main) Insert() (string, error) {
	uid := ""

	DateStart, err := time.Parse("2006-01-02", s.Actions.Dates.DateStart)
	if err != nil {
		s.log.Error().Err(err).Msg("Error with parsing from datestart")
		return uid, err
	}

	DateFinish, err1 := time.Parse("2006-01-02", s.Actions.Dates.DateFinish)
	if err1 != nil {
		s.log.Error().Err(err1).Msg("Error with parsing from datefinish")
		return uid, err1
	}

	err = s.db.QueryRow(`INSERT INTO actions (senderuid, maecenas, owneruid, ownertype, title, longtitle, 
	description, result, needsumm, totalwinners, active, status, flagstart, flagfinish, criteriasumm, 
	criteriasdate, datestart, datefinish, date, minsumm, currentsumm, minbet, isauction) VALUES ($1, $2, $3, $4, $5, $6,
	$7,$8, $9, $10, $11, $12, $13, $14, $15,
	$16, $17, $18, CURRENT_TIMESTAMP, $19, $20, $21, $22) RETURNING uid`, s.Actions.OwnerUID, s.Actions.MaecenasUID, s.Actions.FundraisingUID, models.OwnerFundraising, s.Actions.Title, s.Actions.LongTitle,
		s.Actions.Description, s.Actions.Result, s.Actions.NeedSumm, s.Actions.TotalWinners, s.Actions.Flags.Active, s.Actions.Status, s.Actions.Flags.Flagstart, s.Actions.Flags.Flagfinish, s.Actions.Flags.Criteriasumm,
		s.Actions.Flags.Criteriasdate, DateStart, DateFinish, s.Actions.Auction.MinSumm, s.Actions.Auction.CurrentRate, s.Actions.Auction.MinBet, s.Actions.Auction.IsAuction).Scan(&uid)

	if err != nil {
		s.log.Error().Err(err).Msg("Error with inserting new actions to DB")
	}

	// Send notification email
	MMM := jobs.Messtr{Qname: models.QnameSend, Getter: uid, Message: "forAdmins"}

	inDB, err := jobs.IsInDB(s.db, MMM, models.ActionNewAction)
	if err != nil {
		s.log.Error().Err(err).Msg("Error with checking in queue db")
	} else {
		if !inDB {
			// Sending job to DB
			_ = jobs.ActionToDB(s.db, MMM, models.ActionNewAction)
		}
	}

	return uid, err
}

// FileExist - check for file
func (s *Main) FileExist(filename string) bool {
	return s.ci.FileExist(filename)
}

// CheckSender - check for sender
func (s *Main) CheckSender() (bool, error) {
	isOk := false
	err := s.db.QueryRow(`SELECT COUNT(uid)>0 from actions WHERE uid = $1 AND maecenas = $2`, s.Actions.UID, s.Actions.OwnerUID).Scan(&isOk)
	return isOk, err
}

//GetPhotoOwner - get owner of Photo by photouid
func GetPhotoOwner(db *pgx.ConnPool, photouid string) (string, error) {
	var maecenas string
	err := db.QueryRow(`SELECT actions.maecenas FROM crossimages LEFT JOIN actions on actions.uid::VARCHAR = crossimages.owneruid
	WHERE crossimages.ownertype = $1 AND crossimages.uid = $2`, Area, photouid).Scan(&maecenas)
	return maecenas, err
}

//ChangeActive - change active status of action
func ChangeActive(db *pgx.ConnPool, log logger.Logger, uid, key, team string) error {
	var active bool
	ownerUID := ""
	err := db.QueryRow(`UPDATE actions SET active = NOT active WHERE uid = $1 RETURNING active, owneruid`, uid).Scan(&active, ownerUID)
	if err != nil {
		return err
	}
	err = jobs.Notification(db, log, uid, ownerUID, key, team)
	if err != nil {
		log.Error().Err(err).Msg("Error with notofication about new action")
	}

	return err
}

// GetMaecenas - get maecenas
func GetMaecenas(db *pgx.ConnPool, picsURL string, uid string) ([]*Maecenas, error) {
	list := make([]*Maecenas, 0)

	sql := `SELECT 
	users.UID, users.firstname || ' ' || users.lastname as name, COALESCE(users.instagram,'') as instagram,
	COALESCE(users.description,'') as mdescription,
	COALESCE(usersphotos.filename,'') as photoname, pics.imagelink
	FROM fundraising
	LEFT JOIN actions on actions.owneruid = fundraising.uid::VARCHAR
	LEFT JOIN users on users.uid::VARCHAR = actions.maecenas
	LEFT JOIN usersphotos on usersphotos.usersuid::VARCHAR = actions.maecenas
		LEFT JOIN 
		  (SELECT array_agg(imagelink) as imagelink, usersuid FROM socnet GROUP BY usersuid)
		as pics on pics.usersuid = actions.maecenas
	Where fundraising.uid = $1`
	rows, err := db.Query(sql, uid)

	for rows.Next() {
		item := &Maecenas{}

		var maecenasImages []string
		maecenasProfilePhoto := ""

		err := rows.Scan(&item.UID, &item.Name, &item.Instagram, &item.Description, &maecenasProfilePhoto, &maecenasImages)
		if err != nil && err != pgx.ErrNoRows {
			return list, err
		}

		//Get actual maecenas photo
		if maecenasProfilePhoto != "" {
			item.Photo = picsURL + string(models.ProfilesArea) + "/" + maecenasProfilePhoto
		} else {
			for _, pic := range maecenasImages {
				if len(pic) > 0 {
					item.Photo = pic
				}
			}
			if len(item.Photo) < 1 {
				item.Photo = picsURL + models.NoPhoto
			}
		}
		list = append(list, item)

	}

	return list, err
}

// Getone - get information about current fundraising
func Getone(db *pgx.ConnPool, picsURL string, uid string, usersUID string, Domain string) (Actions, error) {
	item := &Actions{}

	donatedsumm := 0
	needsumm := 0
	var DateStart time.Time
	var DateFinish time.Time
	var Date time.Time

	sql := `SELECT 
	actions.uid, actions.senderuid, actions.maecenas, actions.owneruid, actions.title, actions.longtitle, actions.havevideo, actions.ticketprice/100 as ticketprice,
	actions.description, actions.result, actions.needsumm, actions.donatedsumm, actions.donated, actions.totalwinners, actions.active, actions.status, 
	actions.flagstart, actions.flagfinish, actions.criteriasumm, 
	actions.criteriasdate, actions.date ,actions.datestart, actions.datefinish, actions.likes, images.uids,
	users.UID as maecenasuid,
	users.firstname || ' ' || users.lastname as maecenasname,
	COALESCE(users.instagram,'') as instagram, COALESCE(users.description,'') as mdescription, COALESCE(usersphotos.filename,'') as photoname,
	pics.imagelink,
	actions.minsumm, actions.currentsumm, actions.minbet, actions.isauction,
	COALESCE(fund.uid,'00000000-0000-0000-0000-000000000000') as funduid, COALESCE(fund.title,'') as fundtitle,
	COALESCE(fund.description,'') as funddescription,
	COALESCE(sections.title,'') as sectiontitle, 
	COALESCE(sections.uid,'00000000-0000-0000-0000-000000000000') as fundsowneruid,
	COALESCE(links.link,'') as shortlink, COALESCE(sections.description,'') as description,
	fundimages.imagenames

  	FROM actions
	LEFT JOIN users ON users.uid::VARCHAR = actions.maecenas
	LEFT JOIN usersphotos on usersphotos.usersuid::VARCHAR = actions.maecenas
	LEFT JOIN 
	  (SELECT array_remove(array_agg(imagelink),NULL) as imagelink, usersuid FROM socnet GROUP BY usersuid)
	as pics on pics.usersuid = actions.maecenas
	LEFT JOIN (select array_remove(array_agg(uid),NULL) as uids, owneruid FROM crossimages GROUP BY owneruid) 
	as images on images.owneruid = actions.uid::VARCHAR
	LEFT JOIN 
		(select funduid, raisinguid, owneruid from crossfundsraising) as funds on funds.raisinguid = actions.owneruid
	  LEFT JOIN fund on fund.uid::VARCHAR = funds.funduid

	  LEFT JOIN (select array_remove(array_agg(imagename), NULL) as imagenames, owneruid FROM crossimages GROUP BY owneruid) 
	as fundimages on fundimages.owneruid = fund.uid::VARCHAR

	  LEFT JOIN sections on sections.uid::VARCHAR = funds.owneruid
	  LEFT JOIN links on links.action = actions.uid::VARCHAR
	WHERE actions.uid = $1 order by actions.date`

	var images []string
	maecenasProfilePhoto := ""
	var maecenasImages, fundImages []string

	err := db.QueryRow(sql, uid).Scan(&item.UID, &item.OwnerUID, &item.MaecenasUID, &item.FundraisingUID, &item.Title, &item.LongTitle, &item.HaveVideo, &item.TicketPrice,
		&item.Description, &item.Result, &needsumm, &donatedsumm, &item.Donated, &item.TotalWinners, &item.Flags.Active, &item.Status,
		&item.Flags.Flagstart, &item.Flags.Flagfinish, &item.Flags.Criteriasumm,
		&item.Flags.Criteriasdate, &Date, &DateStart, &DateFinish, &item.Likes, &images,
		&item.Maecenas.UID,
		&item.Maecenas.Name, &item.Maecenas.Instagram, &item.Maecenas.Description, &maecenasProfilePhoto, &maecenasImages,
		&item.Auction.MinSumm, &item.Auction.CurrentRate, &item.Auction.MinBet, &item.Auction.IsAuction,
		&item.Fund.UID, &item.Fund.Title, &item.Fund.Description, &item.Section.Title, &item.Section.UID, &item.ShortLink.Link, &item.Section.Description, &fundImages)

	if err != nil && err != pgx.ErrNoRows {
		return *item, err
	}

	item.ShortLink.URL = Domain + "/link/" + item.ShortLink.Link

	// Image of section
	pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")

	// Path to video
	pathtovideo := filepath.Join(binpath.BinaryPath(), "images/video/actions")

	SectionFileExist := false
	if _, err := os.Stat(filepath.Join(pathtofile, item.Section.UID+".jpg")); !os.IsNotExist(err) {
		SectionFileExist = true
	}

	// Formation of payment buttons
	item.Prices = GetPrices(item.Auction.IsAuction, item.TicketPrice, item.Auction.MinSumm, item.Auction.CurrentRate, item.Auction.MinBet)

	if SectionFileExist {
		item.Section.Image = picsURL + "sections/" + item.Section.UID + ".jpg"
	} else {
		item.Section.Image = picsURL + "nophoto.jpg"
	}
	item.VideoURL = ""
	if item.HaveVideo {
		if _, err := os.Stat(filepath.Join(pathtovideo, item.UID+".mp4")); !os.IsNotExist(err) {
			item.VideoURL = picsURL + "video/actions/" + item.UID + ".mp4"
		}
	}

	if len(fundImages) > 0 {
		item.Fund.Image = picsURL + "fund/" + fundImages[0]
	} else {
		item.Fund.Image = picsURL + "nophoto.jpg"
	}

	mySearch := prize.Search{}
	mySearch.Paginator = false
	item.Prizes, _, err = prize.Get(db, picsURL, uid, mySearch)
	if err != nil && err != pgx.ErrNoRows {
		return *item, err
	}

	if len(images) > 0 {
		listImage := make([]*models.Images, 0)
		for _, image := range images {
			pic := &models.Images{}
			pic.Image = picsURL + Area + "/" + image + ".jpg"
			pic.UID = image
			listImage = append(listImage, pic)
			// element is the element from someSlice for where we are
		}

		item.Images = listImage
	} else {
		listImage := make([]*models.Images, 0)
		pic := &models.Images{}
		pic.Image = picsURL + models.NoPhoto
		pic.UID = ""
		listImage = append(listImage, pic)
		item.Images = listImage
	}

	item.Dates.DaysLeft = 0
	item.Dates.HoursLeft = 0
	if item.Flags.Criteriasdate {
		date := time.Now()
		duration := date.Sub(DateFinish)
		item.Dates.DaysLeft = 0 - int(duration.Hours()/24)
		if item.Dates.DaysLeft < 0 {
			item.Dates.DaysLeft = 0
		}
		if item.Dates.DaysLeft == 0 {
			item.Dates.HoursLeft = 0 - int(duration.Hours())
			if item.Dates.HoursLeft < 0 {
				item.Dates.HoursLeft = 0
			}
		}
	}

	//Get actual maecenas photo
	if maecenasProfilePhoto != "" {
		item.Maecenas.Photo = picsURL + string(models.ProfilesArea) + "/" + maecenasProfilePhoto
	} else {
		for _, pic := range maecenasImages {
			if len(pic) > 0 {
				item.Maecenas.Photo = pic
			}
		}
		if len(item.Maecenas.Photo) < 1 {
			item.Maecenas.Photo = picsURL + models.NoPhoto
		}
	}

	item.NeedSumm = float64(needsumm / 100)
	item.DonatedSumm = float64(donatedsumm / 100)
	item.Auction.CurrentRate = float64(item.Auction.CurrentRate / 100)
	item.Auction.MinBet = float64(item.Auction.MinBet / 100)
	item.Auction.MinSumm = float64(item.Auction.MinSumm / 100)
	item.Dates.DateStart = DateStart.Format("2006-01-02")
	item.Dates.DateFinish = DateFinish.Format("2006-01-02")
	item.Dates.Date = Date.Unix()

	// item.Foruser.IsLiked, err = likes.IsLiked(db, item.UID, "Action", usersUID)

	// if err != nil {
	// 	item.Foruser.IsLiked = false
	// }

	// // Likes for action
	// item.IsLiked = item.Foruser.IsLiked
	// ActionLikes, err1 := likes.GetMyCount(db, item.UID, "Action")
	// if err1 != nil {
	// 	ActionLikes = 0
	// }
	// item.Likes = int64(ActionLikes)

	// // Likes for section
	// sectionlikes, err2 := likes.GetMyCount(db, item.Section.UID, "Section")
	// if err2 != nil {
	// 	sectionlikes = 0
	// }
	// item.Section.Likes = (sectionlikes)

	// // Likes for fund
	// fundlikes, err3 := likes.GetMyCount(db, item.Fund.UID, "Fund")
	// if err3 != nil {
	// 	fundlikes = 0
	// }
	// item.Fund.Likes = (fundlikes)

	item.Foruser.IsPaid = false
	if usersUID != "" {
		item.Foruser.IsPaid, _ = IsPaidByUser(db, usersUID, item.UID)
		item.Foruser.IsWin, _ = IsWinner(db, usersUID, item.UID)
		if item.Foruser.IsPaid {
			item.Foruser.TicketAmount, item.Foruser.TickenUID, _ = GetTicket(db, usersUID, item.UID)
		}

	}

	actusers := &models.ActusersAnswer{}
	actusers.HasNext = false
	pagin := &models.Paginator{}
	pagin.Search = ""

	actusers.Actusers, actusers.Total, err = Getlistusers(db, picsURL, item.UID, true, *pagin)
	if len(actusers.Actusers) < actusers.Total {
		actusers.HasNext = true
	}
	actusers.Count = len(actusers.Actusers)
	item.Users = *actusers

	return *item, nil
}

//GetNews - get a list of news
func GetNews(db *pgx.ConnPool, picsURL string, UID string, activeFlag bool, TypeOfAction string) ([]*Actions, error) {
	list := make([]*Actions, 0)
	var err error
	var rows *pgx.Rows
	var checkActive = `datefinish >= NOW() AND actions.active = true AND actions.flagstart = true AND actions.flagfinish = false `
	switch activeFlag {
	case false:
		checkActive = `datefinish < NOW() AND actions.active = false AND actions.flagstart = true AND actions.flagfinish = true `
	default:

	}

	switch TypeOfAction {
	case "Auction":
		checkActive = checkActive + ` AND actions.isauction = true`
	case "Action":
		checkActive = checkActive + ` AND actions.isauction = false`
	default:
	}

	sql := `SELECT 
	actions.uid,
	actions.senderuid, actions.maecenas, actions.owneruid, actions.title, actions.longtitle, actions.havevideo, actions.ticketPrice/100 as ticketprice,
	actions.description, actions.result, actions.needsumm, actions.donatedsumm, actions.totalwinners, actions.active, actions.status, 
	actions.flagstart, actions.flagfinish, actions.criteriasumm, 
	actions.criteriasdate, actions.datestart, actions.datefinish, actions.likes, images.uids,
	users.uid as maecenasuid,
	users.firstname || ' ' || users.lastname as maecenasname,
	COALESCE(users.instagram,'') as instagram, COALESCE(users.description,'') as mdescription, COALESCE(usersphotos.filename,'') as photoname,
	pics.imagelink,
	actions.minsumm, actions.currentsumm, actions.minbet, actions.isauction,
	funds.funduid, COALESCE(fund.title,'') as fundtitle, COALESCE(sections.title,'') as sectiontitle, funds.owneruid
  	FROM actions
	LEFT JOIN users ON users.uid::VARCHAR = actions.maecenas
	LEFT JOIN usersphotos on usersphotos.usersuid::VARCHAR = actions.maecenas
	LEFT JOIN 
	  (SELECT array_agg(imagelink) as imagelink, usersuid FROM socnet GROUP BY usersuid)
	as pics on pics.usersuid = actions.maecenas
	LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
	as images on images.owneruid = actions.uid::VARCHAR
	LEFT JOIN 
		(select funduid, raisinguid, owneruid from crossfundsraising) as funds on funds.raisinguid = actions.owneruid
	  LEFT JOIN fund on fund.uid::VARCHAR = funds.funduid
	  LEFT JOIN sections on sections.uid::VARCHAR = funds.owneruid
	where ` + checkActive + ` order by actions.date desc`
	rows, err = db.Query(sql)
	for rows.Next() {
		item := &Actions{}
		var images []string
		var maecenasImages []string
		maecenasProfilePhoto := ""

		donatedsumm := 0
		needsumm := 0
		var DateStart time.Time
		var DateFinish time.Time

		err = rows.Scan(&item.UID, &item.OwnerUID, &item.MaecenasUID, &item.FundraisingUID, &item.Title, &item.LongTitle, &item.HaveVideo, &item.TicketPrice,
			&item.Description, &item.Result, &needsumm, &donatedsumm, &item.TotalWinners, &item.Flags.Active, &item.Status,
			&item.Flags.Flagstart, &item.Flags.Flagfinish, &item.Flags.Criteriasumm,
			&item.Flags.Criteriasdate, &DateStart, &DateFinish, &item.Likes,
			&images,
			&item.Maecenas.UID, &item.Maecenas.Name, &item.Maecenas.Instagram, &item.Maecenas.Description, &maecenasProfilePhoto, &maecenasImages,
			&item.Auction.MinSumm, &item.Auction.CurrentRate, &item.Auction.MinBet, &item.Auction.IsAuction, &item.Fund.UID, &item.Fund.Title,
			&item.Section.Title, &item.Section.UID)

		if err != nil {

			return list, err
		}
		// Path to video
		pathtovideo := filepath.Join(binpath.BinaryPath(), "images/video/actions")

		item.VideoURL = ""
		if item.HaveVideo {
			if _, err := os.Stat(filepath.Join(pathtovideo, item.UID+".mp4")); !os.IsNotExist(err) {
				item.VideoURL = picsURL + "video/actions/" + item.UID + ".mp4"
			}
		}

		// Formation of payment buttons
		item.Prices = GetPrices(item.Auction.IsAuction, item.TicketPrice, item.Auction.MinSumm, item.Auction.CurrentRate, item.Auction.MinBet)

		item.Dates.DaysLeft = 0
		item.Dates.HoursLeft = 0
		if item.Flags.Criteriasdate {
			date := time.Now()
			duration := date.Sub(DateFinish)
			item.Dates.DaysLeft = 0 - int(duration.Hours()/24)
			if item.Dates.DaysLeft < 0 {
				item.Dates.DaysLeft = 0
			}
			if item.Dates.DaysLeft == 0 {
				item.Dates.HoursLeft = 0 - int(duration.Hours())
				if item.Dates.HoursLeft < 0 {
					item.Dates.HoursLeft = 0
				}
			}
		}

		//Get actual maecenas photo
		if maecenasProfilePhoto != "" {
			item.Maecenas.Photo = picsURL + string(models.ProfilesArea) + "/" + maecenasProfilePhoto
		} else {
			for _, pic := range maecenasImages {
				if len(pic) > 0 {
					item.Maecenas.Photo = pic
				}
			}
			if len(item.Maecenas.Photo) < 1 {
				item.Maecenas.Photo = picsURL + models.NoPhoto
			}
		}

		mySearch := prize.Search{}
		mySearch.Paginator = false

		item.Prizes, _, err = prize.Get(db, picsURL, item.UID, mySearch)
		if err != nil && err != pgx.ErrNoRows {
			return list, err
		}
		item.Foruser.IsPaid = false
		if UID != "" {
			item.Foruser.IsPaid, err = IsPaidByUser(db, UID, item.UID)
			if err != nil && err != pgx.ErrNoRows {
				return list, err
			}

			item.Foruser.IsWin, err = IsWinner(db, UID, item.UID)
			if err != nil && err != pgx.ErrNoRows {
				return list, err
			}
			if item.Foruser.IsPaid {
				item.Foruser.TicketAmount, item.Foruser.TickenUID, err = GetTicket(db, UID, item.UID)
				if err != nil && err != pgx.ErrNoRows {
					return list, err
				}
			}
		}

		if len(images) > 0 {
			listImage := make([]*models.Images, 0)
			for _, image := range images {
				pic := &models.Images{}
				pic.Image = picsURL + Area + "/" + image + ".jpg"
				pic.UID = image
				listImage = append(listImage, pic)
				// element is the element from someSlice for where we are
			}
			item.Images = listImage
		} else {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = picsURL + models.NoPhoto
			pic.UID = ""
			listImage = append(listImage, pic)
			item.Images = listImage
		}
		item.NeedSumm = float64(needsumm / 100)
		item.DonatedSumm = float64(donatedsumm / 100)
		item.Dates.DateStart = DateStart.Format("2006-01-02")
		item.Dates.DateFinish = DateFinish.Format("2006-01-02")
		item.Foruser.IsLiked, err = likes.IsLiked(db, item.UID, "Action", UID)

		if err != nil {
			item.Foruser.IsLiked = false
		}

		item.IsLiked = item.Foruser.IsLiked
		likes, err1 := likes.GetMyCount(db, item.UID, "Action")
		if err1 != nil {
			likes = 0
		}
		item.Likes = int64(likes)

		actusers := &models.ActusersAnswer{}
		actusers.HasNext = false
		pagin := &models.Paginator{}
		pagin.Search = ""

		actusers.Actusers, actusers.Total, err = Getlistusers(db, picsURL, item.UID, true, *pagin)
		if len(actusers.Actusers) < actusers.Total {
			actusers.HasNext = true
		}
		actusers.Count = len(actusers.Actusers)
		item.Users = *actusers

		list = append(list, item)
	}

	return list, nil

}

// // Get - get a list of items
// func GetOld(db *pgx.ConnPool, picsURL string, fundraisinguid string, search Search, UID string, Domain string, TypeOfAction string) ([]*Actions, int, error) {
// 	list := make([]*Actions, 0)

// 	var Offset int
// 	var err error
// 	var rows *pgx.Rows

// 	WithSearch := false
// 	GetFundraising := false
// 	GetFlags := false
// 	WithTypeAction := false
// 	SearchSQL := ` `
// 	LimitSQL := ` `
// 	WhereSQL := ``

// 	if fundraisinguid != "" {
// 		GetFundraising = true
// 		SearchSQL = ` actions.owneruid = $1`
// 	}

// 	if search.Search != "" {

// 		WithSearch = true
// 		if GetFundraising {
// 			SearchSQL = SearchSQL + ` (actions.title like $2 OR users.firstname like $2 OR users.lastname like $2)`
// 		} else {
// 			SearchSQL = ` (actions.title like $1 OR users.firstname like $1 OR users.lastname like $1) `
// 		}
// 	}

// 	switch TypeOfAction {
// 	case "Auction":
// 		if GetFundraising || WithSearch {
// 			SearchSQL = SearchSQL + ` AND`
// 		}
// 		SearchSQL = SearchSQL + ` actions.isauction = true`
// 		WithTypeAction = true
// 	case "Action":
// 		if GetFundraising || WithSearch {
// 			SearchSQL = SearchSQL + ` AND`
// 		}
// 		SearchSQL = SearchSQL + ` actions.isauction = false`
// 		WithTypeAction = true
// 	default:
// 	}

// 	if search.OnlyActive && !search.NotActive {
// 		GetFlags = true
// 		if GetFundraising || WithSearch || WithTypeAction {
// 			SearchSQL = SearchSQL + ` AND`
// 		}

// 		SearchSQL = SearchSQL + ` ((actions.datefinish > NOW() AND actions.criteriasdate=true AND actions.active = false) OR (actions.active = true AND actions.flagstart = true AND actions.flagfinish = false)) `
// 	}

// 	if !search.OnlyActive && search.NotActive {
// 		GetFlags = true
// 		if GetFundraising || WithSearch {
// 			SearchSQL = SearchSQL + ` AND`
// 		}
// 		SearchSQL = SearchSQL + ` ((actions.datefinish <= NOW() AND actions.criteriasdate = true AND actions.active = false) OR (actions.active = false OR actions.flagstart = false OR actions.flagfinish = true))`
// 	}
// 	if GetFlags || WithSearch || GetFundraising {
// 		WhereSQL = ` WHERE `
// 	}

// 	if search.Paginator {

// 		if GetFundraising || WithSearch {
// 			LimitSQL = ` LIMIT $3 OFFSET $4`
// 		}

// 		if (!GetFundraising && WithSearch) || (GetFundraising && !WithSearch) {
// 			LimitSQL = ` LIMIT $2 OFFSET $3`
// 		}

// 		if (GetFundraising && WithSearch) || (!GetFundraising && !WithSearch) {
// 			LimitSQL = ` LIMIT $1 OFFSET $2`
// 		}

// 		// if WithSearch {
// 		// 	if GetFundraising || WithSearch
// 		// 	LimitSQL = ` LIMIT $3 OFFSET $4`
// 		// } else {
// 		// 	LimitSQL = ` LIMIT $2 OFFSET $3`
// 		// }
// 		Offset = search.Current*search.Onpage - search.Onpage
// 	}

// 	sql := `SELECT count(actions.uid) OVER() AS total_count,
// 	actions.uid,
// 	actions.senderuid, actions.maecenas, actions.owneruid, actions.title, actions.longtitle, actions.havevideo, actions.ticketprice / 100 as ticketprice,
// 	actions.description, actions.result, actions.needsumm, actions.donatedsumm, actions.donated, actions.totalwinners, actions.active, actions.status,
// 	actions.flagstart, actions.flagfinish, actions.criteriasumm,
// 	actions.criteriasdate, actions.datestart, actions.datefinish, actions.likes, images.uids,
// 	users.uid as maecenasuid,
// 	users.firstname || ' ' || users.lastname as maecenasname,
// 	COALESCE(users.instagram,'') as instagram, COALESCE(users.description,'') as mdescription, COALESCE(usersphotos.filename,'') as photoname,
// 	pics.imagelink,
// 	actions.minsumm, actions.currentsumm, actions.minbet, actions.isauction,
// 	COALESCE(links.link,'') as link,
// 	COALESCE(fund.uid,'00000000-0000-0000-0000-000000000000') as funduid, COALESCE(fund.title,'') as fundtitle,
// 	COALESCE(fund.description,'') as funddescription, fundimages.imagenames,
// 	COALESCE(sections.title,'') as sectiontitle,
// 	COALESCE(sections.uid,'00000000-0000-0000-0000-000000000000') as fundsowneruid,
// 	COALESCE(sections.description,'') as description
//   	FROM actions
// 	LEFT JOIN users ON users.uid::VARCHAR = actions.maecenas
// 	LEFT JOIN links ON links.action = actions.uid::VARCHAR
// 	LEFT JOIN usersphotos on usersphotos.usersuid::VARCHAR = actions.maecenas
// 	LEFT JOIN
// 	  (SELECT array_agg(imagelink) as imagelink, usersuid FROM socnet GROUP BY usersuid)
// 	as pics on pics.usersuid = actions.maecenas
// 	LEFT JOIN
// 	(select funduid, raisinguid, owneruid from crossfundsraising) as funds on funds.raisinguid = actions.owneruid
//   	LEFT JOIN fund on fund.uid::VARCHAR = funds.funduid
//   	LEFT JOIN (select array_remove(array_agg(imagename), NULL) as imagenames, owneruid FROM crossimages GROUP BY owneruid)
// as fundimages on fundimages.owneruid = fund.uid::VARCHAR
// 	LEFT JOIN sections on sections.uid::VARCHAR = funds.owneruid
// 	LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid)
// 	as images on images.owneruid = actions.uid::VARCHAR
// 	 ` + WhereSQL + SearchSQL + ` order by actions.date desc` + LimitSQL

// 	if GetFundraising || WithSearch {
// 		LimitSQL = ` LIMIT $3 OFFSET $4`
// 	}

// 	if (!GetFundraising && WithSearch) || (GetFundraising && !WithSearch) {
// 		LimitSQL = ` LIMIT $2 OFFSET $3`
// 	}

// 	if GetFundraising && WithSearch {
// 		LimitSQL = ` LIMIT $1 OFFSET $2`
// 	}

// 	if search.Paginator {

// 		if WithSearch && GetFundraising {
// 			rows, err = db.Query(sql, fundraisinguid, "%"+search.Search+"%", search.Onpage, Offset)
// 		}
// 		if !WithSearch && GetFundraising {

// 			rows, err = db.Query(sql, fundraisinguid, search.Onpage, Offset)
// 		}

// 		if WithSearch && !GetFundraising {
// 			rows, err = db.Query(sql, "%"+search.Search+"%", search.Onpage, Offset)
// 		}

// 		if !WithSearch && !GetFundraising {
// 			rows, err = db.Query(sql, search.Onpage, Offset)
// 		}
// 		//	rows, err = db.Query(sql, fundraisinguid, "%"+search.Search+"%", search.Onpage, Offset)
// 	}

// 	if !search.Paginator {

// 		if WithSearch && GetFundraising {
// 			rows, err = db.Query(sql, fundraisinguid, "%"+search.Search+"%")
// 		}
// 		if !WithSearch && GetFundraising {
// 			rows, err = db.Query(sql, fundraisinguid)
// 		}

// 		if WithSearch && !GetFundraising {
// 			rows, err = db.Query(sql, "%"+search.Search+"%")
// 		}

// 		if !WithSearch && !GetFundraising {
// 			rows, err = db.Query(sql)
// 		}
// 		// rows, err = db.Query(sql, fundraisinguid, "%"+search.Search+"%")

// 	}

// 	if err != nil && err != pgx.ErrNoRows {
// 		return list, 0, err
// 	}
// 	totalCount := 0
// 	for rows.Next() {
// 		item := &Actions{}
// 		var images, fundImages []string
// 		var maecenasImages []string
// 		maecenasProfilePhoto := ""

// 		donatedsumm := 0
// 		needsumm := 0
// 		var DateStart time.Time
// 		var DateFinish time.Time

// 		err = rows.Scan(&totalCount, &item.UID, &item.OwnerUID, &item.MaecenasUID, &item.FundraisingUID, &item.Title, &item.LongTitle, &item.HaveVideo, &item.TicketPrice,
// 			&item.Description, &item.Result, &needsumm, &donatedsumm, &item.Donated, &item.TotalWinners, &item.Flags.Active, &item.Status,
// 			&item.Flags.Flagstart, &item.Flags.Flagfinish, &item.Flags.Criteriasumm,
// 			&item.Flags.Criteriasdate, &DateStart, &DateFinish, &item.Likes,
// 			&images,
// 			&item.Maecenas.UID, &item.Maecenas.Name, &item.Maecenas.Instagram, &item.Maecenas.Description, &maecenasProfilePhoto, &maecenasImages,
// 			&item.Auction.MinSumm, &item.Auction.CurrentRate, &item.Auction.MinBet, &item.Auction.IsAuction, &item.ShortLink.Link,
// 			&item.Fund.UID, &item.Fund.Title, &item.Fund.Description, &fundImages,
// 			&item.Section.Title, &item.Section.UID, &item.Section.Description)

// 		if err != nil {

// 			return list, 0, err
// 		}

// 		// Path to video
// 		pathtovideo := filepath.Join(binpath.BinaryPath(), "images/video/actions")

// 		item.VideoURL = ""
// 		if item.HaveVideo {
// 			if _, err := os.Stat(filepath.Join(pathtovideo, item.UID+".mp4")); !os.IsNotExist(err) {
// 				item.VideoURL = picsURL + "video/actions/" + item.UID + ".mp4"
// 			}
// 		}

// 		// Formation of payment buttons
// 		item.Prices = GetPrices(item.Auction.IsAuction, item.TicketPrice, item.Auction.MinSumm, item.Auction.CurrentRate, item.Auction.MinBet)

// 		if len(item.ShortLink.Link) > 0 {
// 			item.ShortLink.URL = Domain + "/link/" + item.ShortLink.Link
// 		}

// 		item.Dates.DaysLeft = 0
// 		item.Dates.HoursLeft = 0
// 		if item.Flags.Criteriasdate {
// 			date := time.Now()
// 			duration := date.Sub(DateFinish)
// 			item.Dates.DaysLeft = 0 - int(duration.Hours()/24)
// 			if item.Dates.DaysLeft < 0 {
// 				item.Dates.DaysLeft = 0
// 			}
// 			if item.Dates.DaysLeft == 0 {
// 				item.Dates.HoursLeft = 0 - int(duration.Hours())
// 				if item.Dates.HoursLeft < 0 {
// 					item.Dates.HoursLeft = 0
// 				}
// 			}
// 		}

// 		item.ShortLink.URL = Domain + "/link/" + item.ShortLink.Link

// 		// Image of section
// 		pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")
// 		SectionFileExist := false
// 		if _, err := os.Stat(filepath.Join(pathtofile, item.Section.UID+".jpg")); !os.IsNotExist(err) {
// 			SectionFileExist = true
// 		}

// 		if SectionFileExist {
// 			item.Section.Image = picsURL + "sections/" + item.Section.UID + ".jpg"
// 		} else {
// 			item.Section.Image = picsURL + "nophoto.jpg"
// 		}

// 		// Get fund images
// 		if len(fundImages) > 0 {
// 			item.Fund.Image = picsURL + "fund/" + fundImages[0]
// 		} else {
// 			item.Fund.Image = picsURL + "nophoto.jpg"
// 		}

// 		//Get actual maecenas photo
// 		if maecenasProfilePhoto != "" {
// 			item.Maecenas.Photo = picsURL + string(models.ProfilesArea) + "/" + maecenasProfilePhoto
// 		} else {
// 			for _, pic := range maecenasImages {
// 				if len(pic) > 0 {
// 					item.Maecenas.Photo = pic
// 				}
// 			}
// 			if len(item.Maecenas.Photo) < 1 {
// 				item.Maecenas.Photo = picsURL + models.NoPhoto
// 			}
// 		}

// 		mySearch := prize.Search{}
// 		mySearch.Paginator = false
// 		item.Prizes, _, err = prize.Get(db, picsURL, item.UID, mySearch)
// 		if err != nil && err != pgx.ErrNoRows {
// 			return list, 0, err
// 		}
// 		item.Foruser.IsPaid = false
// 		if UID != "" {
// 			item.Foruser.IsPaid, err = IsPaidByUser(db, UID, item.UID)
// 			if err != nil && err != pgx.ErrNoRows {
// 				return list, 0, err
// 			}

// 			item.Foruser.IsWin, err = IsWinner(db, UID, item.UID)
// 			if err != nil && err != pgx.ErrNoRows {
// 				return list, 0, err
// 			}

// 			if item.Foruser.IsPaid {
// 				item.Foruser.TicketAmount, item.Foruser.TickenUID, err = GetTicket(db, UID, item.UID)
// 				if err != nil && err != pgx.ErrNoRows {
// 					return list, 0, err
// 				}
// 			}
// 		}

// 		if len(images) > 0 {
// 			listImage := make([]*models.Images, 0)
// 			for _, image := range images {
// 				pic := &models.Images{}
// 				pic.Image = picsURL + Area + "/" + image + ".jpg"
// 				pic.UID = image
// 				listImage = append(listImage, pic)
// 				// element is the element from someSlice for where we are
// 			}
// 			item.Images = listImage
// 		} else {
// 			listImage := make([]*models.Images, 0)
// 			pic := &models.Images{}
// 			pic.Image = picsURL + models.NoPhoto
// 			pic.UID = ""
// 			listImage = append(listImage, pic)
// 			item.Images = listImage
// 		}
// 		item.NeedSumm = float64(needsumm / 100)
// 		item.DonatedSumm = float64(donatedsumm / 100)
// 		item.Auction.CurrentRate = float64(item.Auction.CurrentRate / 100)
// 		item.Auction.MinBet = float64(item.Auction.MinBet / 100)
// 		item.Auction.MinSumm = float64(item.Auction.MinSumm / 100)
// 		item.Dates.DateStart = DateStart.Format("2006-01-02")
// 		item.Dates.DateFinish = DateFinish.Format("2006-01-02")
// 		item.Foruser.IsLiked, err = likes.IsLiked(db, item.UID, "Action", UID)

// 		if err != nil {
// 			item.Foruser.IsLiked = false
// 		}

// 		item.IsLiked = item.Foruser.IsLiked
// 		likes, err1 := likes.GetMyCount(db, item.UID, "Action")
// 		if err1 != nil {
// 			likes = 0
// 		}
// 		item.Likes = int64(likes)

// 		actusers := &models.ActusersAnswer{}
// 		actusers.HasNext = false
// 		pagin := &models.Paginator{}
// 		pagin.Search = ""

// 		actusers.Actusers, actusers.Total, err = Getlistusers(db, picsURL, item.UID, true, *pagin)
// 		if len(actusers.Actusers) < actusers.Total {
// 			actusers.HasNext = true
// 		}
// 		actusers.Count = len(actusers.Actusers)
// 		item.Users = *actusers

// 		list = append(list, item)
// 	}

// 	return list, totalCount, nil
// }

// IsPaidByUser - check for paid by user
func IsPaidByUser(db *pgx.ConnPool, usersUID string, actionsUID string) (bool, error) {
	isPaid := false
	err := db.QueryRow(`SELECT count(*)>0 as payd from tickets where usersuid = $1 and actionsuid = $2`, usersUID, actionsUID).Scan(&isPaid)
	return isPaid, err
}

// IsWinner - check for win
func IsWinner(db *pgx.ConnPool, usersUID string, actionsUID string) (bool, error) {
	isWin := false
	err := db.QueryRow(`SELECT count(uid)>0 from tickets where usersuid = $1 and actionsuid = $2 and iswin`, usersUID, actionsUID).Scan(&isWin)
	return isWin, err
}

// GetTicket - check For Ticket
func GetTicket(db *pgx.ConnPool, usersUID string, actionsUID string) (float64, string, error) {
	var ticketUID string
	var amount int64
	err := db.QueryRow(`SELECT amount, uid from tickets where usersuid = $1 and actionsuid = $2 order by AMOUNT DESC`, usersUID, actionsUID).Scan(&amount, &ticketUID)
	return float64(amount / 100), ticketUID, err
}

// Getlistusers - get list of users of actions
func Getlistusers(db *pgx.ConnPool, picsURL string, actionsUID string, short bool, paginator models.Paginator) ([]*models.Actusers, int, error) {
	var Offset int
	var err error
	var rows *pgx.Rows
	// WithSearch := false
	SearchSQL := ``
	LimitSQL := ``

	i := 2
	args := make([]interface{}, 0)
	args = append(args, actionsUID)

	switch short {
	case false:
		if paginator.Search != "" {
			SearchSQL = SearchSQL + fmt.Sprintf(` AND Name like $%d`, i)
			i++
			args = append(args, paginator.Search)
		}

		if paginator.HasPaginator {
			SearchSQL = SearchSQL + fmt.Sprintf(` LIMIT $%d OFFSET $%d`, i, i+1)
			Offset = paginator.CurrentPage*paginator.OnPage - paginator.OnPage
			args = append(args, paginator.OnPage, Offset)
		}

	default:
		LimitSQL = ` LIMIT 5 `
	}

	sql := `	
	SELECT count(users.uid) OVER() AS total_count, 
	COALESCE(users.uid,'00000000-0000-0000-0000-000000000000'), 
	COALESCE(users.firstname,'') || ' ' || 
	COALESCE(users.lastname,'') as Name,
	COALESCE(usersphotos.filename,'') as Photo, COALESCE(imagelink, '') as image,
	tickets.amount/100 as amount,
	tickets.date
	FROM tickets 
	LEFT JOIN users on users.uid::VARCHAR = tickets.usersuid
	LEFT JOIN usersphotos on usersphotos.usersuid = users.uid
	LEFT JOIN socnet on socnet.usersuid = users.uid::VARCHAR
	WHERE ((tickets.ispaid=true AND tickets.isauction=false)OR(tickets.isauction=true)) AND tickets.actionsuid = $1  ` + SearchSQL + `
	ORDER BY tickets.date DESC
	 ` + LimitSQL

	list := make([]*models.Actusers, 0)
	rows, err = db.Query(sql, args...)

	if err != nil && err != pgx.ErrNoRows {
		return list, 0, err
	}

	totalCount := 0
	for rows.Next() {

		item := &models.Actusers{}
		photo := ""
		image := ""
		var dat time.Time
		err = rows.Scan(&totalCount, &item.UID, &item.Name, &photo, &image, &item.Amount, &dat)

		if err != nil {
			return list, 0, err
		}

		item.Date = dat.Format("2006-01-02 15:04:05")

		item.Image = picsURL + string(models.ProfilesArea) + "/" + models.NoPhoto
		if len(photo) > 0 {
			item.Image = picsURL + string(models.ProfilesArea) + "/" + photo
		} else {
			if len(image) > 0 {
				item.Image = image
			}
		}

		list = append(list, item)

	}
	return list, totalCount, nil

}

// ActionCount - get count of actions for fundraising
func ActionCount(db *pgx.ConnPool, UID string) (count int64, err error) {
	err = db.QueryRow(`SELECT count(*) FROM actions WHERE actions.owneruid = $1 AND actions.active=true AND actions.flagfinish=false AND actions.flagstart=true`, UID).Scan(&count)
	return
}

package actions

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/likes"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/prize"
	"github.com/jackc/pgx"
)

//SQLString ...
type SQLString string

const (
	// SQL ...
	SQL = `SELECT count(actions.uid) OVER() AS total_count, 
	actions.uid,
	actions.senderuid, actions.maecenas, actions.owneruid, actions.title, actions.longtitle, actions.havevideo, actions.ticketprice / 100 as ticketprice,
	actions.description, actions.result, actions.needsumm, actions.donatedsumm, actions.donated, actions.totalwinners, actions.active, actions.status, 
	actions.flagstart, actions.flagfinish, actions.criteriasumm, 
	actions.criteriasdate, actions.date, actions.datestart, actions.datefinish, actions.likes, images.uids,
	users.uid as maecenasuid,
	users.firstname || ' ' || users.lastname as maecenasname,
	COALESCE(users.instagram,'') as instagram, COALESCE(users.description,'') as mdescription, COALESCE(usersphotos.filename,'') as photoname,
	pics.imagelink,
	actions.minsumm, actions.currentsumm, actions.minbet, actions.isauction,
	COALESCE(links.link,'') as link,
	COALESCE(fund.uid,'00000000-0000-0000-0000-000000000000') as funduid, COALESCE(fund.title,'') as fundtitle,
	COALESCE(fund.description,'') as funddescription, fundimages.imagenames,
	COALESCE(sections.title,'') as sectiontitle, 
	COALESCE(sections.uid,'00000000-0000-0000-0000-000000000000') as fundsowneruid,
	COALESCE(sections.description,'') as description,
	COALESCE(ticket.payd, false) as isPaid, COALESCE(ticketwin.iswin, false) as iswin,
	COALESCE(ticketinfo.amount, 0) / 100 as ticketAmount,
	COALESCE(ticketinfo.uid,'00000000-0000-0000-0000-000000000000') as ticketUID	
	%v 
  	FROM actions
	LEFT JOIN users ON users.uid::VARCHAR = actions.maecenas
	LEFT JOIN links ON links.action = actions.uid::VARCHAR 
	LEFT JOIN usersphotos on usersphotos.usersuid::VARCHAR = actions.maecenas
	LEFT JOIN 
	  (SELECT array_agg(imagelink) as imagelink, usersuid FROM socnet GROUP BY usersuid)
	as pics on pics.usersuid = actions.maecenas
	LEFT JOIN 
	(select funduid, raisinguid, owneruid from crossfundsraising) as funds on funds.raisinguid = actions.owneruid
  	LEFT JOIN fund on fund.uid::VARCHAR = funds.funduid
  	LEFT JOIN (select array_remove(array_agg(imagename), NULL) as imagenames, owneruid FROM crossimages GROUP BY owneruid) 
as fundimages on fundimages.owneruid = fund.uid::VARCHAR
	LEFT JOIN sections on sections.uid::VARCHAR = funds.owneruid
	LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
	as images on images.owneruid = actions.uid::VARCHAR
	%v %v 
	
GROUP BY actions.uid, actions.senderuid, actions.maecenas, actions.owneruid, actions.title, actions.longtitle, actions.havevideo,
actions.ticketprice, actions.description, actions.result, actions.needsumm, actions.donatedsumm, actions.donated, actions.totalwinners,
actions.active, actions.status, actions.flagstart, actions.flagfinish, actions.criteriasumm, actions.criteriasdate, actions.datestart,
actions.datefinish, actions.likes, images.uids, users.uid, users.firstname, users.lastname, users.instagram, users.description, usersphotos.filename, pics.imagelink,
actions.minsumm, actions.currentsumm, actions.minbet, actions.isauction, links.link, fund.uid, fund.title, fund.description, fundimages.imagenames,
sections.title, sections.uid, sections.description, ticket.payd, ticketwin.iswin, ticketinfo.amount, ticketinfo.uid, actions.date
	
	order by actions.date desc %v`

	MegaSQLAddon = `
	, array_to_json(ARRAY( SELECT  json_build_object('total', count(users.uid) OVER(), 
	   'uid', COALESCE(users.uid,'00000000-0000-0000-0000-000000000000'), 
	   'name', COALESCE(users.firstname,'') || ' ' || COALESCE(users.lastname,''),
	   'photo', COALESCE(usersphotos.filename,''), 
	 'link', COALESCE(imagelink, ''),
	   'amount', tickets.amount/100,
		'date', tickets.date)
	   FROM tickets 
	   LEFT JOIN users on users.uid::VARCHAR = tickets.usersuid
	   LEFT JOIN usersphotos on usersphotos.usersuid = users.uid
	   LEFT JOIN socnet on socnet.usersuid = users.uid::VARCHAR
	   WHERE ((tickets.ispaid=true AND tickets.isauction=false)OR(tickets.isauction=true)) AND tickets.actionsuid = actions.uid::VARCHAR 
	   ORDER BY tickets.date DESC  LIMIT 5)) as userslist,
   array_to_json(
   ARRAY (
   SELECT json_build_object(
	'total', count(prize.uid) OVER(), 
	   'uid', prize.uid,
	   'sender', prize.senderuid, 'maecenasuid', prize.maecenas, 'owneruid', prize.owneruid, 'title', prize.title, 'longtitle', prize.longtitle, 
	   'description', prize.description,'place', prize.place, 'imagesuids', images.uids, 'winneruid', COALESCE(winners.winneruid,''),
	   'winnername', COALESCE(users.firstname || ' ' || users.lastname,''),
	   'ticketuid', COALESCE(winners.ticketuid,''),
	   'ispaywaiting', COALESCE(winners.iswaiting, false),
	   'isrealwin', COALESCE(winners.iswin, false)) 
	   FROM prize
	   LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
	   as images on images.owneruid = prize.uid::VARCHAR
	   LEFT JOIN winners ON winners.ticketuid = prize.ticketuid
	   LEFT JOIN users ON users.uid::VARCHAR = winners.winneruid
	   WHERE prize.owneruid = actions.uid::VARCHAR order by prize.place asc
   )) as prize`
)

// Get -  get actions
func Get(db *pgx.ConnPool, picsURL string, fundraisinguid string, search Search, UID string, Domain string, TypeOfAction string) ([]*Actions, int, error) {
	list := make([]*Actions, 0)
	var err error
	var rows *pgx.Rows

	var SQLWhere SQLString
	var LimitSQL, JoinSQL string

	args := make([]interface{}, 0)
	i := 1

	if fundraisinguid != "" {
		SQLWhere = *SQLWhere.checkWhere(fmt.Sprintf(`actions.owneruid = $%d`, i))
		args = append(args, fundraisinguid)
		i++
	}

	if search.Search != "" {
		SQLWhere = *SQLWhere.checkWhere(fmt.Sprintf(`(actions.title like $%d OR users.firstname like $%d OR users.lastname like $%d)`, i, i, i))
		args = append(args, search.Search)
		i++
	}

	switch TypeOfAction {
	case "Auction":
		SQLWhere = *SQLWhere.checkWhere(`actions.isauction = true`)
	case "Action":
		SQLWhere = *SQLWhere.checkWhere(`actions.isauction = false`)
	default:
	}

	if search.OnlyActive && !search.NotActive {
		SQLWhere = *SQLWhere.checkWhere(`((actions.datefinish > NOW() AND actions.criteriasdate=true AND actions.active = false) OR (actions.active = true AND actions.flagstart = true AND actions.flagfinish = false))`)
	}

	if !search.OnlyActive && search.NotActive {
		SQLWhere = *SQLWhere.checkWhere(`((actions.datefinish <= NOW() AND actions.criteriasdate = true AND actions.active = false) OR (actions.active = false OR actions.flagstart = false OR actions.flagfinish = true))`)
	}

	if search.Paginator {
		LimitSQL = fmt.Sprintf(`LIMIT $%d OFFSET $%d`, i, i+1)
		args = append(args, search.Onpage, (search.Current*search.Onpage - search.Onpage))
		i = i + 2
	}

	join := fmt.Sprintf(`LEFT JOIN (SELECT count(*)>0 as payd, actionsuid from tickets WHERE tickets.usersuid = $%d GROUP BY actionsuid) as ticket on ticket.actionsuid = actions.uid::VARCHAR
	LEFT JOIN (SELECT count(uid)>0 as iswin, actionsuid from tickets where usersuid = $%d AND iswin GROUP BY actionsuid) as ticketwin on ticketwin.actionsuid = actions.uid::VARCHAR
	LEFT JOIN (SELECT amount, uid, actionsuid from tickets  where usersuid =  $%d ORDER BY amount DESC LIMIT 1) as ticketinfo on ticketinfo.actionsuid = actions.uid::VARCHAR`, i, i, i)
	i++
	args = append(args, UID)
	JoinSQL = fmt.Sprintf(`%v %v`, JoinSQL, join)

	rows, err = db.Query(fmt.Sprintf(SQL, MegaSQLAddon, JoinSQL, SQLWhere.String(), LimitSQL), args...)
	if err != nil && err != pgx.ErrNoRows {
		return list, 0, err
	}
	totalCount := 0

	type userMy struct {
		Total  int     `json:"total"`
		UID    string  `json:"uid"`
		Name   string  `json:"name"`
		Photo  string  `json:"photo"`
		Link   string  `json:"link"`
		Amount float64 `json:"amount"`
		Date   string  `json:"date"`
	}

	type prizeMy struct {
		Total        int      `json:"total"`
		UID          string   `json:"uid"`
		Sender       string   `json:"sender"`
		Maecenas     string   `json:"maecenasuid"`
		Owneruid     string   `json:"owneruid"`
		Title        string   `json:"title"`
		Longtitle    string   `json:"longTitle"`
		Description  string   `json:"description"`
		Place        int      `json:"place"`
		Imagesuids   []string `json:"imagesuids"`
		Winneruid    string   `json:"winneruid"`
		Winnername   string   `json:"winnername"`
		Ticketuid    string   `json:"ticketuid"`
		IsPayWaiting bool     `json:"ispaywaiting"`
		IsRealWin    bool     `json:"isrealwin"`
	}

	for rows.Next() {
		item := &Actions{}
		var images, fundImages []string
		var maecenasImages []string

		maecenasProfilePhoto := ""

		donatedsumm := 0
		needsumm := 0
		var DateStart time.Time
		var DateFinish time.Time
		var Date time.Time
		var MyUser []*userMy
		var MyPrize []*prizeMy
		err = rows.Scan(&totalCount, &item.UID, &item.OwnerUID, &item.MaecenasUID, &item.FundraisingUID, &item.Title, &item.LongTitle, &item.HaveVideo, &item.TicketPrice,
			&item.Description, &item.Result, &needsumm, &donatedsumm, &item.Donated, &item.TotalWinners, &item.Flags.Active, &item.Status,
			&item.Flags.Flagstart, &item.Flags.Flagfinish, &item.Flags.Criteriasumm,
			&item.Flags.Criteriasdate, &Date, &DateStart, &DateFinish, &item.Likes,
			&images,
			&item.Maecenas.UID, &item.Maecenas.Name, &item.Maecenas.Instagram, &item.Maecenas.Description, &maecenasProfilePhoto, &maecenasImages,
			&item.Auction.MinSumm, &item.Auction.CurrentRate, &item.Auction.MinBet, &item.Auction.IsAuction, &item.ShortLink.Link,
			&item.Fund.UID, &item.Fund.Title, &item.Fund.Description, &fundImages,
			&item.Section.Title, &item.Section.UID, &item.Section.Description, &item.Foruser.IsPaid, &item.Foruser.IsWin,
			&item.Foruser.TicketAmount, &item.Foruser.TickenUID, &MyUser, &MyPrize)

		if err != nil {
			return list, 0, err
		}
		// fmt.Println(&MyUser)
		// var actUsetsans models.ActusersAnswer
		// var actUsersArray []*models.Actusers

		for _, ut := range MyUser {
			var newUser models.Actusers
			newUser.Amount = ut.Amount
			yourDate, err := time.Parse(time.RFC3339, ut.Date)
			if err != nil {
				fmt.Println(err)

			}
			newUser.Date = yourDate.Format("2006-01-02 15:04:05")
			newUser.Image = fmt.Sprintf(`%vnophoto.jpg`, picsURL)
			if len(ut.Photo) > 0 || len(ut.Link) > 0 {
				switch len(ut.Photo) {
				case 0:
					newUser.Image = ut.Link
				default:
					newUser.Image = fmt.Sprintf(`%v%v/%v`, picsURL, string(models.ProfilesArea), ut.Photo)

				}
			}
			newUser.Name = ut.Name
			newUser.UID = ut.UID
			item.Users.Actusers = append(item.Users.Actusers, &newUser)
			item.Users.Total = ut.Total
		}
		item.Users.Count = len(item.Users.Actusers)
		if item.Users.Count == 0 {
			item.Users.Actusers = make([]*models.Actusers, 0)
		}
		if item.Users.Count < item.Users.Total {
			item.Users.HasNext = true
		}
		item.Users.HasNext = false

		for _, it := range MyPrize {
			var out prize.Prize
			out.Description = it.Description
			out.IsPayWait = it.IsPayWaiting
			out.LongTitle = it.Longtitle
			out.MaecenasUID = it.Maecenas
			out.OwnerUID = it.Owneruid
			out.Place = it.Place
			out.SenderUID = it.Sender
			out.TicketUID = it.Ticketuid
			out.Title = it.Title
			out.UID = it.UID
			out.WinnerName = it.Winnername
			out.WinnerUID = it.Winneruid
			out.IsRealWinner = it.IsRealWin
			var image []*models.Images
			for _, im := range it.Imagesuids {
				var imOut models.Images
				imOut.Image = fmt.Sprintf(`%vprize/%v.jpg`, picsURL, im)
				imOut.UID = im
				image = append(image, &imOut)
			}

			if len(image) == 0 {
				var imOut models.Images
				imOut.Image = fmt.Sprintf(`%vnophoto.jpg`, picsURL)
				imOut.UID = ""
				image = append(image, &imOut)
			}

			out.Images = image
			item.Prizes = append(item.Prizes, &out)
		}

		if len(item.Prizes) == 0 {
			item.Prizes = []*prize.Prize{}
		}

		if len(item.Users.Actusers) == 0 {
			item.Users = models.ActusersAnswer{}
			item.Users.Actusers = []*models.Actusers{}
		}

		// Path to video
		pathtovideo := filepath.Join(binpath.BinaryPath(), "images/video/actions")

		item.VideoURL = ""
		if item.HaveVideo {
			if _, err := os.Stat(filepath.Join(pathtovideo, item.UID+".mp4")); !os.IsNotExist(err) {
				item.VideoURL = picsURL + "video/actions/" + item.UID + ".mp4"
			}
		}

		// Formation of payment buttons
		item.Prices = GetPrices(item.Auction.IsAuction, item.TicketPrice, item.Auction.MinSumm, item.Auction.CurrentRate, item.Auction.MinBet)

		if len(item.ShortLink.Link) > 0 {
			item.ShortLink.URL = Domain + "/link/" + item.ShortLink.Link
		}

		item.Dates.DaysLeft = 0
		item.Dates.HoursLeft = 0
		if item.Flags.Criteriasdate {
			date := time.Now()
			duration := date.Sub(DateFinish)
			item.Dates.DaysLeft = 0 - int(duration.Hours()/24)
			if item.Dates.DaysLeft < 0 {
				item.Dates.DaysLeft = 0
			}
			if item.Dates.DaysLeft == 0 {
				item.Dates.HoursLeft = 0 - int(duration.Hours())
				if item.Dates.HoursLeft < 0 {
					item.Dates.HoursLeft = 0
				}
			}
		}

		item.ShortLink.URL = Domain + "/link/" + item.ShortLink.Link

		// Image of section
		pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")
		SectionFileExist := false
		if _, err := os.Stat(filepath.Join(pathtofile, item.Section.UID+".jpg")); !os.IsNotExist(err) {
			SectionFileExist = true
		}

		if SectionFileExist {
			item.Section.Image = picsURL + "sections/" + item.Section.UID + ".jpg"
		} else {
			item.Section.Image = picsURL + "nophoto.jpg"
		}

		// Get fund images
		if len(fundImages) > 0 {
			item.Fund.Image = picsURL + "fund/" + fundImages[0]
		} else {
			item.Fund.Image = picsURL + "nophoto.jpg"
		}

		//Get actual maecenas photo
		if maecenasProfilePhoto != "" {
			item.Maecenas.Photo = picsURL + string(models.ProfilesArea) + "/" + maecenasProfilePhoto
		} else {
			for _, pic := range maecenasImages {
				if len(pic) > 0 {
					item.Maecenas.Photo = pic
				}
			}
			if len(item.Maecenas.Photo) < 1 {
				item.Maecenas.Photo = picsURL + models.NoPhoto
			}
		}

		// mySearch := prize.Search{}
		// mySearch.Paginator = false
		// item.Prizes, _, err = prize.Get(db, picsURL, item.UID, mySearch)
		// if err != nil && err != pgx.ErrNoRows {
		// 	return list, 0, err
		// }

		if len(images) > 0 {
			listImage := make([]*models.Images, 0)
			for _, image := range images {
				pic := &models.Images{}
				pic.Image = picsURL + Area + "/" + image + ".jpg"
				pic.UID = image
				listImage = append(listImage, pic)
				// element is the element from someSlice for where we are
			}
			item.Images = listImage
		} else {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = picsURL + models.NoPhoto
			pic.UID = ""
			listImage = append(listImage, pic)
			item.Images = listImage
		}
		item.NeedSumm = float64(needsumm / 100)
		item.DonatedSumm = float64(donatedsumm / 100)
		item.Auction.CurrentRate = float64(item.Auction.CurrentRate / 100)
		item.Auction.MinBet = float64(item.Auction.MinBet / 100)
		item.Auction.MinSumm = float64(item.Auction.MinSumm / 100)
		item.Dates.DateStart = DateStart.Format("2006-01-02")
		item.Dates.DateFinish = DateFinish.Format("2006-01-02")
		item.Dates.Date = Date.Unix()
		item.Foruser.IsLiked, err = likes.IsLiked(db, item.UID, "Action", UID)

		if err != nil {
			item.Foruser.IsLiked = false
		}

		// item.IsLiked = item.Foruser.IsLiked
		// likes, err1 := likes.GetMyCount(db, item.UID, "Action")
		// if err1 != nil {
		// 	likes = 0
		// }
		// item.Likes = int64(likes)

		// actusers := &models.ActusersAnswer{}
		// actusers.HasNext = false
		// pagin := &models.Paginator{}
		// pagin.Search = ""

		// actusers.Actusers, actusers.Total, err = Getlistusers(db, picsURL, item.UID, true, *pagin)
		// if len(actusers.Actusers) < actusers.Total {
		// 	actusers.HasNext = true
		// }
		// actusers.Count = len(actusers.Actusers)
		// item.Users = *actusers

		list = append(list, item)
	}

	return list, totalCount, nil
}

func (s SQLString) String() string {
	return string(s)
}

func makeSQLstring(str string) *SQLString {
	s := SQLString(str)
	return &s
}

func (s *SQLString) checkWhere(params ...string) *SQLString {

	if len(*s) > 0 {
		if len(params) > 1 {
			return makeSQLstring(fmt.Sprintf(`%v %v %v`, *s, params[1], params[0]))
		}
		return makeSQLstring(fmt.Sprintf(`%v AND %v`, *s, params[0]))
	}
	return makeSQLstring(fmt.Sprintf(`WHERE %v`, params[0]))

}

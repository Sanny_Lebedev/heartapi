package sections

import (
	"os"
	"path/filepath"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"github.com/jackc/pgx"
)

// Section  - main structure
type Section struct {
	UID         string `json:"uid"`
	OwnerUID    string `json:"owneruid"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Step        string `json:"step"`
	DB          *pgx.ConnPool
	Log         logger.Logger
}

// List - list of sections
type List struct {
	UID         string `json:"uid"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Image       string `json:"image"`
	Step        string `json:"step"`
	Likes       int64  `json:"likes"`
}

// Answer - answer for API with list of sections
type Answer struct {
	List    []*List `json:"list"`
	Success bool    `json:"success"`
	Action  string  `json:"message"`
	Count   int     `json:"count"`
}

// New - returns new sections instance
func New(db *pgx.ConnPool, ctx *app.SenditemsSectionsContext, OwnerUID string, Log logger.Logger) *Section {
	sectionUID := ""
	step := "0"
	if ctx.Sectionuid != nil {
		sectionUID = *ctx.Sectionuid

	}

	if ctx.Step != nil {
		step = *ctx.Step
	}

	u := &Section{
		DB:          db,
		UID:         sectionUID,
		OwnerUID:    OwnerUID,
		Title:       *ctx.Title,
		Description: *ctx.Description,
		Step:        step,
		Log:         Log,
	}
	return u
}

func Delete(db *pgx.ConnPool, uid string) error {
	var err error
	_, err = db.Exec(`DELETE from sections WHERE uid = $1`, uid)
	return err
}

// Exist - check for exist of section
func Exist(db *pgx.ConnPool, uid string) bool {
	var exist bool
	err := db.QueryRow(`SELECT COUNT(uid)>0 from sections WHERE uid = $1`, uid).Scan(&exist)
	if err != nil || err == pgx.ErrNoRows {

		return false
	}
	return true
}

// Save - Checking for a similar entry
func (s *Section) Save() (string, error) {
	var newUID string
	var err error

	if s.UID == "" {
		newUID, err = s.Addnew()
		if err != nil {
			s.Log.Error().Err(err).Msg("Error adding section")
			return "", err
		}
		return newUID, nil
	}

	err = s.Edit()
	if err != nil {
		s.Log.Error().Err(err).Msg("Error adding section")
		return s.UID, err
	}

	return s.UID, nil
}

// Addnew - adding new section
func (s *Section) Addnew() (string, error) {
	var newUID string
	var err error

	err = s.DB.QueryRow(`INSERT INTO sections (owneruid, title, description, date, step) VALUES ($1, $2, $3, CURRENT_TIMESTAMP, $4) RETURNING uid`, s.OwnerUID, s.Title, s.Description, s.Step).Scan(&newUID)

	return newUID, err

}

// Edit - edit section
func (s *Section) Edit() error {
	sql := `UPDATE sections SET title = $1, description = $2, step=$4 WHERE uid = $3`
	_, err := s.DB.Exec(sql, s.Title, s.Description, s.UID, s.Step)
	return err
}

// FileExist - check for file
func FileExist(filename string) bool {

	pathtofile := filepath.Join(binpath.BinaryPath(), "images/sections")

	if _, err := os.Stat(filepath.Join(pathtofile, filename+".jpg")); !os.IsNotExist(err) {
		return true
	}
	return false

}

// Get - get list of items
func Get(db *pgx.ConnPool, picsURL string) ([]*List, error) {
	list := make([]*List, 0)
	sql := `SELECT uid, title, description, step, likes from sections order by step desc`
	rows, err := db.Query(sql)
	if err != nil && err != pgx.ErrNoRows {
		return list, err
	}

	for rows.Next() {
		item := &List{}
		err = rows.Scan(&item.UID, &item.Title, &item.Description, &item.Step, &item.Likes)
		if err != nil {
			return list, err
		}
		item.Image = picsURL
		if FileExist(item.UID) {
			item.Image = item.Image + "sections/" + item.UID + ".jpg"
		} else {
			item.Image = item.Image + "nophoto.jpg"
		}

		list = append(list, item)
	}

	return list, nil
}

package profile

import (
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

const (
	// NoPhoto
	NoPhoto = "nophoto.jpg"
)

type (
	// Main - main structure for fundraising
	Main struct {
		Profile   Profile
		db        *pgx.ConnPool
		log       logger.Logger
		ImagePath string
	}

	// Profile  - Profile structure
	Profile struct {
		UID         string      `json:"uid"`
		LastName    string      `json:"lastName"`
		FirstName   string      `json:"firstName"`
		Email       string      `json:"email"`
		Phone       string      `json:"phone"`
		Birthday    string      `json:"birthday"`
		Instagram   string      `json:"instagram"`
		Description string      `json:"description"`
		MainImage   string      `json:"mainImage"`
		Providers   []*Provider `json:"providers"`
		Roles       []*Roles    `json:"roles"`
		Passwords   Passwords   `json:"-"`
		Date        string      `json:"date"`
		IsVerify    bool        `json:"isVerified"`
		Push        bool        `json:"push"`
	}

	// Passwords - all passwords
	Passwords struct {
		Password    string
		Temppass    string
		Endtemppass *time.Time
	}

	// Provider - structure of actual providers
	Provider struct {
		Name      string     `json:"provider"`
		Email     string     `json:"email"`
		Imagelink string     `json:"imageLink"`
		Date      *time.Time `json:"date"`
		Token     string     `json:"-"`
	}

	// Roles - structure of user's roles
	Roles struct {
		Role   string `json:"role"`
		Status bool   `json:"active"`
	}

	// TotalMess ...
	TotalMess struct {
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}

	// Answer - answer for API with list of sections
	Answer struct {
		Profile Profile `json:"profile"`
		TotalMess
	}

	// AnswerRoles - answer for API with list of sections
	AnswerRoles struct {
		Roles []*Roles `json:"roles"`
		Count int      `json:"count"`
		TotalMess
	}
)

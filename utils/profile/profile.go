package profile

import (
	"fmt"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

// New - returns new sections instance
func New(db *pgx.ConnPool, Log logger.Logger, ImagePath string) *Main {

	u := &Main{
		db:        db,
		log:       Log,
		ImagePath: ImagePath,
	}
	return u
}

// Get - get profile information about user
func (m *Main) Get(uid string) (Profile, error) {
	var err error
	My := Profile{}
	var birthday *time.Time
	err = m.db.QueryRow(`SELECT users.firstname, users.lastname, users.phone, users.email, users.birthday, users.instagram, 
		COALESCE(users.description,'') as description,
		COALESCE(usersphotos.filename,'') as photo,
		COALESCE(mailverify.verify,false) as isverify,
		users.getpush
		FROM users 
		LEFT JOIN usersphotos on usersphotos.usersuid = users.uid
		LEFT JOIN mailverify on users.email = mailverify.email
		WHERE users.uid = $1 `, uid).Scan(&My.FirstName, &My.LastName, &My.Phone, &My.Email, &birthday, &My.Instagram, &My.Description, &My.MainImage, &My.IsVerify, &My.Push)
	if err != nil {
		return Profile{}, err
	}

	var errGetProvider error
	My.Providers, errGetProvider = m.GetProviders(uid)

	My.Roles, err = m.GetActiveRoles(uid)

	if len(My.MainImage) > 0 {
		My.MainImage = m.ImagePath + "profiles/" + My.MainImage
	} else {
		My.MainImage = m.ImagePath + "profiles/" + NoPhoto
		if errGetProvider == nil {
			for _, v := range My.Providers {
				if len(v.Imagelink) > 0 {
					My.MainImage = v.Imagelink
				}
			}
		}

	}
	My.UID = uid
	// My.Birthday = birthday.Format("02-01-2006")
	My.Birthday = birthday.Format("02.01.2006")
	if My.Birthday == "01.01.1900" {
		My.Birthday = ""
	}
	return My, err
}

// ChangePushStatus ...
func (m *Main) ChangePushStatus(UID string) error {
	_, err := m.db.Exec(`UPDATE users SET getpush = NOT getpush WHERE uid=$1`, UID)
	return err
}

// GetActiveRoles - get a list of active user's roles
func (m *Main) GetActiveRoles(uid string) ([]*Roles, error) {
	MyRoles := make([]*Roles, 0)
	rows, err := m.db.Query(`SELECT usersroletab.role, usersroletab.active from usersroletab WHERE usersroletab.uid = $1`, uid)
	if err != nil {
		return MyRoles, err
	}
	for rows.Next() {
		Pr := &Roles{}

		err = rows.Scan(&Pr.Role, &Pr.Status)
		if err != nil {
			m.log.Error().Err(err).Msg("Error with getting users roles")
			return MyRoles, err
		}

		MyRoles = append(MyRoles, Pr)

	}
	return MyRoles, nil
}

// GetProviders - get list of providers for current user
func (m *Main) GetProviders(uid string) ([]*Provider, error) {

	Providers := make([]*Provider, 0)
	rows, err := m.db.Query(`SELECT socnet.email, socnet.provider, socnet.imagelink, socnet.token, socnet.date from socnet WHERE socnet.usersuid = $1`, uid)
	if err != nil {
		return Providers, err
	}
	for rows.Next() {
		Pr := &Provider{}

		err = rows.Scan(&Pr.Email, &Pr.Name, &Pr.Imagelink, &Pr.Token, &Pr.Date)
		if err != nil {
			m.log.Error().Err(err).Msg("Error with getting users auth providers")
			return Providers, err
		}

		Providers = append(Providers, Pr)

	}
	return Providers, nil

}

// Put - save to profile
func (m *Main) Put(uid, FirstName, LastName, Phone, Birthday, Instagram, description string) error {

	if len(FirstName) == 0 && len(LastName) == 0 && len(Phone) == 0 && len(Birthday) == 0 && len(Instagram) == 0 && len(description) == 0 {
		return nil
	}

	SQL := `UPDATE users SET`
	SQLWHERE := ` WHERE uid=$1`
	args := make([]interface{}, 0)
	args = append(args, uid)
	i := 2

	if Birthday != "" {

		bd, err := time.Parse("02.01.2006", Birthday)
		if err != nil {
			bd, err = time.Parse("2006-01-02", Birthday)
			if err != nil {
				bd, err = time.Parse("02-01-2006", Birthday)
				if err != nil {
					m.log.Error().Err(err).Msg("Error with date parse")
					return err
				}
			}
		}

		SQL = SQL + fmt.Sprintf(` birthday=$%d`, i)
		i++
		args = append(args, bd)
	}

	if len(FirstName) > 0 {
		if i > 2 {
			SQL = SQL + `,`
		}
		SQL = SQL + fmt.Sprintf(` firstname=$%d`, i)
		i++
		args = append(args, FirstName)
	}

	if len(LastName) > 0 {
		if i > 2 {
			SQL = SQL + `,`
		}
		SQL = SQL + fmt.Sprintf(` lastname=$%d`, i)
		i++
		args = append(args, LastName)
	}

	if len(Phone) > 0 {
		if i > 2 {
			SQL = SQL + `,`
		}
		SQL = SQL + fmt.Sprintf(` phone=$%d`, i)
		i++
		args = append(args, Phone)
	}
	if len(Instagram) > 0 {
		if i > 2 {
			SQL = SQL + `,`
		}
		SQL = SQL + fmt.Sprintf(` instagram=$%d`, i)
		i++
		args = append(args, Instagram)
	}

	if len(description) > 0 {
		if i > 2 {
			SQL = SQL + `,`
		}
		SQL = SQL + fmt.Sprintf(` description=$%d`, i)
		i++
		args = append(args, description)
	}

	_, err := m.db.Exec(SQL+SQLWHERE, args...)

	// _, err = m.db.Exec(`UPDATE users SET firstname = $1, lastname = $2, phone = $3, birthday = $4, instagram = $5, description = $7 WHERE uid = $6`, FirstName, LastName, Phone, bd, Instagram, uid, description)
	return err
}

package pay

import (
	"regexp"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"github.com/jackc/pgx"
)

const (
	//Area - area name for os
	Area = "likes"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, userUID string, Log logger.Logger) *Main {
	return &Main{
		UID: userUID,
		db:  db,
		log: Log,
	}

}

// Newcard - set new cart for user
func (m *Main) Newcard() (string, string, error) {
	var err error
	var orderUID string
	orderUID, err = m.Neworder(minPayment)
	if err != nil {
		m.log.Error().Err(err).Msg("Error with sending new payment order")
		return "", "", err
	}

	shortOrder := MakeShortOrder(orderUID)

	err = m.Addcard(orderUID, shortOrder, minPayment)
	if err != nil {
		m.log.Error().Err(err).Msg("Error with adding new card for user")
	}

	return shortOrder, orderUID, err
}

//MakeShortOrder ...
func MakeShortOrder(order string) string {
	var re = regexp.MustCompile(`[[:punct:]]`)
	return re.ReplaceAllString(order, "")
}

// Addcard - add new card to DB
func (m *Main) Addcard(orderUID, shortOrder string, amount int64) error {

	_, err := m.db.Exec(`INSERT INTO cards (clientid, orderid, needrefund, amount, date, shortorder) VALUES 
	($1, $2, true, $3, CURRENT_TIMESTAMP, $4)`, m.UID, orderUID, amount, shortOrder)
	return err
}

// SavePay  - sending pay
func (m *Main) SavePay(Order PayRequest) error {
	_, err := m.db.Exec(`INSERT INTO pay (needrefund, savenewcard, amount, date, shortorder, bankorder, orderuid) 
	VALUES ($1, $2, $3, CURRENT_TIMESTAMP, $4, $5, $6 )`, Order.NeedRefund, Order.SaveNewCard, Order.Amount, Order.ShortOrder, Order.BankOrder, Order.OrderUID)
	return err
}

// Neworder - add new order to DB
func (m *Main) Neworder(amount int64) (string, error) {
	var orderUID string
	err := m.db.QueryRow(`INSERT INTO orders (usersuid, success, date, amount, lotteryuid) VALUES ($1, $2, CURRENT_TIMESTAMP, $3, $4) RETURNING uid`, m.UID, false, amount, "{}").Scan(&orderUID)
	return orderUID, err
}

package pay

import "fmt"

// Status is the low level order status
type Status uint

// nolint: golint
const (
	StatusRegisteredNotPaid = iota
	StatusHold
	StatusAuthorized
	StatusCanceled
	StatusRefund
	StatusAuthorizing
	StatusDeclined
	StatusUnknown
)

var statusText = map[Status]string{
	StatusRegisteredNotPaid: "Заказ зарегистрирован, но не оплачен",
	StatusHold:              "Предавторизованная сумма захолдирована",
	StatusAuthorized:        "Проведена полная авторизация суммы заказа",
	StatusCanceled:          "Авторизация отменена",
	StatusRefund:            "По транзакции была проведена операция возврата",
	StatusAuthorizing:       "Инициирована авторизация через ACS банка-эмитента",
	StatusDeclined:          "Авторизация отклонена",
	StatusUnknown:           "Неизвестный статус",
}

func (s Status) String() string {
	if text, ok := statusText[s]; ok {
		return text
	}
	return fmt.Sprintf("Status code %d", s)
}

// Error is order status error code
type Error uint

// nolint: golint
const (
	ErrorNoError = iota
	ErrorBadInput
	ErrorAccessDenied
	ErrorBadOrderID
	ErrorSystemError
	ErrorOrderID
	ErrorUnknownCurrency
)

var errorText = map[Error]string{
	ErrorNoError:         "Обработка запроса прошла без системных ошибок",
	ErrorBadInput:        "Заказ отклонен по причине ошибки в реквизитах платежа",
	ErrorAccessDenied:    "Доступ запрещён",
	ErrorBadOrderID:      "Неверный номер заказа",
	ErrorSystemError:     "Системная ошибка",
	ErrorOrderID:         "Заказ с таким номером уже обработан или неверный номер заказа",
	ErrorUnknownCurrency: "Неизвестная валюта",
}

func (e Error) String() string {
	if text, ok := errorText[e]; ok {
		return text
	}
	return fmt.Sprintf("Error code %d", e)
}

package sberbank

import (
	"encoding/json"
	"fmt"
)

// Status is the low level order status
type Status uint

// nolint: golint
const (
	StatusNoPay = iota
	StatusOnHold
	StatusFullAuthorization
	StatusAuthorizationCanceled
	StatusHeldReturn
	StatusInitACS
	StatusAuthorizationRejected
)

var statusText = map[Status]string{
	StatusNoPay:                 "Заказ зарегистрирован, но не оплачен",
	StatusOnHold:                "Предавторизованная сумма захолдирована",
	StatusFullAuthorization:     "Проведена полная авторизация суммы заказа",
	StatusAuthorizationCanceled: "Авторизация отменена",
	StatusHeldReturn:            "По транзакции была проведена операция возврата",
	StatusInitACS:               "Инициирована авторизация через ACS банка-эмитента",
	StatusAuthorizationRejected: "Авторизация отклонена",
}

func (s Status) String() string {
	if text, ok := statusText[s]; ok {
		return text
	}
	return fmt.Sprintf("Status code %d", s)
}

type (
	//PaymentOut ...
	PaymentOut struct {
		Status Status `json:"status"`
	}

	// CheckRespOut ...
	CheckRespOut struct {
		Status State `json:"error"`
	}

	// State - State structure for the answer
	State struct {
		Code    json.Number `json:"errorCode"`
		Message string      `json:"errorMessage"`
	}

	// Bindings - struct of binding cards
	Bindings struct {
		State
		Cards []*Binds `json:"bindings"`
	}

	//Binds - list of bind cards
	Binds struct {
		UID        string `json:"bindingId"`
		PAN        string `json:"maskedPan"`
		ExpiryDate string `json:"expiryDate"`
	}
)

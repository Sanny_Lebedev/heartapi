package pay

import (
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// GetCards - get cards list for user
func (m *Main) GetCards() ([]*Binds, error) {

	rows, err := m.db.Query(`SELECT bindingid, maskedpan, expirydate FROM cards WHERE clientid = $1 and bindingid!= ''`, m.UID)
	cards := make([]*Binds, 0)

	if err != nil && err != pgx.ErrNoRows {
		m.log.Error().Err(err).Msg("Error with getting card list for user")
		return cards, err
	}

	if err == pgx.ErrNoRows {
		return cards, nil
	}

	for rows.Next() {
		card := &Binds{}
		err = rows.Scan(&card.UID, &card.PAN, &card.ExpiryDate)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan query result")
		}
		cards = append(cards, card)
	}

	return cards, err

}

// DeleteCard - delete card from list for user
func (m *Main) DeleteCard(bindingid string) ([]*Binds, error) {
	_, err := m.db.Exec(`DELETE FROM cards WHERE bindingid = $1`, bindingid)
	if err != nil {
		m.log.Error().Err(err).Msg("Error with deleting card from list for user")
		return nil, err
	}

	cards, err := m.GetCards()
	return cards, err
}

//CleanCards - delete users cards
func (m *Main) CleanCards(clientid string) error {
	_, err := m.db.Exec(`DELETE FROM cards WHERE clientid = $1`, clientid)
	if err != nil {
		m.log.Error().Err(err).Msg("Error with deleting cards from users list")
	}
	return err
}

//AddCard - delete users cards
func (m *Main) AddCard(PAN, bindingID, clientID, exp string) error {
	_, err := m.db.Query(`INSERT INTO cards (clientid, bindingid, maskedpan, expirydate, amount, date, isrefund, ispaid) 
	VALUES ($1, $2, $3, $4, 100, CURRENT_TIMESTAMP, true, true)`, clientID, bindingID, PAN, exp)
	if err != nil {
		m.log.Error().Err(err).Msg("Error with inserting new card")
	}
	return err
}

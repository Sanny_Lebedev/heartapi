package pay

// OrderStatus is order status returned by payment gateway
type OrderStatus struct {
	Status       Status
	ErrorCode    Error
	ErrorMessage string
	OrderNumber  string
	PAN          string
	Expiration   uint
	CardHolder   string
	Amount       uint
	IP           string
	UserID       string
	BindingID    string
}

// OrderRequest is input order data for new order request
type OrderRequest struct {
	Amount             uint64
	OrderNumber        string
	ReturnURL          string
	FailURL            string
	PageView           PageView
	UserID             string
	BindingID          string
	Description        string
	AutoPayment        bool
	SessionTimeoutSecs int
}

// PageView is page view for payment gateway form
type PageView uint

// nolint
const (
	MobilePageView PageView = iota
	DesktopPageView
	BindingPageView
)

// OrderResponse is used in output of RegisterOrder call
type OrderResponse struct {
	OrderID      string
	FormURL      string
	Error        Error
	ErrorMessage string
}

// Gateway is a payment gateway interface
type Gateway interface {
	OrderStatus(ID string) (*OrderStatus, error)
	OrderPreAuth(OrderRequest) (*OrderResponse, error)
	Cancel(ID string) error
}

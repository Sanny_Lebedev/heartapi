package pay

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

const (
	minPayment = 100
)

type (
	// Main - main structure for fundraising
	Main struct {
		UID string
		db  *pgx.ConnPool
		log logger.Logger
	}

	// PayRequest ...
	PayRequest struct {
		NeedRefund  bool   `json:"needRefund"`
		SaveNewCard bool   `json:"saveNewCard"`
		Amount      int64  `json:"amount"`
		OrderUID    string `json:"orderUID"`
		BankOrder   string `json:"bankOrder"`
		ShortOrder  string `json:"shortOrder"`
	}

	// CheckPaymentResult ...
	CheckPaymentResult struct {
		ErrorCode             string      `json:"errorCode"`
		ErrorMessage          string      `json:"errorMessage"`
		OrderNumber           string      `json:"orderNumber"`
		OrderStatus           int         `json:"orderStatus"`
		ActionCode            int         `json:"actionCode"`
		ActionCodeDescription string      `json:"actionCodeDescription"`
		Amount                int64       `json:"amount"`
		Currency              string      `json:"currency"`
		Date                  int64       `json:"date"`
		OrderDescription      string      `json:"orderDescription"`
		Attributes            []PayAttr   `json:"attributes"`
		TerminalId            string      `json:"terminalId"`
		PaymentAmountInfo     PAmountInfo `json:"paymentAmountInfo"`
		BankInfo              BInfo       `json:"bankInfo"`
	}

	// PayAttr ...
	PayAttr struct {
		Name  string `json:"name"`
		Value string `json:"value"`
	}

	// PAmountInfo ...
	PAmountInfo struct {
		PaymentState    string `json:"paymentState"`
		ApprovedAmount  int64  `json:"approvedAmount"`
		DepositedAmount int64  `json:"depositedAmount"`
		RefundedAmount  int64  `json:"refundedAmount"`
	}

	// BInfo ...
	BInfo struct {
		BankCountryCode string `json:"bankCountryCode"`
		BankCountryName string `json:"bankCountryName"`
	}

	// AnswerURL - answer with link
	AnswerURL struct {
		FormUrl       string `json:"formUrl"`
		OrderId       string `json:"orderId"`
		Success       bool   `json:"success"`
		Message       string `json:"message"`
		EmailVerified bool   `json:"emailVerified"`
	}
	//Binds - list of bind cards
	Binds struct {
		UID        string `json:"bindingId"`
		PAN        string `json:"maskedPan"`
		ExpiryDate string `json:"expiryDate"`
	}

	//AnswerCards ...
	AnswerCards struct {
		Cards   []*Binds `json:"list"`
		Success bool     `json:"success"`
		Action  string   `json:"message"`
		Count   int      `json:"count,omitempty"`
	}
)

package prize

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crossimages"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

type (
	// Main - main structure for fundraising
	Main struct {
		Prize Prize
		ci    crossimages.Main
		db    *pgx.ConnPool
		log   logger.Logger
	}

	// Prize  - main structure
	Prize struct {
		UID          string           `json:"uid"`
		OwnerUID     string           `json:"ActionsUID"`
		TicketUID    string           `json:"TicketUID"`
		WinnerUID    string           `json:"winnerUID"`
		WinnerName   string           `json:"winnerName"`
		SenderUID    string           `json:"-"`
		MaecenasUID  string           `json:"maecenasUID"`
		Title        string           `json:"title"`
		LongTitle    string           `json:"longTitle"`
		Description  string           `json:"description"`
		Place        int              `json:"place"`
		Images       []*models.Images `json:"images"`
		IsPayWait    bool             `json:"isWaitForPay"`
		IsRealWinner bool             `json:"isRealWinner"`
	}

	// Search - structure for search and pagination
	Search struct {
		Search    string `json:"search"`
		Current   int    `json:"current"`
		Onpage    int    `json:"onPage"`
		Paginator bool   `json: "paginator"`
	}

	// Answer - answer for API with list of Prizes
	Answer struct {
		List       []*Prize `json:"list"`
		Success    bool     `json:"success"`
		Action     string   `json:"message"`
		Count      int      `json:"count,omitempty"`
		TotalCount int      `json:"totalCount,omitempty"`
		HasNext    bool     `json:"hasNext"`
	}

	// AnswerOne - answer for API with info about current Prize
	AnswerOne struct {
		Info    Prize  `json:"info"`
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}

	//AnswerWithUID - asnwer with UID of Prize
	AnswerWithUID struct {
		UID     string `json:"UID"`
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}
)

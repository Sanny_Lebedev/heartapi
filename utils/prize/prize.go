package prize

import (

	// "bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crossimages"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"

	"github.com/jackc/pgx"
)

const (
	//Area - area name for os
	Area = "prize"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, OwnerUID string, PrizeUID string, Log logger.Logger) *Main {

	u := &Main{
		db:  db,
		log: Log,
		ci:  *crossimages.Initial(db, Log, Area),
		Prize: Prize{
			UID:         PrizeUID,
			OwnerUID:    OwnerUID,
			MaecenasUID: OwnerUID,
		},
	}

	return u
}

// New - returns new sections instance
func New(db *pgx.ConnPool, ctx *app.SenditemsPrizeContext, SenderUID string, Log logger.Logger) *Main {
	uid := ""
	MaecenasUID := SenderUID
	if ctx.Payload.UID != nil {
		uid = *ctx.Payload.UID
	}

	if &ctx.Payload.MaecenasUID != nil {
		MaecenasUID = ctx.Payload.MaecenasUID
	}

	u := &Main{
		db:  db,
		log: Log,
		ci:  *crossimages.Initial(db, Log, Area),
		Prize: Prize{
			UID:         uid,
			OwnerUID:    ctx.Payload.ActionsUID,
			SenderUID:   SenderUID,
			MaecenasUID: MaecenasUID,
			Title:       ctx.Payload.Title,
			LongTitle:   *ctx.Payload.LongTitle,
			Description: *ctx.Payload.Description,
			Place:       *ctx.Payload.Place,
		},
	}

	return u
}

// Delete - delete prize from db
func Delete(db *pgx.ConnPool, log logger.Logger, uid string) error {
	var err error

	// delete photos of actions
	err = DeleteFRPhotos(db, log, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	_, err = db.Exec(`DELETE from prize WHERE uid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// DeleteFRPhotos - delete all photos of prizes from db
func DeleteFRPhotos(db *pgx.ConnPool, log logger.Logger, uid string) error {
	ci := crossimages.Initial(db, log, Area)
	return ci.DeleteFRPhotos(uid)
}

// DeleteFRPhotos - delete all photos of prizes from db
func (s *Main) DeleteFRPhotos(uid string) error {
	return s.ci.DeleteFRPhotos(uid)
}

// Exist - check for exist of prizes
func Exist(db *pgx.ConnPool, uid string) (bool, error) {
	var exist bool
	err := db.QueryRow(`SELECT COUNT(uid)>0 from prize WHERE uid = $1`, uid).Scan(&exist)
	if err != nil || err == pgx.ErrNoRows {
		return false, err
	}
	return exist, nil
}

// IsExist - check for exist
func (s *Main) IsExist() (bool, error) {
	isExist, err := Exist(s.db, s.Prize.UID)
	if err != nil {
		s.log.Error().Err(err).Msg("Error with checking action for exist")
	}
	return isExist, err
}

// DeleteImageByUID - deleting Image by image UID
func (s *Main) DeleteImageByUID(imageuid string) error {
	return s.ci.DeleteImageByUID(imageuid)
}

// SaveImage - saving image to DB
func (s *Main) SaveImage(senderuid, owneruid string) (models.Images, error) {
	img, err := s.ci.SaveImage(senderuid, owneruid)
	return img, err
}

//PrizeCheck - check Prize for exist
func (s *Main) PrizeCheck() (bool, error) {
	var ans bool
	var err error
	ans, err = Exist(s.db, s.Prize.UID)

	if err != nil && err != pgx.ErrNoRows {
		s.log.Error().Err(err).Msg("Error with checking exists of action")
		return false, err
	}
	return ans, nil

}

//Save - saving actions data
func (s *Main) Save() (string, error) {

	if s.Prize.UID == "" {
		return s.Insert()
	}

	isExist, err := s.IsExist()
	if err != nil && err != pgx.ErrNoRows {
		return "", err
	}

	if !isExist {
		return s.Insert()
	}

	return s.Update()
}

//Update - updating information abour prize
func (s *Main) Update() (string, error) {
	var err error

	_, err = s.db.Exec(`UPDATE prize SET 
	maecenas = $1, title = $2, longtitle = $3, description = $4, place=$5,  date = CURRENT_TIMESTAMP WHERE uid = $6`, s.Prize.MaecenasUID,
		s.Prize.Title, s.Prize.LongTitle, s.Prize.Description, s.Prize.Place, s.Prize.UID)
	if err != nil {
		s.log.Error().Err(err).Msg("Error with update actions DB")
	}

	return s.Prize.UID, nil
}

// Insert - insert actions data
func (s *Main) Insert() (string, error) {
	uid := ""

	err := s.db.QueryRow(`INSERT INTO prize (senderuid, maecenas, owneruid, ownertype, title, longtitle, 
	description, place, date) VALUES ($1, $2, $3, $4, $5, $6,
	$7,$8, CURRENT_TIMESTAMP) RETURNING uid`, s.Prize.SenderUID, s.Prize.MaecenasUID, s.Prize.OwnerUID,
		models.ActionsArea, s.Prize.Title, s.Prize.LongTitle,
		s.Prize.Description, s.Prize.Place).Scan(&uid)

	if err != nil {
		s.log.Error().Err(err).Msg("Error with inserting new prize to DB")
	}

	return uid, err
}

// CheckSamePlace - checking for existing prize for place
func (s *Main) CheckSamePlace() (bool, error) {
	isOk := false
	err := s.db.QueryRow(`SELECT COUNT(uid)>0 from prize WHERE owneruid = $1 AND maecenas = $2 AND place = $3`, s.Prize.OwnerUID, s.Prize.MaecenasUID, s.Prize.Place).Scan(&isOk)
	return isOk, err
}

// CheckSender - check for sender
func (s *Main) CheckSender() (bool, error) {
	isOk := false
	err := s.db.QueryRow(`SELECT COUNT(uid)>0 from prize WHERE uid = $1 AND maecenas = $2 `, s.Prize.UID, s.Prize.OwnerUID).Scan(&isOk)
	return isOk, err
}

//GetPhotoOwner - get owner of Photo by photouid
func GetPhotoOwner(db *pgx.ConnPool, photouid string) (string, error) {
	var maecenas string
	err := db.QueryRow(`SELECT prize.maecenas FROM crossimages LEFT JOIN prize on prize.uid::VARCHAR = crossimages.owneruid
	WHERE crossimages.ownertype = $1 AND crossimages.uid = $2`, Area, photouid).Scan(&maecenas)
	return maecenas, err
}

// Getone - get information about current fundraising
func Getone(db *pgx.ConnPool, picsURL string, uid string) (Prize, error) {
	item := &Prize{}

	sql := `SELECT 
	prize.uid,
	prize.senderuid, prize.maecenas, prize.owneruid, prize.title, prize.longtitle, 
	prize.description, prize.place, images.uids,  COALESCE(winners.winneruid,'') as winnerUID,
	COALESCE(users.firstname || ' ' || users.lastname,'') as winnerName
	FROM prize
	LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
	as images on images.owneruid = prize.uid::VARCHAR
	LEFT JOIN winners ON winners.ticketuid = prize.ticketuid
	LEFT JOIN users ON users.uid::VARCHAR = winners.winneruid
	WHERE prize.uid = $1`
	var images []string
	err := db.QueryRow(sql, uid).Scan(&item.UID, &item.SenderUID, &item.MaecenasUID, &item.OwnerUID, &item.Title, &item.LongTitle,
		&item.Description, &item.Place, &images, &item.WinnerUID, &item.WinnerName)

	if err != nil && err != pgx.ErrNoRows {
		return *item, err
	}

	if len(images) > 0 {
		listImage := make([]*models.Images, 0)
		for _, image := range images {
			pic := &models.Images{}
			pic.Image = picsURL + Area + "/" + image + ".jpg"
			pic.UID = image
			listImage = append(listImage, pic)
			// element is the element from someSlice for where we are
		}
		item.Images = listImage
	}

	return *item, nil
}

// Get - get list of items
func Get(db *pgx.ConnPool, picsURL string, ActionsUID string, search Search) ([]*Prize, int, error) {
	list := make([]*Prize, 0)

	var Offset int
	var err error
	var rows *pgx.Rows

	WithSearch := false
	SearchSQL := ` `
	LimitSQL := ` `

	if search.Search != "" {

		WithSearch = true
		SearchSQL = ` AND prize.title like $2 `
	}

	if search.Paginator {
		if WithSearch {
			LimitSQL = ` LIMIT $3 OFFSET $4`
		} else {
			LimitSQL = ` LIMIT $2 OFFSET $3`
		}
		Offset = search.Current*search.Onpage - search.Onpage
	}

	sql := `SELECT count(prize.uid) OVER() AS total_count, 
	prize.uid,
	prize.senderuid, prize.maecenas, prize.owneruid, prize.title, prize.longtitle, 
	prize.description, prize.place, images.uids, COALESCE(winners.winneruid,'') as winnerUID,
	COALESCE(users.firstname || ' ' || users.lastname,'') as winnerName,
	COALESCE(winners.ticketuid,'') as ticketuid,
	COALESCE(winners.iswaiting, false) as ispaywaiting,
	COALESCE(winners.iswin, false) as isrealwin
	FROM prize
	LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
	as images on images.owneruid = prize.uid::VARCHAR
	LEFT JOIN winners ON winners.ticketuid = prize.ticketuid
	LEFT JOIN users ON users.uid::VARCHAR = winners.winneruid
	WHERE prize.owneruid = $1 ` + SearchSQL + ` order by place asc` + LimitSQL

	if WithSearch && search.Paginator {
		rows, err = db.Query(sql, ActionsUID, "%"+search.Search+"%", search.Onpage, Offset)
	}

	if WithSearch && !search.Paginator {
		rows, err = db.Query(sql, ActionsUID, "%"+search.Search+"%")

	}

	if !WithSearch && !search.Paginator {
		rows, err = db.Query(sql, ActionsUID)

	}

	if !WithSearch && search.Paginator {
		rows, err = db.Query(sql, ActionsUID, search.Onpage, Offset)
	}

	if err != nil && err != pgx.ErrNoRows {
		return list, 0, err
	}
	totalCount := 0
	for rows.Next() {
		item := &Prize{}
		var images []string

		err = rows.Scan(&totalCount, &item.UID, &item.SenderUID, &item.MaecenasUID, &item.OwnerUID, &item.Title, &item.LongTitle,
			&item.Description, &item.Place, &images, &item.WinnerUID, &item.WinnerName, &item.TicketUID, &item.IsPayWait, &item.IsRealWinner)

		if err != nil {

			return list, 0, err
		}

		if len(images) > 0 {
			listImage := make([]*models.Images, 0)
			for _, image := range images {
				pic := &models.Images{}
				pic.Image = picsURL + Area + "/" + image + ".jpg"
				pic.UID = image
				listImage = append(listImage, pic)
				// element is the element from someSlice for where we are
			}

			item.Images = listImage
		} else {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = picsURL + models.NoPhoto
			listImage = append(listImage, pic)
			item.Images = listImage
		}
		list = append(list, item)
	}

	return list, totalCount, nil
}

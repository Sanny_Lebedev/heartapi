package bank

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

// Get - get items
func (m *Main) GetReport(search ReportSearch) ([]*ReportItem, ReportSum, error) {
	var WithSearch, WithSearchType bool
	var err error
	var rows *pgx.Rows
	var SearchSQL, LimitSQL, typeSearch string
	var Offset int
	WithSearch = false

	LimitSQL = ``
	SearchSQL = ``

	typeSearch = ``
	WithSearchType = true
	switch search.Type {
	case "fundraising":
		if search.Search != "" {
			typeSearch = ` AND orders.fundraisinguid = $4`
		} else {
			typeSearch = ` AND orders.fundraisinguid = $3`
		}

	case "fund":
		if search.Search != "" {
			typeSearch = ` AND fund.uid = $4`
		} else {
			typeSearch = ` AND fund.uid = $3`
		}

	case "action":
		if search.Search != "" {
			typeSearch = ` AND orders.actionuid = $4`
		} else {
			typeSearch = ` AND orders.actionuid = $3`
		}
	default:
		WithSearchType = false
	}

	if search.Success {
		SearchSQL = SearchSQL + ` AND orders.success`
	} else {
		if search.Failed {
			SearchSQL = SearchSQL + ` AND orders.success = false`
		}
	}

	if search.Search != "" {
		WithSearch = true
		SearchSQL = SearchSQL + ` AND (COALESCE(users.phone,'') like $3 OR COALESCE(users.firstname,'') like $3 
		OR users.lastname like $3 OR COALESCE(users.email,'') like $3 OR COALESCE(actions.title,'') like $3 
		OR COALESCE(tickets.uid,'00000000-0000-0000-0000-000000000000') like $3 OR fundraising.title like $3)`
	}

	if search.Paginator {
		LimitSQL = ` LIMIT $1 OFFSET $2`
		Offset = search.Current*search.Onpage - search.Onpage
	}

	if search.Start != nil {
		if WithSearchType && WithSearch {
			SearchSQL = SearchSQL + ` AND orders.date between $5 and $6`
		}

		if (!WithSearchType && WithSearch) || (WithSearchType && !WithSearch) {
			SearchSQL = SearchSQL + ` AND orders.date between $4 and $5`
		}

		if !WithSearchType && !WithSearch {
			SearchSQL = SearchSQL + ` AND orders.date between $3 and $4`
		}
	}

	sql := `SELECT  count(orders.uid) OVER() AS total_count,
	COALESCE(SUM(orders.amount) filter (where orders.success) OVER(),0) as successamount,
	 COALESCE(SUM(orders.amount) filter (where orders.success = false) OVER(),0) as notsuccessamount,
	   COALESCE(users.firstname,'') || ' ' || COALESCE(users.lastname,'') as name,
	   COALESCE(users.uid, '00000000-0000-0000-0000-000000000000') as usersuid,
	   COALESCE(users.phone, '') as phone, COALESCE(users.email,'') as email,
	   COALESCE(maecenas.firstname,'') || ' ' || COALESCE(maecenas.lastname,'') as maecenasname,
	   COALESCE(maecenas.uid, '00000000-0000-0000-0000-000000000000') as maecenasuid,
	   COALESCE(maecenas.phone, '') as maecenasphone, COALESCE(maecenas.email,'') as maecenasemail,
	   COALESCE(tickets.uid,'00000000-0000-0000-0000-000000000000') as ticketuid, 
	   COALESCE(banklog.message,'') as bankmessage, COALESCE(banklog.code,0) as bankcode, COALESCE(banklog.bankorder,'') as bankorder,
	   COALESCE(banklog.date,now()) as datecheck,
	   orders.amount, orders.success, orders.date,
	   COALESCE(tickets.ispaid,false) as ispaid, COALESCE(tickets.iswin, false) as iswin,
	   COALESCE(actions.uid,'00000000-0000-0000-0000-000000000000') as actionsuid, 
	   COALESCE(actions.isauction, false) as isauction, COALESCE(actions.description,'') as actiondescription, 
	   COALESCE(actions.title,'') as actiontitle,
	   orders.fundraisinguid,
	   COALESCE(fundraising.title,'') as fundraisingtitle, 
	   COALESCE(fundraising.description,'') as fundraisingdescription,
	   fund.uid, fund.title, fundimage.imagename
	   FROM orders 
	   LEFT JOIN banklog on banklog.orderuid = orders.uid::VARCHAR
	   LEFT JOIN tickets on tickets.orderuid = orders.uid::VARCHAR
	   LEFT JOIN actions on actions.uid::VARCHAR = orders.actionuid
	   LEFT JOIN fundraising on fundraising.uid::VARCHAR = orders.fundraisinguid
	   LEFT JOIN crossfundsraising ON crossfundsraising.raisinguid = fundraising.uid::VARCHAR
	   LEFT JOIN fund on fund.uid::VARCHAR = crossfundsraising.funduid
	   LEFT JOIN users as maecenas ON maecenas.uid::VARCHAR = actions.maecenas
	   LEFT JOIN users on users.uid::VARCHAR = orders.usersuid
	   LEFT JOIN crossimages as fundimage on fundimage.owneruid = fund.uid::VARCHAR 
	   WHERE fundraisinguid != ''` + typeSearch + SearchSQL + `
	   GROUP BY orders.uid, users.firstname, users.lastname, users.uid, users.phone,
   users.email, maecenas.firstname, maecenas.lastname, maecenas.uid, maecenas.phone, maecenas.email,
   tickets.uid, banklog.message, banklog.code, banklog.bankorder, banklog.date, orders.amount, orders.success, tickets.ispaid,
   tickets.iswin, actions.uid, actions.isauction, actions.description, actions.title, orders.fundraisinguid, fundraising.title,
   fundraising.description, fund.uid, fund.title, fundimage.imagename, orders.date
   ORDER BY orders.date DESC` + LimitSQL

	if search.Start == nil {

		switch WithSearch {
		case true:
			if search.OwnerUID != "" {
				rows, err = m.db.Query(sql, search.Onpage, Offset, "%"+search.Search+"%", search.OwnerUID)
			} else {
				rows, err = m.db.Query(sql, search.Onpage, Offset, "%"+search.Search+"%")
			}

		default:
			if search.OwnerUID != "" {
				rows, err = m.db.Query(sql, search.Onpage, Offset, search.OwnerUID)
			} else {
				rows, err = m.db.Query(sql, search.Onpage, Offset)
			}
		}
	} else {
		switch WithSearch {
		case true:
			if search.OwnerUID != "" {
				rows, err = m.db.Query(sql, search.Onpage, Offset, "%"+search.Search+"%", search.OwnerUID, search.Start, search.Finish)
			} else {
				rows, err = m.db.Query(sql, search.Onpage, Offset, "%"+search.Search+"%", search.Start, search.Finish)
			}

		default:
			if search.OwnerUID != "" {

				rows, err = m.db.Query(sql, search.Onpage, Offset, search.OwnerUID, search.Start, search.Finish)
			} else {
				rows, err = m.db.Query(sql, search.Onpage, Offset, search.Start, search.Finish)
			}
		}
	}
	sums := ReportSum{}
	Items := make([]*ReportItem, 0)
	if err != nil && err != pgx.ErrNoRows {
		m.log.Error().Err(err).Msg("Error with SQL")
		return Items, sums, err
	}

	totalCount := 0
	summFailed := 0
	summSuccess := 0
	for rows.Next() {
		item := &ReportItem{}
		var fundimage string

		err = rows.Scan(&totalCount, &summSuccess, &summFailed,
			&item.Client.Name, &item.Client.UID, &item.Client.Phone, &item.Client.Email,
			&item.Maecenas.Name, &item.Maecenas.UID, &item.Maecenas.Phone, &item.Maecenas.Email,
			&item.Ticket.UID,
			&item.Bank.Message, &item.Bank.Code, &item.Bank.BankOrder, &item.Bank.Date,
			&item.Ticket.Amount, &item.Flags.Success,
			&item.OrderDate,
			&item.Flags.IsPaid, &item.Flags.IsWin,
			&item.Action.UID,
			&item.Flags.IsAuction,
			&item.Action.Description, &item.Action.Title,
			&item.Fundraising.UID, &item.Fundraising.Title, &item.Fundraising.Description,
			&item.Fund.UID, &item.Fund.Title, &fundimage)

		if err != nil {
			m.log.Error().Err(err).Msg("Error with scaning ...")
			return Items, sums, err
		}

		item.Fund.Image = m.ImagePath + models.NoPhoto

		if len(fundimage) > 0 {
			item.Fund.Image = m.ImagePath + string(models.FundsArea) + "/" + fundimage
		}

		Items = append(Items, item)

	}
	sums.FailedSum = float64(summFailed / 100)
	sums.SuccessSum = float64(summSuccess / 100)
	sums.TotalCount = totalCount

	return Items, sums, err
}

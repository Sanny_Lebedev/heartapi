package bank

import (
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"github.com/jackc/pgx"
)

const (
	// Fundraising ...
	Fundraising = "Fundraising"
	// Action ...
	Action = "Action"
	// User ...
	User = "User"
)

type (
	// Main - main structure for fundraising
	Main struct {
		db        *pgx.ConnPool
		log       logger.Logger
		ImagePath string
	}

	// Search - structure for search and pagination
	Search struct {
		Search    string `json:"search"`
		Current   int    `json:"current"`
		Onpage    int    `json:"onPage"`
		Paginator bool   `json:"paginator"`
		OnlyPaid  bool   `json:"onlyPaid"`
		OnlyWrong bool   `json:"onlyWrong"`
		OwnerType string `json:"ownerType"`
		OwnerUID  string `json:"ownerUID"`
	}

	// ReportSearch - structure for search in all report
	ReportSearch struct {
		OwnerUID  string     `json:"ownerUID"`
		Type      string     `json:"ownerType"`
		Search    string     `json:"search"`
		Current   int        `json:"current"`
		Onpage    int        `json:"onPage"`
		Paginator bool       `json:"paginator"`
		Failed    bool       `json:"failed"`
		Success   bool       `json:"success"`
		Start     *time.Time `json:"start"`
		Finish    *time.Time `json:"finish"`
	}

	// ReportItem - structure for the report

	// Item - main items structure
	Item struct {
		Client   Client   `json:"user"`
		Maecenas Maecenas `json:"maecenas"`
		Ticket   Ticket   `json:"ticket"`
		Bank     Bank     `json:"bank"`
		Owner    Owner    `json:"owner"`
		Flags    Flags    `json:"flags"`
	}

	// ReportItem - struct for report answer
	ReportItem struct {
		Client      Client       `json:"user"`
		Maecenas    Maecenas     `json:"maecenas"`
		Ticket      Ticket       `json:"ticket"`
		Bank        Bank         `json:"bank"`
		Flags       Flags        `json:"flags"`
		Action      Actions      `json:"action"`
		Fundraising Fundraisings `json:"fundraising"`
		Fund        Fund         `json:"fund"`
		OrderDate   *time.Time   `json:"orderDate"`
	}

	// ReportSum ...
	ReportSum struct {
		SuccessSum float64 `json:"successSum"`
		FailedSum  float64 `json:"failesSum"`
		TotalCount int     `json:"total"`
	}

	// Actions - struct of actions
	Actions struct {
		UID         string `json:"UID"`
		IsAuction   bool   `json:"isAuction"`
		Title       string `json:"title"`
		Description string `json:"description"`
	}

	// Fundraisings - struct of fundraising information
	Fundraisings struct {
		UID         string `json:"UID"`
		Title       string `json:"title"`
		Description string `json:"description"`
	}

	// Fund - struct of fund information
	Fund struct {
		UID   string `json:"UID"`
		Title string `json:"title"`
		Image string `json:"image"`
	}

	// Owner - structure of owner
	Owner struct {
		Title          string `json:"title"`
		LongTitle      string `json:"longTitle"`
		Description    string `json:"description"`
		FundraisingUID string `json:"fundraisingUID"`
	}

	// Bank - structure from bank request
	Bank struct {
		Message   string     `json:"message"`
		Code      int        `json:"code"`
		BankOrder string     `json:"bankOrder"`
		Date      *time.Time `json:"date"`
	}

	// Flags - flags structure
	Flags struct {
		IsPaid    bool `json:"isPaid"`
		IsWin     bool `json:"isWin"`
		IsAuction bool `json:"isAuction"`
		Success   bool `json:"success"`
	}

	// Ticket - ticket structure
	Ticket struct {
		UID         string `json:"UID"`
		OrderAmount uint   `json:"orderAmount"`
		Amount      uint   `json:"amount"`
	}

	// Client - clients structure
	Client struct {
		UID    string `json:"UID"`
		Name   string `json:"name"`
		Phone  string `json:"phone"`
		Email  string `json:"email"`
		Photo  string `json:"photo,omitempty"`
		Socnet string `json:"socnet,omitempty"`
	}

	// Maecenas - maecenas structure
	Maecenas struct {
		UID   string `json:"UID"`
		Name  string `json:"name"`
		Phone string `json:"phone"`
		Email string `json:"email,omitempty"`
		Photo string `json:"photo,omitempty"`
	}

	// Answer - structure of answer
	Answer struct {
		List       []*Item `json:"list"`
		Success    bool    `json:"success"`
		Action     string  `json:"message"`
		Count      int     `json:"count,omitempty"`
		TotalCount int     `json:"totalCount,omitempty"`
		HasNext    bool    `json:"hasNext"`
	}

	// ReportAnswer - struct of report anser
	ReportAnswer struct {
		List         []*ReportItem `json:"list"`
		TotalAmounts ReportSum     `json:"totalAmounts"`
		Success      bool          `json:"success"`
		Action       string        `json:"message"`
		Count        int           `json:"count,omitempty"`
		TotalCount   int           `json:"totalCount,omitempty"`
		HasNext      bool          `json:"hasNext"`
	}
)

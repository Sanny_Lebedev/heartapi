package bank

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, Log logger.Logger, Image string) *Main {

	u := &Main{
		db:        db,
		log:       Log,
		ImagePath: Image,
	}

	return u
}

// Get - get items
func (m *Main) Get(search Search) ([]*Item, int, error) {
	var WithSearch bool
	var err error
	var rows *pgx.Rows
	var SearchSQL, LimitSQL string
	var Offset int
	WithSearch = false

	LimitSQL = ``
	SearchSQL = ``

	if search.OnlyPaid {
		SearchSQL = SearchSQL + ` AND COALESCE(banklog.code,0) = 2`
	} else {
		if search.OnlyWrong {
			SearchSQL = SearchSQL + ` AND COALESCE(banklog.code,0) != 2`
		}
	}

	if search.Search != "" {
		WithSearch = true
		SearchSQL = SearchSQL + ` AND (users.phone like $2 OR users.firstname like $2 OR users.lastname like $2 OR users.email like $2 OR actions.title like $2 OR
			tickets.orderuid like $2)`
	}

	if search.Paginator {
		// Set with paginator
		switch WithSearch {
		case true:
			LimitSQL = ` LIMIT $3 OFFSET $4`
		default:
			LimitSQL = ` LIMIT $2 OFFSET $3`
		}
		Offset = search.Current*search.Onpage - search.Onpage
	}

	switch search.OwnerType {
	case Action:
		SearchSQL = `orders.actionuid = $1 ` + SearchSQL

	default:
		SearchSQL = `orders.fundraisinguid = $1 ` + SearchSQL
	}

	sql := `SELECT  count(banklog.uid) OVER() AS total_count,
	COALESCE(users.firstname,'') || ' ' || COALESCE(users.lastname,'') as name,
	COALESCE(users.uid, '00000000-0000-0000-0000-000000000000') as usersuid,
	COALESCE(users.phone, '') as phone, COALESCE(users.email,'') as email,
	COALESCE(usersphotos.filename,'') as photofile,
	COALESCE(socnet.imagelink,'') as imageLink, COALESCE(socnet.provider,'') as socnet,
	COALESCE(maecenas.firstname,'') || ' ' || COALESCE(maecenas.lastname,'') as maecenasname,
	COALESCE(maecenas.uid, '00000000-0000-0000-0000-000000000000') as maecenasuid,
	COALESCE(maecenas.phone, '') as maecenasphone, COALESCE(maecenas.email,'') as maecenasemail,
	COALESCE(maecenasphoto.filename,'') as photofilemaecenas,
	COALESCE(tickets.uid, '00000000-0000-0000-0000-000000000000') as ticketuid,  
	COALESCE(banklog.message,'') as bankmessage, COALESCE(banklog.code,0) as bankcode, COALESCE(banklog.bankorder,'') as bankorder,
	COALESCE(banklog.date,now()) as datecheck,
	COALESCE(tickets.amount,0) as ticketamount, orders.amount, COALESCE(orders.success,false) as ordersuccess, COALESCE(tickets.ispaid, false) as ispaid, 
	COALESCE(tickets.iswin,false) as iswin,
	COALESCE(actions.isauction, false) as isauction, actions.description, COALESCE(actions.title,'') as actiontitle, COALESCE(actions.longtitle,'') as actionlongtitle,
	COALESCE(orders.fundraisinguid, '00000000-0000-0000-0000-000000000000') as fundraisinguid
	FROM orders 
	LEFT JOIN banklog on banklog.orderuid = orders.uid::VARCHAR
	JOIN tickets on tickets.uid::VARCHAR = ANY(orders.lotteryuid::VARCHAR[])
	LEFT JOIN users on users.uid::VARCHAR = orders.usersuid
	LEFT JOIN usersphotos on users.uid = usersphotos.usersuid
	LEFT JOIN socnet on socnet.usersuid = users.uid::VARCHAR
	LEFT JOIN actions on actions.uid::VARCHAR = orders.actionuid
	LEFT JOIN users as maecenas ON maecenas.uid::VARCHAR = actions.maecenas
	LEFT JOIN usersphotos as maecenasphoto on maecenas.uid = maecenasphoto.uid
	WHERE ` + SearchSQL + ` order by banklog.date desc` + LimitSQL

	switch WithSearch {
	case true:
		rows, err = m.db.Query(sql, search.OwnerUID, "%"+search.Search+"%", search.Onpage, Offset)
	default:
		rows, err = m.db.Query(sql, search.OwnerUID, search.Onpage, Offset)
	}

	Items := make([]*Item, 0)
	if err != nil && err != pgx.ErrNoRows {
		m.log.Error().Err(err).Msg("Error with SQL")
		return Items, 0, err
	}

	totalCount := 0
	for rows.Next() {
		item := &Item{}
		var userImage, UserPhoto, MaecenasImage string

		err = rows.Scan(&totalCount, &item.Client.Name, &item.Client.UID, &item.Client.Phone, &item.Client.Email, &userImage,
			&UserPhoto, &item.Client.Socnet, &item.Maecenas.Name, &item.Maecenas.UID, &item.Maecenas.Phone, &item.Maecenas.Email,
			&MaecenasImage, &item.Ticket.UID, &item.Bank.Message, &item.Bank.Code, &item.Bank.BankOrder, &item.Bank.Date,
			&item.Ticket.Amount, &item.Ticket.OrderAmount, &item.Flags.Success, &item.Flags.IsPaid, &item.Flags.IsWin, &item.Flags.IsAuction,
			&item.Owner.Description, &item.Owner.Title, &item.Owner.LongTitle, &item.Owner.FundraisingUID)

		if err != nil {
			m.log.Error().Err(err).Msg("Error with scaning ...")
			return Items, 0, err
		}

		item.Client.Photo = m.ImagePath + models.NoPhoto
		item.Maecenas.Photo = m.ImagePath + models.NoPhoto

		if len(MaecenasImage) > 0 {
			item.Maecenas.Photo = m.ImagePath + string(models.ProfilesArea) + "/" + MaecenasImage
		}

		if len(userImage) > 1 {
			item.Client.Photo = m.ImagePath + string(models.ProfilesArea) + "/" + userImage
		} else {
			if len(UserPhoto) > 1 {
				item.Client.Photo = UserPhoto
			}
		}
		Items = append(Items, item)
	}

	return Items, totalCount, err
}

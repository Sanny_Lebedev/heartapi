package requests

import (
	"database/sql"

	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"github.com/jackc/pgx"
)

type (

	// Req ...
	Req struct {
		ID     string `db:"id" json:"id"`
		Result bool   `db:"result" json:"result"`
		Search string `db:"search" json:"search"`
	}

	// UsersInfo ...
	UsersInfo struct {
		UID        string    `db:"uid" json:"uid"`
		Phone      string    `db:"phone" json:"phone"`
		Email      string    `db:"email" json:"email"`
		FirstName  string    `db:"firstname" json:"firstname"`
		LastName   string    `db:"lastname" json:"lastname"`
		Regdata    time.Time `db:"regdata" json:"regdata"`
		UsersPhoto string    `db:"usersPhoto" json:"usersPhoto"`
		Actives    []bool    `json:"actives"`
		Roles      []string  `json:"roles"`
		Provider   string    `json:"provider"`
	}

	// Answer ...
	Answer struct {
		Total int          `db:"total" json:"total"`
		Data  []*UsersInfo `db:"data" json:"data"`
	}

	// SearchPayload ...
	SearchPayload struct {
		Search      string
		CurrentPage int
		ItemsOnPage int
		Types       app.UsersSearchType
		Actives     bool
	}
)

//GetUserList ...
func (r *Req) GetUserList(search SearchPayload, db *pgx.ConnPool, picsurl string) (Answer, error) {

	//search := search.Search

	var roles []string
	var sqlRoleString string
	count := 0
	offset := (search.CurrentPage - 1) * search.ItemsOnPage
	//var sqlstr string
	var rows *pgx.Rows

	if *search.Types.Admin {
		roles = append(roles, "usersroletab.role like 'admin'")
	}
	if *search.Types.User {
		roles = append(roles, "usersroletab.role like 'user'")
	}
	if *search.Types.Maecenas {
		roles = append(roles, "usersroletab.role like 'sponsor'")
	}

	sqlnonActivesString := ""

	if search.Actives {
		sqlnonActivesString = "(usersroletab.active = false)"
	}

	for i, v := range roles {

		if i > 0 {
			sqlRoleString = sqlRoleString + " OR " + v
		}
		if i == 0 {
			sqlRoleString = "users.uid IN ( select usersroletab.uid from usersroletab where (" + v
		}

	}

	if len(sqlRoleString) > 0 {
		sqlRoleString = sqlRoleString + " ) "

		if len(sqlnonActivesString) > 0 {
			sqlRoleString = sqlRoleString + " AND (" + sqlnonActivesString + ") )"
		} else {
			sqlRoleString = sqlRoleString + " ) "
		}

	} else {
		if len(sqlnonActivesString) > 0 {
			sqlRoleString = " WHERE " + sqlnonActivesString
		}
	}

	if len(sqlRoleString) > 0 {
		sqlRoleString = sqlRoleString + " AND "
	}
	sqlcount := "select count(*) from users WHERE " + sqlRoleString + " ((phone like $1)or(firstname like $1)or(lastname like $1))"

	_ = db.QueryRow(sqlcount, "%"+search.Search+"%").Scan(&count)

	sqlstr := `select users.uid, users.phone, users.email, users.firstname, users.lastname, users.regdata, 
	array_agg(usersroletab.active) as actives, array_agg(usersroletab.role) as roles, COALESCE(usersphotos.filename,'') as usersphoto,
	COALESCE(socnet.imagelink,'') as imagelink, COALESCE(socnet.provider,'') as provider
	from users 
	LEFT JOIN usersroletab ON usersroletab.uid = users.uid
	LEFT JOIN usersphotos ON usersphotos.usersuid = users.uid 
	LEFT JOIN socnet ON socnet.usersuid = users.uid::VARCHAR
	WHERE ` + sqlRoleString + ` ((phone like $1)or(firstname like $1)or(lastname like $1)) 
	GROUP BY users.uid, users.phone, users.email, users.firstname, users.lastname, users.regdata, usersphotos.filename, socnet.imagelink, socnet.provider
	ORDER BY users.regdata DESC
	LIMIT $2 OFFSET $3`

	rows, _ = db.Query(sqlstr, "%"+search.Search+"%", search.ItemsOnPage, offset)

	usrs := make([]*UsersInfo, 0)

	for rows.Next() {
		var err error
		photo := ""
		image := ""
		usr := new(UsersInfo)

		err = rows.Scan(&usr.UID, &usr.Phone, &usr.Email, &usr.FirstName, &usr.LastName, &usr.Regdata, &usr.Actives, &usr.Roles, &photo, &image, &usr.Provider)

		if err == sql.ErrNoRows {

			return Answer{0, nil}, err
		} else if err != nil {

			return Answer{0, nil}, err
		}
		if photo != "" {
			usr.UsersPhoto = picsurl + "profiles/" + photo
		} else {
			if image != "" {
				usr.UsersPhoto = image
			}
		}

		// datas := UsersInfo{Uid: usr.Uid, Email: usr.Email, Phone: usr.Phone, firstname: usr.firstname, lastname: usr.lastname, Regdata: usr.Regdata}
		usrs = append(usrs, usr)
	}
	Ans := new(Answer)
	Ans.Data = usrs
	Ans.Total = count

	return *Ans, nil
}

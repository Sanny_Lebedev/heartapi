package tickets

import (
	// "bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"

	"strconv"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, Log logger.Logger, userUID string) *Config {
	return &Config{db: db, log: Log, UserUID: userUID}
}

func (a *Action) GetPaymentLink() (string, error) {
	link := ""
	a.Config.log.Info().Str("Action", a.UID).Msg("Test")
	a.Config.log.Info().Str("User", a.Config.UserUID).Msg("Test")
	err := a.Config.db.QueryRow(`SELECT paylink FROM winners WHERE actionsuid=$1 AND winneruid=$2 AND iswaiting`, a.UID, a.Config.UserUID).Scan(&link)
	a.Config.log.Info().Str("link", link).Msg("Test")
	if err != nil {
		a.Config.log.Error().Err(err).Msg("Error with GetPaymentLink")
		return "", err
	}

	return link, nil
}

// New - returns new sections instance
func New(db *pgx.ConnPool, Log logger.Logger, SenderUID string, Amount uint, ActionUID string) (string, string, []string, bool, error) {
	ticketList := make([]string, 0)
	IsAuction := false

	// Main := Initial(db, Log, SenderUID)
	action := Action{UID: ActionUID, Config: Config{db: db, log: Log, UserUID: SenderUID}}

	err := action.Get()
	if err != nil {
		return "", "", ticketList, IsAuction, err
	}

	if !action.Flags.IsActual {
		if !action.Flags.IsPaid {
			err := errors.New("NotPaid")
			link := ""
			link, err = action.GetPaymentLink()
			return link, "NotPaid", ticketList, action.IsAuction, err
		}

		err := errors.New("Action is not actual")
		return "", "Акция не актуальна", ticketList, IsAuction, err
	}
	IsAuction = action.IsAuction
	if action.IsAuction {

		if Amount < action.Auction.MinBet+action.Auction.CurrentSumm {
			err := errors.New("too small bet")
			return "", "Ставка ниже установленной. Минимальная ставка: " + strconv.FormatFloat(float64(float64(action.Auction.MinBet+action.Auction.CurrentSumm)/float64(100)), 'f', -1, 64), ticketList, IsAuction, err
		}

	}

	OrderUID, ticketList, err := action.NewTicket(Amount, SenderUID)
	return OrderUID, "", ticketList, IsAuction, err
}

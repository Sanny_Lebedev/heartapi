package tickets

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
	"github.com/lib/pq"
)

// "bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"

// Get - check for actual action
func (a *Action) Get() error {

	err := a.Config.db.QueryRow(`SELECT active, minsumm, currentsumm, 
	minbet, isauction, owneruid, ticketprice FROM actions WHERE uid = $1`, a.UID).Scan(&a.Flags.IsActual, &a.Auction.MinSumm, &a.Auction.CurrentSumm,
		&a.Auction.MinBet, &a.IsAuction, &a.FundraisingUID, &a.TicketPrice)

	if err == pgx.ErrNoRows {
		return nil
	}
	if err != nil {
		a.Config.log.Error().Err(err).Msg("Error with getting action info for the ticket")
		return err
	}

	return nil
}

//NewTicket ...
func (a *Action) NewTicket(amount uint, UID string) (string, []string, error) {

	ticket := Ticket{
		ActionsUID:     a.UID,
		FundraisingUID: a.FundraisingUID,
		Amount:         amount,
		UsersUID:       UID,
		Config:         a.Config,
		TicketPrice:    a.TicketPrice,
		Flags: TicketFlags{
			IsAuction: a.IsAuction,
			IsDraw:    true,
			IsWin:     false,
			IsPaid:    false,
		},
	}
	err := ticket.Save()
	return ticket.OrderUID, ticket.TicketList, err
}

//Save - save new ticket
func (t *Ticket) Save() error {
	err := t.SaveOrder()
	if err != nil {
		return err
	}

	err = t.SaveTicket()
	return err
}

//NewOrderSaving - saving new order - universal function
func NewOrderSaving(db *pgx.ConnPool, log logger.Logger, UserUID, FundraisingUID, ActionUID string, Amount uint, TicketUID string) (string, error) {
	var NewOrderUID string
	if len(TicketUID) < 1 {
		TicketUID = "{}"
	}
	err := db.QueryRow(`INSERT INTO orders (usersuid, fundraisinguid, actionuid, amount, date, status, success, lotteryuid)
	VALUES ($1, $2, $3, $4, CURRENT_TIMESTAMP, 1, false, $5) RETURNING uid`, UserUID, FundraisingUID, ActionUID, Amount, TicketUID).Scan(&NewOrderUID)
	if err != nil {
		log.Error().Err(err).Msg("Error with inserting new order")
		return "", err
	}
	return NewOrderUID, nil
}

//SaveOrder - save new ticket in order table
func (t *Ticket) SaveOrder() error {
	var err error
	t.OrderUID, err = NewOrderSaving(t.db, t.log, t.UsersUID, t.FundraisingUID, t.ActionsUID, t.Amount, t.UID)
	return err
	// err := t.Config.db.QueryRow(`INSERT INTO orders (usersuid, fundraisinguid, actionuid, amount, date, status, success)
	// VALUES ($1, $2, $3, $4, CURRENT_TIMESTAMP, 1, false) RETURNING uid`, t.UsersUID, t.FundraisingUID, t.ActionsUID, t.Amount).Scan(&t.OrderUID)
	// if err != nil {
	// 	t.log.Error().Err(err).Msg("Error with inserting new order")
	// }

}

//SaveTicket - save new ticket in ticket table
func (t *Ticket) SaveTicket() error {
	TicketList := make([]string, 0)

	if !t.Flags.IsAuction {
		tickets := float64(t.Amount/100) / (t.TicketPrice / 100)
		for j := 0; j < int(tickets); j++ {
			curTicket := ""
			err := t.Config.db.QueryRow(`INSERT INTO tickets (actionsuid, usersuid, amount, iswin, isdraw, willparticipate, date, ispaid, isauction, orderuid)
			VALUES ($1, $2, $3, false, false, false, CURRENT_TIMESTAMP, false, $4, $5) RETURNING uid`, t.ActionsUID, t.UsersUID, t.TicketPrice, t.Flags.IsAuction, t.OrderUID).Scan(&curTicket)
			if err != nil {
				t.log.Error().Err(err).Msg("Error with inserting new ticket")
			}
			TicketList = append(TicketList, curTicket)
		}
	}

	if t.Flags.IsAuction {
		curTicket := ""
		err := t.Config.db.QueryRow(`INSERT INTO tickets (actionsuid, usersuid, amount, iswin, isdraw, willparticipate, date, ispaid, isauction)
	VALUES ($1, $2, $3, false, false, false, CURRENT_TIMESTAMP, false, $4) RETURNING uid`, t.ActionsUID, t.UsersUID, t.Amount, t.Flags.IsAuction).Scan(&curTicket)
		if err != nil {
			t.log.Error().Err(err).Msg("Error with inserting new ticket")
		}
		TicketList = append(TicketList, curTicket)
	}
	err := t.CorrectOrderTicketUID(TicketList)
	t.TicketList = TicketList
	return err
}

//CorrectOrderTicketUID - correctiong data for order
func (t *Ticket) CorrectOrderTicketUID(UID []string) error {
	_, err := t.Config.db.Exec(`UPDATE orders SET lotteryuid = $1 WHERE uid = $2`, pq.Array(UID), t.OrderUID)
	if err != nil {
		t.log.Error().Err(err).Msg("Error with update order for lotteryuid")
	}
	return err
}

//SaveBet - saving new bet
func (c *Config) SaveBet(actionUID, ticketUID string, amount uint) error {
	_, err := c.db.Exec(`INSERT INTO bets (actionsuid, ticketuid, bet, date) VALUES ($1, $2, $3, CURRENT_TIMESTAMP)`, actionUID, ticketUID, amount)
	if err != nil {
		c.log.Error().Err(err).Msg("Error with saving new bet")
		return err
	}
	var currentSumm int64
	err = c.db.QueryRow(`SELECT currentsumm FROM actions where uid = $1`, actionUID).Scan(&currentSumm)
	if err != nil {
		c.log.Error().Err(err).Msg("Error with getting current summ")
		return err
	}

	_, err = c.db.Exec(`UPDATE actions SET currentsumm = $1 WHERE uid = $2`, amount, actionUID)
	if err != nil {
		c.log.Error().Err(err).Msg("Error with updating current summ")
		return err
	}

	return nil
}

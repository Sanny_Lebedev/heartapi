package tickets

import (
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

type (
	// Main - main structure for fundraising
	Main struct {
		Config Config `json:"config"`
		Ticket Ticket `json:"ticket"`
	}

	// Config ...
	Config struct {
		db      *pgx.ConnPool
		log     logger.Logger
		UserUID string
	}

	// Ticket  - main structure
	Ticket struct {
		UID            string      `json:"UID"`
		OrderUID       string      `json:"order"`
		ActionsUID     string      `json:"actionsUID"`
		FundraisingUID string      `json:"fundraisingUID"`
		UsersUID       string      `json:"UsersUID"`
		Amount         uint        `json:"amount"`
		TicketPrice    float64     `json:"ticketPrice"`
		TicketList     []string    `json:"tickets"`
		Flags          TicketFlags `json:"flags"`
		Comment        string      `json:"comment"`
		Date           *time.Time  `json:"date"`
		Config
	}

	//TicketFlags - structure of tickets flags
	TicketFlags struct {
		IsWin           bool `json:"isWin"`
		IsDraw          bool `json:"isDraw"`
		WillParticipate bool `json:"willParticipate"`
		IsPaid          bool `json:"isPaid"`
		IsAuction       bool `json:"isAuction"`
	}

	//Action - short structure of action
	Action struct {
		Config         Config
		UID            string      `json:"UID"`
		FundraisingUID string      `json:"fundraisingUID"`
		TicketPrice    float64     `json:"ticketPrice"`
		Flags          ActionFlags `json:"flags"`
		Auction        AuctionInfo `json:"auction"`
		IsAuction      bool        `json:"isAuction"`
	}

	//ActionFlags - flags of actions
	ActionFlags struct {
		IsActual bool `json:"isActual"`
		IsPaid   bool `json:"isPaid"`
	}

	//AuctionInfo - struct of auction
	AuctionInfo struct {
		MinBet      uint `json:"minBet"`
		CurrentSumm uint `json:"currentSumm"`
		MinSumm     uint `json:"minSumm"`
	}

	// Search - structure for search and pagination
	Search struct {
		Search    string `json:"search"`
		Current   int    `json:"current"`
		Onpage    int    `json:"onPage"`
		Paginator bool   `json: "paginator"`
	}

	// Answer - answer for API with list of Prizes
	Answer struct {
		List       []*Ticket `json:"list"`
		Success    bool      `json:"success"`
		Action     string    `json:"message"`
		Count      int       `json:"count,omitempty"`
		TotalCount int       `json:"totalCount,omitempty"`
		HasNext    bool      `json:"hasNext"`
	}

	// AnswerOne - answer for API with info about current Prize
	AnswerOne struct {
		Info    Ticket `json:"info"`
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}

	//AnswerWithUID - asnwer with UID of Prize
	AnswerWithUID struct {
		UID     string `json:"UID"`
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}
)

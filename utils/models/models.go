package models

type (
	// Images - structure for pics for fundraising
	Images struct {
		UID   string `json:"UID"`
		Image string `json:"image"`
	}

	// Contacts - structure for contacts
	Contacts struct {
		UID     string `json:"UID"`
		Person  string `json:"person"`
		Phone   string `json:"phone"`
		Email   string `json:"email"`
		Website string `json:"website"`
		Skype   string `json:"skype"`
	}

	// Address - structure for contacts
	Address struct {
		UID     string `json:"UID"`
		Country string `json:"country"`
		Postal  string `json:"postal"`
		City    string `json:"city"`
		Address string `json:"address"`
	}

	// Sections - structure of sections for current fundraising
	Sections struct {
		SectionUID     string `json:"sectionUID"`
		FundraisingUID string `json:"fundraisingUID"`
		SectionName    string `json:"sectionName"`
	}

	// Prize - structure for prizes
	Prize struct {
		UID         string    `json:"uid"`
		OwnerUID    string    `json:"ActionsUID"`
		TicketUID   string    `json:"TicketUID"`
		SenderUID   string    `json:"-"`
		MaecenasUID string    `json:"maecenasUID"`
		Title       string    `json:"title"`
		LongTitle   string    `json:"longTitle"`
		Description string    `json:"description"`
		Place       int       `json:"place"`
		Images      []*Images `json:"images"`
	}

	//Actusers ...
	Actusers struct {
		UID    string  `json:"uid"`
		Name   string  `json:"name"`
		Image  string  `json:"imageLink"`
		Amount float64 `json:"amount"`
		Date   string  `json:"date"`
	}

	//ActusersAnswer ...
	ActusersAnswer struct {
		Actusers []*Actusers `json:"list"`
		Count    int         `json:"count"`
		Total    int         `json:"totalCount"`
		HasNext  bool        `json:"hasNext"`
	}

	//Paginator ...
	Paginator struct {
		OnPage       int    `json:"onPage"`
		CurrentPage  int    `json:"currentPage"`
		HasPaginator bool   `json:"hasPaginator"`
		Search       string `json:"search"`
		OnlyActive   bool   `json:"onlyActive"`
		NotActive    bool   `json:"notActive"`
	}

	//OwnerType  - type of owner for DB
	OwnerType string

	//PhotoPath - path for photo for sectoin
	PhotoPath string

	// AreaType ...
	AreaType string
)

const (
	// OwnerFundraising - type of owner
	OwnerFundraising OwnerType = "Fundraising"

	// OwnerActions - type of owner
	OwnerActions OwnerType = "Actions"

	// ActionsArea - name of actions area
	ActionsArea AreaType = "actions"

	//ProfilesArea - name of profiles area
	ProfilesArea AreaType = "profiles"

	//FundsArea - name of funds area
	FundsArea AreaType = "funds"

	// QnameSend ...
	QnameSend = "send"

	// ActionMaecenas ...
	ActionMaecenas = "maecenas"

	// ActionNewAction ...
	ActionNewAction = "crtact"

	// GetterName ...
	GetterNameAdmin = "admin"

	//NoPhoto - nophoto filename
	NoPhoto = "nophoto.jpg"
)

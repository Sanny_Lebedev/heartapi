package fundraising

import (
	"fmt"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/likes"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

func cut(text string, limit int) string {
	runes := []rune(text)
	if len(runes) >= limit {
		return string(runes[:limit])
	}
	return text
}

// GetFromActions - get list of items
func GetFromActions(db *pgx.ConnPool, picsURL string, sectionuid string, search Search, MyUserUID string, filter Filters) ([]*Fundraising, int, error) {
	list := make([]*Fundraising, 0)
	args := make([]interface{}, 0)

	var Offset int
	var err error
	var rows *pgx.Rows
	var times time.Time
	var SearchSQL, LimitSQL string

	donatedsumm := 0
	i := 1
	needsumm := 0
	GetAnd := false

	if sectionuid != "" {
		GetAnd = true
		SearchSQL = fmt.Sprintf(`%v crossfundsraising.owneruid = $%d`, SearchSQL, i)
		i++
		args = append(args, sectionuid)
	}

	if search.Search == "" {
		if GetAnd {
			SearchSQL = SearchSQL + ` AND `
		} else {
			GetAnd = true
		}
		SearchSQL = fmt.Sprintf(`%v (actions.title != '' OR actions.longtitle != '' OR actions.description != '')`, SearchSQL)
	}

	if search.Search != "" {
		if GetAnd {
			SearchSQL = SearchSQL + ` AND `
		} else {
			GetAnd = true
		}
		SearchSQL = fmt.Sprintf(`%v setweight(to_tsvector(actions.title), 'A') ||
		setweight(to_tsvector(actions.description), 'B') @@ plainto_tsquery($%d)`, SearchSQL, i)
		i++
		args = append(args, "%"+search.Search+"%")
	}

	if search.OnlyActive && !search.NotActive {

		if GetAnd {
			SearchSQL = SearchSQL + ` AND `
		} else {
			GetAnd = true
		}
		SearchSQL = SearchSQL + `(actions.active = true AND actions.flagfinish = false)`
	}

	if !search.OnlyActive && search.NotActive {

		if GetAnd {
			SearchSQL = SearchSQL + ` AND `
		} else {
			GetAnd = true
		}
		SearchSQL = SearchSQL + `(actions.active = false OR actions.flagfinish = true) `
	}

	if len(filter.Other) > 0 {
		for _, item := range filter.Other {
			if item.Active {
				switch item.UID {
				case "onlyActions":
					if GetAnd {
						SearchSQL = SearchSQL + ` AND `
					} else {
						GetAnd = true
					}
					SearchSQL = fmt.Sprintf(`%v actions.isauction = false`, SearchSQL)

				case "onlyAuctions":
					if GetAnd {
						SearchSQL = SearchSQL + ` AND `
					} else {
						GetAnd = true
					}
					SearchSQL = fmt.Sprintf(`%v actions.isauction = true`, SearchSQL)
				case "onlyFundraising":
					return make([]*Fundraising, 0), 0, nil
				default:
				}
			}
		}
	}

	if len(filter.Sections) > 0 {
		sect := make([]string, 0)
		for _, item := range filter.Sections {
			sect = append(sect, item.UID)
		}
		if GetAnd {
			SearchSQL = SearchSQL + ` AND `
		} else {
			GetAnd = true
		}
		SearchSQL = fmt.Sprintf(`%v crossfundsraising.owneruid = ANY($%d)`, SearchSQL, i)
		args = append(args, sect)
		i++
	}

	if search.Paginator {

		LimitSQL = fmt.Sprintf(` LIMIT $%d OFFSET $%d`, i, (i + 1))
		Offset = search.Current*search.Onpage - search.Onpage
		args = append(args, search.Onpage)
		args = append(args, Offset)
	}

	if len(SearchSQL) > 2 {
		SearchSQL = ` WHERE ` + SearchSQL
	}

	GroupBy := `GROUP BY actions.uid, fundraising.uid, actions.uid, actions.title, actions.longtitle, actions.description,  
	actions.result, actions.donated, actions.donatedsumm, actions.needsumm, fundraising.actions, 
	actions.active, actions.status, actions.flagstart, actions.flagfinish, actions.date, 
	fundraising.step, actions.likes, images.uids, fund.uid, fund.title, fund.description, fund.inn, fund.status, 
	address.uid, address.postal, address.address, fundimage.uids, address.country,address.city, 
	contact.uid, contact.person, contact.phone,contact.email, contact.website, contact.skype, actions.isauction, 
	mywin.winneruid, mywin.iswin, actions.criteriasdate, actions.datefinish, prizeimage.imagelink`

	sql := `SELECT count(actions.uid) OVER() AS total_count, fundraising.uid, 
	actions.uid, actions.title, actions.longtitle, actions.description,  
	actions.result, actions.donated, actions.donatedsumm, actions.needsumm, fundraising.actions, 
	actions.active, actions.status, actions.flagstart, actions.flagfinish, actions.date, 
	fundraising.step, actions.likes, images.uids,
	COALESCE(fund.uid,'00000000-0000-0000-0000-000000000000') as fundUID, COALESCE(fund.title,'') as fundTitle, COALESCE(fund.description,'') as fundDescription, 
	COALESCE(fund.inn,'') as fundInn, COALESCE(fund.status,'') as fundStatus,
	COALESCE(address.uid,'00000000-0000-0000-0000-000000000000') as addressUID, COALESCE(address.country,'') as addressCountry, 
	COALESCE(address.city,'') as addressCity, COALESCE(address.postal,'') as addressPostal, COALESCE(address.address,'') as addressAddress,
	fundimage.uids as fundPics,
	COALESCE(contact.uid,'00000000-0000-0000-0000-000000000000') as contactUID, COALESCE(contact.person,'') as contactPerson, 
	COALESCE(contact.phone,'') as contactPhone, COALESCE(contact.email,'') as contactEmail, COALESCE(contact.website,'') as contactWeb, 
	COALESCE(contact.skype,'') as contactSkype, actions.isauction, 
	COALESCE(mywin.winneruid,'00000000-0000-0000-0000-000000000000') as winneruid, 
	COALESCE(mywin.iswin, false) as iswin,
	actions.criteriasdate, actions.datefinish,
	COALESCE(prizeimage.imagelink,'') as prizeimage
	from actions 
LEFT JOIN (SELECT winneruid, iswin, actionsuid FROM winners WHERE iswin) as mywin on mywin.actionsuid = actions.uid::VARCHAR
LEFT JOIN fundraising on fundraising.uid::VARCHAR = actions.owneruid
		LEFT JOIN crossfundsraising on crossfundsraising.raisinguid = fundraising.uid::varchar
		LEFT JOIN fund ON fund.uid::VARCHAR = crossfundsraising.funduid
		LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
			as fundimage on fundimage.owneruid = fund.uid::VARCHAR
	  	LEFT JOIN 
			(SELECT address.uid, address.country, address.city, address.postal, address.address, address.owneruid from address where address.ownertype = 'fund') as
			address ON address.owneruid = fund.uid::VARCHAR 	
		LEFT JOIN 
			(SELECT contacts.uid, contacts.person, contacts.phone, contacts.email, contacts.website, contacts.skype, contacts.owneruid FROM contacts where contacts.ownertype = 'fund') as
			contact ON contact.owneruid = fund.uid::VARCHAR   
		LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
		as images on images.owneruid = actions.uid::VARCHAR
		LEFT JOIN 
		(select uid, owneruid FROM prize WHERE place = 1) as pr 
		on pr.owneruid = actions.uid::VARCHAR
		LEFT JOIN crossimages as prizeimage on prizeimage.owneruid = pr.uid::VARCHAR
		
		` + SearchSQL + ` ` + GroupBy + ` order by actions.date desc` + LimitSQL

	rows, err = db.Query(sql, args...)

	if err != nil && err != pgx.ErrNoRows {
		return list, 0, err
	}
	totalCount := 0

	for rows.Next() {
		item := &Fundraising{}
		var images []string
		var fundimages []string
		var haveWinner bool
		var winnerUID, prizeImage string
		var DateFinish time.Time

		err = rows.Scan(&totalCount, &item.OwnerUID, &item.UID, &item.Title, &item.Longtitle, &item.Description, &item.Result, &item.Donated, &donatedsumm, &needsumm,
			&item.ActionsCount, &item.Active, &item.Status, &item.Flagstart, &item.Flagfinish, &times, &item.Step, &item.Likes, &images,
			&item.Fund.UID, &item.Fund.Title, &item.Fund.Description, &item.Fund.Inn, &item.Fund.Status,
			&item.Fund.Address.UID, &item.Fund.Address.Country, &item.Fund.Address.City, &item.Fund.Address.Postal, &item.Fund.Address.Address, &fundimages,
			&item.Fund.Contacts.UID, &item.Fund.Contacts.Person, &item.Fund.Contacts.Phone, &item.Fund.Contacts.Email,
			&item.Fund.Contacts.Website, &item.Fund.Contacts.Skype, &item.IsAuction, &winnerUID, &haveWinner,
			&item.CriteriasDate, &DateFinish, &prizeImage)

		if err != nil {

			return list, 0, err
		}
		item.IsAction = true
		item.IsFundraising = false

		if len(item.Title) < 1 {
			item.Title = cut(item.Description, 150)
		}
		if !haveWinner {
			winnerUID = ""
		}
		item.WinnerUID = winnerUID

		item.Days = 0
		item.Hours = 0
		if item.CriteriasDate {
			date := time.Now()
			duration := date.Sub(DateFinish)
			item.Days = 0 - int(duration.Hours()/24)
			if item.Days < 0 {
				item.Days = 0
			}
			if item.Days == 1 {
				item.Hours = 0 - int(duration.Hours())
				if item.Hours < 0 {
					item.Hours = 0
				}
			}
		}

		if len(prizeImage) > 0 {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = picsURL + "prize/" + prizeImage
			pic.UID = ""
			listImage = append(listImage, pic)
			item.Images = listImage
		} else {

			if len(images) > 0 {
				listImage := make([]*models.Images, 0)
				for _, image := range images {
					pic := &models.Images{}
					pic.Image = picsURL + "actions/" + image + ".jpg"
					pic.UID = image
					listImage = append(listImage, pic)
					// element is the element from someSlice for where we are
				}
				item.Images = listImage
			}
		}

		if item.Images == nil {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = picsURL + models.NoPhoto
			pic.UID = ""
			listImage = append(listImage, pic)
			item.Images = listImage
		}

		if len(fundimages) > 0 {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = picsURL + "funds/" + item.Fund.UID + ".jpg"
			pic.UID = item.Fund.UID
			listImage = append(listImage, pic)

			// for _, image := range fundimages {
			// 	pic := &models.Images{}
			// 	pic.Image = picsURL + "funds/" + image + ".jpg"
			// 	pic.UID = image
			// 	listImage = append(listImage, pic)
			// 	// element is the element from someSlice for where we are
			// }
			item.Fund.Images = listImage
		}

		if item.Fund.Images == nil {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = picsURL + models.NoPhoto
			pic.UID = ""
			listImage = append(listImage, pic)
			item.Fund.Images = listImage
		}

		item.Needsumm = float64(needsumm / 100)
		item.Donatedsumm = float64(donatedsumm / 100)
		item.Date = times.Format("2006-01-02")
		item.IsLiked = false

		item.IsLiked, err = likes.IsLiked(db, item.UID, "Action", MyUserUID)
		if err != nil {
			item.IsLiked = false
		}

		likes, err1 := likes.GetMyCount(db, item.UID, "Action")
		if err1 != nil {
			likes = 0
		}
		item.Likes = int64(likes)

		item.Sections, err = GetlistofSections(db, item.OwnerUID)
		if err != nil {

		} else {
			item.CurrentSection = item.Sections[0].SectionUID
		}
		// Is paid for fundraising
		item.IsPaid = false
		if MyUserUID != "" {
			item.IsPaid, _ = IsPaidByUserFORActions(db, MyUserUID, item.UID)

		}

		// Get a list of actions for fundraising
		Search := actions.Search{}
		Search.Paginator = false
		// item.Actions, _, _ = actions.Get(db, picsURL, item.UID, Search, MyUserUID)
		item.Actions = nil
		item.Maecenas, _ = actions.GetMaecenas(db, picsURL, item.UID)
		// Get a list of users for fundraising
		actusers := &models.ActusersAnswer{}
		actusers.HasNext = false
		pagin := &models.Paginator{}
		pagin.Search = ""

		actusers.Actusers, actusers.Total, err = Getlistactionsusers(db, picsURL, item.UID, true, *pagin)
		if len(actusers.Actusers) < actusers.Total {
			actusers.HasNext = true
		}
		actusers.Count = len(actusers.Actusers)
		item.Users = *actusers

		list = append(list, item)
	}

	return list, totalCount, nil
}

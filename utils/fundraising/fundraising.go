package fundraising

import (
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crossimages"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/jobs"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/likes"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"

	"github.com/jackc/pgx"
)

const (
	//Area - area name for os
	Area = "fundraising"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, OwnerUID string, FundraisingUID string, Log logger.Logger) *Main {

	u := &Main{
		db:  db,
		log: Log,
		ci:  *crossimages.Initial(db, Log, Area),
		Fundraising: Fundraising{
			UID:      FundraisingUID,
			OwnerUID: OwnerUID,
		},
	}

	return u
}

// New - returns new sections instance
func New(db *pgx.ConnPool, ctx *app.SenditemsFundraisingContext, donatedsumm int, donated int, OwnerUID string, Log logger.Logger) *Main {
	uid := ""
	step := "0"
	if ctx.Payload.UID != nil && *ctx.Payload.UID != "" {
		uid = *ctx.Payload.UID
	}

	if ctx.Payload.Step != nil {
		step = *ctx.Payload.Step
	}

	u := &Main{
		db:  db,
		log: Log,
		Fundraising: Fundraising{
			UID:            uid,
			OwnerUID:       OwnerUID,
			CurrentSection: *ctx.Payload.Sectionuid,
			Title:          ctx.Payload.Title,
			Longtitle:      ctx.Payload.Longtitle,
			Description:    *ctx.Payload.Description,
			Result:         *ctx.Payload.Result,
			Status:         *ctx.Payload.Status,
			Step:           step,
			Active:         *ctx.Payload.Active,
			Flagstart:      *ctx.Payload.Flagstart,
			Flagfinish:     *ctx.Payload.Flagfinish,
			Needsumm:       float64(ctx.Payload.NeedSumm),
			Donatedsumm:    float64(donatedsumm),
			Donated:        int64(donated),
		},
	}

	return u
}

// Delete - delete fundraising from db
func Delete(db *pgx.ConnPool, log logger.Logger, uid string) error {
	var err error

	// delete photos of fundraising
	err = DeleteFRPhotos(db, log, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	_, err = db.Exec(`DELETE from fundraising WHERE uid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// DeleteFRPhotos - delete all photos of findraising from db
func DeleteFRPhotos(db *pgx.ConnPool, log logger.Logger, uid string) error {
	ci := crossimages.Initial(db, log, Area)
	return ci.DeleteFRPhotos(uid)
}

// Exist - check for exist of section
func Exist(db *pgx.ConnPool, uid string) bool {
	var exist bool
	err := db.QueryRow(`SELECT COUNT(uid)>0 from fundraising WHERE uid = $1`, uid).Scan(&exist)
	if err != nil || err == pgx.ErrNoRows {

		return false
	}
	return true
}

// DeleteImageByUID - deleting Image by image UID
func (s *Main) DeleteImageByUID(imageuid string) error {
	return s.ci.DeleteImageByUID(imageuid)
}

// SaveImage - saving image to DB
func (s *Main) SaveImage(senderuid, fundraising string) (models.Images, error) {
	img, err := s.ci.SaveImage(senderuid, fundraising)
	return img, err
}

// SaveFund - saving information about current fund
func (s *Main) SaveFund(fundUID string) error {
	var err error
	_, err = s.db.Exec(`UPDATE crossfundsraising SET funduid = $1 where raisinguid = $2`, fundUID, s.Fundraising.UID)
	return err
}

// Save - Checking for a similar entry
func (s *Main) Save() (string, error) {
	var newUID string
	var err error

	if s.Fundraising.UID == "" {

		newUID, err = s.Addnew()
		if err != nil {
			s.log.Error().Err(err).Msg("Error adding section")
			return "", err
		}
		return newUID, nil
	}

	err = s.Edit()
	if err != nil {
		s.log.Error().Err(err).Msg("Error adding section")
		return s.Fundraising.UID, err
	}

	return s.Fundraising.UID, nil
}

//FundraisingCheck - check fundraising for exist
func (s *Main) FundraisingCheck() (bool, error) {
	var ans bool
	err := s.db.QueryRow(`SELECT COUNT(*)>0 as ANS FROM fundraising WHERE UID = $1`, s.Fundraising.UID).Scan(&ans)
	if err != nil && err != pgx.ErrNoRows {
		return false, err
	}
	return ans, nil
}

//SectionCheck - check section for exist
func (s *Main) SectionCheck() (bool, error) {

	var ans bool
	err := s.db.QueryRow(`SELECT COUNT(*)>0 as ANS FROM sections WHERE UID = $1`, s.Fundraising.CurrentSection).Scan(&ans)
	if err != nil && err != pgx.ErrNoRows {
		return false, err
	}
	return ans, nil
}

// CheckSectionsCross - check for existing in cross table
func (s *Main) CheckSectionsCross() (bool, error) {
	var ans bool
	err := s.db.QueryRow(`SELECT COUNT(*)>0 as ANS FROM crossfundsraising WHERE owneruid = $1 AND raisinguid = $2 AND active = true`, s.Fundraising.CurrentSection, s.Fundraising.UID).Scan(&ans)
	if err != nil && err != pgx.ErrNoRows {
		return false, err
	}
	return ans, nil
}

// SaveSectionsCross - save new data to cross table
func (s *Main) SaveSectionsCross() error {

	_, err := s.db.Query(`INSERT INTO crossfundsraising (owneruid, raisinguid, active, date) VALUES($1, $2, true, CURRENT_TIMESTAMP)`, s.Fundraising.CurrentSection, s.Fundraising.UID)
	return err

}

// Addnew - adding new section
func (s *Main) Addnew() (string, error) {
	var newUID string
	var err error

	err = s.db.QueryRow(`INSERT INTO fundraising (owneruid, title, longtitle, description, result, donated, donatedsumm, 
		needsumm, active, status, flagstart, flagfinish, date, step) VALUES ($1, $2, $3, $4, $5, 
		$12, $13, $6, $7, $8, $9, $10, CURRENT_TIMESTAMP, $11) RETURNING uid`, s.Fundraising.OwnerUID,
		s.Fundraising.Title, s.Fundraising.Longtitle, s.Fundraising.Description, s.Fundraising.Result, s.Fundraising.Needsumm,
		s.Fundraising.Active, s.Fundraising.Status, s.Fundraising.Flagstart, s.Fundraising.Flagfinish, s.Fundraising.Step,
		s.Fundraising.Donated, s.Fundraising.Donatedsumm).Scan(&newUID)
	s.Fundraising.UID = newUID

	inSection, errCross := s.CheckSectionsCross()

	if errCross != nil && errCross != pgx.ErrNoRows {
		return newUID, err
	}

	if inSection == false {

		newerr := s.SaveSectionsCross()
		if newerr != nil {

			return newUID, newerr
		}
	}

	if s.Fundraising.Active {
		newPush := jobs.NewPush(s.db, "", s.Fundraising.UID)
		err := newPush.Send()
		if err != nil {
			s.log.Error().Err(err).Msg("Error with sending to push")
		}
	}

	return newUID, err

}

// Edit - edit section
func (s *Main) Edit() error {

	inSection, errCross := s.CheckSectionsCross()
	if errCross != nil && errCross != pgx.ErrNoRows {
		return errCross
	}

	if !inSection {
		newerr := s.SaveSectionsCross()
		if newerr != nil {
			return newerr
		}
	}

	sql := `UPDATE fundraising SET title = $1, longtitle = $2, description = $3, 
	result = $4, donated = $5, donatedsumm = $6, needsumm = $7, active = $8, status = $9, flagstart = $10, flagfinish = $11, 
	step=$12 WHERE uid = $13`
	_, err := s.db.Exec(sql, s.Fundraising.Title, s.Fundraising.Longtitle, s.Fundraising.Description,
		s.Fundraising.Result, s.Fundraising.Donated, s.Fundraising.Donatedsumm, s.Fundraising.Needsumm,
		s.Fundraising.Active, s.Fundraising.Status, s.Fundraising.Flagstart, s.Fundraising.Flagfinish,
		s.Fundraising.Step, s.Fundraising.UID)

	if s.Fundraising.Active {
		newPush := jobs.NewPush(s.db, "", s.Fundraising.UID)
		err := newPush.Send()
		if err != nil {
			s.log.Error().Err(err).Msg("Error with sending to push")
		}
	}

	return err
}

// FileExist - check for file
func FileExist(filename string) bool {

	pathtofile := filepath.Join(binpath.BinaryPath(), "images/fundraising")

	if _, err := os.Stat(filepath.Join(pathtofile, filename+".jpg")); !os.IsNotExist(err) {
		return true
	}
	return false

}

//GetlistofSections - getting list of current sections
func GetlistofSections(db *pgx.ConnPool, fundraisingUID string) ([]*models.Sections, error) {
	rows, err := db.Query(`SELECT sections.uid, crossfundsraising.raisinguid, sections.title from sections 
	left join crossfundsraising on crossfundsraising.owneruid = sections.uid::varchar
	WHERE crossfundsraising.raisinguid = $1
	ORDER BY sections.title `, fundraisingUID)
	list := make([]*models.Sections, 0)
	if err != nil && err != pgx.ErrNoRows {
		return list, err
	}

	for rows.Next() {
		item := &models.Sections{}
		err = rows.Scan(&item.SectionUID, &item.FundraisingUID, &item.SectionName)
		if err != nil {
			return list, err
		}
		list = append(list, item)
	}
	return list, nil

}

// Getone - get information about current fundraising
func Getone(db *pgx.ConnPool, picsURL string, uid string, MyUserUID string, Domain string) (Fundraising, error) {
	item := &Fundraising{}
	if uid == "" {
		return *item, nil
	}
	if len(uid) == 0 {
		return *item, nil
	}
	donatedsumm := 0
	needsumm := 0
	var time time.Time

	sql := `SELECT fundraising.uid, fundraising.title, fundraising.longtitle, fundraising.description,  
	fundraising.result, fundraising.donated, fundraising.donatedsumm, fundraising.needsumm, fundraising.actions, 
	fundraising.active, fundraising.status, fundraising.flagstart, fundraising.flagfinish, fundraising.date, 
	fundraising.step, fundraising.likes, images.uids,
	COALESCE(fund.uid,'00000000-0000-0000-0000-000000000000') as fundUID, COALESCE(fund.title,'') as fundTitle, COALESCE(fund.description,'') as fundDescription, 
	COALESCE(fund.inn,'') as fundInn, COALESCE(fund.status,'') as fundStatus,
	COALESCE(address.uid,'00000000-0000-0000-0000-000000000000') as addressUID, COALESCE(address.country,'') as addressCountry, 
	COALESCE(address.city,'') as addressCity, COALESCE(address.postal,'') as addressPostal, COALESCE(address.address,'') as addressAddress,
	fundimage.uids as fundPics,
	COALESCE(contact.uid,'00000000-0000-0000-0000-000000000000') as contactUID, COALESCE(contact.person,'') as contactPerson, 
	COALESCE(contact.phone,'') as contactPhone, COALESCE(contact.email,'') as contactEmail, COALESCE(contact.website,'') as contactWeb, 
	COALESCE(contact.skype,'') as contactSkype
		from fundraising 
	
		LEFT JOIN crossfundsraising on crossfundsraising.raisinguid = fundraising.uid::varchar
		LEFT JOIN fund ON fund.uid::VARCHAR = crossfundsraising.funduid
		LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
			as fundimage on fundimage.owneruid = fund.uid::VARCHAR
	  	LEFT JOIN 
			(SELECT address.uid, address.country, address.city, address.postal, address.address, address.owneruid from address where address.ownertype = 'fund') as
			address ON address.owneruid = fund.uid::VARCHAR 
		LEFT JOIN 
			(SELECT contacts.uid, contacts.person, contacts.phone, contacts.email, contacts.website, contacts.skype, contacts.owneruid FROM contacts where contacts.ownertype = 'fund') as
			contact ON contact.owneruid = fund.uid::VARCHAR 	  
		LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid) 
		as images on images.owneruid = fundraising.uid::VARCHAR
		WHERE fundraising.uid = $1 order by step desc`

	// sql := `SELECT fundraising.uid, title, longtitle, description,  result, donated, donatedsumm, needsumm, actions,
	// fundraising.active, status, flagstart, flagfinish, fundraising.date,
	// step, likes, images.uids from fundraising
	// LEFT JOIN (select array_agg(uid) as uids, owneruid FROM crossimages GROUP BY owneruid)
	// as images on images.owneruid = fundraising.uid::VARCHAR
	// WHERE fundraising.uid = $1
	// order by step desc`

	var images []string
	var fundimages []string
	err := db.QueryRow(sql, uid).Scan(&item.UID, &item.Title, &item.Longtitle, &item.Description, &item.Result, &item.Donated, &donatedsumm, &needsumm,
		&item.ActionsCount, &item.Active, &item.Status, &item.Flagstart, &item.Flagfinish, &time, &item.Step, &item.Likes, &images,
		&item.Fund.UID, &item.Fund.Title, &item.Fund.Description, &item.Fund.Inn, &item.Fund.Status,
		&item.Fund.Address.UID, &item.Fund.Address.Country, &item.Fund.Address.City, &item.Fund.Address.Postal, &item.Fund.Address.Address, &fundimages,
		&item.Fund.Contacts.UID, &item.Fund.Contacts.Person, &item.Fund.Contacts.Phone, &item.Fund.Contacts.Email,
		&item.Fund.Contacts.Website, &item.Fund.Contacts.Skype)

	if err != nil && err != pgx.ErrNoRows {
		return *item, err
	}

	if len(images) > 0 {
		listImage := make([]*models.Images, 0)
		for _, image := range images {
			pic := &models.Images{}
			pic.Image = picsURL + "fundraising/" + image + ".jpg"
			pic.UID = image
			listImage = append(listImage, pic)
			// element is the element from someSlice for where we are
		}
		item.Images = listImage
	}

	if item.Images == nil {
		listImage := make([]*models.Images, 0)
		pic := &models.Images{}
		pic.Image = picsURL + models.NoPhoto
		pic.UID = ""
		listImage = append(listImage, pic)
		item.Images = listImage
	}

	if len(fundimages) > 0 {
		listImage := make([]*models.Images, 0)
		pic := &models.Images{}
		pic.Image = picsURL + "funds/" + item.Fund.UID + ".jpg"
		pic.UID = item.Fund.UID
		listImage = append(listImage, pic)
		// for _, image := range fundimages {
		// 	pic := &models.Images{}
		// 	// pic.Image = picsURL + "funds/" + image + ".jpg"
		// 	pic.Image = picsURL + "funds/" + item.Fund.UID + ".jpg"
		// 	pic.UID = image
		// 	listImage = append(listImage, pic)
		// 	// element is the element from someSlice for where we are
		// }

		item.Fund.Images = listImage
	}

	if item.Fund.Images == nil {
		listImage := make([]*models.Images, 0)
		pic := &models.Images{}
		pic.Image = picsURL + models.NoPhoto
		pic.UID = ""
		listImage = append(listImage, pic)
		item.Fund.Images = listImage
	}

	item.Needsumm = float64(float64(needsumm) / float64(100))

	item.Donatedsumm = float64(float64(donatedsumm) / float64(100))
	item.Date = time.Format("2006-01-02")
	item.IsLiked, err = likes.IsLiked(db, item.UID, "Fundraising", MyUserUID)
	if err != nil {
		item.IsLiked = false
	}

	likes, err1 := likes.GetMyCount(db, item.UID, "Fundraising")
	if err1 != nil {
		likes = 0
	}
	item.Likes = int64(likes)

	item.Sections, err = GetlistofSections(db, item.UID)
	if err != nil {

	} else {
		if len(item.Sections) > 0 {
			item.CurrentSection = item.Sections[0].SectionUID
		}
	}

	// Is paid for fundraising
	item.IsPaid = false
	if MyUserUID != "" {
		item.IsPaid, _ = IsPaidByUser(db, MyUserUID, item.UID)

	}

	// GetActions list
	Search := actions.Search{}
	Search.Paginator = false
	item.Actions, _, _ = actions.Get(db, picsURL, item.UID, Search, MyUserUID, Domain, "ALL")
	item.Maecenas, _ = actions.GetMaecenas(db, picsURL, item.UID)
	// Get a list of users for fundraising
	actusers := &models.ActusersAnswer{}
	actusers.HasNext = false
	pagin := &models.Paginator{}
	pagin.Search = ""

	actusers.Actusers, actusers.Total, err = Getlistusers(db, picsURL, item.UID, true, *pagin)
	if len(actusers.Actusers) < actusers.Total {
		actusers.HasNext = true
	}
	actusers.Count = len(actusers.Actusers)

	var actCount int64

	actCount, err = actions.ActionCount(db, item.UID)
	if err != nil && err != pgx.ErrNoRows {

	} else {
		item.ActionsCount = actCount
	}

	item.Users = *actusers

	return *item, nil
}

//GetImageList - get images for fundraising
func GetImageList(db *pgx.ConnPool, picsURL string, fundraising string) error {
	return nil
}

// IsPaidByUser - check for paid by user
func IsPaidByUser(db *pgx.ConnPool, usersUID string, fundraisingUID string) (bool, error) {
	isPaid := false
	err := db.QueryRow(`SELECT count(*)>0 as payd from orders where usersuid = $1 and fundraisingUID = $2`, usersUID, fundraisingUID).Scan(&isPaid)
	return isPaid, err
}

// IsPaidByUserFORActions - check for paid by user
func IsPaidByUserFORActions(db *pgx.ConnPool, usersUID string, actionUID string) (bool, error) {
	isPaid := false
	err := db.QueryRow(`SELECT count(*)>0 as payd from orders where usersuid = $1 and actionuid = $2`, usersUID, actionUID).Scan(&isPaid)
	return isPaid, err
}

// Getlistusers - get list of users of actions
func Getlistusers(db *pgx.ConnPool, picsURL string, actionsUID string, short bool, paginator models.Paginator) ([]*models.Actusers, int, error) {
	var Offset int
	var err error
	var rows *pgx.Rows
	WithSearch := false
	SearchSQL := ``
	LimitSQL := ``

	if short {
		LimitSQL = ` LIMIT 5 `
	} else {

		if paginator.Search != "" {
			WithSearch = true
			SearchSQL = ` AND Name like $2`
		}

		if paginator.HasPaginator {
			if WithSearch {
				LimitSQL = ` LIMIT $3 OFFSET $4`
			} else {
				LimitSQL = ` LIMIT $2 OFFSET $3`
			}
			Offset = paginator.CurrentPage*paginator.OnPage - paginator.OnPage
		}

	}

	sql := `
	SELECT count(users.uid) OVER() AS total_count, 
	COALESCE(users.uid,'00000000-0000-0000-0000-000000000000'), 
	COALESCE(users.firstname,'') || ' ' || 
	COALESCE(users.lastname,'') as Name,
	COALESCE(usersphotos.filename,'') as Photo, COALESCE(imagelink, '') as image
	FROM orders 
	LEFT JOIN users on users.uid::VARCHAR = orders.usersuid
	LEFT JOIN usersphotos on usersphotos.usersuid = users.uid
	LEFT JOIN socnet on socnet.usersuid = users.uid::VARCHAR
	WHERE orders.fundraisinguid = $1  ` + SearchSQL + `
	ORDER BY orders.date DESC
	` + LimitSQL
	list := make([]*models.Actusers, 0)

	if !short {
		if WithSearch && paginator.HasPaginator {
			rows, err = db.Query(sql, actionsUID, "%"+paginator.Search+"%", paginator.OnPage, Offset)
		}

		if !WithSearch && paginator.HasPaginator {
			rows, err = db.Query(sql, actionsUID, paginator.OnPage, Offset)
		}
		if !WithSearch && !paginator.HasPaginator {
			rows, err = db.Query(sql, actionsUID)
		}
	}
	if short {
		rows, err = db.Query(sql, actionsUID)
	}

	if err != nil && err != pgx.ErrNoRows {
		return list, 0, err
	}

	totalCount := 0
	for rows.Next() {

		item := &models.Actusers{}
		photo := ""
		image := ""
		err = rows.Scan(&totalCount, &item.UID, &item.Name, &photo, &image)

		if err != nil {
			return list, 0, err
		}
		item.Image = picsURL + string(models.ProfilesArea) + "/" + models.NoPhoto
		if len(photo) > 0 {
			item.Image = picsURL + string(models.ProfilesArea) + "/" + photo
		} else {
			if len(image) > 0 {
				item.Image = image
			}
		}

		list = append(list, item)

	}
	return list, totalCount, nil

}

// Getlistactionsusers - get list of users of actions
func Getlistactionsusers(db *pgx.ConnPool, picsURL string, actionsUID string, short bool, paginator models.Paginator) ([]*models.Actusers, int, error) {
	var Offset int
	var err error
	var rows *pgx.Rows
	WithSearch := false
	SearchSQL := ``
	LimitSQL := ``

	if short {
		LimitSQL = ` LIMIT 5 `
	} else {

		if paginator.Search != "" {
			WithSearch = true
			SearchSQL = ` AND Name like $2`
		}

		if paginator.HasPaginator {
			if WithSearch {
				LimitSQL = ` LIMIT $3 OFFSET $4`
			} else {
				LimitSQL = ` LIMIT $2 OFFSET $3`
			}
			Offset = paginator.CurrentPage*paginator.OnPage - paginator.OnPage
		}

	}

	sql := `
	SELECT count(users.uid) OVER() AS total_count, 
	COALESCE(users.uid,'00000000-0000-0000-0000-000000000000'), 
	COALESCE(users.firstname,'') || ' ' || 
	COALESCE(users.lastname,'') as Name,
	COALESCE(usersphotos.filename,'') as Photo, COALESCE(imagelink, '') as image
	FROM orders 
	LEFT JOIN users on users.uid::VARCHAR = orders.usersuid
	LEFT JOIN usersphotos on usersphotos.usersuid = users.uid
	LEFT JOIN socnet on socnet.usersuid = users.uid::VARCHAR
	WHERE orders.actionuid = $1  ` + SearchSQL + `
	ORDER BY orders.date DESC
	` + LimitSQL
	list := make([]*models.Actusers, 0)

	if !short {
		if WithSearch && paginator.HasPaginator {
			rows, err = db.Query(sql, actionsUID, "%"+paginator.Search+"%", paginator.OnPage, Offset)
		}

		if !WithSearch && paginator.HasPaginator {
			rows, err = db.Query(sql, actionsUID, paginator.OnPage, Offset)
		}
		if !WithSearch && !paginator.HasPaginator {
			rows, err = db.Query(sql, actionsUID)
		}
	}
	if short {
		rows, err = db.Query(sql, actionsUID)
	}

	if err != nil && err != pgx.ErrNoRows {
		return list, 0, err
	}

	totalCount := 0
	for rows.Next() {

		item := &models.Actusers{}
		photo := ""
		image := ""
		err = rows.Scan(&totalCount, &item.UID, &item.Name, &photo, &image)

		if err != nil {
			return list, 0, err
		}
		item.Image = picsURL + string(models.ProfilesArea) + "/" + models.NoPhoto
		if len(photo) > 0 {
			item.Image = picsURL + string(models.ProfilesArea) + "/" + photo
		} else {
			if len(image) > 0 {
				item.Image = image
			}
		}

		list = append(list, item)

	}
	return list, totalCount, nil

}

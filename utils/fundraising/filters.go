package fundraising

import (
	"github.com/jackc/pgx"
)

// GetFilter - get list of current sections
func GetFilter(db *pgx.ConnPool) ([]SectionFilter, []OtherFilter, error) {
	sect, err := getSections(db)
	if err != nil {
		return make([]SectionFilter, 0), make([]OtherFilter, 0), err
	}

	flt, err1 := otherFilter()
	if err1 != nil {
		return make([]SectionFilter, 0), make([]OtherFilter, 0), err
	}
	return sect, flt, nil
}

// GetSections - get list of current sections
func getSections(db *pgx.ConnPool) ([]SectionFilter, error) {

	list := make([]SectionFilter, 0)
	rows, err := db.Query(`SELECT uid, title FROM sections order by title DESC`)
	if err != nil && err != pgx.ErrNoRows {
		return list, err
	}

	for rows.Next() {
		item := &SectionFilter{}
		err = rows.Scan(&item.UID, &item.Name)
		item.Active = true
		if err != nil {
			return list, err
		}
		list = append(list, *item)
	}
	return list, nil
}

// OtherFilter - get list of current filters
func otherFilter() ([]OtherFilter, error) {
	list := make([]OtherFilter, 0)
	list = append(list, OtherFilter{Name: "Только акции", Active: false, UID: "onlyActions"})
	list = append(list, OtherFilter{Name: "Только аукционы", Active: false, UID: "onlyAuctions"})
	list = append(list, OtherFilter{Name: "Только сборы", Active: false, UID: "onlyFundraising"})
	list = append(list, OtherFilter{Name: "Показать все", Active: true, UID: "all"})
	return list, nil
}

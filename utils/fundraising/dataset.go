package fundraising

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crossimages"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

type (
	// Main - main structure for fundraising
	Main struct {
		Fundraising Fundraising
		ci          crossimages.Main
		db          *pgx.ConnPool
		log         logger.Logger
	}

	// Fundraising  - main structure
	Fundraising struct {
		UID            string                `json:"uid"`
		OwnerUID       string                `json:"owneruid"`
		CurrentSection string                `json:"sectionuid"`
		Sections       []*models.Sections    `json:"section"`
		Images         []*models.Images      `json:"images"`
		Maecenas       []*actions.Maecenas   `json:"maecenas"`
		Actions        []*actions.Actions    `json:"actions,omitempty"`
		Users          models.ActusersAnswer `json:"users"`
		Fund           Fund                  `json:"fund"`
		Title          string                `json:"title"`
		Longtitle      string                `json:"longTitle"`
		Description    string                `json:"description"`
		Result         string                `json:"result"`
		Status         string                `json:"status"`
		Step           string                `json:"step"`
		Likes          int64                 `json:"likes"`
		IsLiked        bool                  `json:"isLiked"`
		IsPaid         bool                  `json:"isPaid"`
		ActionsCount   int64                 `json:"actionsCount"`
		Active         bool                  `json:"active"`
		Flagstart      bool                  `json:"flagstart"`
		Flagfinish     bool                  `json:"flagfinish"`
		Needsumm       float64               `json:"needSumm"`
		Donatedsumm    float64               `json:"donatedSumm"`
		Donated        int64                 `json:"donated"`
		Date           string                `json:"date"`
		IsFundraising  bool                  `json:"isFundraising"`
		IsAction       bool                  `json:"isAction"`
		IsAuction      bool                  `json:"isAuction"`
		WinnerUID      string                `json:"winner"`
		CriteriasDate  bool                  `json:"criteriasDate"`
		Days           int                   `json:"days"`
		Hours          int                   `json:"hours"`
	}

	// Filters ...
	Filters struct {
		Sections []SectionFilter `json:"sections"`
		Other    []OtherFilter   `json:"filters"`
	}

	// SectionFilter ...
	SectionFilter struct {
		UID    string `json:"UID"`
		Name   string `json:"title"`
		Active bool   `json:"active"`
	}

	// OtherFilter ...
	OtherFilter struct {
		UID    string `json:"UID"`
		Name   string `json:"title"`
		Active bool   `json:"active"`
	}

	// FilterAnswer ...
	FilterAnswer struct {
		List    Filters `json:"filter"`
		Actions string  `json:"message"`
		Success bool    `json:"success"`
	}
	// // Images - structure for pics for fundraising
	// Images struct {
	// 	UID   string `json:"UID"`
	// 	Image string `json:"image"`
	// }

	// // Contacts - structure for contacts
	// Contacts struct {
	// 	UID     string `json:"UID"`
	// 	Person  string `json:"person"`
	// 	Phone   string `json:"phone"`
	// 	Email   string `json:"email"`
	// 	Website string `json:"website"`
	// 	Skype   string `json:"skype"`
	// }

	// // Address - structure for contacts
	// Address struct {
	// 	UID     string `json:"UID"`
	// 	Country string `json:"country"`
	// 	Postal  string `json:"postal"`
	// 	City    string `json:"city"`
	// 	Address string `json:"address"`
	// }

	// Fund - structure for fund
	Fund struct {
		UID         string           `json:"UID"`
		Title       string           `json:"title"`
		Description string           `json:"description"`
		Inn         string           `json:"inn"`
		Status      string           `json:"status"`
		Images      []*models.Images `json:"images"`
		Contacts    models.Contacts  `json:"contacts"`
		Address     models.Address   `json:"address"`
	}

	// Search - structure for search and pagination
	Search struct {
		Search     string `json:"search"`
		Current    int    `json:"current"`
		Onpage     int    `json:"onPage"`
		Paginator  bool   `json:"paginator"`
		OnlyActive bool   `json:"onlyActive"`
		NotActive  bool   `json:"notActive"`
	}

	// Answer - answer for API with list of sections
	Answer struct {
		List       []*Fundraising `json:"list"`
		Success    bool           `json:"success"`
		Action     string         `json:"message"`
		Count      int            `json:"count,omitempty"`
		TotalCount int            `json:"totalCount,omitempty"`
		HasNext    bool           `json:"hasNext"`
	}

	// AnswerOne - answer for API with info about current fundraising
	AnswerOne struct {
		Info    Fundraising `json:"info"`
		Success bool        `json:"success"`
		Action  string      `json:"message"`
	}
)

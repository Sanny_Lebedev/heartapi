package ibase

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	"github.com/jackc/pgx"
)

// IsExist - check for existing
func (i *Sections) IsExist() bool {
	return IsExist(i.DB, i.Log, Sectionname, i.Item.UID)
}

// IsExist - check for existing
func (i *Fund) IsExist() bool {
	return IsExist(i.DB, i.Log, Fundname, i.Fund.UID)
}

// IsExist - check for existing
func (i *Fundraising) IsExist() bool {
	return IsExist(i.DB, i.Log, Fundraisingname, i.UID)
}

// IsExist - check for existing
func (i *Actions) IsExist() bool {
	return IsExist(i.DB, i.Log, Actionsname, i.UID)
}

// IsExist - check for existing
func (i *Prize) IsExist() bool {
	return IsExist(i.DB, i.Log, Prizename, i.UID)
}

// Maecenas - check for existing of maecenas
func (i *Maecenas) IsExist() bool {
	var exist bool
	err := i.DB.QueryRow(`SELECT  COUNT(uid)>0 FROM usersroletab WHERE uid = $1 AND role = $2 AND active = true`, i.UID, user.RoleSponsor).Scan(&exist)
	if err != nil || err == pgx.ErrNoRows {
		i.Log.Error().Err(err).Msg("Error with getting IsExist")
		return false
	}
	return exist
}

// IsExist - check for existing
func IsExist(db *pgx.ConnPool, log logger.Logger, itemName ItemName, uid string) bool {
	var exist bool
	err := db.QueryRow(`SELECT COUNT(uid)>0 from `+string(itemName)+` WHERE uid = $1`, uid).Scan(&exist)

	if err != nil || err == pgx.ErrNoRows {
		log.Error().Err(err).Msg("Error with getting IsExist")
		return false
	}
	return exist
}

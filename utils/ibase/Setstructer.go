package ibase

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/fundraising"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/funds"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/sections"
	"github.com/jackc/pgx"
)

// SetStruct - initila setting struct
func (i *Sections) SetStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) Sections {

	Answer := Sections{}
	Answer.DB = DB
	Answer.Log = Log
	Answer.Item = sections.Section{UID: UID}
	return Answer
}

// SetStruct - initila setting struct
func (i *Fund) SetStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) Fund {
	Answer := Fund{}
	Answer.DB = DB
	Answer.Log = Log
	Answer.Fund = funds.Fund{UID: UID}
	return Answer
}

// Maecenas - initila setting struct
func (i *Maecenas) SetStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) Maecenas {
	Answer := Maecenas{}
	Answer.DB = DB
	Answer.Log = Log
	Answer.UID = UID
	return Answer
}

// SetStruct - initila setting struct
func (i *Actions) SetStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) Actions {
	Answer := Actions{}
	Answer.DB = DB
	Answer.Log = Log
	Answer.UID = UID
	return Answer
}

// SetStruct - initila setting struct
func (i *Prize) SetStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) Prize {
	Answer := Prize{}
	Answer.DB = DB
	Answer.Log = Log
	Answer.UID = UID
	return Answer
}

// SetStruct - initila setting struct
func (i *Fundraising) SetStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) Fundraising {
	Answer := Fundraising{}
	Answer.DB = DB
	Answer.Log = Log
	Answer.Fundraising = fundraising.Fundraising{UID: UID}
	return Answer
}

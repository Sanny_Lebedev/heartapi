package ibase

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

func (i *Fund) IsExistActive() bool {
	return IsExistActive(i.DB, i.Log, Fundname, i.Fund.UID)
}

func (i *Fundraising) IsExistActive() bool {
	return IsExistActive(i.Conf.DB, i.Conf.Log, Fundraisingname, i.UID)
}

func IsExistActive(db *pgx.ConnPool, log logger.Logger, itemName ItemName, uid string) bool {
	var exist bool
	err := db.QueryRow(`SELECT COUNT(uid)>0 from `+string(itemName)+` WHERE uid = $1 and active = true`, uid).Scan(&exist)
	if err != nil || err == pgx.ErrNoRows {
		log.Error().Err(err).Msg("Error with getting IsExistActive")
		return false
	}
	return exist
}

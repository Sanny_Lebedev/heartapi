package ibase

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/fundraising"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/funds"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/sections"
	"github.com/jackc/pgx"
)

//IsExister - is exits in DB
type IsExister interface {
	IsExist()
	SetStruct(DB *pgx.ConnPool, Log logger.Logger, UID string)
	ReinitStruct(DB *pgx.ConnPool, Log logger.Logger, UID string)
	IsExistActive()
}

type (
	// Conf - config structure
	Conf struct {
		DB  *pgx.ConnPool
		Log logger.Logger
	}

	// ItemName is name of table
	ItemName string

	// Maecenas ...
	Maecenas struct {
		Conf
		UID string
	}
	// Actions ...
	Actions struct {
		Conf
		UID string
	}

	// Actions ...
	Prize struct {
		Conf
		UID string
	}

	// Sections is a sections pointer
	Sections struct {
		Conf
		Item sections.Section
	}
	// Fund is a Fund pointer
	Fund struct {
		Conf
		funds.Fund
	}
	// Fundraising is a Fundraising pointer
	Fundraising struct {
		Conf
		fundraising.Fundraising
	}
)

// String returns string representation of the RoleName
func (in ItemName) String() string {
	return string(in)
}

// Set sets role name
func (in *ItemName) Set(name string) {
	*in = ItemName(name)
}

const (
	//Section - name of section table
	Sectionname ItemName = "sections"
	//Fund - name of fund table
	Fundname ItemName = "fund"
	//Fundraising - name of fundraising table
	Fundraisingname ItemName = "fundraising"

	//Actionsname - name of actions table
	Actionsname ItemName = "actions"

	//Prizename - name of actions table
	Prizename ItemName = "prize"
)

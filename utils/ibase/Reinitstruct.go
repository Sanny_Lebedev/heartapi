package ibase

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/sections"
	"github.com/jackc/pgx"
)

// ReinitStruct - reinitila setting struct
func (i *Sections) ReinitStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) {

	i.DB = DB
	i.Log = Log
	i.Item = sections.Section{UID: UID}

}

// ReinitStruct - reinitila setting struct
func (i *Fund) ReinitStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) {
	i.DB = DB
	i.Log = Log
	i.UID = UID
}

// ReinitStruct - reinitila setting struct
func (i *Fundraising) ReinitStruct(DB *pgx.ConnPool, Log logger.Logger, UID string) {
	i.DB = DB
	i.Log = Log
	i.UID = UID

}

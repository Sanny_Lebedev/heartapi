package user

import (
	"strings"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

// NetUser - interfaces for VK and FB
type NetUser interface {
	AddToDataBase(u *User, Payload app.SocnetLoginPayload) (string, error)
	SetCorBirthday(u *User, uid string) error
}

// AddUserToDB - union adding to DB
func (u *User) AddUserToDB(Payload app.SocnetLoginPayload, user NetUser) (string, error) {
	return user.AddToDataBase(u, Payload)
}

// SetCorrectBirthday - correct birthday
func (u *User) SetCorrectBirthday(uid string, user NetUser) error {
	return user.SetCorBirthday(u, uid)
}

// SetCorBirthday - correcting birthday for FB user
func (user UserData) SetCorBirthday(u *User, uid string) error {
	return correctBirthday(uid, user.Birthday, u.log, u.db)
}

// SetCorBirthday - correcting birthday for VK user
func (user UserDataVK) SetCorBirthday(u *User, uid string) error {
	return correctBirthday(uid, user.Birthday, u.log, u.db)
}

func correctBirthday(uid, birthday string, log logger.Logger, db *pgx.ConnPool) error {
	if birthday != "" {

		Birthday, err := time.Parse("02/01/2006", birthday)
		if err != nil {

			Birthday, err = time.Parse("02.01.2006", birthday)
			if err != nil {
				Birthday, err = time.Parse("02.1.2006", birthday)
				if err != nil {
					log.Error().Err(err).Msg("Error with parsing new birthday")
					return nil
				}
			}

		}
		_, err = db.Exec(`UPDATE users SET birthday = $1 where uid = $2`, Birthday, uid)
		if err != nil {
			log.Error().Err(err).Msg("Error with saving new birthday")
			return err
		}
	}
	return nil
}

// // SetCorBirthday - correcting birthday for FB user
// func (user UserData) SetCorBirthday(u *User, uid string) error {

// 	Birthday, err := time.Parse("01/02/2006", user.Birthday)

// 	if err != nil {
// 		u.log.Error().Err(err).Msg("Error with parsing new birthday from FB")
// 		return nil
// 	}

// 	_, err = u.db.Exec(`UPDATE users SET birthday = $1 where uid = $2`, Birthday, uid)
// 	if err != nil {
// 		u.log.Error().Err(err).Msg("Error with saving new birthday from FB")
// 		return err
// 	}

// 	return nil
// }

// SetCorBirthday - correcting birthday for VK user

// func (user UserDataVK) SetCorBirthday(u *User, uid string) error {

// 	Birthday, err := time.Parse("01/02/2006", user.Birthday)

// 	if err != nil {
// 		u.log.Error().Err(err).Msg("Error with parsing new birthday from VK")
// 		return nil
// 	}

// 	_, err = u.db.Exec(`UPDATE users SET birthday = $1 where uid = $2`, Birthday, uid)
// 	if err != nil {
// 		u.log.Error().Err(err).Msg("Error with saving new birthday from FB")
// 		return err
// 	}

// 	return nil
// }

// AddToDataBase - Add information about a user with a social network login
func (user UserData) AddToDataBase(u *User, Payload app.SocnetLoginPayload) (string, error) {
	var err error
	u.Info.Email = user.Email
	u.Provider = Payload.Provider

	//FIO := strings.Split(user.Username, " ")
	u.Info.Name = user.FirstName
	u.Info.Family = user.LastName
	u.RoleName = "user"
	u.RoleActive = true
	picture := u.SocNetCFG.Facebook.Endpoint + Payload.Socnetuid + `/picture?type=normal`
	if u.UID != "" {
		// Just update data
		_, err = u.db.Exec(`UPDATE socnet SET socnetuid = $2, provider = $3, email = $4, token = $5, imagelink = $6, date = CURRENT_TIMESTAMP
		WHERE usersuid = $1`, u.UID, Payload.Socnetuid, Payload.Provider, u.Info.Email, Payload.Token, picture)
		if err != nil {
			return "", err
		}

		return u.UID, nil

	}

	newUserUID, errDBL := u.AddToDB("")
	if errDBL != nil && errDBL != pgx.ErrNoRows {
		return "", errDBL
	}

	_, err = u.db.Exec(`INSERT INTO socnet (usersuid, socnetuid, provider, email, token, imagelink, date) 
		VALUES ($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP)`, newUserUID, Payload.Socnetuid, Payload.Provider, user.Email, Payload.Token, picture)
	if err != nil {
		return "", err
	}

	return newUserUID, nil
}

// AddToDataBase - add new DB to base for users from VK
func (user UserDataVK) AddToDataBase(u *User, Payload app.SocnetLoginPayload) (string, error) {
	var err error

	if Payload.Email == nil {
		u.log.Info().Msg("Wrong email from payload")
		u.Info.Email = ""
	} else {
		u.Info.Email = *Payload.Email
 	}
	u.Provider = Payload.Provider

	u.Info.Name = user.FirstName
	u.Info.Family = user.LastName
	u.RoleName = "user"
	u.RoleActive = true

	user.Birthday = strings.Replace(user.Birthday, ".", "/", -1)
	picture := user.Photo

	if u.UID != "" {
		// Just update data
		_, err = u.db.Exec(`UPDATE socnet SET socnetuid = $2, provider = $3, email = $4, token = $5, imagelink = $6, date = CURRENT_TIMESTAMP 
		WHERE usersuid = $1`, u.UID, Payload.Socnetuid, Payload.Provider, u.Info.Email, Payload.Token, picture)
		if err != nil {
			return "", err
		}

		return u.UID, nil

	}

	// Saving new data
	newUserUID, errDBL := u.AddToDB("")
	if errDBL != nil && errDBL != pgx.ErrNoRows {
		return "", errDBL
	}

	_, err = u.db.Exec(`INSERT INTO socnet (usersuid, socnetuid, provider, email, token, imagelink, date) 
		VALUES ($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP)`, newUserUID, Payload.Socnetuid, Payload.Provider, u.Info.Email, Payload.Token, picture)
	if err != nil {
		return "", err
	}

	return newUserUID, nil
}

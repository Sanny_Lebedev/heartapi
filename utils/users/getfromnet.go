package user

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
)

// SocialNerwork - interfaces for VK and FB
type SocialNerwork interface {
	GetInfo(u *User) (string, error)
}

// // AddUserToDB - union adding to DB
// func (u *User) AddUserToDB(Payload app.SocnetLoginPayload, user NetUser) (string, error) {
// 	return user.AddToDataBase(u, Payload)
// }

//GetInfo - get users birthday from FB
func (n VKNetwork) GetInfo(u *User) (NetUser, error) {

	Result := InfoFromVK{}
	g := NewGateway(n.UID, n.AccessToken, u.SocNetCFG.VK.Endpoint, u.log)

	params := g.authParamsVK(false)
	params.Set("fields", u.SocNetCFG.VK.Fields)

	params.Set("v", u.SocNetCFG.VK.Ver)
	fmt.Println(g.endpoint + "?" + params.Encode())
	req, err := http.NewRequest(http.MethodGet, g.endpoint+"?"+params.Encode(), nil)
	if err != nil {
		return UserDataVK{}, errors.Wrap(err, "failed to create req")
	}
	res, err := g.httpClient.Do(req)
	if err != nil {
		return UserDataVK{}, errors.Wrap(err, "failed to send request")
	}
	defer res.Body.Close()

	err = json.NewDecoder(res.Body).Decode(&Result)
	if Result.Error.ErrorCode != 0 {
		err = errors.New(Result.Error.ErrorMsg)
		u.log.Error().Err(err).Msg("Error with VK auth")
		return UserDataVK{}, err
	}

	if err != nil {
		u.log.Error().Err(err).Msg("Error with VK auth")
		return UserDataVK{}, errors.Wrap(err, "failed to decode response")
	}

	return Result.Response[0], nil
}

// GetInfo ...
func (n FBNetwork) GetInfo(u *User) (NetUser, error) {
	var err error
	Result := UserData{}
	g := NewGateway(n.UID, n.AccessToken, u.SocNetCFG.Facebook.Endpoint, u.log)

	params := g.authParams(false)
	params.Set("fields", u.SocNetCFG.Facebook.Fields)

	req, err := http.NewRequest(http.MethodGet, g.endpoint+n.UID+"?"+params.Encode(), nil)
	if err != nil {
		return Result, errors.Wrap(err, "failed to create req")
	}
	res, err := g.httpClient.Do(req)
	if err != nil {
		return Result, errors.Wrap(err, "failed to send request")
	}
	defer res.Body.Close()

	// var result struct {
	// 	Birthday     string      `json:"birthday"`
	// 	Name         string      `json:"name"`
	// 	Email        string      `json:"email"`
	// 	FirstName    string      `json:"first_name"`
	// 	LastName     string      `json:"last_name"`
	// 	ErrorCode    json.Number `json:"errorCode"`
	// 	ErrorMessage string      `json:"errorMessage"`
	// }

	err = json.NewDecoder(res.Body).Decode(&Result)

	if Result.Error.Code != 0 {

		err = errors.New(Result.Error.Message)
		u.log.Error().Err(err).Msg("failed with FB Auth")
		return Result, err
	}
	if err != nil {
		return Result, errors.Wrap(err, "failed to decode response")
	}

	return Result, nil
}

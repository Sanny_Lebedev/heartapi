package user

import (
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crypto"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

func (u *User) AddNewPass(UID string, password string) error {
	salt, err := crypto.GenerateSalt()
	if err != nil {
		return errors.Wrap(err, "failed to generate salt")
	}
	_, err = u.db.Exec(`UPDATE users SET password=$1, salt=$2 WHERE uid = $3`, password, salt, UID)

	return err
}

// AddToDB - Direct addition of a new user to the database
func (u *User) AddToDB(password string) (string, error) {
	var uid string
	var t time.Time
	var err2 error
	salt, err := crypto.GenerateSalt()
	if err != nil {
		return "", errors.Wrap(err, "failed to generate salt")
	}

	if u.Info.Birthday != "" {
		t, err2 = time.Parse("2006-01-02", u.Info.Birthday)
	} else {
		t, err2 = time.Parse("2006-01-02", "1900-01-01")
	}

	// t, err2 := time.Parse("2006-01-02", u.Info.Birthday)

	if err2 != nil {
		return uid, errors.Wrap(err2, "failed to parse birthday input")
	}

	err = u.db.QueryRow(
		`INSERT INTO users (firstName, lastName, email, phone, password, salt, regdata, temppass, birthday)
		VALUES ($1, $2, $3, $4, $5, $6, now(), $7, $8)
		RETURNING uid`,
		u.Info.Name, u.Info.Family, u.Info.Email, u.Info.Phone, password, salt, ``, t).Scan(&uid)
	return uid, err
}

// AddToRoletab - add new user's role
func (u *User) AddToRoletab() error {
	inDB := false
	err := u.db.QueryRow(`SELECT count(*)>0 as ans FROM UsersRoleTab WHERE uid = $1 AND role = $2`, u.UID, u.RoleName).Scan(&inDB)
	if err != nil {
		return err
	}
	if !inDB {
		_, err = u.db.Exec(
			`INSERT INTO UsersRoleTab (uid, role, active) VALUES ($1, $2, $3)`,
			u.UID, u.RoleName, u.RoleActive,
		)

	}
	return err
}

// SaveRefreshToken - adding new record to DB
func (u *User) SaveRefreshToken(jid string) error {
	_, err := u.db.Exec(
		`DELETE FROM reftokens WHERE uid = $1 AND role = $2`,
		u.UID, u.RoleName,
	)
	if err != nil {
		return errors.Wrap(err, "delete query failed")
	}

	_, err = u.db.Exec(
		`INSERT INTO reftokens ( uid, jti, role ) VALUES ( $1, $2, $3 ) `,
		u.UID, jid, u.RoleName,
	)
	if err != nil {
		return errors.Wrap(err, "insert query failed")
	}
	return nil
}

//GetUserRolebyID - gettign infromation about current user by phone number
func (u *User) GetUserRoles() error {

	rows, err := u.db.Query(
		`SELECT usersroletab.role, usersroletab.active
		FROM users INNER JOIN usersroletab ON usersroletab.uid=users.uid
		WHERE users.uid = $1`,
		u.UID)

	if err != nil {
		if err == pgx.ErrNoRows {
			return errors.New("no roles found")
		}
		return errors.Wrap(err, "query failed")
	}

	roles := make([]*Role, 0)

	for rows.Next() {
		role := &Role{}
		err = rows.Scan(&role.Name, &role.Active)
		if err != nil {
			return errors.Wrap(err, "failed to scan query result")
		}
		roles = append(roles, role)
	}
	u.Roles = roles
	return nil
}

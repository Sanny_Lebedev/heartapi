package user

// func (u *User) SetCorrectBD(uid string, data UserData) error {
// 	Birthday, err := time.Parse("01/02/2006", data.Birthday)

// 	if err != nil {
// 		u.log.Error().Err(err).Msg("Error with parsing new birthday from FB")
// 		return err
// 	}

// 	_, err = u.db.Exec(`UPDATE users SET birthday = $1 where uid = $2`, Birthday, uid)
// 	if err != nil {
// 		u.log.Error().Err(err).Msg("Error with saving new birthday from FB")
// 		return err
// 	}

// 	return nil
// }

// //GetBDFromFB - get users birthday from FB
// func (u *User) GetBDFromFB(uid, access_token string) (UserData, error) {
// 	Result := UserData{}
// 	g := NewGateway(uid, access_token, "https://graph.facebook.com/v3.1/", u.log)

// 	params := g.authParams(false)
// 	params.Set("fields", "birthday, name, email,  picture, first_name, last_name")

// 	req, err := http.NewRequest(http.MethodGet, g.endpoint+uid+"?"+params.Encode(), nil)
// 	if err != nil {
// 		return Result, errors.Wrap(err, "failed to create req")
// 	}
// 	res, err := g.httpClient.Do(req)
// 	if err != nil {
// 		return Result, errors.Wrap(err, "failed to send request")
// 	}
// 	defer res.Body.Close()
// 	// var result struct {
// 	// 	Birthday     string      `json:"birthday"`
// 	// 	Name         string      `json:"name"`
// 	// 	Email        string      `json:"email"`
// 	// 	FirstName    string      `json:"first_name"`
// 	// 	LastName     string      `json:"last_name"`
// 	// 	ErrorCode    json.Number `json:"errorCode"`
// 	// 	ErrorMessage string      `json:"errorMessage"`
// 	// }

// 	err = json.NewDecoder(res.Body).Decode(&Result)

// 	if err != nil {
// 		return Result, errors.Wrap(err, "failed to decode response")
// 	}

// 	fmt.Println(Result)

// 	return Result, nil
// }

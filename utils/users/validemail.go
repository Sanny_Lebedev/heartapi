package user

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crypto"
	"github.com/jackc/pgx"
)

// InitMail - initial structure for mail
func InitMail(email string, config Conf, Gate MailGateway) Email {
	return Email{
		MailGateway: Gate,
		Mail: Mail{
			Code:   "",
			Getter: email,
			Link:   Gate.LinkURL,
		},
		Config: config,
	}
}

// GenCode - generate salt string
func GenCode() (string, error) {
	salt, err := crypto.GenerateSalt()
	if err != nil {
		return "", err
	}

	return salt, nil
}

// MakeValidCode - making code for validation
func (e Email) MakeValidCode(email string) (Email, error) {
	mail := &e
	isExist, err := e.Config.CheckMail(email)
	if err != nil && !isExist {
		return *mail, err
	}

	err = e.Config.DB.QueryRow(`SELECT count(email)>0 as isexist FROM mailverify WHERE email=$1`, email).Scan(&isExist)
	if err != nil {
		e.Config.Log.Error().Err(err).Msg("Error with getting count of mails from mailverify")
		return *mail, err
	}
	var code string
	code, err = GenCode()
	if err != nil {
		e.Config.Log.Error().Err(err).Msg("Error with salt generation")
		return *mail, err
	}

	if isExist {
		// Update with new salt
		_, err := e.Config.DB.Exec(`UPDATE mailverify SET salt = $1 WHERE email = $2`, code, email)
		if err != nil {
			e.Config.Log.Error().Err(err).Msg("Error with saving salt to mailverify")
			return *mail, err
		}
		mail.Mail.Code = code
		err = mail.SendSalt()
		if err != nil {
			e.Config.Log.Error().Err(err).Msg("Error with saving mail to queue")
			return *mail, err
		}
		return *mail, nil
	}
	// insert salt
	_, err = e.Config.DB.Exec(`INSERT INTO mailverify (salt, email, date, datever) VALUES($1, $2, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)`, code, email)
	if err != nil {
		e.Config.Log.Error().Err(err).Msg("Error with saving salt to mailverify")
		return *mail, err
	}
	mail.Mail.Code = code
	err = mail.SendSalt()
	if err != nil {
		e.Config.Log.Error().Err(err).Msg("Error with saving mail to queue")
		return *mail, err
	}

	return *mail, nil
}

// SendSalt - Send to queue
func (e Email) SendSalt() error {
	var err error
	_, err = e.Config.DB.Exec(`INSERT INTO queue_classic_jobs (q_name, action, getter, message, hold) VALUES ($1, $2, $3, $4, $5)`, "send", "mailConf", e.Mail.Getter, e.Mail.Code, false)

	if err != nil {
		e.Config.Log.Error().Err(err).Msg("Error with sending task to queue")
		return err
	}
	return nil
}

// CheckMail - check for email count
func (c Conf) CheckMail(email string) (bool, error) {
	var isExist bool
	err := c.DB.QueryRow(`select count(email)>0 as cnt from users where email=$1`, email).Scan(&isExist)
	if err == pgx.ErrNoRows {
		return false, nil
	}

	if err != nil {
		c.Log.Error().Err(err).Msg("Error with getting count of email")
		return false, err
	}

	return isExist, nil
}

// CheckMailCode - check email and salt
func (c Conf) CheckMailCode(email, code string) (bool, error) {
	var UID string
	err := c.DB.QueryRow(`select uid FROM mailverify WHERE email=$1 AND salt=$2`, email, code).Scan(&UID)
	if err == pgx.ErrNoRows {
		c.Log.Error().Msg("No rows for mailverify")
		return false, nil
	}
	c.Log.Info().Str("UID", UID).Msg("No rows for mailverify")
	if err != nil {
		c.Log.Error().Err(err).Msg("Error with getting count of email")
		return false, err
	}

	_, err = c.DB.Exec(`UPDATE mailverify SET verify = true, datever=CURRENT_TIMESTAMP WHERE uid=$1`, UID)
	if err != nil {
		c.Log.Error().Err(err).Msg("Error with saving new status of email")
		return false, err
	}

	return true, nil
}

// GetEmailStatus - get status
func (c Conf) GetEmailStatus(email string) (bool, error) {
	isVerify := false
	err := c.DB.QueryRow(`SELECT verify FROM mailverify WHERE email=$1`, email).Scan(&isVerify)
	if err != nil {
		c.Log.Error().Err(err).Msg("Error with getting status of email from mailverify")
		return false, err
	}

	return isVerify, nil
}

// GetMailByUID - get users email by UID
func (c Conf) GetMailByUID(UID string) (string, error) {
	var email string
	err := c.DB.QueryRow(`SELECT email FROM users WHERE uid=$1`, UID).Scan(&email)
	if err != nil {
		c.Log.Error().Err(err).Msg("Error with getting email for user")
		return "", err
	}

	return email, nil
}

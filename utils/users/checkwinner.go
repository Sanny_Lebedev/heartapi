package user

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

// CheckWinnerAuction ...
func (c Conf) CheckWinnerAuction(UID string) ([]WinnerReport, error) {
	// Search for need to pay
	rows, err := c.DB.Query(`SELECT winners.actionsuid, winners.ticketuid,
	winners.iswaiting, winners.paylink, 
	prize.title, prize.place, prize.description,
	images.imagelinks, images.uids,
	tickets.amount/100 as amount, tickets.ispaid, tickets.iswin, tickets.isauction,
	((tickets.isauction and tickets.ispaid)or(tickets.isauction=false and tickets.ispaid)) as realwinner,
	COALESCE(maecenas.firstname || ' '  || maecenas.lastname, '') as MaecenasName
	FROM winners
	LEFT JOIN prize on prize.ticketuid = winners.ticketuid
	LEFT JOIN tickets on tickets.uid::VARCHAR = winners.ticketuid
	LEFT JOIN users as maecenas on prize.maecenas = maecenas.uid::VARCHAR 
	LEFT JOIN (select array_remove(array_agg(imagelink),NULL) as imagelinks, array_remove(array_agg(uid),NULL) as uids, owneruid FROM crossimages GROUP BY owneruid) 
		as images on images.owneruid = prize.uid::VARCHAR
	WHERE winneruid = $1 AND (winners.iswin = true OR winners.iswaiting = true) AND payforwinners = false`, UID)
	TotalActions := make([]WinnerReport, 0)
	if err == pgx.ErrNoRows {
		return TotalActions, nil
	}

	if err != nil {
		c.Log.Error().Err(err).Msg("Error with getting list for winner")
		return TotalActions, err
	}

	for rows.Next() {
		action := &WinnerReport{}
		var images, imageuids []string
		err = rows.Scan(&action.ActionUID, &action.TicketUID, &action.Flags.IsWaiting, &action.PayLink, &action.Prize.Title, &action.Prize.Place, &action.Prize.Description,
			&images, &imageuids, &action.TicketAmount, &action.Flags.IsPaid, &action.Flags.IsWin, &action.Flags.IsAuction, &action.Flags.IsRealWin, &action.Maecenas)
		if err != nil {
			c.Log.Error().Err(err).Msg("Error with scaning list for winner")
			return TotalActions, err
		}

		Images := make([]*models.Images, 0)
		for i, pic := range images {
			Image := &models.Images{}
			Image.Image = pic
			Image.UID = imageuids[i]
			Images = append(Images, Image)
		}
		action.Prize.Images = Images

		TotalActions = append(TotalActions, *action)
	}

	return TotalActions, nil
}

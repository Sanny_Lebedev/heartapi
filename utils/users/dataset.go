package user

import (
	"encoding/json"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

type (
	//UserData - structure of data from FB
	UserData struct {
		Birthday     string      `json:"birthday"`
		Name         string      `json:"name"`
		Email        string      `json:"email"`
		FirstName    string      `json:"first_name"`
		LastName     string      `json:"last_name"`
		ErrorCode    json.Number `json:"errorCode"`
		ErrorMessage string      `json:"errorMessage"`
		Error        FBError     `json:"error"`
	}

	//UserDataVK - structure of data from VK
	UserDataVK struct {
		ID        json.Number `json:"id"`
		FirstName string      `json:"first_name"`
		LastName  string      `json:"last_name"`
		Birthday  string      `json:"bdate"`
		Photo     string      `json:"photo_200_orig"`
	}

	// FBError - struct of Error for FB
	FBError struct {
		Message      string `json:"message"`
		Type         string `json:"type"`
		Code         int    `json:"code"`
		ErrorSubcode int    `json:"error_subcode"`
		FbtraceID    string `json:"fbtrace_id"`
	}

	// InfoFromVK - main JSON for VK answer
	InfoFromVK struct {
		Error    VKError      `json:"error"`
		Response []UserDataVK `json:"response"`
	}

	// VKError - struct of error for VK answer
	VKError struct {
		ErrorCode     int               `json:"error_code"`
		ErrorMsg      string            `json:"error_msg"`
		RequestParams []VKRequestParams `json:"request_params"`
	}

	// VKRequestParams - params set for VK
	VKRequestParams struct {
		Key   string `json:"key"`
		Value string `json:"value"`
	}

	//Socnet ...
	SocNetCFG struct {
		Facebook Fb `json:"Facebook" cfg:"Facebook"`
		VK       Vk `json:"VK" cfg:"VK"`
		Google   Gl `json:"Google" cfg:"Google"`
	}

	// Fb - structure for FB API options
	Fb struct {
		Endpoint string `json:"endpoint" cfg:"Endpoint"`
		Fields   string `json:"fields" cfg:"Fields"`
	}

	// Vk - structure for FB API options
	Vk struct {
		Endpoint string `json:"endpoint" cfg:"Endpoint"`
		Fields   string `json:"fields" cfg:"Fields"`
		Ver      string `json:"ver" cfg:"Ver"`
	}

	// Gl - structure for FB API options
	Gl struct {
		Endpoint string `json:"endpoint" cfg:"Endpoint"`
		Fields   string `json:"fields" cfg:"Fields"`
	}

	// FBNetwork ...
	FBNetwork struct {
		AccessToken string `json:"accessToken"`
		UID         string `json:"UID"`
	}
	// VKNetwork ...
	VKNetwork struct {
		AccessToken string `json:"accessToken"`
		UID         string `json:"UID"`
	}
	// GNetwork ...
	GNetwork struct {
		AccessToken string `json:"accessToken"`
		UID         string `json:"UID"`
	}

	// Conf ...
	Conf struct {
		DB  *pgx.ConnPool
		Log logger.Logger
	}

	// Email ...
	Email struct {
		Config      Conf
		MailGateway MailGateway
		Mail        Mail
	}

	Mail struct {
		Code   string `json:"code"`
		Getter string `json:"getter"`
		Link   string `json:"link"`
	}

	// MailGateway ...
	MailGateway struct {
		From    string `json:"from" cfg:"From"`
		Pass    string `json:"pass" cfg:"Pass"`
		Serv    string `json:"serv" cfg:"Serv"`
		Port    string `json:"port" cfg:"Port"`
		LinkURL string `json:"linkUrl" cfg:"LinkUrl"`
	}

	//WinnerReport ...
	WinnerReport struct {
		TicketUID    string      `json:"ticketUID"`
		ActionUID    string      `json:"actionUID"`
		Flags        WinnerFlags `json:"flags"`
		Maecenas     string      `json:"maecenas"`
		TicketAmount float64     `json:"ticketAmount"`
		PayLink      string      `json:"paymentLink"`
		Prize        WinnerPrize `json:"prize"`
	}

	//WinnerPrize ...
	WinnerPrize struct {
		Title       string           `json:"title"`
		Place       int              `json:"place"`
		Description string           `json:"description"`
		Images      []*models.Images `json:"images"`
	}

	//Image ...

	//WinnerFlags ...
	WinnerFlags struct {
		IsWaiting bool `json:"isWaiting"`
		IsPaid    bool `json:"isPaid"`
		IsWin     bool `json:"isWin"`
		IsAuction bool `json:"isAuction"`
		IsRealWin bool `json:"isRealWin"`
	}

	//AnswerForWinner ...
	AnswerForWinner struct {
		Success bool           `db:"success" json:"success"`
		Action  string         `db:"message" json:"message"`
		List    []WinnerReport `json:"list" omitempty`
	}
)

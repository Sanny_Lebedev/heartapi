package user

import (
	"strings"
	"time"

	"net/http"
	"net/url"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
)

// Gateway is FB gateway
type Gateway struct {
	uid        string
	access_key string
	endpoint   string
	httpClient http.Client
	log        logger.Logger
}

// NewGateway returns new Gateway instance
func NewGateway(uid, access_key, endpoint string, log logger.Logger) *Gateway {
	g := &Gateway{
		uid:        uid,
		access_key: access_key,
		endpoint:   endpoint,
		httpClient: http.Client{
			Timeout: 30 * time.Second,
		},
		log: log,
	}
	return g
}

func (g *Gateway) authParams(encode bool) url.Values {
	if !encode {
		return url.Values{
			"client_id":    {g.uid},
			"access_token": {g.access_key},
		}
	}
	return url.Values{
		"client_id":    {g.uid},
		"access_token": {strings.Replace(g.access_key, "&", "%26", -1)},
	}
}

func (g *Gateway) authParamsVK(encode bool) url.Values {
	if !encode {
		return url.Values{
			"uids":         {g.uid},
			"access_token": {g.access_key},
		}
	}
	return url.Values{
		"uids":         {g.uid},
		"access_token": {strings.Replace(g.access_key, "&", "%26", -1)},
	}
}

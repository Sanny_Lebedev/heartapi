package user

import (
	"math/rand"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/repositories"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

type Socnet string

//String returns string representation of the Socnet
func (rn Socnet) String() string {
	return string(rn)
}

// Set sets socnet name
func (rn *Socnet) Set(net string) {
	*rn = Socnet(net)
}

// RoleName is a role name
type RoleName string
type RoleTitle string

// String returns string representation of the RoleName
func (rn RoleName) String() string {
	return string(rn)
}

// String returns string representation of the RoleTitle
func (rn RoleTitle) String() string {
	return string(rn)
}

// Set sets role title
func (rn *RoleTitle) Set(role string) {
	*rn = RoleTitle(role)
}

// Set sets role name
func (rn *RoleName) Set(role string) {
	*rn = RoleName(role)
}

const (
	// NetFB ...
	NetFB Socnet = "facebook"

	// NetVK ...
	NetVK Socnet = "VK"

	// RoleUser ...
	RoleUser RoleName = "user"
	// RoleSponsor ...
	RoleSponsor RoleName = "sponsor"
	// RoleAdmin ...
	RoleAdmin RoleName = "admin"

	//RoleTitleUser ...
	RoleTitleUser RoleTitle = "Пользователь"
	//RoleTitleSponsor ...
	RoleTitleSponsor RoleTitle = "Спонсор"
	//RoleTitleAdmin ...
	RoleTitleAdmin RoleTitle = "Администратор"
)

// Role is a user role
type Role struct {
	Name   RoleName  `db:"usersroletab.role" json:"role"`
	Title  RoleTitle `json:"roleTitle"`
	Active bool      `db:"usersroletab.active" json:"active"`
}

// User is a main user struct
type User struct {
	UID        string `json:"uid"`
	Info       ShortInfo
	Password   Password
	Token      Token
	Roles      []*Role
	RoleName   RoleName `json:"usersRole"`
	RoleActive bool     `json:"roleActive"`
	db         *pgx.ConnPool
	log        logger.Logger
	isAdmin    bool
	isUser     bool
	isSponsor  bool
	Config     Config
	Provider   string `json:"provider"`
	SocNetCFG  SocNetCFG
}

//Config - local configutration for user
type Config struct {
}

// Password contains all user passwords
type Password struct {
	Main             string
	Temporary        string
	Getting          string
	TemporaryExpires time.Time
}

// Token contains all user tokens
type Token struct {
	Refresh string `json:"refreshToken"`
	Auth    string `json:"accessToken"`
}

// ShortInfo is a user info
type ShortInfo struct {
	Name     string `json:"username"`
	Family   string `json:"userfamily"`
	Email    string `json:"usermail"`
	Phone    string `json:"userphone"`
	Birthday string `json:"birthday"`
}

type ProfileInfo struct {
	UID    string `json:"uid"`
	Name   string `json:"firstName"`
	Family string `json:"lastName"`
	Email  string `json:"email"`
	Phone  string `json:"phone"`
	Photo  string `json:"photo"`
}

// NewUser returns new User instance
func NewUser(db *pgx.ConnPool, log logger.Logger) *User {
	u := &User{
		db:  db,
		log: log,
	}
	return u
}

// NewUserSN returns new User instance for socnet
func NewUserSN(db *pgx.ConnPool, log logger.Logger, socNet SocNetCFG) *User {
	u := &User{
		db:        db,
		log:       log,
		SocNetCFG: socNet,
	}
	return u
}

// CheckingForSimilar - Checking for a similar entry
func (u *User) CheckingForSimilar() (bool, error) {
	var exists bool
	err := u.db.QueryRow(
		`SELECT EXISTS (SELECT 1 FROM users WHERE users.email = $1)`, u.Info.Email).Scan(&exists)
	return exists, err
}

// CheckingForEmptyUsers - Checking for an empty table
func (u *User) CheckingForEmptyUsers() bool {
	var exists bool
	_ = u.db.QueryRow(`SELECT count(*)>0 FROM users limit 1`).Scan(&exists)
	return !exists
}

// CheckOldPass - compare 2 passwords
func (u *User) CheckOldPass(oldpass, UID string) bool {
	var passfromDB string
	errGet := u.db.QueryRow(`SELECT password FROM users where UID = $1`, UID).Scan(&passfromDB)

	if errGet != nil {
		return false
	}

	if err := bcrypt.CompareHashAndPassword([]byte(passfromDB), []byte(oldpass)); err == nil {
		return true
	}
	return false
}

//CodeThePassword - generate hash of password
func (u *User) CodeThePassword(pass, uid string) error {

	hash, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		return errors.Wrap(err, "failed to generate hash")
	}
	newpass := string(hash)

	_, err = u.db.Exec(`UPDATE users SET password = $1 WHERE uid = $2`, newpass, uid)
	return err

}

//CheckUID - check uid for existing in DB
func (u *User) CheckUID(uid string) (bool, error) {
	isOk := false
	err := u.db.QueryRow(`SELECT count(*)=1 FROM users WHERE uid = $1`, uid).Scan(&isOk)
	return isOk, err

}

//SaveNewUser - Saving data about a new user in the database
func (u *User) SaveNewUser() (bool, error) {
	// If new user sending permanent password - saving this datas to main user's tab and making login procedure

	var newpass string
	// Getting password from request -> We keep the permanent password
	if u.Password.Getting != "" {
		hash, err := bcrypt.GenerateFromPassword([]byte(u.Password.Getting), bcrypt.DefaultCost)
		if err != nil {
			return false, errors.Wrap(err, "failed to generate hash")
		}
		newpass = string(hash)
	}

	uidnew, err := u.AddToDB(newpass)
	if err != nil {
		return false, err
	}
	u.Password.Main = newpass
	if uidnew != "" {
		u.UID = uidnew
	}
	return true, err
}

// LoadFromSigninContext populates user data from SigninAuthContext
func (u *User) LoadFromSigninContext(ctx *app.SigninAuthContext) {
	payload := ctx.Payload
	u.RoleActive = true
	// u.DriversLicense = payload.
	if payload.Apptype == nil {
		u.RoleName = RoleUser
	} else {
		if (*payload.Apptype == RoleSponsor.String()) || (*payload.Apptype == RoleAdmin.String()) {
			u.RoleName.Set(*payload.Apptype)
			u.RoleActive = false
		} else {
			u.RoleName = RoleUser
		}

	}

	u.Info.Email = strings.ToLower(payload.Email)

	if payload.Password != `` {
		u.Password.Main = ""
		u.Password.Getting = payload.Password
		u.Password.Temporary = ""
	}
}

// LoadFromRegisterContext - populate User with data from RegisterAuthContext
func (u *User) LoadFromRegisterContext(ctx *app.RegisterAuthContext) {
	payload := ctx.Payload
	u.RoleActive = true
	u.Password = Password{
		Main:      payload.Password,
		Getting:   payload.Password,
		Temporary: "",
	}
	u.Info = ShortInfo{
		Phone:    ``,
		Name:     payload.FirstName,
		Family:   payload.LastName,
		Email:    strings.ToLower(payload.Email),
		Birthday: payload.Birthday,
	}

	if payload.Apptype == nil {
		u.RoleName = RoleUser
	} else {
		if (*payload.Apptype == RoleSponsor.String()) || (*payload.Apptype == RoleAdmin.String()) {
			u.RoleName.Set(*payload.Apptype)
			u.RoleActive = false
		} else {
			u.RoleName = RoleUser
		}
	}
}

//GetUserRolebyID - gettign infromation about current user by phone number
func GetUserRolebyID(uid string, db *pgx.ConnPool) ([]*Role, error) {

	rows, err := db.Query(
		`SELECT usersroletab.role, usersroletab.active
		FROM users INNER JOIN usersroletab ON usersroletab.uid=users.uid
		WHERE users.uid = $1`,
		uid,
	)
	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, errors.New("no roles found")
		}
		return nil, errors.Wrap(err, "query failed")
	}

	roles := make([]*Role, 0)

	for rows.Next() {
		role := &Role{}
		err = rows.Scan(&role.Name, &role.Active)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan query result")
		}
		roles = append(roles, role)
	}

	return roles, nil
}

// LoadRolesFromDB - gettign infromation about current user by phone number
func (u *User) LoadRolesFromDB(uid string) error {
	var err error
	u.UID = uid
	u.Roles, err = GetUserRolebyID(u.UID, u.db)
	if err != nil {
		return err
	}
	return err
}

//GetByPhone - gettign infromation about current user by phone number
func (u *User) GetByPhone() error {
	//and usersroletab.role like $2
	err := u.db.QueryRow(`SELECT users.uid, users.first_name, users.last_name, users.email, usersroletab.role,
		usersroletab.active, password, temppass, endtemppass
	FROM users LEFT JOIN usersroletab on usersroletab.uid = users.uid
	WHERE phone = $1 and usersroletab.role like $2`,
		u.Info.Phone, u.RoleName,
	).Scan(
		&u.UID, &u.Info.Name, &u.Info.Family, &u.Info.Email, &u.RoleName,
		&u.RoleActive, &u.Password.Main, &u.Password.Temporary, &u.Password.TemporaryExpires,
	)
	if err != nil {
		return errors.Wrap(err, "query failed")
	}
	return nil
}

//GetByUID - getting infromation about current user by uid
func (u *User) GetByUID(uid string) error {

	var birthday time.Time

	err := u.db.QueryRow(`SELECT users.uid, users.phone, users.firstName, users.lastName, users.email, password, temppass, endtemppass, 
	COALESCE(users.birthday,'1960-01-01') as birthday
	FROM users WHERE users.uid = $1`,
		uid).Scan(
		&u.UID, &u.Info.Phone, &u.Info.Name, &u.Info.Family, &u.Info.Email, &u.Password.Main, &u.Password.Temporary, &u.Password.TemporaryExpires,
		&birthday)

	u.Info.Birthday = birthday.Format("2006-01-02")

	if err != nil {
		// Unauthorisation
		return errors.Wrap(err, "WRONG TOKEN")
	}
	return nil
}

// GetUserByUID gets a user by ID
func GetUserByUID(db *pgx.ConnPool, uid string) (*User, error) {
	const sqlstr = `
	select 
		firstname,
		lastname,
		phone,
		email
	FROM users where users.uid = $1`
	var user User
	err := db.QueryRow(sqlstr, uid).Scan(&user.Info.Name, &user.Info.Family, &user.Info.Phone, &user.Info.Email)
	return &user, err
}

// CheckForRole - check for role
func (u *User) CheckForRole(role RoleName) (bool, error) {
	isOK := false
	err := u.db.QueryRow(`SELECT COUNT(*)>0 FROM users 
	LEFT JOIN usersroletab on usersroletab.uid = users.uid
	WHERE email = $1 and usersroletab.role like $2 and usersroletab.active = true`,
		u.Info.Email, role).Scan(&isOK)

	if err != nil {
		return false, errors.Wrap(err, "query failed")
	}

	return isOK, err
}

// CheckForRoleNA - check for role for not active users
func (u *User) CheckForRoleNA(role RoleName) (bool, error) {
	isOK := false
	err := u.db.QueryRow(`SELECT COUNT(*)>0 FROM users 
	LEFT JOIN usersroletab on usersroletab.uid = users.uid
	WHERE email = $1 and usersroletab.role like $2 and usersroletab.active = false`,
		u.Info.Email, role).Scan(&isOK)

	if err != nil {
		return false, errors.Wrap(err, "query failed")
	}

	return isOK, err
}

// GetByEmail - getting information about current user by email
func (u *User) GetByEmail() error {
	var bdate time.Time
	err := u.db.QueryRow(`SELECT users.uid, users.phone, users.firstName, users.lastName, users.email, password, temppass, endtemppass, birthday
	FROM users WHERE users.email = $1`, u.Info.Email).Scan(
		&u.UID, &u.Info.Phone, &u.Info.Name, &u.Info.Family, &u.Info.Email, &u.Password.Main, &u.Password.Temporary, &u.Password.TemporaryExpires,
		&bdate)
	if err != nil {
		return err
	}

	u.Info.Birthday = bdate.Format("2006-01-02")
	return nil
}

//Devicetoken - getting devices tokens for push notification
func (u *User) Devicetoken(token string, opersys string, role string) error {
	var err error
	isDBL := false

	err = u.db.QueryRow(`SELECT COUNT(*)>0 as ANS FROM pushnavigator WHERE useruid = $4 AND token = $1 AND opersys = $2 AND role = $3`, token, opersys, role, u.UID).Scan(&isDBL)
	if err != nil {
		return errors.Wrap(err, "failed to query row")
	}
	if !isDBL {

		_, err = u.db.Exec(`INSERT INTO pushnavigator (useruid, token, opersys, date, role) VALUES ($1, $2, $3, CURRENT_TIMESTAMP, $4)`, u.UID, token, opersys, role)
	} else {
		_, err = u.db.Exec(`UPDATE pushnavigator SET date = CURRENT_TIMESTAMP, useruid = $1  WHERE token = $2 AND opersys = $3 AND role = $4`, u.UID, token, opersys, role)
	}
	return err
}

//AddTempPass - adding temp pass to user's tab
func (u *User) AddTempPass(tempPass string) error {
	_, err := u.db.Exec(
		`UPDATE users SET temppass = $2, endtemppass = CURRENT_TIMESTAMP + INTERVAL '30 min' where uid = $1`,
		u.UID, tempPass,
	)
	if err != nil {
		return errors.Wrap(err, "query failed")
	}
	return nil

}

//CheckUIDByEmailNoSocnet - get user by email
func (u *User) CheckUIDByEmailNoSocnet(email string) (string, error) {
	UID := ""
	err := u.db.QueryRow(`SELECT users.uid FROM users WHERE email = $1 AND users.uid::VARCHAR not in (select socnet.usersuid from socnet WHERE email=$1)`, email).Scan(&UID)
	return UID, err
}

//CheckUIDByEmail - get user by email
func (u *User) CheckUIDByEmail(email string) (string, error) {
	UID := ""
	err := u.db.QueryRow(`SELECT uid FROM users WHERE email = $1`, email).Scan(&UID)
	return UID, err
}

//MakeTempPass - making tempPassword for user
func (u *User) MakeTempPass() (string, error) {
	r1 := rand.New(rand.NewSource(time.Now().UnixNano()))
	TempPassCode := strconv.Itoa(r1.Intn(1000000))
	hashTMP, err := bcrypt.GenerateFromPassword([]byte(TempPassCode), bcrypt.DefaultCost)
	if err != nil {
		return "", errors.Wrap(err, "failed to generate temporary password")
	}
	err = u.AddTempPass(string(hashTMP))
	if err != nil {
		return "", errors.Wrap(err, "failed to add temporary password")
	}
	return TempPassCode, nil
}

//Checktemprecover - check temp password for recover
func (u *User) Checktemprecover(temppass, UID string) bool {
	var tp string
	var endtp time.Time

	err := u.db.QueryRow(`SELECT temppass, endtemppass FROM users WHERE uid=$1`, UID).Scan(&tp, &endtp)
	if err != nil {
		return false
	}

	if &tp != nil && repositories.InTimeValid(endtp, time.Now()) {
		if err := bcrypt.CompareHashAndPassword([]byte(tp), []byte(temppass)); err == nil {
			return true
		}
	}

	return false

}

//CheckPassword - Compare the incoming password with passwords from DB
func (u *User) CheckPassword() bool {
	authPassCHECK := false
	if &u.Password.Temporary != nil && repositories.InTimeValid(u.Password.TemporaryExpires, time.Now()) {
		if err := bcrypt.CompareHashAndPassword([]byte(u.Password.Temporary), []byte(u.Password.Getting)); err == nil {
			return true
		}
	}

	if authPassCHECK == false {
		if err := bcrypt.CompareHashAndPassword([]byte(u.Password.Main), []byte(u.Password.Getting)); err == nil {
			return true
		}
	}

	return false

}

// CheckNoZeroPass - check for no zero pass
func (u *User) CheckNoZeroPass(UID string) bool {
	ans := false
	err := u.db.QueryRow(`SELECT COUNT(*)=0 FROM users WHERE uid = $1 AND password=''`, UID).Scan(&ans)
	if err != nil {
		return false
	}
	return ans
}

// FillTheRoles - filling the current user's role's status
func (u *User) FillTheRoles() error {

	u.isUser = false
	u.isSponsor = false
	u.isAdmin = false

	for _, value := range u.Roles {
		switch value.Name {
		case RoleUser:
			u.isUser = true
		case RoleAdmin:
			u.isAdmin = true
		case RoleSponsor:
			u.isSponsor = true
		default:
			return errors.New("unknown role")
		}
	}
	return nil
}

// Is - sending active role's status
func (u *User) Is(role RoleName) bool {

	switch role {
	case RoleUser:
		//return true
	case RoleAdmin:
		//return true
	case RoleSponsor:
		//return true
	default:
		return false
	}
	tempRole := u.RoleName
	u.RoleName = role

	isOK, err := u.CheckForRole(role)
	if err != nil {
		u.RoleName = tempRole
		return false
	}

	if !isOK {
		u.RoleName = tempRole
		u.RoleActive = false
		return false
	}
	u.RoleName = tempRole
	u.RoleActive = true
	return true
}

// IsnotActive - sending active role's status
func (u *User) IsnotActive(role RoleName) bool {

	switch role {
	case RoleUser:
		//return true
	case RoleAdmin:
		//return true
	case RoleSponsor:
		//return true
	default:
		return false
	}

	u.RoleName = role
	isOK, err := u.CheckForRoleNA(role)
	if err != nil {
		return false
	}

	if u.RoleActive {
		return false
	}

	return isOK
}

// GetUserFromSocnet - Getting information about a user with a social network login
func (u *User) GetUserFromSocnet(socnetuid string, provider Socnet) (string, string, error) {
	var err error
	var usersUID string
	var Email string
	err = u.db.QueryRow(`SELECT usersuid, email FROM socnet WHERE provider=$1 AND socnetuid=$2`, provider, socnetuid).Scan(&usersUID, &Email)
	return usersUID, Email, err
}

// AddUserToSocnetVK - Add information about a user with a social network login
func (u *User) AddUserToSocnetVK(Payload app.SocnetLoginPayload, user UserDataVK) (string, error) {
	var err error
	if Payload.Email == nil {
		u.log.Info().Msg("Wrong email from payload")
		return "", nil
	}
	u.Info.Email = *Payload.Email
	u.Info.Phone = ""

	//FIO := strings.Split(user.Username, " ")

	u.Info.Name = user.FirstName
	u.Info.Family = user.LastName
	u.RoleName = "user"
	u.RoleActive = true
	user.Birthday = strings.Replace(user.Birthday, ".", "/", -1)
	Birthday, err := time.Parse("02/1/2006", user.Birthday)

	newUserUID, errDBL := u.AddToDB("")
	if errDBL != nil && errDBL != pgx.ErrNoRows {
		return "", errDBL
	}
	picture := user.Photo

	_, err = u.db.Exec(`INSERT INTO socnet (usersuid, socnetuid, provider, email, token, imagelink, date) 
		VALUES ($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP)`, newUserUID, Payload.Socnetuid, Payload.Provider, u.Info.Email, Payload.Token, picture)
	if err != nil {
		return "", err
	}

	_, err = u.db.Exec(`UPDATE users SET birthday = $1 where uid = $2`, Birthday, newUserUID)
	if err != nil {
		u.log.Error().Err(err).Msg("Error with saving new birthday from FB")
		return "", err
	}

	return newUserUID, nil
}

// AddUserToSocnet - Add information about a user with a social network login
//func (u *User) AddUserToSocnet(socnetuid string, provider Socnet, email string, token string, imagelink string, name string) (string, error) {
func (u *User) AddUserToSocnet(Payload app.SocnetLoginPayload, user UserData) (string, error) {
	var err error
	u.Info.Email = user.Email
	u.Info.Phone = ""

	//FIO := strings.Split(user.Username, " ")

	u.Info.Name = user.FirstName
	u.Info.Family = user.LastName
	u.RoleName = "user"
	u.RoleActive = true

	newUserUID, errDBL := u.AddToDB("")
	if errDBL != nil && errDBL != pgx.ErrNoRows {
		return "", errDBL
	}
	picture := `https://graph.facebook.com/v3.1/` + Payload.Socnetuid + `/picture?type=normal`

	_, err = u.db.Exec(`INSERT INTO socnet (usersuid, socnetuid, provider, email, token, imagelink, date) 
	VALUES ($1, $2, $3, $4, $5, $6, CURRENT_TIMESTAMP)`, newUserUID, Payload.Socnetuid, Payload.Provider, user.Email, Payload.Token, picture)
	if err != nil {
		return "", err
	}

	return newUserUID, nil
}

// CheckTheRole  - checking active role for login
func (u *User) CheckTheRole(uid string) (string, error) {
	var err error
	var role string
	err = u.db.QueryRow(`SELECT role FROM usersroletab WHERE uid=$1 AND active=true`, uid).Scan(&role)
	return role, err
}

// CheckInSocnetwork - Checking the existence of a user on a token from social networks
func (u *User) CheckInSocnetwork(socnetuid string, provider Socnet) (string, bool, error) {
	var err error
	var usersuid string
	isExist := false
	err = u.db.QueryRow(`SELECT usersuid FROM socnet WHERE provider=$1 AND socnetuid=$2`, provider, socnetuid).Scan(&usersuid)
	if err == pgx.ErrNoRows {
		usersuid = ""
	} else {
		isExist = true
	}

	return usersuid, isExist, err
}

// ChangeRoleActivity - change active status of role
func (u *User) ChangeRoleActivity(UID, Role string, active bool) error {

	roleExist, err := u.CheckIfRoleExist(UID, Role)
	if err != pgx.ErrNoRows && err != nil {
		return err
	}

	if roleExist {
		_, err = u.db.Exec(`UPDATE usersroletab SET active = $1 WHERE uid=$2 AND role = $3`, active, UID, Role)
		if err != pgx.ErrNoRows && err != nil {
			return err
		}
		return nil
	}

	_, err = u.db.Exec(`INSERT INTO usersroletab (uid, role, active) VALUES ($1, $2, $3)`, UID, Role, active)
	return err
}

// CheckIfRoleExist - checking for role of user
func (u *User) CheckIfRoleExist(UID string, Role string) (bool, error) {
	isExist := false
	err := u.db.QueryRow(`SELECT COUNT(*)>0 as ans FROM usersroletab WHERE uid=$1 AND role=$2`, UID, Role).Scan(&isExist)
	return isExist, err
}

// CheckIfUserExist - checking for role of user
func (u *User) CheckIfUserExist(UID string) (bool, error) {
	if UID == "" {
		return false, nil
	}

	isExist := false
	err := u.db.QueryRow(`SELECT COUNT(*)>0 as ans FROM users WHERE uid=$1`, UID).Scan(&isExist)
	return isExist, err
}

//Setuserrole ...
func (u *User) Setuserrole(UID string, Role string) error {

	isExist := false
	actStatus := false
	err := u.db.QueryRow(`SELECT COUNT(*)>0 as ans FROM usersroletab WHERE uid=$1 AND role=$2`, UID, Role).Scan(&isExist)
	if err != nil {
		return err
	}
	if isExist {
		_, err := u.db.Exec(`DELETE FROM usersroletab WHERE uid=$1 AND role=$2`, UID, Role)
		if err != nil {
			return err
		}
	}

	if Role == string(RoleUser) {
		actStatus = true
	}
	_, err = u.db.Exec(`INSERT INTO usersroletab (uid, role, active) VALUES ($1, $2, $3)`, UID, Role, actStatus)

	return err

}

//DeleteUsersProfile - delete user
func (u *User) DeleteUsersProfile(uid string) error {
	var err error
	_, err = u.db.Exec(`DELETE FROM users WHERE uid = $1`, uid)
	if err != nil {
		return err
	}

	_, err = u.db.Exec(`DELETE FROM usersroletab WHERE uid = $1`, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	_, err = u.db.Exec(`DELETE FROM socnet WHERE usersuid = $1`, uid)
	if err != nil && err != pgx.ErrNoRows {
		return err
	}

	_, err = u.db.Exec(`DELETE FROM usersphotos WHERE usersuid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

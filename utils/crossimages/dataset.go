package crossimages

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

type (
	// Main - main structure for fundraising
	Main struct {
		db   *pgx.ConnPool
		log  logger.Logger
		Area string
	}

	// // Images - structure for pics for fundraising
	// Images struct {
	// 	UID   string `json:"UID"`
	// 	Image string `json:"image"`
	// }
)

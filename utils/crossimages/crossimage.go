package crossimages

import (
	"os"
	"path/filepath"

	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/uploads"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

const (
	//Area - area name for os
	Area = "actions"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, Log logger.Logger, area string) *Main {

	u := &Main{
		db:   db,
		log:  Log,
		Area: area,
	}

	return u
}

// DeleteFRPhotos - delete all photos from db
func (s *Main) DeleteFRPhotos(uid string) error {

	// Get array of photos
	rows, err := s.db.Query(`SELECT uid from crossimages WHERE owneruid = $1`, uid)
	if err == pgx.ErrNoRows {
		return nil
	}
	if err != nil {
		return err
	}

	for rows.Next() {
		var uid string
		var err error
		err = rows.Scan(&uid)
		if err != nil {
			return err
		}
		err = uploads.DeletePhotoFile(uid+".jpg", s.Area)
		if err != nil {
			return err
		}
	}

	// Deleting from DB
	_, err = s.db.Exec(`DELETE FROM crossimages WHERE owneruid = $1`, uid)
	return err
}

// DeleteImageByUID - deleting Image by image UID
func (s *Main) DeleteImageByUID(imageuid string) error {
	_, err := s.db.Exec(`DELETE FROM crossimages WHERE uid = $1`, imageuid)
	return err
}

// SaveImage - saving image to DB
func (s *Main) SaveImage(senderuid, owneruid string) (models.Images, error) {

	var err error
	ans := &models.Images{}

	sql := `INSERT INTO crossimages (senderuid, owneruid, ownertype, active , date) 
	VALUES ($1, $2, $3, true, CURRENT_TIMESTAMP) RETURNING uid`
	err = s.db.QueryRow(sql, senderuid, owneruid, s.Area).Scan(&ans.UID)

	if err != nil {
		return *ans, errors.Wrap(err, "query failed")
	}
	ans.Image = ans.UID + ".jpg"
	sql = `UPDATE crossimages SET imagename = $2, imagelink = $2 WHERE uid = $1`
	_, err = s.db.Exec(sql, &ans.UID, ans.Image)
	return *ans, err

}

// FileExist - check for file
func (s *Main) FileExist(filename string) bool {
	pathtofile := filepath.Join(binpath.BinaryPath(), "images/"+s.Area)
	if _, err := os.Stat(filepath.Join(pathtofile, filename+".jpg")); !os.IsNotExist(err) {
		return true
	}
	return false
}

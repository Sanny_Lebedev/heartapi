package likes

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

type (
	// Main - main structure for fundraising
	Main struct {
		MyLike Like
		db     *pgx.ConnPool
		log    logger.Logger
	}
	// Like ...
	Like struct {
		OwnerUID  string `json:"ownerUID"`
		OwnerType string `json:"ownerType"`
		SenderUID string `json:"senderUID"`
		Count     int    `json:"count"`
	}

	// AnswerOne - answer for API with info about current Action
	AnswerOne struct {
		MyLike  Like   `json:"likes"`
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}
)

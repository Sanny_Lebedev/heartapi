package likes

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"github.com/jackc/pgx"
)

const (
	//Area - area name for os
	Area = "likes"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, ownerUID string, ownerType string, senderUID string, Log logger.Logger) *Main {

	u := &Main{
		db:  db,
		log: Log,
		MyLike: Like{
			OwnerUID:  ownerUID,
			OwnerType: ownerType,
			SenderUID: senderUID,
		},
	}

	return u
}

// Delete - delete from db
func (m *Main) Delete() error {
	var err error
	_, err = m.db.Exec(`DELETE from likes WHERE owneruid = $1 and ownertype = $2 and senderuid = $3`, m.MyLike.OwnerUID, m.MyLike.OwnerType, m.MyLike.SenderUID)
	if err == pgx.ErrNoRows {
		return nil
	}
	return err
}

// Insert - get count of likes
func (m *Main) Insert() error {
	var err error
	_, err = m.db.Exec(`INSERT INTO likes (owneruid, ownertype, senderuid ) VALUES ($1, $2, $3)`, m.MyLike.OwnerUID, m.MyLike.OwnerType, m.MyLike.SenderUID)
	return err
}

// IsExist - check for exist
func (m *Main) IsExist() (bool, error) {
	return IsLiked(m.db, m.MyLike.OwnerUID, m.MyLike.OwnerType, m.MyLike.SenderUID)
}

// Getcount - get count of likes
func (m *Main) Getcount() (int, error) {
	return GetMyCount(m.db, m.MyLike.OwnerUID, m.MyLike.OwnerType)
}

//IsLiked - check is user already send like for object
func IsLiked(db *pgx.ConnPool, OwnerUID, OwnerType, SenderUID string) (bool, error) {
	var err error
	isExist := false
	if OwnerType == "" {
		err = db.QueryRow(`SELECT COUNT(*)>0 as CNT FROM likes WHERE owneruid = $1 and senderuid = $2`, OwnerUID, SenderUID).Scan(&isExist)
	} else {
		err = db.QueryRow(`SELECT COUNT(*)>0 as CNT FROM likes WHERE owneruid = $1 and ownertype = $2 and senderuid = $3`, OwnerUID, OwnerType, SenderUID).Scan(&isExist)
	}

	return isExist, err
}

//GetMyCount - get count of likes for object
func GetMyCount(db *pgx.ConnPool, OwnerUID, OwnerType string) (int, error) {
	var err error
	Count := 0
	if OwnerType == "" {
		err = db.QueryRow(`SELECT COUNT(*) as CNT FROM likes WHERE owneruid = $1`, OwnerUID).Scan(&Count)
	} else {
		err = db.QueryRow(`SELECT COUNT(*) as CNT FROM likes WHERE owneruid = $1 and ownertype = $2`, OwnerUID, OwnerType).Scan(&Count)
	}

	return Count, err
}

package queue

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"github.com/jackc/pgx"
)

const (
	// Addcard - q_name for queue table
	Addcard = "addcard"

	// Addcard - q_name for queue table
	Pay = "pay"

	// ActionCheck - action for queue table
	ActionCheck = "check"
)

type (
	//Conf - main structure for fundraising
	Conf struct {
		DB  *pgx.ConnPool
		Log logger.Logger
	}

	// Task ...
	Task interface {
		Add(Conf) error
	}

	// Newcard ...
	Newcard struct {
		UID          string `json:"UID"`
		OrderUID     string `json:"orderUID"`
		BankOrderUID string `json:"bankOrderUID"`
	}

	// NewPay ...
	NewPay struct {
		UID          string `json:"UID"`
		OrderUID     string `json:"orderUID"`
		BankOrderUID string `json:"bankOrderUID"`
	}
)

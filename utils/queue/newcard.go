package queue

// Add - add new job for check new card to queue
func (j *Newcard) Add(conf Conf) error {
	_, err := conf.DB.Exec(`INSERT INTO  queue_classic_jobs (q_name, action, getter, message, hold) VALUES ($1, $2, $3, $4, $5)`, Addcard, ActionCheck, j.OrderUID, j.BankOrderUID, false)
	return err
}

// Add - add new job for check new card to queue
func (j *NewPay) Add(conf Conf) error {
	_, err := conf.DB.Exec(`INSERT INTO  queue_classic_jobs (q_name, action, getter, message, hold) VALUES ($1, $2, $3, $4, $5)`, Pay, ActionCheck, j.OrderUID, j.BankOrderUID, false)
	return err
}

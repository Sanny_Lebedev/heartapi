package history

import (
	"fmt"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

//SQLString ...
type SQLString string

func (s SQLString) String() string {
	return string(s)
}

func makeSQLstring(str string) *SQLString {
	s := SQLString(str)
	return &s
}

// GetLiteHistory - get full history
func (m *Main) GetLiteHistory(UID string, search Search) (*AnswerHistoryLite, error) {
	Answer := AnswerHistoryLite{}
	SQL := `
WITH actions_refined AS  (
SELECT 	
CASE
WHEN COALESCE(tickets.actionsuid,'') != '' THEN 'Action'
ELSE 'Fundraising'
END 
AS typeName,
CASE
WHEN COALESCE(tickets.actionsuid,'') != '' THEN tickets.actionsuid
ELSE orders.fundraisinguid
END 
AS typeUID,
orders.amount, 
COALESCE(actions.description,fundraising.description) as desctiption,
COALESCE(actions.needsumm,fundraising.needsumm)/100 as needsumm,
COALESCE(tickets.isauction,false) as isauction, 
COALESCE(tickets.amount,0)/100 as ticketamount, 
COALESCE(tickets.iswin, false) as iswin,
COALESCE(sections.uid,'00000000-0000-0000-0000-000000000000') as sectionuid, 
COALESCE(sections.title,'') as sectionstitle,
COALESCE(users.uid,'00000000-0000-0000-0000-000000000000') as maecenasUID, 
COALESCE(users.firstname,'') || ' ' || COALESCE(users.lastname,'') as maecenasName,
COALESCE(users.instagram,'') as maecenasinst,
COALESCE(users.description,'') as maecenasdesc,
COALESCE(usersphotos.filename,'') as maecenasPhoto,
COALESCE(socnet.imagelink,'') as maecenasPic,
COALESCE(actions.active, false) as actionactive,
COALESCE(actions.criteriasdate, false) as critdate,
COALESCE(actions.criteriasumm, false) as critsumm,
COALESCE(actions.flagstart, false) as flagstart,
COALESCE(actions.flagfinish, false) as flagfinish,
orders.date, actions.datestart, actions.datefinish,
imagesact, imagesfnd,
CASE
WHEN COALESCE(actions.flagfinish,false) = false AND COALESCE(actions.criteriasdate,false) = true THEN (EXTRACT(epoch FROM (SELECT (actions.datefinish - NOW())))/86400)::int
ELSE 0
END 
AS daysLeft,
CASE
WHEN COALESCE(actions.flagfinish,false) = false AND COALESCE(actions.criteriasdate,false) = true THEN (EXTRACT(epoch FROM (SELECT (actions.datefinish - NOW())))/3600)::int
ELSE 0
END 
AS hoursLeft,
COALESCE(actions.uid, '00000000-0000-0000-0000-000000000000') as actionuid,
COALESCE(fundraising.uid, '00000000-0000-0000-0000-000000000000') as fundraisinguid
FROM orders
LEFT JOIN tickets ON tickets.orderuid = orders.uid::VARCHAR
LEFT JOIN fundraising on fundraising.uid::VARCHAR = orders.fundraisinguid
LEFT JOIN crossfundsraising on crossfundsraising.raisinguid = orders.fundraisinguid
LEFT JOIN sections on sections.uid::VARCHAR = crossfundsraising.owneruid
LEFT JOIN actions on actions.uid::VARCHAR = tickets.actionsuid 
LEFT JOIN users on actions.maecenas = users.uid::VARCHAR
LEFT JOIN usersphotos on users.uid = usersphotos.usersuid
LEFT JOIN socnet on users.uid::VARCHAR = socnet.usersuid
LEFT JOIN 
( SELECT array_remove(array_agg(uid), NULL) as imagesact, owneruid FROM crossimages WHERE active GROUP BY owneruid) as actionimages on actionimages.owneruid = tickets.actionsuid
LEFT JOIN 
( SELECT array_remove(array_agg(uid), NULL) as imagesfnd, owneruid FROM crossimages WHERE active GROUP BY owneruid) as fundimages on fundimages.owneruid = orders.fundraisinguid
%v
GROUP BY orders.uid, tickets.actionsuid, orders.fundraisinguid, orders.amount, actions.description, fundraising.description,
actions.needsumm, fundraising.needsumm,tickets.isauction, tickets.amount, tickets.iswin, sections.uid, sections.title,
users.uid, users.firstname, users.lastname, users.instagram, users.description, usersphotos.filename, socnet.imagelink,
actions.active, actions.criteriasdate, actions.criteriasumm, actions.flagstart, actions.flagfinish, actions.date,
actions.datestart, actions.datefinish, actionimages.imagesact, fundimages.imagesfnd, orders.date,
actions.uid, fundraising.uid
ORDER BY orders.date DESC
)
SELECT count(*) OVER() AS total_count, * FROM actions_refined WHERE (typename = 'Action' AND actionuid != '00000000-0000-0000-0000-000000000000') OR
(typename = 'Fundraising' AND fundraisinguid != '00000000-0000-0000-0000-000000000000')
%v`

	var err error
	var rows *pgx.Rows

	var SQLWhere SQLString
	var LimitSQL string

	args := make([]interface{}, 0)
	i := 1

	if UID != "" {
		SQLWhere = *SQLWhere.checkWhere(fmt.Sprintf(`orders.usersuid = $%d`, i))
		args = append(args, UID)
		i++
	}

	if search.OnlyActive {
		SQLWhere = *SQLWhere.checkWhere(`fundraising.active = true`)
	}

	if search.OnlyFinished {
		SQLWhere = *SQLWhere.checkWhere(`(fundraising.flagfinish = true OR actions.flagfinish = true)`)
	}

	if search.OnlyWin {
		SQLWhere = *SQLWhere.checkWhere(`tickets.iswin = true`)
	}

	if search.Search != "" {
		SQLWhere = *SQLWhere.checkWhere(fmt.Sprintf(`(actions.title like $%d or fundraising.title like $%d)`, i, i))
		args = append(args, search.Search)
		i++
	}

	SQLWhere = *SQLWhere.checkWhere(`orders.fundraisinguid != ''`)

	if search.Paginator {
		LimitSQL = fmt.Sprintf(`LIMIT $%d OFFSET $%d`, i, i+1)
		args = append(args, search.Onpage, (search.Current*search.Onpage - search.Onpage))
		i = i + 2
	}

	rows, err = m.db.Query(fmt.Sprintf(SQL, SQLWhere.String(), LimitSQL), args...)
	if err == pgx.ErrNoRows {
		m.log.Error().Err(err).Msg("No history...")
	}

	if err != nil && err != pgx.ErrNoRows {
		m.log.Error().Err(err).Msg("Error with getting datas about orders for history section")
		return &Answer, err
	}
	totalCount := 0
	history := make([]*HistoryLite, 0)
	for rows.Next() {
		item := HistoryLite{}
		var DateStart *time.Time
		var DateFinish *time.Time
		var Date *time.Time
		var MaecenasProfilePic, MaecenasSocnetPic, uidact, uidfr string
		var ImagesAction []string
		var ImagesFundraising []string
		err := rows.Scan(&totalCount, &item.Type.TypeName, &item.Type.TypeUID,
			&item.TicketAmount, &item.Description, &item.NeedSumm, &item.Type.IsAuction,
			&item.TicketAmount, &item.IsWin, &item.Section.UID, &item.Section.Title,
			&item.Maecenas.UID, &item.Maecenas.Name, &item.Maecenas.Instagram, &item.Maecenas.Description, &MaecenasProfilePic, &MaecenasSocnetPic,
			&item.Flags.Active, &item.Flags.Criteriasdate, &item.Flags.Criteriasumm,
			&item.Flags.Flagstart, &item.Flags.Flagfinish, &Date, &DateStart, &DateFinish,
			&ImagesAction, &ImagesFundraising, &item.Dates.DaysLeft, &item.Dates.HoursLeft, &uidact, &uidfr)
		if err != nil {
			m.log.Error().Err(err).Msg("Error with SCAN datas about orders for history section")
			return &Answer, err
		}
		if Date != nil {
			item.Dates.Date = Date.Format("2006-01-02")
		}
		if DateStart != nil {
			item.Dates.DateStart = DateStart.Format("2006-01-02")
		}
		if DateFinish != nil {
			item.Dates.DateFinish = DateFinish.Format("2006-01-02")
		}

		item.Maecenas.Photo = m.Picsurl + "profiles/" + NoPhoto
		if MaecenasProfilePic != "" {
			item.Maecenas.Photo = m.Picsurl + "profiles/" + MaecenasProfilePic + `.jpg`
		} else {
			if MaecenasSocnetPic != "" {
				item.Maecenas.Photo = MaecenasSocnetPic
			}
		}

		if len(ImagesAction) > 0 {
			listImage := make([]*models.Images, 0)
			for _, image := range ImagesAction {
				pic := &models.Images{}
				pic.Image = m.Picsurl + "actions/" + image + ".jpg"
				pic.UID = image
				listImage = append(listImage, pic)
				// element is the element from someSlice for where we are
			}

			item.Images = listImage
		} else {
			listImage := make([]*models.Images, 0)
			pic := &models.Images{}
			pic.Image = m.Picsurl + models.NoPhoto
			pic.UID = ""
			listImage = append(listImage, pic)
			item.Images = listImage
		}
		history = append(history, &item)
	}

	Answer.Items = history
	Answer.Count = len(history)
	Answer.TotalCount = totalCount
	Answer.HasNext = false
	if search.Onpage*search.Current < Answer.TotalCount && Answer.Count != totalCount {
		Answer.HasNext = true
	}
	return &Answer, nil
}

func (s *SQLString) checkWhere(params ...string) *SQLString {

	if len(*s) > 0 {
		if len(params) > 1 {
			return makeSQLstring(fmt.Sprintf(`%v %v %v`, *s, params[1], params[0]))
		}
		return makeSQLstring(fmt.Sprintf(`%v AND %v`, *s, params[0]))
	}
	return makeSQLstring(fmt.Sprintf(`WHERE %v`, params[0]))

}

package history

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/fundraising"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"github.com/jackc/pgx"
)

const (
	// NoPhoto
	NoPhoto = "nophoto.jpg"
)

type (
	// Main - main structure for fundraising
	Main struct {
		Picsurl string
		Domain  string
		db      *pgx.ConnPool
		log     logger.Logger
	}
	// Dates ...
	Dates struct {
		DateStart  string `json:"dateStart"`
		DateFinish string `json:"dateFinish"`
		DaysLeft   int    `json:"daysLeft"`
		HoursLeft  int    `json:"hoursLeft"`
		Date       string `json:"date"`
	}

	// AnswerHistoryLite ...
	AnswerHistoryLite struct {
		Items      []*HistoryLite `json:"list"`
		Count      int            `json:"count"`
		TotalCount int            `json:"totalCount"`
		HasNext    bool           `json:"hasNext"`
		Success    bool           `json:"success"`
		Action     string         `json:"message"`
	}

	// HistoryLite ...
	HistoryLite struct {
		Type           Types            `json:"type"`
		Section        CurrentSection   `json:"section"`
		Images         []*models.Images `json:"images"`
		Flags          actions.Flags    `json:"flags"`
		Dates          Dates            `json:"dates"`
		Maecenas       actions.Maecenas `json:"maecenas"`
		TicketAmount   float64          `json:"ticketAmount"`
		IsWin          bool             `json:"isWin"`
		WinnersMessage string           `json:"winnersMessage"`
		Description    string           `json:"description"`
		NeedSumm       float64          `json:"needSumm"`
	}

	// History ...
	History struct {
		OwnerUID       string           `json:"ownerUID"`
		OwnerType      string           `json:"ownerType"`
		SenderUID      string           `json:"senderUID"`
		Type           Types            `json:"type"`
		Auction        actions.Auct     `json:"auction"`
		Images         []*models.Images `json:"images"`
		Maecenas       actions.Maecenas `json:"maecenas"`
		FundraisingUID string           `json:"fundraisingUID"`
		MaecenasUID    string           `json:"maecenasUID"`
		Title          string           `json:"title"`
		LongTitle      string           `json:"longTitle"`
		Description    string           `json:"description"`
		Result         string           `json:"result"`
		NeedSumm       float64          `json:"needSumm"`
		Donated        int64            `json:"donated"`
		Likes          int64            `json:"likes"`
		DonatedSumm    float64          `json:"donatedSumm"`
		TotalWinners   int64            `json:"totalWinners"`
		Status         string           `json:"status"`
		Flags          actions.Flags    `json:"flags"`
		Fund           fundraising.Fund `json:"fund"`
		Section        CurrentSection   `json:"section"`
		actions.Foruser

		Dates actions.Dates `json:"dates"`
	}

	// CurrentSection ...
	CurrentSection struct {
		UID   string `json:"UID"`
		Title string `json:"title"`
	}

	// Types ...
	Types struct {
		TypeName  string `json:"typeName"`
		TypeUID   string `json:"typeUID"`
		IsAuction bool   `json:"isAuction"`
	}

	// AnswerHistory - answer for API with history list
	AnswerHistory struct {
		AnswerFromGetItems
		Success bool   `json:"success"`
		Action  string `json:"message"`
	}

	// AnswerFromGetItems ...
	AnswerFromGetItems struct {
		Items      []*History `json:"list"`
		Count      int        `json:"count"`
		TotalCount int        `json:"totalCount"`
		HasNext    bool       `json:"hasNext"`
	}

	// FromOrders ...
	FromOrders struct {
		FundraisingUID  string `json:"fundraisingUID"`
		Amount          int64  `json:"amount"`
		Status          int64  `json:"status"`
		Success         bool   `json:"success"`
		LotteryUID      string `json:"lotteryUID"`
		ActionsUID      string `json:"ActionsUID"`
		IsAuction       bool   `json:"isAuction"`
		TicketAmount    int64  `json:"ticketAmount"`
		TicketUID       string `json:"ticketUID"`
		IsDraw          bool   `json:"isDraw"`
		IsPaid          bool   `json:"isPaid"`
		IsWin           bool   `json:"isWin"`
		SectionUID      string `json:"sectionUID"`
		SectionTitle    string `json:"sectionTitle"`
		WillParticipate bool   `json:"willParticipate"`
		ID              int
	}
	// ForChan ...
	ForChan struct {
		ID             int
		UID            string
		ActionsUID     string
		FundraisingUID string
		IsWin          bool
		SectionUID     string
		SectionTitle   string
		IsAuction      bool
		TicketAmount   int64
		LotteryUID     string
	}

	// FromChan ...
	FromChan struct {
		ID      int
		Error   error
		History History
	}

	// Search ...
	Search struct {
		Search       string `json:"search"`
		Current      int    `json:"current"`
		Onpage       int    `json:"onPage"`
		Paginator    bool   `json:"paginator"`
		OnlyActive   bool   `json:"onlyActive"`
		NotActive    bool   `json:"notActive"`
		OnlyWin      bool   `json:"onlyWin"`
		OnlyFinished bool   `json:"notFinished"`
	}
)

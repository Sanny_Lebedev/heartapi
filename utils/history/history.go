package history

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"github.com/jackc/pgx"
)

const (
	//Area - area name for os
	Area = "history"
)

// Initial - returns new sections instance
func Initial(db *pgx.ConnPool, Log logger.Logger, picsurl, domain string) *Main {
	return &Main{db: db, log: Log, Picsurl: picsurl, Domain: domain}
}

// GetFromOrders - get datas about orders for history section
// func (m *Main) GetFromOrders(UID string, search Search) ([]*FromOrders, int, error) {
// 	var Offset int
// 	var err error
// 	var rows *pgx.Rows

// 	andSQL := ``
// 	WithSearch := false
// 	WithMySearch := false

// 	if UID != "" {
// 		andSQL = `WHERE orders.usersuid = $1 `
// 		WithSearch = true
// 	}

// 	if search.OnlyActive {
// 		if !WithSearch {
// 			andSQL = andSQL + ` WHERE `
// 			WithSearch = true
// 		} else {
// 			andSQL = andSQL + ` AND `
// 		}
// 		andSQL = andSQL + `fundraising.active = true`
// 	}

// 	if search.OnlyFinished {
// 		if !WithSearch {
// 			andSQL = andSQL + ` WHERE `
// 			WithSearch = true
// 		} else {
// 			andSQL = andSQL + ` AND `
// 		}
// 		andSQL = andSQL + `(fundraising.flagfinish = true OR actions.flagfinish = true)`
// 	}

// 	if search.OnlyWin {
// 		if !WithSearch {
// 			andSQL = andSQL + ` WHERE `
// 			WithSearch = true
// 		} else {
// 			andSQL = andSQL + ` AND `
// 		}
// 		andSQL = andSQL + `tickets.iswin = true`
// 	}

// 	if search.Search != "" {
// 		WithMySearch = true
// 		if !WithSearch {
// 			andSQL = andSQL + ` WHERE `
// 			andSQL = andSQL + ` (actions.title like $1 or fundraising.title like $1)`
// 			WithSearch = true

// 		} else {
// 			andSQL = andSQL + ` AND `
// 			andSQL = andSQL + ` (actions.title like $2 or fundraising.title like $2)`
// 		}

// 	}
// 	andSQL = andSQL + ` AND orders.fundraisinguid != ''`
// 	list := make([]*FromOrders, 0)
// 	LimitSQL := ``
// 	if search.Paginator {
// 		if UID != "" {

// 			if WithMySearch {
// 				LimitSQL = ` LIMIT $3 OFFSET $4`
// 			} else {
// 				LimitSQL = ` LIMIT $2 OFFSET $3`
// 			}
// 		} else {
// 			if WithMySearch {
// 				LimitSQL = ` LIMIT $2 OFFSET $3`
// 			} else {
// 				LimitSQL = ` LIMIT $1 OFFSET $2`
// 			}
// 		}
// 		Offset = search.Current*search.Onpage - search.Onpage

// 	}

// 	SQL := `SELECT count(orders.uid) OVER() AS total_count, orders.fundraisinguid, orders.amount, orders.status,
// 	orders.success, COALESCE(tickets.uid,'00000000-0000-0000-0000-000000000000') as lotteryuid, COALESCE(tickets.actionsuid,'') as actionsuid,
//    COALESCE(tickets.isauction,false) as isauction, COALESCE(tickets.amount,0) as ticketamount,
//    COALESCE(tickets.isdraw, false) as isdraw, COALESCE(tickets.ispaid, false) as ispaid, COALESCE(tickets.iswin, false) as iswin,
//    COALESCE(tickets.willparticipate, false) as willparticipate,
//    COALESCE(sections.uid,'00000000-0000-0000-0000-000000000000') as sectionuid,
//    COALESCE(sections.title,'') as sectionstitle
// 	FROM orders
// 	LEFT JOIN tickets ON tickets.orderuid = orders.uid::VARCHAR
// 	LEFT JOIN fundraising on fundraising.uid::VARCHAR = orders.fundraisinguid
// 	LEFT JOIN
// 	(SELECT crossfundsraising.owneruid, crossfundsraising.raisinguid FROM crossfundsraising)
// 	 as cr on cr.raisinguid = orders.fundraisinguid
// 	LEFT JOIN sections on sections.uid::VARCHAR = cr.owneruid
// 	LEFT JOIN actions on actions.uid::VARCHAR = tickets.actionsuid ` + andSQL + ` ORDER BY orders.date DESC` + LimitSQL

// 	// Поиск, Пагинатор, Идентификатор
// 	if (WithMySearch) && search.Paginator && UID != "" {
// 		rows, err = m.db.Query(SQL, UID, "%"+search.Search+"%", search.Onpage, Offset)
// 	}

// 	// !Поиск, Пагинатор, Идентификатор
// 	if (!WithMySearch) && search.Paginator && UID != "" {
// 		rows, err = m.db.Query(SQL, UID, search.Onpage, Offset)

// 	}

// 	// !Поиск, !Пагинатор, Идентификатор
// 	if (!WithMySearch) && !search.Paginator && UID != "" {
// 		rows, err = m.db.Query(SQL, UID)

// 	}

// 	// !Поиск, !Пагинатор, !Идентификатор
// 	if (!WithMySearch) && !search.Paginator && UID == "" {
// 		rows, err = m.db.Query(SQL)
// 	}

// 	// !Поиск, Пагинатор, !Идентификатор
// 	if (!WithMySearch) && search.Paginator && UID == "" {
// 		rows, err = m.db.Query(SQL, search.Onpage, Offset)
// 	}

// 	// Поиск, Пагинатор, !Идентификатор
// 	if (WithMySearch) && search.Paginator && UID == "" {
// 		rows, err = m.db.Query(SQL, "%"+search.Search+"%", search.Onpage, Offset)
// 	}

// 	// Поиск, !Пагинатор, !Идентификатор
// 	if (WithMySearch) && !search.Paginator && UID == "" {
// 		rows, err = m.db.Query(SQL, "%"+search.Search+"%")
// 	}

// 	if err == pgx.ErrNoRows {
// 		m.log.Error().Err(err).Msg("No history...")
// 	}

// 	if err != nil && err != pgx.ErrNoRows {
// 		m.log.Error().Err(err).Msg("Error with getting datas about orders for history section")
// 		return list, 0, err
// 	}

// 	totalCount := 0
// 	p := 1
// 	for rows.Next() {
// 		Ans := &FromOrders{ID: p}
// 		p++
// 		err = rows.Scan(&totalCount, &Ans.FundraisingUID, &Ans.Amount, &Ans.Status, &Ans.Success,
// 			&Ans.LotteryUID, &Ans.ActionsUID, &Ans.IsAuction, &Ans.TicketAmount, &Ans.IsDraw, &Ans.IsPaid,
// 			&Ans.IsWin, &Ans.WillParticipate, &Ans.SectionUID, &Ans.SectionTitle)

// 		if err != nil {
// 			m.log.Error().Err(err).Msg("Error with scan")
// 			return list, 0, err
// 		}
// 		list = append(list, Ans)
// 	}

// 	return list, totalCount, err

// }

// // GetItemRT ...
// func (m *Main) GetItemRT(UID string, search Search) (AnswerFromGetItems, error) {
// 	Answer := &AnswerFromGetItems{}

// 	Orders, TotalCount, err := m.GetFromOrders(UID, search)

// 	if err != nil {
// 		return *Answer, err
// 	}

// 	jobs := make(chan ForChan, 50)
// 	out := make(chan FromChan, 50)
// 	var Wg sync.WaitGroup
// 	for i := 0; i < 50; i++ {
// 		Wg.Add(1)
// 		go waitGetOne(i, jobs, out, &Wg, m)
// 	}

// 	// list := make([]*History, 0)

// 	for _, order := range Orders {
// 		jobs <- ForChan{ID: order.ID, UID: UID, ActionsUID: order.ActionsUID, FundraisingUID: order.FundraisingUID, IsWin: order.IsWin, SectionUID: order.SectionUID, SectionTitle: order.SectionTitle, IsAuction: order.IsAuction, TicketAmount: order.TicketAmount, LotteryUID: order.LotteryUID}
// 	}

// 	hs := map[int]History{}

// 	var wgResult sync.WaitGroup
// 	wgResult.Add(1)
// 	go func(ch chan FromChan, wg *sync.WaitGroup) {
// 		for chitem := range ch {
// 			if chitem.Error == nil {
// 				hs[chitem.ID] = chitem.History
// 			}
// 		}
// 		wg.Done()
// 	}(out, &wgResult)

// 	close(jobs)
// 	Wg.Wait()
// 	close(out)
// 	wgResult.Wait()

// 	// for i, op := range hs {
// 	// 	fmt.Println(i, op)
// 	// }
// 	list := make([]*History, 0)
// 	// for i := 1; i <= len(hs); i++ {

// 	// 	hist := hs[i]
// 	// 	list = append(list, &hist)
// 	// }

// 	sortedMapIntString(hs,
// 		func(k int, v History) {
// 			list = append(list, &v)
// 		})
// 	Answer.Items = list
// 	Answer.Count = len(Orders)
// 	Answer.TotalCount = TotalCount
// 	Answer.HasNext = false
// 	if search.Onpage*search.Current < Answer.TotalCount && Answer.Count != TotalCount {
// 		Answer.HasNext = true
// 	}

// 	return *Answer, nil
// }

// func sortedMapIntString(m map[int]History, f func(k int, v History)) {
// 	var keys []int
// 	for k, _ := range m {
// 		keys = append(keys, k)
// 	}
// 	sort.Ints(keys)
// 	for _, k := range keys {
// 		f(k, m[k])
// 	}
// }

// func waitGetOne(id int, jobs chan ForChan, out chan FromChan, Wg *sync.WaitGroup, c *Main) {
// 	defer Wg.Done()
// 	item := History{}
// 	o := FromChan{}

// 	for job := range jobs {
// 		o.ID = job.ID
// 		item.IsWin = job.IsWin
// 		if job.ActionsUID != "" {
// 			Action, err := actions.Getonelt(c.db, c.Picsurl, job.ActionsUID, job.UID, c.Domain)
// 			if err != nil {
// 				c.log.Error().Err(err).Msg("Error with getting actions info")
// 				o.Error = err
// 			}
// 			item.Auction = Action.Auction
// 			item.Dates = Action.Dates
// 			item.Title = Action.Title
// 			item.Description = Action.Description

// 			item.Donated = Action.Donated
// 			item.DonatedSumm = Action.DonatedSumm
// 			item.Flags = Action.Flags
// 			item.Foruser = Action.Foruser
// 			item.FundraisingUID = Action.FundraisingUID
// 			item.Images = Action.Images
// 			item.IsLiked = Action.IsLiked
// 			item.IsPaid = Action.IsPaid
// 			item.TicketAmount = Action.TicketAmount
// 			item.TickenUID = Action.TickenUID
// 			item.WinnersMessage = Action.WinnersMessage

// 			item.Likes = Action.Likes
// 			item.MaecenasUID = Action.MaecenasUID
// 			item.Maecenas = Action.Maecenas

// 			item.NeedSumm = Action.NeedSumm
// 			item.OwnerType = Action.OwnerUID
// 			item.OwnerUID = Action.OwnerUID
// 			item.Result = Action.Result
// 			item.Status = Action.Status
// 			item.TotalWinners = Action.TotalWinners
// 			item.Type.TypeName = "Action"
// 			item.Type.TypeUID = Action.UID
// 			item.Section.UID = job.SectionUID
// 			item.Section.Title = job.SectionTitle
// 			item.Type.IsAuction = false

// 			if job.IsAuction {
// 				// This is Auction
// 				item.Type.IsAuction = true
// 			}
// 			o.History = item
// 		} else {
// 			o.ID = job.ID
// 			fundraising, err := fundraising.Getonelite(c.db, c.Picsurl, job.FundraisingUID, job.UID, c.Domain)
// 			if err != nil {
// 				c.log.Error().Err(err).Msg("Error with getting actions info")
// 				o.Error = err
// 			}
// 			// item.Auction = fundraising.Auction

// 			item.Title = fundraising.Title
// 			item.Description = fundraising.Description

// 			item.Donated = fundraising.Donated
// 			item.DonatedSumm = fundraising.Donatedsumm
// 			item.Flags.Flagfinish = fundraising.Flagfinish
// 			item.Flags.Flagstart = fundraising.Flagstart
// 			item.Flags.Active = fundraising.Active
// 			item.Flags.Criteriasdate = false
// 			item.Flags.Criteriasumm = false

// 			item.Foruser.IsLiked = fundraising.IsLiked
// 			item.Foruser.IsPaid = fundraising.IsPaid

// 			item.FundraisingUID = fundraising.UID
// 			item.Images = fundraising.Images
// 			item.IsLiked = fundraising.IsLiked
// 			item.IsPaid = fundraising.IsPaid
// 			item.IsWin = job.IsWin
// 			item.Section.UID = job.SectionUID
// 			item.Section.Title = job.SectionTitle

// 			item.TicketAmount = float64(job.TicketAmount / 100)
// 			item.TickenUID = job.LotteryUID
// 			item.WinnersMessage = ""
// 			if item.IsWin {
// 				item.WinnersMessage = "Вы выиграли. В ближайшее время с Вами свяжется администрация сервиса."
// 			}

// 			item.Likes = fundraising.Likes

// 			//item.Maecenas = nil

// 			item.NeedSumm = fundraising.Needsumm
// 			item.OwnerType = fundraising.OwnerUID
// 			item.OwnerUID = fundraising.OwnerUID
// 			item.Result = fundraising.Result
// 			item.Status = fundraising.Status
// 			item.TotalWinners = 0
// 			item.Type.TypeName = "Fundraising"
// 			item.Type.TypeUID = fundraising.UID

// 			item.Type.IsAuction = false

// 			if job.IsAuction {
// 				// This is Auction
// 				item.Type.IsAuction = true
// 			}
// 			o.History = item
// 		}
// 	}

// 	out <- o
// }

//GetItems - Getting history for current user
// func (m *Main) GetItems(UID string, search Search) (AnswerFromGetItems, error) {

// 	Answer := &AnswerFromGetItems{}

// 	Orders, TotalCount, err := m.GetFromOrders(UID, search)
// 	if err != nil {
// 		return *Answer, err
// 	}

// 	list := make([]*History, 0)
// 	for _, order := range Orders {

// 		item := &History{}

// 		item.IsWin = order.IsWin

// 		if order.ActionsUID != "" {
// 			// This is Action
// 			Action, err := actions.Getone(m.db, m.Picsurl, order.ActionsUID, UID, m.Domain)
// 			if err != nil {
// 				m.log.Error().Err(err).Msg("Error with getting actions info")
// 				return *Answer, err
// 			}
// 			item.Auction = Action.Auction
// 			item.Dates = Action.Dates
// 			item.Title = Action.Title
// 			item.Description = Action.Description

// 			item.Donated = Action.Donated
// 			item.DonatedSumm = Action.DonatedSumm
// 			item.Flags = Action.Flags
// 			item.Foruser = Action.Foruser
// 			item.FundraisingUID = Action.FundraisingUID
// 			item.Images = Action.Images
// 			item.IsLiked = Action.IsLiked
// 			item.IsPaid = Action.IsPaid
// 			item.TicketAmount = Action.TicketAmount
// 			item.TickenUID = Action.TickenUID
// 			item.WinnersMessage = Action.WinnersMessage

// 			item.Likes = Action.Likes
// 			item.MaecenasUID = Action.MaecenasUID
// 			item.Maecenas = Action.Maecenas

// 			item.NeedSumm = Action.NeedSumm
// 			item.OwnerType = Action.OwnerUID
// 			item.OwnerUID = Action.OwnerUID
// 			item.Result = Action.Result
// 			item.Status = Action.Status
// 			item.TotalWinners = Action.TotalWinners
// 			item.Type.TypeName = "Action"
// 			item.Type.TypeUID = Action.UID
// 			item.Section.UID = order.SectionUID
// 			item.Section.Title = order.SectionTitle
// 			item.Type.IsAuction = false

// 			if order.IsAuction {
// 				// This is Auction
// 				item.Type.IsAuction = true
// 			}

// 		} else {
// 			// This is Fundraising
// 			fundraising, err := fundraising.Getone(m.db, m.Picsurl, order.FundraisingUID, UID, m.Domain)
// 			if err != nil {
// 				m.log.Error().Err(err).Msg("Error with getting actions info")
// 				return *Answer, err
// 			}
// 			// item.Auction = fundraising.Auction

// 			item.Title = fundraising.Title
// 			item.Description = fundraising.Description

// 			item.Donated = fundraising.Donated
// 			item.DonatedSumm = fundraising.Donatedsumm
// 			item.Flags.Flagfinish = fundraising.Flagfinish
// 			item.Flags.Flagstart = fundraising.Flagstart
// 			item.Flags.Active = fundraising.Active
// 			item.Flags.Criteriasdate = false
// 			item.Flags.Criteriasumm = false

// 			item.Foruser.IsLiked = fundraising.IsLiked
// 			item.Foruser.IsPaid = fundraising.IsPaid

// 			item.FundraisingUID = fundraising.UID
// 			item.Images = fundraising.Images
// 			item.IsLiked = fundraising.IsLiked
// 			item.IsPaid = fundraising.IsPaid
// 			item.IsWin = order.IsWin
// 			item.Section.UID = order.SectionUID
// 			item.Section.Title = order.SectionTitle

// 			item.TicketAmount = float64(order.TicketAmount / 100)
// 			item.TickenUID = order.LotteryUID
// 			item.WinnersMessage = ""
// 			if item.IsWin {
// 				item.WinnersMessage = "Вы выиграли. В ближайшее время с Вами свяжется администрация сервиса."
// 			}

// 			item.Likes = fundraising.Likes

// 			//item.Maecenas = nil

// 			item.NeedSumm = fundraising.Needsumm
// 			item.OwnerType = fundraising.OwnerUID
// 			item.OwnerUID = fundraising.OwnerUID
// 			item.Result = fundraising.Result
// 			item.Status = fundraising.Status
// 			item.TotalWinners = 0
// 			item.Type.TypeName = "Fundraising"
// 			item.Type.TypeUID = fundraising.UID

// 			item.Type.IsAuction = false

// 			if order.IsAuction {
// 				// This is Auction
// 				item.Type.IsAuction = true
// 			}
// 		}

// 		list = append(list, item)
// 	}

// 	// fmt.Println(TotalCount)
// 	Answer.Items = list
// 	Answer.Count = len(list)
// 	Answer.TotalCount = TotalCount
// 	Answer.HasNext = false
// 	if search.Onpage*search.Current < Answer.TotalCount && Answer.Count != TotalCount {
// 		Answer.HasNext = true
// 	}

// 	return *Answer, err
// }

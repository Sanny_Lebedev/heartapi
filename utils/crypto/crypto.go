package crypto

import (
	"crypto/rand"
	"encoding/hex"
	"io"

	"github.com/pkg/errors"
	"golang.org/x/crypto/scrypt"
)

// GenerateSalt generates a random salt
func GenerateSalt() (string, error) {
	saltBytes := make([]byte, 16)
	_, err := io.ReadFull(rand.Reader, saltBytes)
	if err != nil {
		return "", errors.Wrap(err, "failed to read from rand.Reader")
	}
	salt := make([]byte, 32)
	hex.Encode(salt, saltBytes)
	return string(salt), nil
}

// GenerateMessageUID generates a random message UID
func GenerateMessageUID() (string, error) {
	saltBytes := make([]byte, 4)
	_, err := io.ReadFull(rand.Reader, saltBytes)
	if err != nil {
		return "", errors.Wrap(err, "failed to read from rand.Reader")
	}
	salt := make([]byte, 8)
	hex.Encode(salt, saltBytes)
	return string(salt), nil
}

// HashPassword hashes a string
func HashPassword(password, salt string) (string, error) {
	hashedPasswordBytes, err := scrypt.Key([]byte(password), []byte(salt), 16384, 8, 1, 32)
	if err != nil {
		return "", errors.Wrap(err, "failed to hash password")
	}
	hashedPassword := make([]byte, 64)
	hex.Encode(hashedPassword, hashedPasswordBytes)
	return string(hashedPassword), nil
}

package migration

import (
	"fmt"
	"strconv"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/payment"

	"github.com/jackc/pgx"
)

func getMigration(pg *pgx.ConnPool, num int) []Migration {
	var mig = map[int][]Migration{
		0: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS reftokens (
					uid     uuid,
					jti		uuid, 
					role	varchar(50) 
				);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS users (
					uid             uuid UNIQUE DEFAULT uuid_generate_v4(),
					firstName		VARCHAR (250),
					lastName		VARCHAR (250),  					
					email		    VARCHAR (50),
					phone		    VARCHAR (50),
					temppass 		VARCHAR(100),
					password		VARCHAR (100),
					salt		    VARCHAR (100),
					regdata         TIMESTAMPTZ
				);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS email ON users (email);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS temppass ON users (temppass);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS configtab (
								key     varchar(50),
								val		varchar(50) 
								);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS UsersRoleTab (
								UID     uuid,
								role	varchar(50), 
								active	boolean
								);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE users ADD COLUMN IF NOT EXISTS endtemppass TIMESTAMP  
										DEFAULT CURRENT_TIMESTAMP + INTERVAL '30 min'`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS usersphotos (
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					usersuid uuid UNIQUE NOT NULL,
					filename varchar(50) NOT NULL DEFAULT '',			
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)
					)`, nil),
			},

			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE queue_classic_jobs (
					id bigserial PRIMARY KEY,
					q_name text NOT NULL CHECK (length(q_name) > 0),
					action varchar(10) NOT NULL DEFAULT 'push',					
					method text NOT NULL CHECK (length(method) > 0),
					args   text NOT NULL,
					locked_at timestamptz,
					locked_by integer,
					created_at timestamptz DEFAULT now(),
					scheduled_at timestamptz DEFAULT now()
				  );`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX idx_qc_on_name_only_unlocked ON queue_classic_jobs (q_name, id) WHERE locked_at IS NULL;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX idx_qc_on_action_only_unlocked ON queue_classic_jobs (action, id) WHERE locked_at IS NULL;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX idx_qc_on_scheduled_at_only_unlocked ON queue_classic_jobs (scheduled_at, id) WHERE locked_at IS NULL;`, nil),
			},

			Migration{
				Action: NewPgQuery(pg, `CREATE OR REPLACE FUNCTION lock_head(q_name varchar, top_boundary integer, worker integer, serv varchar)
				RETURNS SETOF queue_classic_jobs AS $$
				DECLARE
					mviews RECORD;
				 	unlocked uuid[];					
				  	relative_top integer;
				  	job_count integer;
				BEGIN
				 	LOOP
					BEGIN

					  FOR mviews IN SELECT * FROM queue_classic_jobs 
						WHERE locked_at IS NULL
						AND hold = false	
						ORDER BY uid ASC
						LIMIT top_boundary
						FOR UPDATE NOWAIT
					 LOOP
					unlocked = array_append(unlocked,mviews.uid);
					END LOOP;					
					EXIT;
					EXCEPTION
					  WHEN lock_not_available THEN
						-- do nothing. loop again and hope we get a lock
					END;
				  END LOOP;
				
				  RETURN QUERY EXECUTE 'UPDATE queue_classic_jobs '
					|| ' SET locked_at = (CURRENT_TIMESTAMP),'
					|| ' locked_by = (select pg_backend_pid()),'
					|| ' worker = $2,'
					|| ' serv = $3'
					|| ' WHERE uid = ANY($1)'
					|| ' AND locked_at is NULL'
					|| ' RETURNING *'
				  USING unlocked, worker, serv;
				
				RETURN;
				END $$ LANGUAGE plpgsql;
				
				CREATE OR REPLACE FUNCTION lock_head(tname varchar, worker integer, serv varchar) RETURNS SETOF queue_classic_jobs AS $$ BEGIN
				  RETURN QUERY EXECUTE 'SELECT * FROM lock_head($1,50, $2, $3)' USING tname, worker, serv;
				END $$ LANGUAGE plpgsql;
				
				`, nil),
			},

			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS pushnavigator ( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					useruid varchar(50) NOT NULL DEFAULT '',
					token varchar(500) NOT NULL DEFAULT '',
					opersys varchar(10) NOT NULL DEFAULT 'ios',
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS useruid ON pushnavigator (useruid);`, nil),
			},
		},

		1: []Migration{
			Migration{
				Action: NewPgQuery(pg, `INSERT INTO configtab (key, val) VALUES ('currentMigration', '0');`, nil),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					fmt.Sprintf(`CREATE TABLE IF NOT EXISTS user_payment (
						uid uuid REFERENCES users(uid),
						order_id varchar NOT NULL DEFAULT '',
						order_number varchar NOT NULL,
						binding_id varchar NOT NULL DEFAULT '',
						status int NOT NULL DEFAULT %d,
						pan varchar NOT NULL DEFAULT '',
						expiration int NOT NULL DEFAULT 0,
						cardholder varchar NOT NULL DEFAULT ''
					)`, payment.StatusAuthorizing),
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`CREATE UNIQUE INDEX ON user_payment(uid)`,
					nil,
				),
			},
		},
		2: []Migration{
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs RENAME COLUMN method TO getter;`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs RENAME COLUMN args TO message;`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs DROP COLUMN id;`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs ADD COLUMN IF NOT EXISTS uid UUID UNIQUE DEFAULT uuid_generate_v4();`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs ADD COLUMN IF NOT EXISTS worker int NOT NULL DEFAULT 0;`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs ADD COLUMN IF NOT EXISTS serv varchar(50) NOT NULL DEFAULT 'main';`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs ADD COLUMN IF NOT EXISTS success bool NOT NULL DEFAULT false;`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs ADD PRIMARY KEY (uid);`,
					nil,
				),
			},
		},
		3: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE OR REPLACE FUNCTION lock_head(q_name varchar, top_boundary integer, worker integer, serv varchar)
				RETURNS SETOF queue_classic_jobs AS $$
				DECLARE
					mviews RECORD;
				 	unlocked uuid[];					
				  	relative_top integer;
				  	job_count integer;
				BEGIN
				 	LOOP
					BEGIN

					  FOR mviews IN SELECT * FROM queue_classic_jobs 
						WHERE locked_at IS NULL
						AND hold = false	
						ORDER BY uid ASC
						LIMIT top_boundary
						FOR UPDATE NOWAIT
					 LOOP
					unlocked = array_append(unlocked,mviews.uid);
					END LOOP;					
					EXIT;
					EXCEPTION
					  WHEN lock_not_available THEN
						-- do nothing. loop again and hope we get a lock
					END;
				  END LOOP;
				
				  RETURN QUERY EXECUTE 'UPDATE queue_classic_jobs '
					|| ' SET locked_at = (CURRENT_TIMESTAMP),'
					|| ' locked_by = (select pg_backend_pid()),'
					|| ' worker = $2,'
					|| ' serv = $3'
					|| ' WHERE uid = ANY($1)'
					|| ' AND locked_at is NULL'
					|| ' RETURNING *'
				  USING unlocked, worker, serv;
				
				RETURN;
				END $$ LANGUAGE plpgsql;
				
				CREATE OR REPLACE FUNCTION lock_head(tname varchar, worker integer, serv varchar) RETURNS SETOF queue_classic_jobs AS $$ BEGIN
				  RETURN QUERY EXECUTE 'SELECT * FROM lock_head($1,50, $2, $3)' USING tname, worker, serv;
				END $$ LANGUAGE plpgsql;
				
				`, nil),
			},

			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE queue_classic_jobs ADD COLUMN IF NOT EXISTS hold bool NOT NULL DEFAULT false;`,
					nil,
				),
			},
		},

		4: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS  orders( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					usersuid varchar(50) NOT NULL DEFAULT '',
					fundraisingUID varchar(50) NOT NULL DEFAULT '',
					lotteryUID varchar(50) NOT NULL DEFAULT '',
					success bool NOT NULL DEFAULT false,
					status int NOT NULL DEFAULT 0,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS fundraisingUID ON orders (fundraisingUID);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS lotteryUID ON orders (lotteryUID);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS usersuid ON orders (usersuid);`, nil),
			},
		},
		5: []Migration{
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE users ADD COLUMN IF NOT EXISTS birthday date NOT NULL DEFAULT '1980-01-01'`,
					nil,
				),
			},
		},
		6: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS  socnet( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					usersuid varchar(50) NOT NULL DEFAULT '',
					socnetuid varchar(150) NOT NULL DEFAULT '',
					provider varchar(10) NOT NULL DEFAULT '',
					email varchar(30) NOT NULL DEFAULT '',
					token varchar(550) NOT NULL DEFAULT '',
					imagelink varchar(250) NOT NULL DEFAULT '',
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS usersuid ON socnet (usersuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS socnetuid ON socnet (socnetuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS email ON socnet (email);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS token ON socnet (token);`, nil),
			},
		},
		7: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS sections( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					owneruid varchar(50) NOT NULL DEFAULT '',					
					image varchar(550) NOT NULL DEFAULT '',
					title varchar(550) NOT NULL DEFAULT '',
					description varchar(3000) NOT NULL DEFAULT '',
					records int NOT NULL DEFAULT 0,
					actions int NOT NULL DEFAULT 0,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON sections (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS title ON sections (title);`, nil),
			},
		},
		8: []Migration{
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE sections ADD COLUMN IF NOT EXISTS step varchar(10) NOT NULL DEFAULT '0'`,
					nil,
				),
			},
		},
		9: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS action( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(),
					senderuid varchar(50) NOT NULL DEFAULT '',
					owneruid varchar(50) NOT NULL DEFAULT '',
					maecenasuid varchar(50) NOT NULL DEFAULT '',
					image varchar(1000) NOT NULL DEFAULT '',
					title varchar(550) NOT NULL DEFAULT '',
					longtitle varchar(1000) NOT NULL DEFAULT '',
					description varchar(5000) NOT NULL DEFAULT '',
					result varchar(5000) NOT NULL DEFAULT '',
					status varchar(50) NOT NULL DEFAULT '',
					donated int NOT NULL DEFAULT 0,
					donatedsumm int NOT NULL DEFAULT 0,
					needsumm int  NOT NULL DEFAULT 0,
					active	bool NOT NULL DEFAULT true,
					timelimit	bool NOT NULL DEFAULT false,
					moneylimit	bool NOT NULL DEFAULT true,
					moneylimitamt int NOT NULL DEFAULT 0,
					timelimitamt timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3), 
					flagstart bool NOT NULL DEFAULT false,
					flagfinish bool NOT NULL DEFAULT false,					
					flagwinnerrevealed bool NOT NULL DEFAULT false,
					flagpaydforwinner bool NOT NULL DEFAULT false,					
					flagpaydforfund bool NOT NULL DEFAULT false,					
					flagstatusclose bool NOT NULL DEFAULT false,
					flagstatuscanseled bool NOT NULL DEFAULT false,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3) 
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS senderuid ON action (senderuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON action (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS maecenasuid ON action (maecenasuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS title ON action (title);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS status ON action (status);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS fundraising( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					owneruid varchar(50) NOT NULL DEFAULT '',					
					image varchar(1000) NOT NULL DEFAULT '',
					title varchar(550) NOT NULL DEFAULT '',
					longtitle varchar(1000) NOT NULL DEFAULT '',
					description varchar(5000) NOT NULL DEFAULT '',
					result varchar(5000) NOT NULL DEFAULT '',
					donated int NOT NULL DEFAULT 0,
					donatedsumm int NOT NULL DEFAULT 0,
					needsumm int  NOT NULL DEFAULT 0,										
					actions int NOT NULL DEFAULT 0,
					active	bool NOT NULL DEFAULT true,
					status varchar(50) NOT NULL DEFAULT '',
					flagstart bool NOT NULL DEFAULT false,
					flagfinish bool NOT NULL DEFAULT false,				
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON fundraising (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS title ON fundraising (title);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS status ON fundraising (status);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS fund( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					senderuid varchar(50) NOT NULL DEFAULT '',	
					title varchar(500) NOT NULL DEFAULT '',	
					description varchar(5000) NOT NULL DEFAULT '',	
					inn	varchar(30) NOT NULL DEFAULT '',	
					status varchar(50) NOT NULL DEFAULT '',	
					active	bool NOT NULL DEFAULT true,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS title ON fund (title);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS status ON fund (status);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS crossfundsraising( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					owneruid varchar(50) NOT NULL DEFAULT '',	
					funduid varchar(50) NOT NULL DEFAULT '',	
					raisinguid varchar(50) NOT NULL DEFAULT '',	
					active	bool NOT NULL DEFAULT true,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS funduid ON crossfundsraising (funduid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS raisinguid ON crossfundsraising (raisinguid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS maincross( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					senderuid varchar(50) NOT NULL DEFAULT '',	
					owneruid varchar(50) NOT NULL DEFAULT '',	
					ownertype varchar(50) NOT NULL DEFAULT '',	
					resourcetype varchar(50) NOT NULL DEFAULT '',	
					resourceuid varchar(50) NOT NULL DEFAULT '',	
					meta varchar(50) NOT NULL DEFAULT '',
					active	bool NOT NULL DEFAULT true,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON maincross (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS resourceuid ON maincross (resourceuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS senderuid ON maincross (senderuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS crossimages( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					senderuid varchar(50) NOT NULL DEFAULT '',	
					owneruid varchar(50) NOT NULL DEFAULT '',	
					ownertype varchar(50) NOT NULL DEFAULT '',	
					imagename varchar(50) NOT NULL DEFAULT '',	
					imagelink varchar(500) NOT NULL DEFAULT '',	
					active	bool NOT NULL DEFAULT true,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON crossimages (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ownertype ON crossimages (ownertype);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS imagename ON crossimages (imagename);`, nil),
			},

			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS address( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					senderuid varchar(50) NOT NULL DEFAULT '',	
					owneruid varchar(50) NOT NULL DEFAULT '',	
					ownertype varchar(50) NOT NULL DEFAULT '',	
					country varchar(200) NOT NULL DEFAULT '',	
					postal varchar(10) NOT NULL DEFAULT '',	
					city varchar(200) NOT NULL DEFAULT '',	
					address varchar(200) NOT NULL DEFAULT '',	
					active	bool NOT NULL DEFAULT true,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON address (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ownertype ON address (ownertype);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS contacts( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					senderuid varchar(50) NOT NULL DEFAULT '',	
					owneruid varchar(50) NOT NULL DEFAULT '',	
					ownertype varchar(50) NOT NULL DEFAULT '',	
					person varchar(150) NOT NULL DEFAULT '',	
					phone varchar(30) NOT NULL DEFAULT '',	
					email varchar(30) NOT NULL DEFAULT '',	
					website varchar(100) NOT NULL DEFAULT '',	
					skype varchar(100) NOT NULL DEFAULT '',						
					active	bool NOT NULL DEFAULT true,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON contacts (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ownertype ON contacts (ownertype);`, nil),
			},
		},
		10: []Migration{
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE fundraising ADD COLUMN IF NOT EXISTS step varchar(10) NOT NULL DEFAULT '0'`,
					nil,
				),
			},
		},
		11: []Migration{
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE fundraising ALTER COLUMN donated TYPE BIGINT`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE fundraising ALTER COLUMN donatedsumm TYPE BIGINT`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE fundraising ALTER COLUMN donatedsumm TYPE BIGINT`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE fundraising ALTER COLUMN actions TYPE BIGINT`,
					nil,
				),
			},
		},
		12: []Migration{
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE fundraising ADD COLUMN IF NOT EXISTS likes int NOT NULL DEFAULT 0;`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE sections ADD COLUMN IF NOT EXISTS likes int NOT NULL DEFAULT 0;`,
					nil,
				),
			},
		},
		13: []Migration{
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE contacts ADD COLUMN IF NOT EXISTS instagram varchar(100) NOT NULL DEFAULT ''`,
					nil,
				),
			},
			Migration{
				Action: NewPgQuery(
					pg,
					`ALTER TABLE users ADD COLUMN IF NOT EXISTS instagram varchar(100) NOT NULL DEFAULT ''`,
					nil,
				),
			},
		},
		14: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE fundraising DROP COLUMN donated;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE fundraising DROP COLUMN donatedsumm;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE fundraising DROP COLUMN needsumm;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE fundraising ADD COLUMN IF NOT EXISTS donated numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE fundraising ADD COLUMN IF NOT EXISTS donatedsumm numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE fundraising ADD COLUMN IF NOT EXISTS needsumm numeric NOT NULL DEFAULT 0`, nil),
			},
		},
		15: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS actions( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					senderuid varchar(50) NOT NULL DEFAULT '',
					maecenas varchar(50) NOT NULL DEFAULT '',	
					owneruid varchar(50) NOT NULL DEFAULT '',	
					ownertype varchar(50) NOT NULL DEFAULT '',	
					title varchar(550) NOT NULL DEFAULT '',
					longtitle varchar(1000) NOT NULL DEFAULT '',
					description varchar(5000) NOT NULL DEFAULT '',
					result varchar(5000) NOT NULL DEFAULT '',
					donated int NOT NULL DEFAULT 0,
					donatedsumm int NOT NULL DEFAULT 0,
					needsumm int  NOT NULL DEFAULT 0,										
					likes int NOT NULL DEFAULT 0,
					totalwinners int NOT NULL DEFAULT 1,					
					active	bool NOT NULL DEFAULT true,
					status varchar(50) NOT NULL DEFAULT '',
					flagstart bool NOT NULL DEFAULT false,
					flagfinish bool NOT NULL DEFAULT false,				
					criteriasumm bool NOT NULL DEFAULT true,
					criteriasdate bool NOT NULL DEFAULT false,
					dateStart timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)	,
					dateFinish timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3),					
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON actions (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS senderuid ON actions (senderuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS maecenas ON actions (maecenas);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ownertype ON actions (ownertype);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS title ON actions (title);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS prize( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					senderuid varchar(50) NOT NULL DEFAULT '',	
					maecenas varchar(50) NOT NULL DEFAULT '',
					ticketuid varchar(50) NOT NULL DEFAULT '',
					owneruid varchar(50) NOT NULL DEFAULT '',	
					ownertype varchar(50) NOT NULL DEFAULT '',	
					title varchar(550) NOT NULL DEFAULT '',
					longtitle varchar(1000) NOT NULL DEFAULT '',
					description varchar(5000) NOT NULL DEFAULT '',
					plaсe int NOT NULL DEFAULT 1,					
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS senderuid ON prize (senderuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS maecenas ON prize (maecenas);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS owneruid ON prize (owneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ownertype ON prize (ownertype);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS title ON prize (title);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ticketuid ON prize (ticketuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS actionsresults( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					actionsuid varchar(50) NOT NULL DEFAULT '',
					description varchar(5000) NOT NULL DEFAULT '',
					dateStart timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
					dateFinish timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
					status varchar(50) NOT NULL DEFAULT '',
					getwinners bool NOT NULL DEFAULT false,
					payforwinners bool NOT NULL DEFAULT false,					
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS actionsuid ON actionsresults (actionsuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS status ON actionsresults (status);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS winners( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					actionsuid varchar(50) NOT NULL DEFAULT '',
					winneruid varchar(50) NOT NULL DEFAULT '',					
					ticketuid varchar(50) NOT NULL DEFAULT '',					
					payforwinners bool NOT NULL DEFAULT false,
					plaсe int NOT NULL DEFAULT 0,
					comment varchar(5000) NOT NULL DEFAULT '',						 					
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS actionsuid ON winners (actionsuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS winneruid ON winners (winneruid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ticketuid ON winners (ticketuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS tickets( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					actionsuid varchar(50) NOT NULL DEFAULT '',
					usersuid varchar(50) NOT NULL DEFAULT '',					
					amount numeric NOT NULL DEFAULT 0,
					iswin bool NOT NULL DEFAULT false,
					isdraw bool NOT NULL DEFAULT false,
					willparticipate bool NOT NULL DEFAULT false,
					comment varchar(5000) NOT NULL DEFAULT '',						 					
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS actionsuid ON tickets (actionsuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS usersuid ON tickets (usersuid);`, nil),
			},
		},
		16: []Migration{

			Migration{
				Action: NewPgQuery(pg, `DROP TABLE IF EXISTS action;`, nil),
			},
		},
		17: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions DROP COLUMN donatedsumm;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions DROP COLUMN needsumm;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions DROP COLUMN donated;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS donatedsumm numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS needsumm numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS donated numeric NOT NULL DEFAULT 0`, nil),
			},
		},
		18: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE prize DROP COLUMN plaсe;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners DROP COLUMN plaсe;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE prize ADD COLUMN IF NOT EXISTS place int NOT NULL DEFAULT 1`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS place int NOT NULL DEFAULT 0`, nil),
			},
		},
		19: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE orders ADD COLUMN IF NOT EXISTS amount numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE orders ADD COLUMN IF NOT EXISTS actionuid varchar(50) NOT NULL DEFAULT ''`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS actionuid ON orders (actionuid);`, nil),
			},
		},
		20: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS minsumm numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS currentsumm numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS minbet numeric NOT NULL DEFAULT 0`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS isauction bool NOT NULL DEFAULT false`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS bets( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					actionsuid varchar(50) NOT NULL DEFAULT '',					
					winneruid varchar(50) NOT NULL DEFAULT '',					
					ticketuid varchar(50) NOT NULL DEFAULT '',					
					bet numeric NOT NULL DEFAULT 0,					
					comment varchar(5000) NOT NULL DEFAULT '',						 					
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE tickets ADD COLUMN IF NOT EXISTS ispaid bool NOT NULL DEFAULT false`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE tickets ADD COLUMN IF NOT EXISTS isauction bool NOT NULL DEFAULT   false`, nil),
			},
		},
		21: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS likes( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					owneruid varchar(50) NOT NULL DEFAULT '',					
					ownertype varchar(50) NOT NULL DEFAULT '',					
					senderuid varchar(50) NOT NULL DEFAULT ''
					)`, nil),
			},
		},
		22: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE users ADD COLUMN IF NOT EXISTS description varchar(550) NOT NULL DEFAULT ''`, nil),
			},
		},
		23: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS cards( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 
					clientid varchar(50) NOT NULL DEFAULT '',					
					bindingid varchar(50) NOT NULL DEFAULT '',
					orderid  varchar(50) NOT NULL DEFAULT '',
					maskedpan varchar(50) NOT NULL DEFAULT '',
					needrefund boolean NOT NULL DEFAULT true,
					isrefund boolean NOT NULL DEFAULT false,
					ispaid boolean NOT NULL DEFAULT false,
					todelete boolean NOT NULL DEFAULT false,
					amount numeric NOT NULL DEFAULT 0,
					expirydate varchar(7) NOT NULL DEFAULT '',
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS clientid ON cards (clientid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS bindingid ON cards (bindingid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS orderid ON cards (orderid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS maskedpan ON cards (maskedpan);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS expirydate ON cards (expirydate);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS pay( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 					
					needrefund boolean NOT NULL DEFAULT true,
					isrefund boolean NOT NULL DEFAULT false,
					ispaid boolean NOT NULL DEFAULT false,					
					savenewcard boolean NOT NULL DEFAULT false,					
					amount numeric NOT NULL DEFAULT 0,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS amount ON pay (amount);`, nil),
			},
		},
		24: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE pay ADD COLUMN IF NOT EXISTS shortorder varchar(550) NOT NULL DEFAULT ''`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE cards ADD COLUMN IF NOT EXISTS shortorder varchar(550) NOT NULL DEFAULT ''`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS shortorder ON pay (shortorder);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS shortorder ON cards (shortorder);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE pay ADD COLUMN IF NOT EXISTS bankorder varchar(550) NOT NULL DEFAULT ''`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS bankorder ON pay (bankorder);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE pay ADD COLUMN IF NOT EXISTS orderuid varchar(550) NOT NULL DEFAULT ''`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS orderuid ON pay (orderuid);`, nil),
			},
		},
		25: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS links( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 					
					action varchar(50) NOT NULL DEFAULT '',
					link varchar(10) NOT NULL DEFAULT '', 
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS action ON links (action);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS link ON links (link);`, nil),
			},
		},
		26: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS banklog( 
				UID uuid UNIQUE DEFAULT uuid_generate_v4(), 					
				orderuid varchar(50) NOT NULL DEFAULT '',
				ticketuid  varchar(50) NOT NULL DEFAULT '',
				usersuid varchar(50) NOT NULL DEFAULT '',
				bankorder varchar(50) NOT NULL DEFAULT '',
				shortorder varchar(50) NOT NULL DEFAULT '',
				message varchar(50) NOT NULL DEFAULT '',
				code int NOT NULL DEFAULT 0,
				othercode int NOT NULL DEFAULT 0,
				date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
				)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS orderuid ON banklog (orderuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS ticketuid ON banklog (ticketuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS usersuid ON banklog (usersuid);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS bankorder ON banklog (bankorder);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS shortorder ON banklog (shortorder);`, nil),
			},
		},
		27: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS isWaiting bool NOT NULL DEFAULT false`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS waittill timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE bets ADD COLUMN IF NOT EXISTS isPaid bool default false`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE bets ADD COLUMN IF NOT EXISTS isWaiting bool default false`, nil),
			},
		},
		28: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS paylink varchar(150) NOT NULL DEFAULT ''`, nil),
			},
		},
		29: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS Rejected bool NOT NULL DEFAULT false`, nil),
			},
		},
		30: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS iswin bool NOT NULL DEFAULT false`, nil),
			},
		},
		31: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS mailverify( 
				UID uuid UNIQUE DEFAULT uuid_generate_v4(), 					
				email varchar(50) NOT NULL DEFAULT '',
				salt  varchar(50) NOT NULL DEFAULT '',
				verify bool NOT NULL DEFAULT false,
				datever timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
				date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
				)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS email ON mailverify (email);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS salt ON mailverify (salt);`, nil),
			},
		},
		32: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS smsnotification bool NOT NULL DEFAULT false`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS emailnotification bool NOT NULL DEFAULT false`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE winners ADD COLUMN IF NOT EXISTS pushnotification bool NOT NULL DEFAULT false`, nil),
			},
		},
		33: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS newmaecenas( 
				UID uuid UNIQUE DEFAULT uuid_generate_v4(), 					
				maecenas varchar(50) NOT NULL DEFAULT '',
				isnotified  bool NOT NULL DEFAULT false,				
				date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
				)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS maecenas ON newmaecenas (maecenas);`, nil),
			},
		},
		34: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS havevideo bool NOT NULL DEFAULT false`, nil),
			},
		},
		35: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE actions ADD COLUMN IF NOT EXISTS ticketprice numeric NOT NULL DEFAULT 100000`, nil),
			},
		},
		36: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE orders ALTER COLUMN lotteryuid TYPE varchar(2048)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE banklog ALTER COLUMN ticketuid TYPE varchar(2048)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE tickets ADD COLUMN IF NOT EXISTS orderuid varchar(50) NOT NULL DEFAULT ''`, nil),
			},
		},
		37: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE OR REPLACE FUNCTION to_tsvector_multilang(text) RETURNS tsvector AS $$
				SELECT to_tsvector('english', $1) || to_tsvector('russian', $1) || to_tsvector('simple', $1)
				$$ LANGUAGE sql IMMUTABLE;`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS search_index ON actions USING GIN((setweight(to_tsvector('russian',title), 'A') 
				|| setweight(to_tsvector('russian', description), 'B')));`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS search_index ON fundraising USING GIN((setweight(to_tsvector('russian',title), 'A') 
				|| setweight(to_tsvector('russian', description), 'B')));`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS searchsub_index ON actions USING GIN(to_tsvector('russian',title));`, nil),
			},
		},
		38: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE queue_classic_jobs ADD COLUMN IF NOT EXISTS tripsuid varchar(50) NOT NULL DEFAULT ''`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE queue_classic_jobs ADD COLUMN IF NOT EXISTS torole varchar(50) NOT NULL DEFAULT ''`, nil),
			},
		},
		39: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS videoconvert( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 					
					action varchar(50) NOT NULL DEFAULT '',
					filein varchar(50) NOT NULL DEFAULT '',
					fileout varchar(50) NOT NULL DEFAULT '',
					converted  bool NOT NULL DEFAULT false,	
					onprogress  bool NOT NULL DEFAULT false,					
					convertdate timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
		},
		40: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE users ALTER COLUMN birthday SET DEFAULT '1900-01-01'`, nil),
			},
		},
		41: []Migration{
			Migration{
				Action: NewPgQuery(pg, `CREATE TABLE IF NOT EXISTS fornotificate( 
					UID uuid UNIQUE DEFAULT uuid_generate_v4(), 					
					fundraising varchar(50) NOT NULL DEFAULT '',
					action varchar(50) NOT NULL DEFAULT '',
					state varchar(50) NOT NULL DEFAULT '',
					send bool NOT NULL DEFAULT false,
					date timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP(3)					
					)`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS action ON fornotificate (action);`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS fundraising ON fornotificate (fundraising);`, nil),
			},
		},
		42: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE users ADD COLUMN IF NOT EXISTS getpush Boolean NOT NULL DEFAULT true`, nil),
			},
		},
		43: []Migration{
			Migration{
				Action: NewPgQuery(pg, `ALTER TABLE pushnavigator ADD COLUMN IF NOT EXISTS role varchar(50) NOT NULL DEFAULT 'user'`, nil),
			},
			Migration{
				Action: NewPgQuery(pg, `CREATE INDEX IF NOT EXISTS role ON pushnavigator (role);`, nil),
			},
		},
	}
	return mig[num]
}

// GetMigrationfromDB - function for getting current migration number from DB
func GetMigrationfromDB(db *pgx.ConnPool) (curmignumber int, err error) {
	const sqlstr = `select val from configtab where key like 'currentMigration' `
	var val string
	err = db.QueryRow(sqlstr).Scan(&val)

	if err != nil {
		val = "0"
		fmt.Println("GetMigrationfromDB problem")
		fmt.Println(err)
	}
	i, errConvert := strconv.Atoi(val)
	if errConvert != nil {
		fmt.Println(errConvert)

		i = 0
	}

	return i, err
}

// SetMigrationtoDB - function for setting current migration number to DB
func SetMigrationtoDB(db *pgx.ConnPool, mignum int) error {
	const sqlstr = `UPDATE configtab SET val=$1 WHERE key = $2`
	_, err := db.Exec(sqlstr, strconv.Itoa(mignum), "currentMigration")

	return err
}

// MakeMigration - function for starting current DB migration
func MakeMigration(db *pgx.ConnPool, mig int) {
	//
	curmignumber, err := GetMigrationfromDB(db)
	if err != nil {
		fmt.Println(err)
		curmignumber = 0
	}

	if curmignumber < mig {
		curnum := curmignumber
		for curnum < mig {

			currentMigration := getMigration(db, curnum)

			for _, element := range currentMigration {
				if e := element.Action.Do(); e != nil {
					fmt.Println(e)
					errSetmigration := SetMigrationtoDB(db, (curnum))
					if errSetmigration != nil {
						fmt.Println(errSetmigration)
					}
				}
			}

			curnum++
		}

		if err == nil {
			errSetmigration1 := SetMigrationtoDB(db, (curnum))
			if errSetmigration1 != nil {
				fmt.Println(errSetmigration1)

			}
		}

	}

}

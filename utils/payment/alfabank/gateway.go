package alfabank

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/Sanny_Lebedev/choppyapi/logger"
	"bitbucket.org/Sanny_Lebedev/choppyapi/utils/payment"
	"github.com/pkg/errors"
)

// Gateway is Alfabank payment gateway
type Gateway struct {
	username   string
	password   string
	endpoint   string
	httpClient http.Client
	log        logger.Logger
}

// NewGateway returns new Gateway instance
func NewGateway(username, password, endpoint string, log logger.Logger) *Gateway {
	g := &Gateway{
		username: username,
		password: password,
		endpoint: endpoint,
		httpClient: http.Client{
			Timeout: 30 * time.Second,
		},
		log: log,
	}
	return g
}

// OrderPreAuth sends pre authorized order
func (g *Gateway) OrderPreAuth(order payment.OrderRequest) (*payment.OrderResponse, error) {
	params := g.authParams(false)
	params.Set("orderNumber", order.OrderNumber)
	params.Set("amount", fmt.Sprintf("%d", order.Amount))
	params.Set("returnUrl", order.ReturnURL)
	params.Set("failUrl", order.FailURL)
	params.Set("clientId", order.UserID)

	if order.Description != "" {
		params.Set("description", order.Description)
	}
	switch order.PageView {
	case payment.MobilePageView:
		params.Set("pageView", "MOBILE")
	case payment.DesktopPageView:
		params.Set("pageView", "DESKTOP")
	case payment.BindingPageView:
		params.Set("pageView", "binding")
	default:
		params.Set("pageView", "DESKTOP")
	}
	if order.BindingID != "" {
		params.Set("bindingId", order.BindingID)
	}
	if order.AutoPayment {
		params.Set("features", "AUTO_PAYMENT")
	}
	req, err := http.NewRequest(http.MethodGet, g.endpoint+"registerPreAuth.do?"+params.Encode(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create req")
	}
	res, err := g.httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to send request")
	}
	defer res.Body.Close()
	var result struct {
		OrderID      string      `json:"orderId"`
		FormURL      string      `json:"formUrl"`
		ErrorCode    json.Number `json:"errorCode"`
		ErrorMessage string      `json:"errorMessage"`
	}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode response")
	}
	out := &payment.OrderResponse{
		OrderID: result.OrderID,
		FormURL: result.FormURL,
	}
	if result.ErrorCode != "" {
		errCode, err := result.ErrorCode.Int64()
		if err != nil {
			return nil, errors.Wrap(err, "failed to decode error code")
		}
		if errCode > 0 {
			out.ErrorMessage = result.ErrorMessage
			switch errCode {
			case 1:
				out.Error = payment.ErrorBadOrderID
			case 3:
				out.Error = payment.ErrorUnknownCurrency
			case 4:
				out.Error = payment.ErrorBadInput
			case 5:
				out.Error = payment.ErrorAccessDenied
			case 7:
				out.Error = payment.ErrorSystemError
			default:
				out.Error = payment.ErrorSystemError
			}
		}
	}
	return out, nil
}

// OrderStatus retrieves order status from alfabank
func (g *Gateway) OrderStatus(ID string) (*payment.OrderStatus, error) {
	params := g.authParams(false)
	params.Set("orderId", ID)
	req, err := http.NewRequest(http.MethodGet, g.endpoint+"getOrderStatus.do?"+params.Encode(), nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create req")
	}
	res, err := g.httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to send request")
	}
	defer res.Body.Close()
	var result struct {
		OrderStatus    uint
		ErrorCode      json.Number
		ErrorMessage   string
		OrderNumber    string
		Pan            string
		Expiration     json.Number
		CardholderName string
		Amount         int64
		ClientID       string
		BindingID      string
		IP             string
	}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode response")
	}
	var expiration int64
	if result.Expiration != "" {
		expiration, err = result.Expiration.Int64()
		if err != nil {
			return nil, errors.Wrapf(err, "failed to decode expiration: %+v", result)
		}
	}
	out := &payment.OrderStatus{
		Amount:      uint(result.Amount),
		BindingID:   result.BindingID,
		CardHolder:  result.CardholderName,
		Expiration:  uint(expiration),
		IP:          result.IP,
		OrderNumber: result.OrderNumber,
		PAN:         result.Pan,
		UserID:      result.ClientID,
		ErrorCode:   payment.ErrorNoError,
	}
	switch result.OrderStatus {
	case 0:
		out.Status = payment.StatusAuthorizing
	case 1:
		out.Status = payment.StatusHold
	case 3:
		out.Status = payment.StatusCanceled
	case 4:
		out.Status = payment.StatusRefund
	case 5:
		out.Status = payment.StatusAuthorizing
	case 6:
		out.Status = payment.StatusDeclined
	default:
		out.Status = payment.StatusUnknown
	}
	if result.ErrorCode != "" {
		errCode, err := result.ErrorCode.Int64()
		if err != nil {
			return nil, errors.Wrap(err, "failed to decode error code")
		}
		if errCode > 0 {
			out.ErrorMessage = result.ErrorMessage
			switch errCode {
			case 2:
				out.ErrorCode = payment.ErrorBadInput
			case 5:
				out.ErrorCode = payment.ErrorAccessDenied
			case 6:
				out.ErrorCode = payment.ErrorBadOrderID
			default:
				out.ErrorCode = payment.ErrorSystemError
			}
		}
	}
	return out, nil
}

// Cancel cancels order
func (g *Gateway) Cancel(ID string) error {
	params := g.authParams(false)
	params.Set("orderId", ID)
	req, err := http.NewRequest(http.MethodGet, g.endpoint+"reverse.do?"+params.Encode(), nil)
	if err != nil {
		return errors.Wrap(err, "failed to create req")
	}
	res, err := g.httpClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to send request")
	}
	defer res.Body.Close()
	var result struct {
		ErrorCode    string
		ErrorMessage string
	}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return errors.Wrap(err, "failed to decode response")
	}
	if result.ErrorCode != "" && result.ErrorCode != "0" {
		return fmt.Errorf("operation failed with result=%+v", result)
	}
	return nil
}

// Pay sends payment order
func (g *Gateway) Pay(userID, orderID, bindingID string, amount int64) error {
	paymentOrderID, err := g.registerPayment(userID, orderID, bindingID, amount)
	if err != nil {
		return errors.Wrap(err, "failed to register payment")
	}
	err = g.paymentOrderBinding(paymentOrderID, bindingID)
	if err != nil {
		return errors.Wrap(err, "failed to execute binding order")
	}
	return nil
}

func (g *Gateway) registerPayment(userID, orderID, bindingID string, amount int64) (string, error) {
	params := g.authParams(false)
	params.Set("amount", fmt.Sprintf("%d", amount))
	params.Set("clientId", userID)
	params.Set("orderNumber", strings.Replace(orderID, "-", "", -1))
	params.Set("returnUrl", "http://localhost/")
	params.Set("features", "AUTO_PAYMENT")
	params.Set("bindingId", bindingID)
	req, err := http.NewRequest(http.MethodGet, g.endpoint+"register.do?"+params.Encode(), nil)
	if err != nil {
		return "", errors.Wrap(err, "failed to create req")
	}
	res, err := g.httpClient.Do(req)
	if err != nil {
		return "", errors.Wrap(err, "failed to send request")
	}
	defer res.Body.Close()
	var result struct {
		OrderID      string `json:"orderId"`
		ErrorCode    string
		ErrorMessage string
	}
	err = json.NewDecoder(res.Body).Decode(&result)
	if err != nil {
		return "", errors.Wrap(err, "failed to decode response")
	}
	if (result.ErrorCode != "" && result.ErrorCode != "0") || result.OrderID == "" {
		return "", fmt.Errorf("operation failed with result=%+v", result)
	}
	return result.OrderID, nil
}

func (g *Gateway) paymentOrderBinding(orderID, bindingID string) error {
	params := g.authParams(true)
	params.Set("bindingId", bindingID)
	params.Set("mdOrder", orderID)
	var reqBody string
	for k := range params {
		var sep string
		if reqBody != "" {
			sep = "&"
		}
		reqBody += sep + k + "=" + params.Get(k)
	}
	req, err := http.NewRequest(http.MethodPost, g.endpoint+"paymentOrderBinding.do", strings.NewReader(reqBody))
	if err != nil {
		return errors.Wrap(err, "failed to create req")
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	res, err := g.httpClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to send request")
	}
	defer res.Body.Close()
	var result struct {
		Info                string
		ErrorCode           int64
		ErrorMessage        string
		ProcessingErrorType string
	}
	b := bytes.NewBuffer(nil)
	resBody := io.TeeReader(res.Body, b)
	err = json.NewDecoder(resBody).Decode(&result)
	if err != nil {
		return errors.Wrapf(err, "failed to decode response: %s, reqBody: %s", b.String(), reqBody)
	}
	g.log.Info().Interface("result", result).Str("body", b.String()).Msg("orderBinding response")
	if result.ErrorCode != 0 || result.ErrorMessage != "" || result.ProcessingErrorType != "" {
		return fmt.Errorf("operation failed with result=%+v, reqBody: %s", result, reqBody)
	}
	return nil
}

func (g *Gateway) authParams(encode bool) url.Values {
	if !encode {
		return url.Values{
			"userName": {g.username},
			"password": {g.password},
		}
	}
	return url.Values{
		"userName": {g.username},
		"password": {strings.Replace(g.password, "&", "%26", -1)},
	}
}

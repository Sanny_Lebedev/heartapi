package message

import (
	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/crypto"
)

type (
	UnauthorizedAnswer struct {
		Event   string `json:"Event"`
		Message string `json:"Message"`
	}

	Mymeta interface{}
	Myout  interface{}

	OK_Auth app.MyservAuthToapp

	Unauthorized struct {
		// Code of answer
		Code string `form:"code" json:"code" xml:"code"`
		// details of answer
		Details string `form:"details" json:"details" xml:"details"`
		// Id of answer
		ID string `form:"id" json:"id" xml:"id"`
		// Other metadatas
		Meta interface{} `form:"meta" json:"meta" xml:"meta"`
		// status of answer
		Status int `form:"status" json:"status" xml:"status"`
		// result of operation
		Success bool `form:"success" json:"success" xml:"success"`
	}

	badRequest struct {
		Meta UnauthorizedAnswer
	}

	Badreq struct {
		// Code of answer
		Code string `form:"code" json:"code" xml:"code"`
		// details of answer
		Details string `form:"details" json:"details" xml:"details"`
		// Id of answer
		ID string `form:"id" json:"id" xml:"id"`
		// Other metadatas
		Meta interface{} `form:"meta" json:"meta" xml:"meta"`
		// status of answer
		Status int `form:"status" json:"status" xml:"status"`
		// result of operation
		Success bool `form:"success" json:"success" xml:"success"`
	}

	Sender interface {
		Prepare(status string, Meta Mymeta)
	}
)

func (o *OK_Auth) Prepare(Code string, Details string, Meta Mymeta) *OK_Auth {
	o.ID, _ = crypto.GenerateMessageUID()
	o.Meta = Meta
	o.Code = Code
	o.Status = 200
	o.Success = true
	o.Details = Details

	return o
}

func (o *Unauthorized) Prepare(Code string, Details string, Meta Mymeta) *Unauthorized {
	o.ID, _ = crypto.GenerateMessageUID()
	o.Meta = Meta
	o.Code = Code
	o.Status = 401
	o.Success = false
	o.Details = Details
	return o
}

func (o *Badreq) Prepare(Code string, Details string, Meta Mymeta) *Badreq {

	o.Meta = Meta
	o.ID, _ = crypto.GenerateMessageUID()
	o.Code = Code
	o.Status = 400
	o.Success = false
	o.Details = Details
	return o
}

package main

import (
	"context"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/queue"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/pay"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/pay/sberbank"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/tickets"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// PaymentController implements the payment resource.
type PaymentController struct {
	*goa.Controller
	DB          *pgx.ConnPool
	log         logger.Logger
	Payment     PaymentStr
	EmailVerify bool
}

// NewPaymentController creates a payment controller.
func NewPaymentController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, options PaymentStr, emailVerify bool) (*PaymentController, error) {
	return &PaymentController{
		Controller:  service.NewController("PaymentController"),
		DB:          pg,
		log:         log,
		Payment:     options,
		EmailVerify: emailVerify,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *PaymentController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *PaymentController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Cards runs the cards action.
func (c *PaymentController) Cards(ctx *app.CardsPaymentContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if !MyUser.Is(user.RoleAdmin) && (uid != ctx.UID) {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong user"}))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {

		payment := pay.Initial(c.DB, uid, c.log)
		cards, err := payment.GetCards()
		if err != nil {
			answer := new(ActionAnswer)
			c.log.Error().Err(err).Msg("Error with getting card list")
			answer.Action = "Ошибка доступа к картам пользователя"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
			return ctx.OK(&res)
		}

		answer := new(pay.AnswerCards)
		answer.Action = "Список карт пользователя"
		answer.Cards = cards
		answer.Success = true
		answer.Count = len(cards)
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
		return ctx.OK(&res)
	}

	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Access denied"}))
	return ctx.OK(&res)
	// PaymentController_Cards: end_implement
}

// Check runs the check action.
func (c *PaymentController) Check(ctx *app.CheckPaymentContext) error {
	// PaymentController_Check: start_implement

	// Put your logic here

	res := &app.MyservAuthToapp{}
	return ctx.OK(res)
	// PaymentController_Check: end_implement
}

// Deletecard runs the deletecard action.
func (c *PaymentController) Deletecard(ctx *app.DeletecardPaymentContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if !MyUser.Is(user.RoleAdmin) && (uid != ctx.Payload.UID) {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong user"}))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {

		payment := pay.Initial(c.DB, uid, c.log)

		Sber := sberbank.NewGateway(c.Payment.Username, c.Payment.Password, c.Payment.Gateway, c.log)
		asnw, err := Sber.UnbindingCard(ctx.Payload.CardUID)
		if err != nil {
			answer := new(ActionAnswer)
			c.log.Error().Err(err).Msg("Error with sberbank API connection. " + asnw.Status.Message)
			answer.Action = "Ошибка связи с банком."
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
			return ctx.OK(&res)
		}

		cards, err := payment.DeleteCard(ctx.Payload.CardUID)
		if err != nil {
			answer := new(ActionAnswer)
			c.log.Error().Err(err).Msg("Error with getting card list")
			answer.Action = "Ошибка доступа к картам пользователя"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
			return ctx.OK(&res)
		}

		answer := new(pay.AnswerCards)
		answer.Action = "Список карт пользователя"
		answer.Cards = cards
		answer.Success = true
		answer.Count = len(cards)
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
		return ctx.OK(&res)
	}

	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Access denied"}))
	return ctx.OK(&res)
	//
}

// Newcard runs the newcard action.
func (c *PaymentController) Newcard(ctx *app.NewcardPaymentContext) error {
	var res app.MyservAuthToapp
	userID, err := jwtUserID(ctx)
	if err != nil {
		return ctx.NotFound()
	}

	payment := pay.Initial(c.DB, userID, c.log)

	orderUID, fullOrder, err := payment.Newcard()
	if err != nil {
		res = c.ErrorAnswer(err, "Error with new card", "Ошибка сохранения новой карты")
		return ctx.OK(&res)
	}

	Sber := sberbank.NewGateway(c.Payment.Username, c.Payment.Password, c.Payment.Gateway, c.log)
	order := pay.OrderRequest{
		Amount:      100,
		OrderNumber: orderUID,
		ReturnURL:   c.Payment.Return.ReturnURL,
		FailURL:     c.Payment.Return.FailURL,
		UserID:      userID,
		AutoPayment: false,
		Description: "Регистрация новой карты",
	}

	resp, err := Sber.RegisterCard(order)
	if err != nil {
		res = c.ErrorAnswer(err, "Error with new card", "Ошибка сохранения новой карты")
		return ctx.OK(&res)
	}

	if resp.ErrorMessage != "" {

		res = c.ErrorAnswer(err, resp.ErrorMessage, resp.ErrorMessage)
		return ctx.OK(&res)
	}

	Order := pay.PayRequest{
		NeedRefund:  true,
		SaveNewCard: true,
		Amount:      100,
		OrderUID:    fullOrder,
		ShortOrder:  orderUID,
		BankOrder:   resp.OrderID,
	}

	err = payment.SavePay(Order)
	if err != nil {
		res = c.ErrorAnswer(err, "Error with new card", "Ошибка сохранения новой карты")
		return ctx.OK(&res)
	}

	task := queue.Newcard{UID: userID, OrderUID: fullOrder, BankOrderUID: resp.OrderID}
	err = task.Add(queue.Conf{DB: c.DB, Log: c.log})
	if err != nil {
		res = c.ErrorAnswer(err, "Error with saving new task", "Ошибка сохранения задачи")
		return ctx.OK(&res)
	}
	Answer := pay.AnswerURL{
		FormUrl: resp.FormURL,
		OrderId: resp.OrderID,
		Success: true,
		Message: "Ссылка сформирована",
	}
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Pay", "Status", &Answer))
	return ctx.OK(&res)
}

//Fundraising - pay for fundraising
func (c *PaymentController) Fundraising(ctx *app.FundraisingPaymentContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if !MyUser.Is(user.RoleAdmin) && (uid != ctx.Payload.UID) {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong user"}))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {

		EmailVerifyStatus := true
		if c.EmailVerify {
			// Check for email verify
			EmailVerifyStatus = false
			ucfg := user.Conf{DB: c.DB, Log: c.log}
			usermail, err := ucfg.GetMailByUID(uid)

			if err == pgx.ErrNoRows {
				EmailVerifyStatus = false
			}

			if err != nil && err != pgx.ErrNoRows {
				res = c.ErrorAnswer(err, "Error with payment", "Ошибка верификации адреса")
				return ctx.OK(&res)
			}

			EmailVerifyStatus, err = ucfg.GetEmailStatus(usermail)
			if err == pgx.ErrNoRows {
				EmailVerifyStatus = false
			}

			if err != nil && err != pgx.ErrNoRows {
				res = c.ErrorAnswer(err, "Error with payment", "Ошибка верификации адреса")
				return ctx.OK(&res)
			}

		}

		// Get new order UID
		fullOrder, err := tickets.NewOrderSaving(c.DB, c.log, ctx.Payload.UID, ctx.Payload.RaisingUID, "", uint(ctx.Payload.Amount), "")
		payment := pay.Initial(c.DB, ctx.Payload.UID, c.log)
		orderUID := pay.MakeShortOrder(fullOrder)

		Sber := sberbank.NewGateway(c.Payment.Username, c.Payment.Password, c.Payment.Gateway, c.log)

		order := pay.OrderRequest{
			Amount:      uint64(ctx.Payload.Amount),
			OrderNumber: orderUID,
			ReturnURL:   c.Payment.Return.ReturnURL,
			FailURL:     c.Payment.Return.FailURL,
			UserID:      ctx.Payload.UID,
			AutoPayment: false,
			Description: "Благотворительный взнос",
		}

		resp, err := Sber.RegisterCard(order)
		if err != nil {
			res = c.ErrorAnswer(err, "Error with payment", "Ошибка регистрации оплаты")
			return ctx.OK(&res)
		}

		if resp.ErrorMessage != "" {

			res = c.ErrorAnswer(err, resp.ErrorMessage, resp.ErrorMessage)
			return ctx.OK(&res)
		}

		Order := pay.PayRequest{
			NeedRefund:  false,
			SaveNewCard: false,
			Amount:      int64(ctx.Payload.Amount),
			OrderUID:    fullOrder,
			ShortOrder:  orderUID,
			BankOrder:   resp.OrderID,
		}

		err = payment.SavePay(Order)
		if err != nil {
			res = c.ErrorAnswer(err, "Error with new payment", "Ошибка сохранения платежя")
			return ctx.OK(&res)
		}

		task := queue.NewPay{UID: ctx.Payload.UID, OrderUID: fullOrder, BankOrderUID: resp.OrderID}
		err = task.Add(queue.Conf{DB: c.DB, Log: c.log})
		if err != nil {
			res = c.ErrorAnswer(err, "Error with saving new task", "Ошибка сохранения задачи")
			return ctx.OK(&res)
		}
		Answer := pay.AnswerURL{
			EmailVerified: EmailVerifyStatus,
			FormUrl:       resp.FormURL,
			OrderId:       resp.OrderID,
			Success:       true,
			Message:       "Ссылка сформирована",
		}
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Pay", "Status", &Answer))
		return ctx.OK(&res)
	}
	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Access denied"}))
	return ctx.OK(&res)
	// PaymentController_Pay: end_implement
}

// Winner - check for winner
func (c *PaymentController) Winner(ctx *app.WinnerPaymentContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if !MyUser.Is(user.RoleAdmin) && (uid != ctx.UID) {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong user"}))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {
		var err error
		answer := new(user.AnswerForWinner)
		cfg := user.Conf{DB: c.DB, Log: c.log}
		answer.List, err = cfg.CheckWinnerAuction(ctx.UID)

		if err != nil {
			c.log.Error().Err(err).Msg("Error with getting list for winner")
			answer.Action = "Error with getting list"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
			return ctx.OK(&res)
		}
		answer.Action = "Список победивших билетов"
		answer.Success = true
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
		return ctx.OK(&res)

	}

	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Access denied"}))
	return ctx.OK(&res)

}

//Ticket - request for new ticket
func (c *PaymentController) Ticket(ctx *app.TicketPaymentContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if !MyUser.Is(user.RoleAdmin) && (uid != ctx.Payload.UID) {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong user"}))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {

		fullOrder, message, ticketUID, isAuction, err := tickets.New(c.DB, c.log, ctx.Payload.UID, uint(ctx.Payload.Amount), ctx.Payload.ActionUID)

		if message == "NotPaid" {
			// Get link for paid for auction
			Answer := pay.AnswerURL{
				EmailVerified: true,
				FormUrl:       fullOrder,
				OrderId:       "",
				Success:       true,
				Message:       "Ссылка сформирована",
			}
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Pay", "Status", &Answer))
			return ctx.OK(&res)
		}

		if err != nil {
			answer := new(ActionAnswer)
			c.log.Error().Err(err).Msg("Error with getting new ticket")
			answer.Action = message
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
			return ctx.OK(&res)
		}

		// verification control
		EmailVerifyStatus := true
		if c.EmailVerify {
			// Check for email verify
			EmailVerifyStatus = false
			ucfg := user.Conf{DB: c.DB, Log: c.log}
			usermail, err := ucfg.GetMailByUID(uid)

			if err == pgx.ErrNoRows {
				EmailVerifyStatus = false
			}

			if err != nil && err != pgx.ErrNoRows {
				res = c.ErrorAnswer(err, "Error with payment", "Ошибка верификации адреса")
				return ctx.OK(&res)
			}

			EmailVerifyStatus, err = ucfg.GetEmailStatus(usermail)
			if err == pgx.ErrNoRows {
				EmailVerifyStatus = false
			}

			if err != nil && err != pgx.ErrNoRows {
				res = c.ErrorAnswer(err, "Error with payment", "Ошибка верификации адреса")
				return ctx.OK(&res)
			}

		}

		if isAuction {

			if !EmailVerifyStatus {
				answer := new(ActionAnswerWithVerify)
				answer.Action = "Необходимо подтвердить адрес электронной почты"
				answer.Success = false
				answer.IsVerify = EmailVerifyStatus
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
				return ctx.OK(&res)
			}

			// Сохранить ставку в таблице ставок
			cf := tickets.Initial(c.DB, c.log, ctx.Payload.UID)

			err := cf.SaveBet(ctx.Payload.ActionUID, ticketUID[0], uint(ctx.Payload.Amount))
			if err != nil {
				answer := new(ActionAnswerWithVerify)
				c.log.Error().Err(err).Msg("Error with saving new bet")
				answer.Action = "Ошибка сохранения новой ставки"
				answer.Success = false
				answer.IsVerify = EmailVerifyStatus
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
				return ctx.OK(&res)
			}

			answer := new(ActionAnswerWithVerify)
			answer.Action = "Ваша ставка принята"
			answer.Success = true
			answer.IsVerify = true
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("payment", "Status", &answer))
			return ctx.OK(&res)
		}

		payment := pay.Initial(c.DB, ctx.Payload.UID, c.log)
		orderUID := pay.MakeShortOrder(fullOrder)

		Sber := sberbank.NewGateway(c.Payment.Username, c.Payment.Password, c.Payment.Gateway, c.log)

		order := pay.OrderRequest{
			Amount:      uint64(ctx.Payload.Amount),
			OrderNumber: orderUID,
			ReturnURL:   c.Payment.Return.ReturnURL,
			FailURL:     c.Payment.Return.FailURL,
			UserID:      ctx.Payload.UID,
			AutoPayment: false,
			Description: "Благотворительный взнос",
		}

		resp, err := Sber.RegisterCard(order)
		if err != nil {
			res = c.ErrorAnswer(err, "Error with payment", "Ошибка регистрации оплаты")
			return ctx.OK(&res)
		}

		if resp.ErrorMessage != "" {

			res = c.ErrorAnswer(err, resp.ErrorMessage, resp.ErrorMessage)
			return ctx.OK(&res)
		}

		Order := pay.PayRequest{
			NeedRefund:  false,
			SaveNewCard: false,
			Amount:      int64(ctx.Payload.Amount),
			OrderUID:    fullOrder,
			ShortOrder:  orderUID,
			BankOrder:   resp.OrderID,
		}

		err = payment.SavePay(Order)
		if err != nil {
			res = c.ErrorAnswer(err, "Error with new payment", "Ошибка сохранения платежа")
			return ctx.OK(&res)
		}

		task := queue.NewPay{UID: ctx.Payload.UID, OrderUID: fullOrder, BankOrderUID: resp.OrderID}
		err = task.Add(queue.Conf{DB: c.DB, Log: c.log})
		if err != nil {
			res = c.ErrorAnswer(err, "Error with saving new task", "Ошибка сохранения задачи")
			return ctx.OK(&res)
		}

		Answer := pay.AnswerURL{
			EmailVerified: EmailVerifyStatus,
			FormUrl:       resp.FormURL,
			OrderId:       resp.OrderID,
			Success:       true,
			Message:       "Ссылка сформирована",
		}
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Pay", "Status", &Answer))
		return ctx.OK(&res)
	}
	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Access denied"}))
	return ctx.OK(&res)
	// PaymentController_Pay: end_implement
}

// Pay runs the pay action.
func (c *PaymentController) Pay(ctx *app.PayPaymentContext) error {
	// PaymentController_Pay: start_implement

	// Put your logic here

	res := &app.MyservAuthToapp{}
	return ctx.OK(res)
	// PaymentController_Pay: end_implement
}

// jwtUserID ...
func jwtUserID(ctx context.Context) (string, error) {
	token, ok := jwt.ContextJWT(ctx).Claims.(jwtgo.MapClaims)
	if !ok {
		return "", errors.New("no claims")
	}
	userID, ok := token["userID"]
	if !ok {
		return "", errors.New("no userID in claims")
	}
	_, ok = userID.(string)
	if !ok {
		return "", errors.New("userID has wrong type")
	}
	return userID.(string), nil
}

// ErrorAnswer - union answer with error message
func (c *PaymentController) ErrorAnswer(err error, LogMessage string, AnswerMessage string) app.MyservAuthToapp {
	c.log.Error().Err(err).Msg(LogMessage)
	AnswerOne := new(ActionAnswer)
	AnswerOne.Action = AnswerMessage
	AnswerOne.Success = false
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Pay", "Status", &AnswerOne))
	return res
}

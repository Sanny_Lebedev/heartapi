package main

import (
	"strconv"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/history"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// HistoryController implements the history resource.
type HistoryController struct {
	*goa.Controller
	DB      *pgx.ConnPool
	log     logger.Logger
	PicsURL string
	Domain  string
}

// NewLikesController creates a polls controller.
func NewHistoryController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, picsUrl, domain string) (*HistoryController, error) {
	return &HistoryController{
		Controller: service.NewController("HistoryController"),
		DB:         pg,
		log:        log,
		PicsURL:    picsUrl,
		Domain:     domain,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *HistoryController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *HistoryController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Items runs the items action.
func (c *HistoryController) Items(ctx *app.ItemsHistoryContext) error {
	var err error
	var res app.MyservAuthToapp
	GetForUser := ""
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if ctx.UsersUID != nil {
		GetForUser = *ctx.UsersUID
	}
	if (ctx.UsersUID != nil) && (!MyUser.Is(user.RoleAdmin) && uid != *ctx.UsersUID) {
		c.log.Info().Msg("Wrong UID of user")
		return ctx.NotFound()
	}

	search := history.Search{}
	search.OnlyActive = false
	search.NotActive = false
	search.OnlyFinished = false
	search.OnlyWin = false
	search.Paginator = false
	search.Current = 1
	search.Onpage = 10
	search.Search = ""
	if ctx.Search != nil {
		search.Search = *ctx.Search
	}

	if ctx.OnlyActive != nil {
		search.OnlyActive = *ctx.OnlyActive
	}
	if ctx.NotActive != nil {
		search.NotActive = *ctx.NotActive
	}
	if ctx.OnlyFinished != nil {
		search.OnlyFinished = *ctx.OnlyFinished
	}
	if ctx.OnlyWin != nil {
		search.OnlyWin = *ctx.OnlyWin
	}
	if ctx.CurrentPage != nil {
		search.Paginator = true

		search.Current, err = strconv.Atoi(*ctx.CurrentPage)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with getting search.Current var")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формирования данных "
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("History", "Status", &answer))
			return ctx.OK(&res)
		}

		if ctx.OnPage != nil {
			search.Onpage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				c.log.Error().Err(err).Msg("Error with getting search on page var")
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("History", "Status", &answer))
				return ctx.OK(&res)
			}
		}
	}

	his := history.Initial(c.DB, c.log, c.PicsURL, c.Domain)
	answer := new(history.AnswerHistoryLite)
	answer, err = his.GetLiteHistory(GetForUser, search)

	if err != nil {
		c.log.Error().Err(err).Msg("Error with getting AnswerFromGetItems")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования данных "
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("History", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.Success = true
	answer.Action = " История участия "
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("History", "Status", &answer))
	return ctx.OK(&res)
	// HistoryController_Items: end_implement
}

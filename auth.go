package main

import (
	"context"
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/repositories"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
)

const (
	//TimeForAuthToken - life time for Auth token in minutes
	//	TimeForAuthToken = 2 * 24 * 60
	TimeForAuthToken = 3 * 60 * 24
	//TimeForAuthToken = 2
	//TimeForRefreshToken - life time for Refresh token in hours
	TimeForRefreshToken = 24 * 7
)

// TokenUserInfo implements the token resource
type TokenUserInfo struct {
	userID    string
	userName  string
	userType  string
	userRoles []*user.Role
	userEmail string
}

// TokenStruct service structure
type TokenStruct struct {
	UserInf   TokenUserInfo
	TokenType string
	ExpTime   float64
}

// AuthController implements the auth resource.
type AuthController struct {
	*goa.Controller
	privateKey *rsa.PrivateKey
	DB         *pgx.ConnPool
	log        logger.Logger
	Conf       configMain
}

//Generate new JWT Token
func (c *AuthController) genNewToken(ts *TokenStruct) (string, string, error) {

	token := jwtgo.New(jwtgo.SigningMethodRS512)
	u, err := uuid.NewV4()
	if err != nil {
		return "", "", errors.Wrap(err, "failed to make new UUID")
	}
	jti := u.String()

	token.Claims = jwtgo.MapClaims{
		"userID":    ts.UserInf.userID,
		"userName":  ts.UserInf.userName,
		"userType":  ts.UserInf.userType,
		"userRoles": ts.UserInf.userRoles,
		"type":      ts.TokenType,
		"iss":       "Issuer",          // who creates the token and signs it
		"aud":       "Audience",        // to whom the token is intended to be sent
		"exp":       ts.ExpTime,        // time when the token will expire (10 minutes from now)
		"jti":       jti,               // a unique identifier for the token
		"iat":       time.Now().Unix(), // when the token was issued/created (now)
		"nbf":       2,                 // time before which the token is not yet valid (2 minutes ago)
		"sub":       "subject",         // the subject/principal is whom the token is about
		"scopes":    "api:access",      // token scope - not a standard claim
	}

	outtoken, err := token.SignedString(c.privateKey)
	return outtoken, jti, err
}

func (c *AuthController) calcToken(g *TokenStruct) (string, string, error) {
	token, jti, err := c.genNewToken(g)
	if err != nil {
		return "", "", errors.Wrap(err, "failed to generate new token")
	}
	return token, jti, nil
}

// NewAuthController creates a auth controller.
func NewAuthController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, configMain configMain) (*AuthController, error) {

	b, err := ioutil.ReadFile(filepath.Join(binpath.BinaryPath(), "keys/rs256-4096-private.rsa"))
	if err != nil {
		return nil, err
	}
	privKey, err := jwtgo.ParseRSAPrivateKeyFromPEM(b)
	if err != nil {
		return nil, fmt.Errorf("jwt: failed to load private key: %s", err) // bug
	}
	return &AuthController{
		Controller: service.NewController("AuthController"),
		privateKey: privKey,
		DB:         pg,
		log:        log,
		Conf:       configMain,
	}, nil

	//return &AuthController{Controller: service.NewController("AuthController")}
}

// ChangePassword runs the change_password action.
func (c *AuthController) ChangePassword(ctx *app.ChangePasswordAuthContext) error {
	// AuthController_ChangePassword: start_implement

	// Put your logic here

	return nil
	// AuthController_ChangePassword: end_implement
}

// Recover runs the recover action.
func (c *AuthController) Recover(ctx *app.RecoverAuthContext) error {
	// AuthController_Recover: start_implement

	// Put your logic here

	return nil
	// AuthController_Recover: end_implement
}

// Getemailstatus - get status of email by UID of user
func (c *AuthController) Getemailstatus(ctx *app.GetemailstatusAuthContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleUser) || MyUser.Is(user.RoleSponsor) {
		conf := user.Conf{
			DB:  c.DB,
			Log: c.log,
		}
		answer := new(ActionAnswer)
		email, err := conf.GetMailByUID(ctx.UID)
		if err == pgx.ErrNoRows {
			answer.Success = false
			answer.Action = "Email не найден."
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
			return ctx.OK(&res)
		}

		if err != nil {
			answer.Success = false
			answer.Action = "Ошибка API"
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
			return ctx.OK(&res)
		}

		isVerify, err := conf.GetEmailStatus(email)
		if err == pgx.ErrNoRows {
			answer.Success = false
			answer.Action = "Адрес не подтвержден"
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
			return ctx.OK(&res)
		}

		if err != nil {
			answer.Success = false
			answer.Action = "Ошибка API"
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
			return ctx.OK(&res)
		}

		answer.Success = isVerify
		answer.Action = "Адрес подтвержден"
		if !isVerify {
			answer.Action = "Адрес не подтвержден"
		}

		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
		return ctx.OK(&res)

	}
	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
	return ctx.OK(&res)

}

// Checkemailcheck - check email by code notification
func (c *AuthController) Checkemailcheck(ctx *app.CheckemailcheckAuthContext) error {
	conf := user.Conf{
		DB:  c.DB,
		Log: c.log,
	}

	isOk, err := conf.CheckMailCode(ctx.Payload.Email, ctx.Payload.Check)

	if err != nil || !isOk {
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Некорректный код. Попробуйте верифицировать письмо снова"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
		return ctx.OK(&res)
	}
	answer := new(ActionAnswer)
	answer.Success = true
	answer.Action = "Ваш адрес проверен. Спасибо!"
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
	return ctx.OK(&res)
}

// Checkemailsend - check email by code notification
func (c *AuthController) Checkemailsend(ctx *app.CheckemailsendAuthContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleUser) || MyUser.Is(user.RoleSponsor) {

		conf := user.Conf{
			DB:  c.DB,
			Log: c.log,
		}

		var email string
		if ctx.Payload.Email != nil {
			email = *ctx.Payload.Email
		} else {
			var err error
			email, err = conf.GetMailByUID(ctx.Payload.UID)
			if err == pgx.ErrNoRows {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Email не найден."
				res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
				return ctx.OK(&res)
			}

			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка API"
				res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
				return ctx.OK(&res)
			}

		}

		gate := user.MailGateway(c.Conf.Email)
		myemail := user.InitMail(email, conf, gate)
		var err error
		_, err = myemail.MakeValidCode(email)
		if err != nil {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формирования письма"
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
			return ctx.OK(&res)
		}
		answer := new(ActionAnswer)
		answer.Success = true
		answer.Action = "Письмо отправлено на Ваш адрес"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Status", &answer))
		return ctx.OK(&res)

	}

	res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
	return ctx.OK(&res)
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *AuthController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *AuthController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

// Refresh runs the refresh action.
func (c *AuthController) Refresh(ctx *app.RefreshAuthContext) error {
	// AuthController_Refresh: start_implement

	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	var refreshTokenID string
	var Approle string

	if role, ok := claims["userType"]; ok {
		switch role.(type) {
		case string:
			Approle = role.(string)
		default:
			c.log.Warn().Interface("claims", claims).Msg("userType claim has wrong type")
			res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong refresh token"}))
			return ctx.Unauthorized(&res)
		}
	} else {
		c.log.Warn().Interface("claims", claims).Msg("userType claim has wrong type")

		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong refresh token"}))
		return ctx.Unauthorized(&res)
	}

	if jti, ok := claims["jti"]; ok {
		switch jti.(type) {
		case string:
			refreshTokenID = jti.(string)
		default:
			c.log.Warn().Interface("claims", claims).Msg("jti claim has wrong type")
			res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong refresh token"}))
			return ctx.Unauthorized(&res)
		}
	} else {
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong refresh token"}))
		c.log.Warn().Interface("claims", claims).Msg("jti claim is missing")
		return ctx.Unauthorized(&res)
	}

	var uid string
	// c.log.Info().Str("tokenID", refreshTokenID).Msg("got refresh token")
	// c.log.Info().Str("tokenID", Approle).Msg("got refresh role")
	uid, _ = repositories.CheckRefreshTokenExist(c.DB, refreshTokenID, Approle)
	if uid == "" {
		c.log.Warn().Interface("claims", claims).Msg("jti or userType claim is wrong")
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong refresh token"}))
		return ctx.Unauthorized(&res)
	}

	u, err := user.GetUserByUID(c.DB, uid)
	if err != nil {
		c.log.Error().Err(err).Str("uid", uid).Msg("failed to get user by UID")
		return ctx.InternalServerError()
	}

	newuserRoles, err := user.GetUserRolebyID(uid, c.DB)
	if err != nil {
		c.log.Error().Err(err).Str("uid", uid).Msg("failed to get user role by ID")
		return ctx.InternalServerError()
	}

	userFullName := u.Info.Family + " " + u.Info.Name

	currentTokenInfo := TokenUserInfo{
		userID:   uid,
		userName: userFullName,
		//		userType:  *u.MainRole,
		userRoles: newuserRoles,
	}

	ts := TokenStruct{
		UserInf:   currentTokenInfo,
		TokenType: "AuthToken",
		ExpTime:   float64(time.Now().Add(time.Hour * TimeForRefreshToken).Unix()),
	}
	AccessTokenString, _, err := c.calcToken(&ts)
	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc access token")
		return ctx.InternalServerError()
	}

	ts = TokenStruct{currentTokenInfo, "RefreshToken", float64(time.Now().Add(time.Hour * TimeForRefreshToken).Unix())}
	RefreshTokenString, jti, err := c.calcToken(&ts)

	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc refresh token")
		return ctx.InternalServerError()
	}

	//Making record for refresh tokens table on DB
	out, err := repositories.RefreshTokenToDatabase(c.DB, uid, jti, Approle)
	if err != nil || out == false {
		c.log.Error().Err(err).Msg("failed to refresh tokein in db")
		return ctx.InternalServerError()
	}

	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Tokens", &app.MyservAuthToken{
		AccessToken:  AccessTokenString,
		RefreshToken: RefreshTokenString,
	}))
	return ctx.OK(&res)
	// AuthController_Refresh: end_implement
}

// Fromsocnet - registrations by token from FB or VK
func (c *AuthController) Fromsocnet(ctx *app.FromsocnetAuthContext) error {

	//	MyUser := user.NewUser(c.DB, c.log)

	newCOF := user.SocNetCFG{
		Facebook: user.Fb(c.Conf.SocialNetworks.Facebook),
		VK:       user.Vk(c.Conf.SocialNetworks.VK),
		Google:   user.Gl(c.Conf.SocialNetworks.Google),
	}

	MyUser := user.NewUserSN(c.DB, c.log, newCOF)
	var newErr error
	MyUser.UID, _, newErr = MyUser.CheckInSocnetwork(ctx.Payload.Socnetuid, user.Socnet(ctx.Payload.Provider))
	if newErr != nil {
		c.log.Error().Err(newErr).Msg("User registration failed via social network")
		return ctx.BadRequest(goa.ErrBadRequest(newErr))

	}

	UserUID := ""

	//Making record for refresh tokens table on DB
	if ctx.Payload.Provider != user.NetFB.String() && ctx.Payload.Provider != user.NetVK.String() {
		c.log.Info().Msg("Wrong socnet provider")
		return ctx.InternalServerError()

	}

	var UsData user.NetUser
	var errDef error
	switch ctx.Payload.Provider {
	case user.NetFB.String():
		{
			Prov := user.FBNetwork{AccessToken: ctx.Payload.Token, UID: ctx.Payload.Socnetuid}
			UsData, errDef = Prov.GetInfo(MyUser)
			if errDef != nil {
				c.log.Error().Err(errDef).Msg("Error with getting datas from FB")
				return ctx.BadRequest(goa.ErrBadRequest(errDef))
			}
			break
		}
	case user.NetVK.String():
		{
			Prov := user.VKNetwork{AccessToken: ctx.Payload.Token, UID: ctx.Payload.Socnetuid}
			UsData, errDef = Prov.GetInfo(MyUser)
			if errDef != nil {
				c.log.Error().Err(errDef).Msg("Error with getting datas from FB")
				return ctx.BadRequest(goa.ErrBadRequest(errDef))
			}
			break
		}
	default:
		{
			c.log.Info().Msg("Wrong socnet provider")
			return ctx.InternalServerError()
			break
		}
	}

	var addErr error

	UserUID, addErr = MyUser.AddUserToDB(*ctx.Payload, UsData)

	// UserUID, addErr = MyUser.AddUserToSocnet(*ctx.Payload, UsData)
	if addErr != nil {
		c.log.Error().Err(addErr).Msg("User registration failed via social network")
		return ctx.BadRequest(goa.ErrBadRequest(addErr))
	}

	errDef = MyUser.SetCorrectBirthday(UserUID, UsData)
	// errDef = MyUser.SetCorrectBD(UserUID, UsData)
	if errDef != nil {
		c.log.Error().Err(errDef).Msg("Error with setting datas")
		return ctx.BadRequest(goa.ErrBadRequest(errDef))
	}

	// if ctx.Payload.Provider == user.NetFB.String() {
	// 	UsData, errDef := MyUser.GetBDFromFB(ctx.Payload.Socnetuid, ctx.Payload.Token)
	// 	if errDef != nil {
	// 		c.log.Error().Err(errDef).Msg("Error with getting datas from FB")
	// 		return ctx.BadRequest(goa.ErrBadRequest(errDef))
	// 	}
	// 	var addErr error
	// 	UserUID, addErr = MyUser.AddUserToSocnet(*ctx.Payload, UsData)
	// 	if addErr != nil {
	// 		c.log.Error().Err(addErr).Msg("User registration failed via social network")
	// 		return ctx.BadRequest(goa.ErrBadRequest(addErr))
	// 	}

	// 	errDef = MyUser.SetCorrectBD(UserUID, UsData)
	// 	if errDef != nil {
	// 		c.log.Error().Err(errDef).Msg("Error with setting datas")
	// 		return ctx.BadRequest(goa.ErrBadRequest(errDef))
	// 	}

	// }

	// if ctx.Payload.Provider == user.NetVK.String() {
	// 	UsData, errDef := MyUser.GetBDFromVK(ctx.Payload.Socnetuid, ctx.Payload.Token)
	// 	if errDef != nil {
	// 		c.log.Error().Err(errDef).Msg("Error with getting datas from FB")
	// 		return ctx.BadRequest(goa.ErrBadRequest(errDef))
	// 	}
	// 	var addErr error
	// 	UserUID, addErr = MyUser.AddUserToSocnetVK(*ctx.Payload, UsData)
	// 	if addErr != nil {
	// 		c.log.Error().Err(addErr).Msg("User registration failed via social network")
	// 		return ctx.BadRequest(goa.ErrBadRequest(addErr))
	// 	}

	// 	// errDef = MyUser.SetCorrectBD(UserUID, UsData)
	// 	// if errDef != nil {
	// 	// 	c.log.Error().Err(errDef).Msg("Error with setting datas")
	// 	// 	return ctx.BadRequest(goa.ErrBadRequest(errDef))
	// 	// }

	// }

	MyUser.UID = UserUID
	err := MyUser.AddToRoletab()
	if err != nil {
		c.log.Error().Err(err).Msg("User registration failed")
		return ctx.InternalServerError()
	}

	if MyUser.GetUserRoles() != nil {
		c.log.Error().Err(err).Msg("User registration failed")
		return ctx.InternalServerError()
	}

	currentTokenInfo := TokenUserInfo{
		userID:    MyUser.UID,
		userName:  string(MyUser.Info.Name) + " " + string(MyUser.Info.Family),
		userType:  MyUser.RoleName.String(),
		userRoles: MyUser.Roles,
		userEmail: MyUser.Info.Email,
	}

	ts := TokenStruct{
		UserInf:   currentTokenInfo,
		TokenType: "AuthToken",
		ExpTime:   float64(time.Now().Add(time.Minute * TimeForAuthToken).Unix()),
	}

	AccessTokenString, _, err := c.calcToken(&ts)
	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc access token")
		return ctx.InternalServerError()
	}
	ts = TokenStruct{
		UserInf:   currentTokenInfo,
		TokenType: "RefreshToken",
		ExpTime:   float64(time.Now().Add(time.Hour * TimeForRefreshToken).Unix()),
	}

	RefreshTokenString, jti, err := c.calcToken(&ts)
	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc refresh token")
		return ctx.InternalServerError()
	}

	//Making record for refresh tokens table on DB
	if err = MyUser.SaveRefreshToken(jti); err != nil {
		c.log.Error().Err(err).Msg("failed to register user")
		return ctx.InternalServerError()

	}

	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Tokens", &app.MyservAuthToken{
		AccessToken:  AccessTokenString,
		RefreshToken: RefreshTokenString,
	}))
	return ctx.OK(&res)

}

// Register runs the register action.
func (c *AuthController) Register(ctx *app.RegisterAuthContext) error {
	// AuthController_Register: start_implement
	success := false
	MyUser := user.NewUser(c.DB, c.log)
	MyUser.LoadFromRegisterContext(ctx)
	MyUser.RoleActive = true
	MyUser.RoleName = user.RoleUser

	isDBL, err := MyUser.CheckingForSimilar()
	if err != nil && err != pgx.ErrNoRows {
		Ans := ActionAnswer{Success: false, Action: `Ошибка регистрации`}
		c.log.Error().Err(err).Msg("Registration error")
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Register", "Status", &Ans))
		return ctx.OK(&res)
	}

	if isDBL {
		Ans := ActionAnswer{Success: false, Action: `Пользователь с такой почтой уже существует`}
		c.log.Error().Msg("An attempt to register a user using a registered email address")
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Register", "Status", &Ans))
		return ctx.OK(&res)

	}

	if MyUser.CheckingForEmptyUsers() {
		//The process of creating the first record with administrator rights
		c.log.Info().Msg("The process of creating the first record with administrator rights")
		MyUser.RoleName = user.RoleAdmin

	}

	success, err = MyUser.SaveNewUser()
	if err != nil || success == false {
		c.log.Error().Err(err).Msg("User registration failed")
		return ctx.InternalServerError()
	}

	err = MyUser.AddToRoletab()
	if err != nil {
		c.log.Error().Err(err).Msg("User registration failed")
		return ctx.InternalServerError()
	}

	if MyUser.GetUserRoles() != nil {
		c.log.Error().Err(err).Msg("User registration failed")
		return ctx.InternalServerError()
	}

	currentTokenInfo := TokenUserInfo{
		userID:    MyUser.UID,
		userName:  string(MyUser.Info.Name) + " " + string(MyUser.Info.Family),
		userType:  MyUser.RoleName.String(),
		userRoles: MyUser.Roles,
		userEmail: MyUser.Info.Email,
	}

	ts := TokenStruct{
		UserInf:   currentTokenInfo,
		TokenType: "AuthToken",
		ExpTime:   float64(time.Now().Add(time.Minute * TimeForAuthToken).Unix()),
	}

	AccessTokenString, _, err := c.calcToken(&ts)
	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc access token")
		return ctx.InternalServerError()
	}
	ts = TokenStruct{
		UserInf:   currentTokenInfo,
		TokenType: "RefreshToken",
		ExpTime:   float64(time.Now().Add(time.Hour * TimeForRefreshToken).Unix()),
	}

	RefreshTokenString, jti, err := c.calcToken(&ts)
	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc refresh token")
		return ctx.InternalServerError()
	}

	//Making record for refresh tokens table on DB
	if err = MyUser.SaveRefreshToken(jti); err != nil {
		c.log.Error().Err(err).Msg("failed to register user")
		return ctx.InternalServerError()

	}

	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Tokens", &app.MyservAuthToken{
		AccessToken:  AccessTokenString,
		RefreshToken: RefreshTokenString,
	}))
	return ctx.OK(&res)
	// AuthController_Register: end_implement
}

// Signin runs the signin action.
func (c *AuthController) Signin(ctx *app.SigninAuthContext) error {
	// AuthController_Signin: start_implement
	MyUser := user.NewUser(c.DB, c.log)
	MyUser.LoadFromSigninContext(ctx)
	emailError := MyUser.GetByEmail()
	if emailError != nil {
		c.log.Error().Err(emailError).Msg("Error with sign in")
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UserSigninAnswer{UID: "", Usertype: "unknown", Message: "Неверный логин или пароль"}))
		return ctx.OK(&res)
	}

	if MyUser.CheckPassword() == false {
		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UserSigninAnswer{UID: "", Usertype: "unknown", Message: "Неверный логин или пароль"}))
		return ctx.OK(&res)
	}

	currentTokenInfo := TokenUserInfo{
		userID:    MyUser.UID,
		userName:  string(MyUser.Info.Name) + " " + string(MyUser.Info.Family),
		userType:  MyUser.RoleName.String(),
		userRoles: MyUser.Roles,
		userEmail: MyUser.Info.Email,
	}

	ts := TokenStruct{
		UserInf:   currentTokenInfo,
		TokenType: "AuthToken",
		ExpTime:   float64(time.Now().Add(time.Minute * TimeForAuthToken).Unix()),
	}

	AccessTokenString, _, err := c.calcToken(&ts)
	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc access token")
		return ctx.InternalServerError()
	}
	ts = TokenStruct{
		UserInf:   currentTokenInfo,
		TokenType: "RefreshToken",
		ExpTime:   float64(time.Now().Add(time.Hour * TimeForRefreshToken).Unix()),
	}

	RefreshTokenString, jti, err := c.calcToken(&ts)
	if err != nil {
		c.log.Error().Err(err).Msg("failed to calc refresh token")
		return ctx.InternalServerError()
	}

	//Making record for refresh tokens table on DB
	if err = MyUser.SaveRefreshToken(jti); err != nil {
		c.log.Error().Err(err).Msg("failed to register user")
		return ctx.InternalServerError()

	}

	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Auth", "Tokens", &app.MyservAuthToken{
		AccessToken:  AccessTokenString,
		RefreshToken: RefreshTokenString,
	}))
	return ctx.OK(&res)
	// AuthController_Signin: end_implement
}

// Validate runs the validate action.
func (c *AuthController) Validate(ctx *app.ValidateAuthContext) error {
	// AuthController_Validate: start_implement

	// Put your logic here

	res := &app.MyservAuthToapp{}
	return ctx.OK(res)
	// AuthController_Validate: end_implement
}

// NewAuthJWTMiddleware creates a middleware that checks for the presence of a JWT Authorization header
// and validates its content. A real app would probably use goa's JWT security middleware instead.
//
// Note: the code below assumes the example is compiled against the master branch of goa.
// If compiling against goa v1 the call to jwt.New needs to be:
//
func NewAuthJWTMiddleware() (goa.Middleware, error) {
	keys, err := LoadAuthJWTPublicKeys()
	if err != nil {
		return nil, err
	}
	return jwt.New(jwt.NewSimpleResolver(keys), ForceFail(), app.NewJWTSecurity()), nil
}

// LoadAuthJWTPublicKeys loads PEM encoded RSA public keys used to validata and decrypt the JWT.
func LoadAuthJWTPublicKeys() ([]jwt.Key, error) {
	keyFiles, err := filepath.Glob(filepath.Join(binpath.BinaryPath(), "keys/*.pem"))
	if err != nil {
		return nil, err
	}
	keys := make([]jwt.Key, len(keyFiles))
	for i, keyFile := range keyFiles {
		pem, err := ioutil.ReadFile(keyFile)
		if err != nil {
			return nil, err
		}
		key, err := jwtgo.ParseRSAPublicKeyFromPEM([]byte(pem))
		if err != nil {
			return nil, fmt.Errorf("failed to load key %s: %s", keyFile, err)
		}
		keys[i] = key
	}
	if len(keys) == 0 {
		return nil, errors.New("couldn't load public keys for JWT security")
	}

	return keys, nil
}

// ForceFail is a middleware illustrating the use of validation middleware with JWT auth.  It checks
// for the presence of a "fail" query string and fails validation if set to the value "true".
func ForceFail() goa.Middleware {
	errValidationFailed := goa.NewErrorClass("validation_failed", 401)
	forceFail := func(h goa.Handler) goa.Handler {
		return func(ctx context.Context, rw http.ResponseWriter, req *http.Request) error {
			if f, ok := req.URL.Query()["fail"]; ok {
				if f[0] == "true" {
					return errValidationFailed("forcing failure to illustrate Validation middleware")
				}
			}
			return h(ctx, rw, req)
		}
	}
	fm, _ := goa.NewMiddleware(forceFail)
	return fm
}

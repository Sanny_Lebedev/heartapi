package repositories

import (
	"fmt"

	"github.com/jackc/pgx"
)

//NewDBfromRToken - creating new table for refresh token
func NewDBfromRToken(db *pgx.ConnPool, tablename, uid, jid string) error {
	const sqlstr = `
		CREATE TABLE IF NOT EXISTS $1 (
		uid     uuid UNIQUE,
		jti		uuid 
	);
	`

	_, err := db.Exec(sqlstr, tablename)
	if err != nil {
		return fmt.Errorf("Problems with inserting to DB: %s", err) // internal error
	}

	return nil
}

//AddNewRtokenRecord - adding new record to DB
func AddNewRtokenRecord(db *pgx.ConnPool, uid, jid, appType string) (string, error) {

	sqlStatement := `DELETE FROM reftokens WHERE uid = $1 and role = $2;`
	_, err := db.Exec(sqlStatement, uid, appType)
	if err != nil {
		return "", fmt.Errorf("Problems with deleting to DB: %s", err) // internal error
	}

	const sqlstr = `
	insert into reftokens (
        uid,
		jti,
		role        
    ) values (
        $1,
		$2, 
		$3
    ) RETURNING uid
	`
	var uidd string
	err = db.QueryRow(sqlstr, uid, jid, appType).Scan(&uidd)
	if err != nil {
		return "", fmt.Errorf("Problems with inserting to DB: %s", err) // internal error
	}
	return uidd, err
}

//CheckDB checks for table in DB
func CheckDB(db *pgx.ConnPool, tablename string) (bool, error) {

	const sqlstr = `select * from pg_tables where tablename = $1`
	var exists bool
	err := db.QueryRow(sqlstr, tablename).Scan(&exists)

	return exists, err
}

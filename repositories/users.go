package repositories

import (
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// RoleItem ...
type RoleItem struct {
	Role   string `db:"usersroletab.role" json:"role"`
	Active bool   `db:"usersroletab.active" json:"active"`
}

// GetUserRolebyID - gettign infromation about current user by phone number
func GetUserRolebyID(db *pgx.ConnPool, uid string) ([]*RoleItem, error) {

	rows, err := db.Query(
		`SELECT usersroletab.role, usersroletab.active
		FROM users INNER JOIN usersroletab ON usersroletab.uid=users.uid
		WHERE users.uid = $1`,
		uid,
	)
	if err != nil {
		return nil, errors.Wrap(err, "query failed")
	}
	roles := make([]*RoleItem, 0)
	for rows.Next() {
		role := &RoleItem{}
		err = rows.Scan(&role.Role, &role.Active)
		if err != nil {
			return nil, errors.Wrap(err, "failed to scan result row")
		}
		roles = append(roles, role)
	}

	return roles, nil
}

// CheckRefreshTokenExist ...
func CheckRefreshTokenExist(db *pgx.ConnPool, jti string, Approle string) (string, error) {
	var uid string
	err := db.QueryRow(`SELECT uid FROM reftokens WHERE jti = $1 AND role = $2`, jti, Approle).Scan(&uid)
	return uid, err
}

// DeleteFromUserrole - deleting from user role tab
func DeleteFromUserrole(db *pgx.ConnPool, uid string, role string) error {
	_, err := db.Exec(`DELETE FROM UsersRoleTab WHERE uid = $1 and role = $2`, uid, role)
	return err
}

// AddUserToRoletab - add new user's role
func AddUserToRoletab(db *pgx.ConnPool, uid, role, active string) error {

	// Delete if exists
	err := DeleteFromUserrole(db, uid, role)
	if err != nil {
		return errors.Wrap(err, "failed to delete from userrole")
	}

	_, err = db.Exec(`INSERT INTO UsersRoleTab (uid, role, active) VALUES ($1, $2, $3)`, uid, role, active)
	return err
}

// AddUsersTemppass - adding temp pass to user's tab
func AddUsersTemppass(db *pgx.ConnPool, uid, temppass string) error {
	_, err := db.Exec(
		`UPDATE users SET temppass = $2, endtemppass = CURRENT_TIMESTAMP + INTERVAL '30 min' WHERE uid = $1`,
		uid, temppass,
	)
	return err
}

// AddUserToDatabase creates a new user
func AddUserToDatabase(db *pgx.ConnPool, firstName, lastName, email, phone, password, mainrole string) (string, error) {
	uid := ``
	return uid, nil

}

// RefreshTokenToDatabase - refreshing token for user on DB
func RefreshTokenToDatabase(db *pgx.ConnPool, uid, jti, appType string) (bool, error) {

	_, err := AddNewRtokenRecord(db, uid, jti, appType)
	if err != nil {
		return false, errors.Wrap(err, "failed to add refresh token to db")
	}

	return true, nil
}

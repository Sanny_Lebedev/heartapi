package main

import (
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/ibase"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/jobs"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	// "io"
	// "mime/multipart"
	// "os"
	// "path/filepath"
	// "strconv"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"

	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"

	"github.com/pkg/errors"
	// "bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	// mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"

	"bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
)

// ActionsController implements the sections resource.
type ActionsController struct {
	*goa.Controller
	DB      *pgx.ConnPool
	log     logger.Logger
	PicsURL string
	Options Options
	Conf    configMain
}

// NewActionsController creates a actions controller.
func NewActionsController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, picsUrl string, options Options, configMain configMain) (*ActionsController, error) {
	return &ActionsController{
		Controller: service.NewController("ActionsController"),
		DB:         pg,
		log:        log,
		PicsURL:    picsUrl,
		Options:    options,
		Conf:       configMain,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *ActionsController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *ActionsController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Cansend runs the cansend action.
func (c *ActionsController) Cansend(ctx *app.CansendActionsContext) error {
	// ActionsController_Cansend: start_implement

	// Put your logic here

	res := &app.MyservAuthToapp{}
	return ctx.OK(res)
	// ActionsController_Cansend: end_implement
}

// Changeactive runs the changeactive action.
func (c *ActionsController) Changeactive(ctx *app.ChangeactiveActionsContext) error {

	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)

	iAction := ibase.Actions{}
	iAction = iAction.SetStruct(c.DB, c.log, *ctx.Payload.UID)
	isExist := iAction.IsExist()

	if !isExist {
		answer.Action = "Акция не найдена."
		answer.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) {
		err := actions.ChangeActive(c.DB, c.log, *ctx.Payload.UID, c.Conf.ForIOS.KeyID, c.Conf.ForIOS.TeamID)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with change actions status")
			answer.Action = "Ошибка изменения статуса акции"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
		answer.Action = "Статус акции изменен"
		answer.Success = true
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.Action = "Ошибка доступа к разделу."
	answer.Success = false
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

// Deleteitems runs the deleteitems action.
func (c *ActionsController) Deleteitems(ctx *app.DeleteitemsActionsContext) error {
	var err error
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)

	iAction := ibase.Actions{}
	iAction = iAction.SetStruct(c.DB, c.log, *ctx.Payload.UID)
	isExist := iAction.IsExist()

	if !isExist {
		answer.Action = "Акция не найдена."
		answer.Success = false
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
		ThinkOwner, err := actions.GetPhotoOwner(c.DB, *ctx.Payload.UID)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with checking actions owner")
		}

		if err != nil || ThinkOwner != MyUser.UID {
			answer.Action = "Ошибка - ограничение доступа."
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
	}

	if !MyUser.Is(user.RoleAdmin) && !MyUser.Is(user.RoleSponsor) {
		answer.Success = false
		answer.Action = "Ошибка удаления акции"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	err = actions.Delete(c.DB, c.log, *ctx.Payload.UID)
	if err != nil {
		c.log.Error().Err(err).Msg("Error forming fundraising info")
		answer.Success = false
		answer.Action = "Ошибка удаления акции"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Action = "Акция удалена"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

// Deletevideo runs the deletephoto action.
func (c *ActionsController) Deletevideo(ctx *app.DeletevideoActionsContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(ActionAnswer)

	if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
		ThinkOwner, err := actions.GetPhotoOwner(c.DB, *ctx.Payload.UID)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with checking actions owner")
		}

		if err != nil || ThinkOwner != MyUser.UID {
			answer.Action = "Доступ к разделу ограничен"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {

		var err error
		pathtofile := filepath.Join(binpath.BinaryPath(), "images/video/"+actions.Area)
		myfilename := *ctx.Payload.UID + ".mp4"
		err = os.Remove(filepath.Join(pathtofile, myfilename))
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}
		Actions := actions.Initial(c.DB, MyUser.UID, "", c.log)

		// TODO: delete video from DB
		err = Actions.DeleteActionVideo(*ctx.Payload.UID)
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}

		answer.Success = true
		answer.Action = "Видео успешно удалено"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Actions", "Status", &answer))
		return ctx.OK(&res)

	}
	answer.Success = false
	answer.Action = "Доступ к разделу ограничен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Actions", "Status", &answer))
	return ctx.OK(&res)
}

// Deletephoto runs the deletephoto action.
func (c *ActionsController) Deletephoto(ctx *app.DeletephotoActionsContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	// Only admin can generate new discounts
	answer := new(ActionAnswer)

	if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
		ThinkOwner, err := actions.GetPhotoOwner(c.DB, *ctx.Payload.UID)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with checking actions owner")
		}

		if err != nil || ThinkOwner != MyUser.UID {
			answer.Action = "Доступ к разделу ограничен"
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {

		var err error
		pathtofile := filepath.Join(binpath.BinaryPath(), "images/"+actions.Area)
		myfilename := *ctx.Payload.UID + ".jpg"
		err = os.Remove(filepath.Join(pathtofile, myfilename))
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}
		Actions := actions.Initial(c.DB, MyUser.UID, "", c.log)

		err = Actions.DeleteImageByUID(*ctx.Payload.UID)
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}

		answer.Success = true
		answer.Action = "Изображение успешно удалено"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Actions", "Status", &answer))
		return ctx.OK(&res)

	}
	answer.Success = false
	answer.Action = "Доступ к разделу ограничен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Actions", "Status", &answer))
	return ctx.OK(&res)
}

// Item runs the item action.
func (c *ActionsController) Item(ctx *app.ItemActionsContext) error {
	var err error
	usersUID := ""
	answer := new(actions.AnswerOne)
	if ctx.UsersUID != nil {
		usersUID = *ctx.UsersUID
	}

	answer.Info, err = actions.Getone(c.DB, c.PicsURL, ctx.UID, usersUID, c.Options.Domain)
	if err != nil {
		c.log.Error().Err(err).Msg("Error forming actions info")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования данных"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Action = "Информация об акции"
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

// News runs the News action.
func (c *ActionsController) News(ctx *app.NewsActionsContext) error {
	var res app.MyservAuthToapp
	var err error
	var TypeOfAction string
	answer := new(actions.Answer)
	getUID := ""
	activeFlag := true

	if ctx.TypeOfAction != nil {
		TypeOfAction = *ctx.TypeOfAction
	} else {
		TypeOfAction = "ALL"
	}

	if ctx.UsersUID != nil {
		getUID = *ctx.UsersUID
	}

	if ctx.Active != nil {
		activeFlag = *ctx.Active
	}

	answer.List, err = actions.GetNews(c.DB, c.PicsURL, getUID, activeFlag, TypeOfAction)

	if err != nil {
		c.log.Error().Err(err).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Count = len(answer.List)
	answer.TotalCount = answer.Count
	answer.HasNext = false
	answer.Action = "Список новых акций"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

// Items runs the items action.
func (c *ActionsController) Items(ctx *app.ItemsActionsContext) error {
	var res app.MyservAuthToapp
	var err error
	var SearchUID = ""
	var TypeOfAction string

	answer := new(actions.Answer)

	search := new(actions.Search)

	search.Paginator = false

	if ctx.TypeOfAction != nil {
		TypeOfAction = *ctx.TypeOfAction
	} else {
		TypeOfAction = "ALL"
	}

	if ctx.UID != nil {
		SearchUID = *ctx.UID
	}

	if ctx.Search != nil {
		search.Search = *ctx.Search

	}
	MyUserUID := ""
	if ctx.UsersUID != nil {
		MyUserUID = *ctx.UsersUID
	}

	search.Onpage = 0
	search.Current = 0
	search.Paginator = false
	search.OnlyActive = false
	search.NotActive = false

	if ctx.OnlyActive != nil {
		search.OnlyActive = *ctx.OnlyActive
	}

	if ctx.NotActive != nil {
		search.NotActive = *ctx.NotActive
	}

	if ctx.OnPage != nil && ctx.CurrentPage != nil {
		if *ctx.OnPage != "" && *ctx.CurrentPage != "" {
			search.Onpage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Current, err = strconv.Atoi(*ctx.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Paginator = true
		}
	}

	answer.List, answer.TotalCount, err = actions.Get(c.DB, c.PicsURL, SearchUID, *search, MyUserUID, c.Options.Domain, TypeOfAction)

	if err != nil {
		c.log.Error().Err(err).Msg("Error forming section list")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка формирования списка"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	answer.Success = true
	answer.Count = len(answer.List)
	answer.HasNext = false
	if search.Onpage*search.Current < answer.TotalCount {
		answer.HasNext = true
	}
	answer.Action = "Список акций сбора"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

// Link runs the Link action.
func (c *ActionsController) Link(ctx *app.LinkActionsContext) error {
	var res app.MyservAuthToapp
	action, err := actions.GetLink(c.DB, ctx.Link)
	if err != nil && err != pgx.ErrNoRows {
		c.log.Error().Err(err).Msg("Error with geting action by link")
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Акция не найдена"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	answer := new(actions.AnsWithUID)
	answer.UID = action
	answer.Success = true
	answer.Action = "Акция найдена"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

// Senditems runs the senditems action.
func (c *ActionsController) Senditems(ctx *app.SenditemsActionsContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) && uid != ctx.Payload.MaecenasUID {
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Вы указали некорректного мецената"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	iMaecenas := ibase.Maecenas{}
	iMaecenas = iMaecenas.SetStruct(c.DB, c.log, ctx.Payload.MaecenasUID)
	if iMaecenas.IsExist() == false {
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Вы указали некорректного мецената"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {
		var err error

		iFundraising := ibase.Fundraising{}
		iFundraising = iFundraising.SetStruct(c.DB, c.log, ctx.Payload.FundraisingUID)
		isExist := iFundraising.IsExist()

		if !isExist {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Сбор средств не найден. Ошибка сохранения"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
		answer := new(actions.AnswerWithUID)
		Actions := actions.New(c.DB, ctx, uid, c.log)

		// Verification of the right to edit information by the patron
		if ctx.Payload.UID != nil {
			if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) && *ctx.Payload.UID != "" {
				isOk, err := Actions.CheckSender()
				if err != nil {
					c.log.Error().Err(err).Msg("Error with checking actions owner")
				}

				if err != nil || !isOk {
					answer.Action = "Ошибка - ограничение доступа."
					answer.Success = false
					res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
					return ctx.OK(&res)
				}
			}
		}

		answer.UID, err = Actions.Save()
		if err != nil && err != pgx.ErrNoRows {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка сохранения данных"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}

		// make link
		var link string
		link, err = actions.MakeLink(c.DB, answer.UID)
		answer.Alert.Message = "Спасибо за созданную акцию! После проверки акция станет доступна нашим посетителям. Не забудьте разместить ссылку в социальных сетях"
		answer.Alert.Link = c.Options.Domain + "/link/" + link
		answer.Info, err = actions.Getone(c.DB, c.PicsURL, answer.UID, ctx.Payload.MaecenasUID, c.Options.Domain)
		if err != nil {
			c.log.Error().Err(err).Msg("Error forming actions info")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формирования данных"
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}

		if answer.Info.Flags.Active {
			err = jobs.Notification(c.DB, c.log, answer.Info.UID, answer.Info.FundraisingUID, c.Conf.ForIOS.KeyID, c.Conf.ForIOS.TeamID)
			if err != nil {
				c.log.Error().Err(err).Msg("Error with notofication about new action")
			}
		}

		answer.Success = true
		answer.Action = "Акция успешно сохранена"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Ошибка доступа к разделу"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)

}

// Sendvideo runs the Sendvideo action.
func (c *ActionsController) Sendvideo(ctx *app.SendvideoActionsContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	reader, _ := ctx.MultipartReader()
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {

		Action := actions.Initial(c.DB, MyUser.UID, *ctx.UID, c.log)

		// Verification of the right to edit information by the patron
		if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
			isOk, err := Action.CheckSender()
			if err != nil {
				c.log.Error().Err(err).Msg("Error with checking actions owner")
			}

			if err != nil || !isOk {
				answer.Action = "Ошибка сохранения видео."
				answer.Success = false
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
				return ctx.OK(&res)
			}
		}

		iAction := ibase.Actions{}
		iAction = iAction.SetStruct(c.DB, c.log, *ctx.UID)
		isExist := iAction.IsExist()

		if !isExist {
			answer.Action = "Акция не найдена."
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}

		conflag := 0

		if reader != nil {

			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/video/toconvert")

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				if err == io.EOF {
					break
				}
				if err != nil {
					break
					// c.log.Error().Err(err).Msg("failed to load part")
					// return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))
				}

				if p.Header["Content-Type"][0] != contentTypeVideo_mp4 &&
					p.Header["Content-Type"][0] != contentTypeVideo_flv &&
					p.Header["Content-Type"][0] != contentTypeVideo_m3u8 &&
					p.Header["Content-Type"][0] != contentTypeVideo_3gp &&
					p.Header["Content-Type"][0] != contentTypeVideo_mov &&
					p.Header["Content-Type"][0] != contentTypeVideo_avi &&
					p.Header["Content-Type"][0] != contentTypeVideo_wmv {

					return ctx.BadRequest(goa.ErrBadRequest("wrong video type"))
				}

				// saveErr := Action.SaveActionVideo(*ctx.UID)
				// if saveErr != nil {
				// 	return ctx.BadRequest(goa.ErrBadRequest(saveErr))
				// }

				f, err := os.OpenFile(filepath.Join(pathtofile, p.FileName()), os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					c.log.Error().Err(err).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()
				io.Copy(f, p)

				saveErr := Action.SaveActionVideoToConvert(*ctx.UID, p.FileName())
				if saveErr != nil {
					return ctx.BadRequest(goa.ErrBadRequest(saveErr))
				}
				conflag = conflag + 1
			}
		}

		answer.Success = true
		answer.Action = "Видео загружено"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.Success = false
	answer.Action = "Ошибка доступа к разделу"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

// Sendphoto runs the sendphoto action.
func (c *ActionsController) Sendphoto(ctx *app.SendphotoActionsContext) error {
	// SectionsController_Senditems: start_implement
	var res app.MyservAuthToapp
	reader, _ := ctx.MultipartReader()
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) {

		Action := actions.Initial(c.DB, MyUser.UID, *ctx.UID, c.log)

		// Verification of the right to edit information by the patron
		if MyUser.Is(user.RoleSponsor) && !MyUser.Is(user.RoleAdmin) {
			isOk, err := Action.CheckSender()
			if err != nil {
				c.log.Error().Err(err).Msg("Error with checking actions owner")
			}

			if err != nil || !isOk {
				answer.Action = "Ошибка сохранения изображения."
				answer.Success = false
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
				return ctx.OK(&res)
			}
		}

		iAction := ibase.Actions{}
		iAction = iAction.SetStruct(c.DB, c.log, *ctx.UID)
		isExist := iAction.IsExist()

		if !isExist {
			answer.Action = "Акция не найдена."
			answer.Success = false
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}

		conflag := 0

		if reader != nil {

			// Image upload
			pathtofile := filepath.Join(binpath.BinaryPath(), "images/"+actions.Area)

			for {
				var p *multipart.Part
				var err error
				p, err = reader.NextPart()

				if err == io.EOF {
					break
				}
				if err != nil {
					break
					// c.log.Error().Err(err).Msg("failed to load part")
					// return ctx.BadRequest(goa.ErrBadRequest("failed to load part: %s", err))
				}

				if p.Header["Content-Type"][0] != contentTypeImage {
					return ctx.BadRequest(goa.ErrBadRequest("wrong image type"))
				}

				im, saveErr := Action.SaveImage(uid, *ctx.UID)
				if saveErr != nil {
					return ctx.BadRequest(goa.ErrBadRequest(saveErr))
				}
				f, err := os.OpenFile(filepath.Join(pathtofile, im.Image), os.O_WRONLY|os.O_CREATE, 0666)
				if err != nil {
					c.log.Error().Err(err).Msg("failed to save file")
					return fmt.Errorf("failed to save file: %s", err) // causes a 500 response
				}
				defer f.Close()
				io.Copy(f, p)
				conflag = conflag + 1
			}
		}

		answer.Success = true
		answer.Action = "Изображение загружено"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.Success = false
	answer.Action = "Ошибка доступа к разделу"

	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
	return ctx.OK(&res)
}

//Getusers - get information about current fundraising
func (c *ActionsController) Getusers(ctx *app.GetusersActionsContext) error {
	var err error

	// Get a list of users for fundraising
	actusers := &models.ActusersAnswer{}
	actusers.HasNext = false

	search := &models.Paginator{}
	search.Search = ""

	if ctx.Search != nil {
		search.Search = *ctx.Search
	}

	search.OnPage = 0
	search.CurrentPage = 0
	search.HasPaginator = false

	if ctx.OnPage != nil && ctx.CurrentPage != nil {
		if *ctx.OnPage != "" && *ctx.CurrentPage != "" {
			search.OnPage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.CurrentPage, err = strconv.Atoi(*ctx.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
				return ctx.OK(&res)
			}
			search.HasPaginator = true
		}
	}

	actusers.Actusers, actusers.Total, err = actions.Getlistusers(c.DB, c.PicsURL, *ctx.Actionuid, false, *search)
	if len(actusers.Actusers) < actusers.Total {
		actusers.HasNext = true
	}
	actusers.Count = len(actusers.Actusers)
	res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &actusers))
	return ctx.OK(&res)
}

package main

import (
	"strconv"
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/bank"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// BankController implements the sections resource.
type BankController struct {
	*goa.Controller
	DB      *pgx.ConnPool
	log     logger.Logger
	PicsURL string
	Options Options
}

// NewActionsController creates a actions controller.
func NewBankController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, picsUrl string, options Options) (*BankController, error) {
	return &BankController{
		Controller: service.NewController("BankController"),
		DB:         pg,
		log:        log,
		PicsURL:    picsUrl,
		Options:    options,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *BankController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *BankController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Item runs the item action.
func (c *BankController) Item(ctx *app.ItemBankContext) error {
	// BankController_Item: start_implement

	// Put your logic here

	res := &app.MyservAuthToapp{}
	return ctx.OK(res)
	// BankController_Item: end_implement
}

// Items runs the items action
func (c *BankController) Report(ctx *app.ReportBankContext) error {
	var err error
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)

	if !MyUser.Is(user.RoleAdmin) {
		answer.Success = false
		answer.Action = "Доступ запрещен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	search := new(bank.ReportSearch)
	if ctx.Payload.Search != nil {
		search.Search = *ctx.Payload.Search

	}

	search.Onpage = 0
	search.Current = 0
	search.Paginator = false
	search.OwnerUID = ""
	search.Success = *ctx.Payload.Success
	search.Failed = *ctx.Payload.Failed
	search.Type = ctx.Payload.Type

	if (ctx.Payload.Start != nil && ctx.Payload.Finish == nil) || (ctx.Payload.Start == nil && ctx.Payload.Finish != nil) {

		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Ошибка. Необходимо указать полный диапазон поиска"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	if ctx.Payload.Start != nil {
		dat, err := time.Parse("02/01/2006", *ctx.Payload.Start)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with date start format")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формата даты начала выборки"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
		search.Start = &dat
	}

	if ctx.Payload.Finish != nil {
		dat, err := time.Parse("02/01/2006", *ctx.Payload.Finish)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with date start format")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формата даты окончания выборки"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
		search.Finish = &dat
	}

	if search.Type != "ALL" {
		if ctx.Payload.UID == nil {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формирования данных. Необходим параметр UID"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
		search.OwnerUID = *ctx.Payload.UID
	}

	if ctx.Payload.OnPage != "" && ctx.Payload.CurrentPage != "" {
		search.Onpage, err = strconv.Atoi(ctx.Payload.OnPage)
		if err != nil {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формирования данных "
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
		search.Current, err = strconv.Atoi(ctx.Payload.CurrentPage)
		if err != nil {
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка формирования данных "
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
			return ctx.OK(&res)
		}
		search.Paginator = true
	}

	answerMain := new(bank.ReportAnswer)
	myBank := bank.Initial(c.DB, c.log, c.PicsURL)
	answerMain.List, answerMain.TotalAmounts, err = myBank.GetReport(*search)

	if err != nil && err != pgx.ErrNoRows {
		c.log.Error().Err(err).Msg("Error with GET items")
		answer.Success = false
		answer.Action = "Ошибка формирования данных"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	answerMain.TotalCount = answerMain.TotalAmounts.TotalCount
	answerMain.Count = len(answerMain.List)
	answerMain.HasNext = false
	if search.Onpage*search.Current < answerMain.TotalCount {
		answerMain.HasNext = true
	}
	answerMain.Success = true
	answerMain.Action = "Основной отчет"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answerMain))
	return ctx.OK(&res)
}

// Items runs the items action.
func (c *BankController) Items(ctx *app.ItemsBankContext) error {
	// BankController_Items: start_implement

	// Put your logic here

	var err error
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)

	if !MyUser.Is(user.RoleAdmin) {
		answer.Success = false
		answer.Action = "Доступ запрещен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	search := new(bank.Search)
	if ctx.Search != nil {
		search.Search = *ctx.Search

	}

	search.Onpage = 0
	search.Current = 0
	search.Paginator = false

	search.OnlyPaid = ctx.OnlyPaid
	search.OnlyWrong = ctx.OnlyWrong
	search.OwnerType = ctx.Type
	search.OwnerUID = ctx.UID

	if ctx.OnPage != nil && ctx.CurrentPage != nil {
		if *ctx.OnPage != "" && *ctx.CurrentPage != "" {
			search.Onpage, err = strconv.Atoi(*ctx.OnPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Current, err = strconv.Atoi(*ctx.CurrentPage)
			if err != nil {
				answer := new(ActionAnswer)
				answer.Success = false
				answer.Action = "Ошибка формирования данных "
				res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
				return ctx.OK(&res)
			}
			search.Paginator = true
		}
	}
	answerMain := new(bank.Answer)
	myBank := bank.Initial(c.DB, c.log, c.PicsURL)
	answerMain.List, answerMain.TotalCount, err = myBank.Get(*search)
	if err != nil && err != pgx.ErrNoRows {
		c.log.Error().Err(err).Msg("Error with GET items")
		answer.Success = false
		answer.Action = "Ошибка формирования данных"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Action", "Status", &answer))
		return ctx.OK(&res)
	}

	answerMain.Count = len(answerMain.List)
	answerMain.HasNext = false
	if search.Onpage*search.Current < answerMain.TotalCount {
		answerMain.HasNext = true
	}
	answerMain.Success = true
	answerMain.Action = "Информация о платежах"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answerMain))
	return ctx.OK(&res)
	// BankController_Items: end_implement
}

package main

// Token ...
type Token struct {
	Token string `json:"token"`
}

// UnauthorizedAnswer ...
type UnauthorizedAnswer struct {
	Event   string `json:"event" cfg:"event"`
	Message string `json:"message" cfg:"message" cfgDefault:"nothing"`
}

type dbmigration struct {
	Current int `json:"current" cfg:"current" cfgDefault:"1"`
}

type postgres struct {
	Host     string `json:"host" cfg:"host" cfgDefault:"example.com"`
	Port     int    `json:"port" cfg:"port" cfgDefault:"999"`
	Password string `json:"password" cfg:"password" cfgDefault:"123"`
	Username string `json:"username" cfg:"username" cfgDefault:"username"`
	Database string `json:"database" cfg:"database" cfgDefault:"database"`
}

type systemAPP struct {
	Name string `json:"name" cfg:"name"`
	Port string `json:"port" cfg:"port"`
}

type systemUser struct {
	Name     string `json:"name" cfg:"name"`
	Password string `json:"passwd" cfg:"passwd"`
}

type systemLog struct {
	Level    string `json:"level" cfg:"level"`
	Path     string `json:"path" cfg:"path"`
	File     string `json:"file" cfg:"file"`
	MainFile string `json:"MainFile" cfg:"MainFile"`
	Mode     string `json:"mode" cfg:"mode"`
}

type gmaps struct {
	Key string `json:"key" cfg:"key"`
}

type picsurl struct {
	URL string `json:"url" cfg:"url"`
}

type workConfig struct {
	AuthCode        string `json:"authCode" cfg:"authCode"`
	Push            string `json:"Push" cfg:"push"`
	PaymentControll bool   `json:"PaymentControll" cfg:"PaymentControll"`
	SmsTitle        string `json:"smsTitle" cfg:"smsTitle"`
}

type ForIOS struct {
	KeyID  string `json:"keyID" cfg:"keyID"`
	TeamID string `json:"teamID" cfg:"teamID"`
}

//ForAndroid - Settings for Android pushing
type ForAndroid struct {
	APIKeyClient string `json:"apiKeyClient" cfg:"APIKeyClient"`
	APIKeyDriver string `json:"apiKeyDriver" cfg:"APIKeyDriver"`
}

//Alfabank - Settings for alfabank integration
type Alfabank struct {
	Username string `json:"username" cfg:"Username"`
	Password string `json:"password" cfg:"Password"`
	URL      string `json:"url" cfg:"URL"`
}

type configMain struct {
	DebugMode      bool `json:"debug" cfg:"debug" cfgDefault:"false"`
	Domain         string
	APP            systemAPP   `json:"app" cfg:"app"`
	User           systemUser  `json:"user" cfg:"user"`
	Logparam       systemLog   `json:"log" cfg:"log"`
	ForIOS         ForIOS      `json:"ForIOS" cfg:"ForIOS"`
	ForAndroid     ForAndroid  `json:"forAndroid" cfg:"ForAndroid"`
	Options        Options     `json:"options" cfg:"Options"`
	Postgres       postgres    `json:"postgres" cfg:"postgres"`
	DBMigration    dbmigration `json:"dbmigration" cfg:"dbmigration"`
	Picsurl        picsurl     `json:"picsurl" cfg:"picsurl"`
	Sberbank       PaymentStr  `json:"sberbank" cfg:"Sberbank"`
	SocialNetworks Socnet      `json:"socialNetworks" cfg:"SocialNetworks"`
	Gmaps          gmaps       `json:"gmaps" cfg:"gmaps"`
	Email          Myemail     `json:"email" cfg:"email"`
}

type Myemail struct {
	From    string `json:"from" cfg:"From"`
	Pass    string `json:"pass" cfg:"Pass"`
	Serv    string `json:"serv" cfg:"Serv"`
	Port    string `json:"port" cfg:"Port"`
	LinkURL string `json:"linkUrl" cfg:"LinkUrl"`
}

// PaymentStr ... for Sberbank
type PaymentStr struct {
	Gateway     string `json:"Gateway" cfg:"Gateway"`
	Username    string `json:"username" cfg:"Username"`
	Password    string `json:"password" cfg:"Password"`
	InvoiceLink string `json:"invoiceLink" cfg:"InvoiceLink"`
	Return      Return `json:"return" cfg:"Return"`
}

// Return - return links for bank
type Return struct {
	ReturnURL string `json:"ReturnURL" cfg:"ReturnURL"`
	FailURL   string `json:"FailURL" cfg:"FailURL"`
}

//Socnet ...
type Socnet struct {
	Facebook Fb `json:"Facebook" cfg:"Facebook"`
	VK       Vk `json:"VK" cfg:"VK"`
	Google   Gl `json:"Google" cfg:"Google"`
}

// Fb - structure for FB API options
type Fb struct {
	Endpoint string `json:"endpoint" cfg:"Endpoint"`
	Fields   string `json:"fields" cfg:"Fields"`
}

// Vk - structure for FB API options
type Vk struct {
	Endpoint string `json:"endpoint" cfg:"Endpoint"`
	Fields   string `json:"fields" cfg:"Fields"`
	Ver      string `json:"ver" cfg:"Ver"`
}

// Gl - structure for FB API options
type Gl struct {
	Endpoint string `json:"endpoint" cfg:"Endpoint"`
	Fields   string `json:"fields" cfg:"Fields"`
}

// Options ...
type Options struct {
	ConfirmNewActionByAdmin bool   `json:"confirmNewActionByAdmin" cfg:"ConfirmNewActionByAdmin"`
	Domain                  string `json:"domain" cfg:"Domain"`
	EmailVerify             bool   `json:"EmailVerify" cfg:"EmailVerify"`
	AdminsEmail             string `json:"AdminsEmail" cfg:"AdminsEmail"`
	AdminPannel             string `json:"adminPannel" cfg:"AdminPannel"`
}

// ActionAnswer ...
type ActionAnswer struct {
	Success bool   `db:"success" json:"success"`
	Action  string `db:"message" json:"message"`
}

// ActionAnswerWithVerify ...
type ActionAnswerWithVerify struct {
	ActionAnswer
	IsVerify bool `json:"isVerified"`
}

// SavingAnswer ...
type SavingAnswer struct {
	ActionAnswer
	UID string `json:"UID"`
}

//UserSigninAnswer - structure for answer
type UserSigninAnswer struct {
	UID      string `json:"uid" cfg:"uid"`
	Usertype string `json:"Usertype" cfg:"Usertype"`
	Message  string `json:"message" cfg:"message" cfgDefault:"nothing"`
}

const (
	smsMethod  = "sms"
	anyMethod  = "any"
	jsonMethod = "json"
	// NoPhoto ...
	NoPhoto = "nophoto.jpg"
)

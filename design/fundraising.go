package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("fundraising", func() {
	BasePath("/api/fundraising")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})

	Action("item", func() {
		Routing(GET("/item/:UID"))
		Description("Getting information about fundraising")
		Params(func() {
			Param("UID", String, "fundraising UID")
			Param("usersUID", String, "UID of current user")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("getfilters", func() {
		Routing(GET("/getfilters"))
		Description("Getting filter for searching section")
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
	})

	Action("itemsearch", func() {
		Routing(POST("/search"))
		Description("Getting search")
		Payload(func() {
			Member("search", String)
			Member("onPage", String)
			Member("currentPage", String)
			Member("usersUID", String)
			Member("onlyActive", Boolean)
			Member("notActive", Boolean)
			Member("filter", SearchFilter)
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("items", func() {
		Routing(GET("/"))
		Description("Getting information about fundraising")
		Params(func() {
			Param("fullSearch", Boolean, "Flag for search section")
			Param("sectionuid", String, "sectionuid")
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
			Param("usersUID", String, "UID of current user")
			Param("onlyActive", Boolean, "Flag for only active fundraisings")
			Param("notActive", Boolean, "Flag for only NOT active fundraisings")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("getusers", func() {
		Routing(GET("/users"))
		Description("Getting information about users of fundraising")
		Params(func() {
			Param("fundraisinguid", String, "UID of fundraising")
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("senditems", func() {
		Description("Send items")
		Security(JWT)
		Routing(PUT("/"))
		Payload(func() {
			Member("uid", String)
			Member("sectionuid", String)
			Member("title", String)
			Member("longtitle", String)
			Member("description", String)
			Member("result", String)
			Member("step", String)
			Member("status", String)
			Member("active", Boolean)
			Member("flagstart", Boolean)
			Member("flagfinish", Boolean)
			Member("needSumm", Integer)
			Member("donatedSumm", Integer)
			Member("donated", Integer)
			Member("fund", FundPayload)
			Required("title", "longtitle", "needSumm")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(Accepted, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
	})

	Action("sendphoto", func() {
		Description("Send photo for items")
		Security(JWT)
		Routing(PUT("/photo"))
		Params(func() {
			Param("uid", String, "uid")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)

	})

	Action("deletephoto", func() {
		Description("Delete photo")
		Security(JWT)
		Routing(DELETE("/photo"))
		Params(func() {
			Param("uid", String, "uid")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)
	})

	Action("deleteitems", func() {
		Description("Delete items")
		Security(JWT)
		Routing(DELETE("/"))
		Payload(func() {
			Member("uid", String)
			Required("uid")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)
	})

})

//FundPayload - struct for fund for fundraising
var FundPayload = Type("FundPayload", func() {
	Attribute("funduid", String, "UID of fund")
	Attribute("fundname", String, "Name of fund")
	Required("funduid", "fundname")
})

//SearchFilter - struct for filters data
var SearchFilter = Type("SearchFilter", func() {
	Attribute("sections", ArrayOf(SearchFilterItem), "UID of fund")
	Attribute("filters", ArrayOf(SearchFilterItem), "Name of fund")
})

// SearchFilterItem ...
var SearchFilterItem = Type("SearchFilterItem", func() {
	Attribute("UID", String, "UID of filter")
	Attribute("title", String, "Title of filter")
	Attribute("active", Boolean, "active status")
})

package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("likes", func() {
	BasePath("/api/likes")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})

	Action("sendlike", func() {
		Description("Send like")
		Routing(PUT("/"))
		Security(JWT)
		Payload(LikesPayload)
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
	})

	Action("getlike", func() {
		Description("Send like")
		Routing(GET("/"))

		Params(func() {
			Param("ownerUID", String, "UID of likes owner")
			Param("ownerType", String, "Type of likes owner")
			Param("senderUID", String, "UID of sender")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
	})

})

//ActionsPayload ...
var LikesPayload = Type("LikesPayload", func() {
	Attribute("ownerUID", String, "UID")
	Attribute("ownerType", String, "ownerUID")
	Attribute("senderUID", String, "senderUID")
	Required("ownerUID", "ownerType", "senderUID")
})

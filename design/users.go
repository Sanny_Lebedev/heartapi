// nolint: golint
package design

import (
	. "github.com/goadesign/goa/design" // Use . imports to enable the DSL
	. "github.com/goadesign/goa/design/apidsl"
)

//UsersSearchType - part of parameter for searching
var UsersSearchType = Type("UsersSearchType", func() {
	Attribute("admin", Boolean, "Searching status for admin")
	Attribute("user", Boolean, "Searchign status for student")
	Attribute("maecenas", Boolean, "Searching status for driver")
})

var _ = Resource("users", func() {
	BasePath("/api/users")
	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("POST", "OPTION")
	})

	Action("getlist", func() {
		Description("Get users list")
		Routing(POST("/getlist"))
		Security(JWT)
		Payload(func() {
			Member("search", String)
			Member("currentPage", Integer)
			Member("itemsOnPage", Integer)
			Member("actives", Boolean)
			Member("types", UsersSearchType)
			Member("trips", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})

		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
	})

})

var Userslist = MediaType("application/vnd.myserv.users.list+json", func() {
	ContentType("application/json")
	Attributes(func() {
		Attribute("id", String, "Id of answer")
		Attribute("code", String, "Code of answer")
		Attribute("status", Integer, "status of answer")
		Attribute("details", String, "details of answer")
		Attribute("meta", Any, "Other metadatas")
		Attribute("success", Boolean, "result of operation")
		Required("id", "code", "success", "status", "details", "meta")
	})
	View("default", func() {
		Attribute("id")
		Attribute("success")
		Attribute("code")
		Attribute("status")
		Attribute("details")
		Attribute("meta")
	})
})

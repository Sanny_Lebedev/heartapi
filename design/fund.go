package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("funds", func() {
	BasePath("/api/funds")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})

	Action("item", func() {
		Routing(GET("/item/:UID"))
		Description("Getting information about fundraising")
		Params(func() {
			Param("UID", String, "UID of fund")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("items", func() {
		Routing(GET("/"))
		Description("Getting information about funds")
		Params(func() {
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("senditems", func() {
		Description("Send items")
		Security(JWT)
		Routing(PUT("/"))
		Payload(func() {
			Member("UID", String)
			Member("title", String)
			Member("description", String)
			Member("inn", String)
			Member("status", String)
			Member("active", Boolean)
			Member("address", AddressPayload)
			Member("contact", ContactsPayload)
			Required("title")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
	})

	Action("sendphoto", func() {
		Description("Send photo for items")
		Security(JWT)
		Routing(PUT("/photo"))
		Params(func() {
			Param("UID", String, "UID of fund")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)

	})

	Action("changeactive", func() {
		Description("Change active status")
		Security(JWT)
		Routing(PUT("/active"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)

	})

	Action("deletephoto", func() {
		Description("Delete photo")
		Security(JWT)
		Routing(DELETE("/photo"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

	Action("deleteitems", func() {
		Description("Delete items")
		Security(JWT)
		Routing(DELETE("/"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

})

//AddressPayload - struct for address information
var AddressPayload = Type("AddressPayload", func() {
	Attribute("country", String, "Country")
	Attribute("postal", String, "Postal code")
	Attribute("city", String, "City")
	Attribute("address", String, "Address")
	Required("country", "city", "address")
})

//ContactsPayload - struct for address information
var ContactsPayload = Type("ContactsPayload", func() {
	Attribute("person", String, "Persons name")
	Attribute("phone", String, "phone")
	Attribute("email", String, "email")
	Attribute("website", String, "website")
	Attribute("skype", String, "skype")
	Required("person", "phone", "email")
})

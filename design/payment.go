package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("payment", func() {
	BasePath("/api/payment")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "DELETE", "PUT", "OPTION")
	})

	Action("newcard", func() {
		Routing(PUT("/cards/"))
		Security(JWT)
		Description("Add new card to users profile")
		Payload(func() {
			Member("UID", String)
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("cards", func() {
		Routing(GET("/cards/:UID"))
		Security(JWT)
		Description("Get a list of cards for user")
		Params(func() {
			Param("UID", String, "Users UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("deletecard", func() {
		Routing(DELETE("/cards/"))
		Security(JWT)
		Description("Delete card from profile")
		Payload(func() {
			Member("UID", String)
			Member("cardUID", String)
			Required("UID", "cardUID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("pay", func() {
		Routing(PUT("/"))
		Security(JWT)
		Description("Send new pay")
		Payload(func() {
			Member("UID", String)
			Member("cardUID", String)
			Member("fundraisingUID", String)
			Member("amount", Number)
			Required("UID", "cardUID", "fundraisingUID", "amount")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("ticket", func() {
		Routing(PUT("/ticket"))
		Security(JWT)
		Description("Get new ticket")
		Payload(func() {
			Member("UID", String)
			Member("cardUID", String)
			Member("actionUID", String)
			Member("amount", Integer)
			Required("UID", "cardUID", "amount", "actionUID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("winner", func() {
		Routing(GET("/winner"))
		Security(JWT)
		Description("Check for win")
		Params(func() {
			Param("UID", String, "Users UID")
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("fundraising", func() {
		Routing(PUT("/fundraising"))
		Security(JWT)
		Description("Get new ticket")
		Payload(func() {
			Member("UID", String)
			Member("cardUID", String)
			Member("raisingUID", String)
			Member("amount", Integer)
			Required("UID", "amount", "raisingUID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("check", func() {
		Routing(GET("/"))
		Security(JWT)
		Description("Check status of payment")
		Payload(func() {
			Member("UID", String)
			Member("orderUID", String)
			Required("UID", "orderUID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

})

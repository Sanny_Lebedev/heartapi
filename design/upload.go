// nolint: golint
package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("image", func() {
	BasePath("/api/images")
	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
		Headers("Authorization", "Content-Type")

	})

	Action("putimageforid", func() {
		Routing(PUT("/profileforid"))
		Security(JWT)
		Description("Upload image to profile for UID")
		Params(func() {
			Param("UID", String, "UID of user")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
		Response(InternalServerError)
	})

	Action("putimage", func() {
		Routing(PUT("/profile"))
		Security(JWT)
		Description("Profile multiple images in multipart request")

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
	})

	Action("delimage", func() {
		Routing(DELETE("/profile"))
		Security(JWT)
		Description("Delete photos from profiles")

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
	})

	Action("delimageforid", func() {
		Routing(DELETE("/profileforid"))
		Security(JWT)
		Description("Delete photos from profiles")
		Params(func() {
			Param("UID", String, "UID of user")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
	})

	Action("show", func() {
		Routing(GET("/:id"))
		Security(JWT)
		Description("Show an image metadata")
		Params(func() {
			Param("id", Integer, "Image ID")
		})
		Response(OK, ImageMedia)
		Response(NotFound)
	})

	// Files("/download/*filename", "images/") // Serve files from the "images" directory
})

var ImageMedia = MediaType("application/vnd.myserv.list+json", func() {
	Description("Image metadata")
	TypeName("ImageMedia")
	ContentType("application/json")
	Attributes(func() {
		Attribute("id", Integer, "Image ID")
		Attribute("filename", String, "Image filename")
		Attribute("uploaded_at", DateTime, "Upload timestamp")
		Required("id", "filename", "uploaded_at")
	})
	View("default", func() {
		Attribute("id")
		Attribute("filename")
		Attribute("uploaded_at")
	})
})

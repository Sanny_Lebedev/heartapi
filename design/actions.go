package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("actions", func() {
	BasePath("/api/actions")

	//Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})

	Action("item", func() {
		Routing(GET("/item/:UID"))
		// Security(JWT)
		Description("Getting information about action")
		Params(func() {
			Param("UID", String, "UID of Action")
			Param("usersUID", String, "UID of current user")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("link", func() {
		Routing(GET("/link/:link"))
		Description("Getting information about action")
		Params(func() {
			Param("link", String, "link of action")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("news", func() {
		Routing(GET("/news"))
		Description("Get news")
		Params(func() {
			Param("UsersUID", String, "UID of current user")
			Param("active", Boolean, "Flag of active status")
			Param("typeOfAction", String, "Type of searching actions for filter")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("items", func() {
		Routing(GET("/"))
		//Security(JWT)
		Description("Getting information about actions if fundraising")
		Params(func() {
			Param("UID", String, "UID of fundraising")
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
			Param("usersUID", String, "UID of current user")
			Param("onlyActive", Boolean, "Flag for only active actions")
			Param("notActive", Boolean, "Flag for only NOT active actions")
			Param("typeOfAction", String, "Type of searching actions for filter")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("senditems", func() {
		Description("Send items")
		Routing(PUT("/"))
		Security(JWT)
		Payload(ActionsPayload)
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
	})

	Action("sendphoto", func() {
		Description("Send photo for items")
		Security(JWT)
		Routing(PUT("/photo"))
		Params(func() {
			Param("UID", String, "UID of fund")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)

	})

	Action("sendvideo", func() {
		Description("Send video for action")
		Security(JWT)
		Routing(PUT("/video"))
		Params(func() {
			Param("UID", String, "UID of action")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)

	})

	Action("changeactive", func() {
		Description("Change active status")
		Security(JWT)
		Routing(PUT("/active"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)

	})

	Action("cansend", func() {
		Description("Change active status")
		Security(JWT)
		Routing(PUT("/cansend"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)

	})

	Action("deletephoto", func() {
		Description("Delete photo")
		Security(JWT)
		Routing(DELETE("/photo"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

	Action("deletevideo", func() {
		Description("Delete video")
		Security(JWT)
		Routing(DELETE("/video"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

	Action("deleteitems", func() {
		Description("Delete items")
		Security(JWT)
		Routing(DELETE("/"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

	Action("getusers", func() {
		Routing(GET("/users"))
		Description("Getting information about users of action")
		Params(func() {
			Param("actionuid", String, "UID of action")
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

})

//ActionsDates ...
var ActionsDates = Type("ActionsDates", func() {
	Attribute("dateStart", String, "dateStart")
	Attribute("dateFinish", String, "dateFinish")

})

//ActionsFlags ...
var ActionsFlags = Type("ActionsFlags", func() {
	Attribute("flagstart", Boolean, "flagstart")
	Attribute("flagfinish", Boolean, "flagfinish")
	Attribute("criteriasumm", Boolean, "criteriasumm")
	Attribute("criteriasdate", Boolean, "criteriasdate")
	Attribute("isAuction", Boolean, "is Auction")
	Attribute("active", Boolean, "Active status of action")
})

//AuctionPayload ...
var AuctionPayload = Type("AuctionPayload", func() {
	Attribute("isAuction", Boolean, "is Auction")
	Attribute("minSumm", Integer, "is minSumm")
	Attribute("currentRate", Integer, "Current rate")
	Attribute("minBet", Integer, "minBet")
	Required("isAuction", "minSumm", "currentRate", "minBet")
})

//ActionsPayload ...
var ActionsPayload = Type("ActionsPayload", func() {
	Attribute("UID", String, "UID")
	Attribute("fundraisingUID", String, "ownerUID")
	Attribute("maecenasUID", String, "UID of maecenas")
	Attribute("title", String, "title of action")
	Attribute("longTitle", String, "longtitle of action")
	Attribute("description", String, "description of action")
	Attribute("result", String, "result of action")
	Attribute("needSumm", Integer, "need summ")
	Attribute("totalWinners", Integer, "number of total winners")
	Attribute("status", String, "status description")
	Attribute("dates", ActionsDates)
	Attribute("flags", ActionsFlags)
	Attribute("auction", AuctionPayload)
	Required("fundraisingUID", "maecenasUID", "title")
})

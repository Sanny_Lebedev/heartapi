package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("sections", func() {
	BasePath("/api/sections")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})

	Action("items", func() {
		Routing(GET("/"))
		Description("Getting information about brand")
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("senditems", func() {
		Description("Send items")
		Security(JWT)
		Routing(PUT("/"))
		Params(func() {
			Param("sectionuid", String, "sectionuid")
			Param("title", String, "title")
			Param("description", String, "description")
			Param("step", String, "step")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(Accepted, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
	})

	Action("sendphoto", func() {
		Description("Send photo for items")
		Security(JWT)
		Routing(PUT("/photo"))
		Params(func() {
			Param("sectionuid", String, "sectionuid")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)

	})

	Action("deletephoto", func() {
		Description("Delete photo")
		Security(JWT)
		Routing(DELETE("/photo"))
		Params(func() {
			Param("sectionuid", String, "sectionuid")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)
	})

	Action("deleteitems", func() {
		Description("Delete items")
		Security(JWT)
		Routing(DELETE("/"))
		Params(func() {
			Param("sectionuid", String, "sectionuid")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)
	})

})

var SectionPayload = Type("SectionPayload", func() {

	Attribute("sectionuid", String, func() {
		MinLength(5)
		MaxLength(50)
		Example("c158a26a-2fc8-471d-be69-f33cea7ac03a")
	})

	Attribute("useruid", String, func() {
		MinLength(5)
		MaxLength(50)
		Example("c158a26a-2fc8-471d-be69-f33cea7ac03a")
	})

	Attribute("title", String, func() {
		MinLength(1)
		MaxLength(250)
		Example("Title of section")
	})

	Attribute("description", String, func() {
		MinLength(1)
		MaxLength(3000)
		Example("Description of section")
	})

	Attribute("image", String, func() {
		MinLength(5)
		MaxLength(1000)
		Example("https://graph.facebook.com/10217621693639545/picture?type=normal")
	})

	Required("useruid", "title", "description")

})

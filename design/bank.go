package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("bank", func() {
	BasePath("/api/bank")

	//Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {

		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})

	Action("item", func() {
		Routing(GET("/item"))
		Security(JWT)
		Description("Getting information about bankorder")
		Params(func() {
			Param("OrderUID", String, "UID of Order")
			Param("usersUID", String, "UID of current user")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("items", func() {
		Routing(GET("/"))
		Security(JWT)
		Description("Getting information about actions if fundraising")
		Params(func() {
			Param("UID", String, "UID of owner")
			Param("Type", String, "Name of type (actions or fundraising)")
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
			Param("usersUID", String, "UID of current user")
			Param("onlyPaid", Boolean, "Flag for only good datas")
			Param("onlyWrong", Boolean, "Flag for only wrong datas")
			Required("UID", "Type", "usersUID", "onlyPaid", "onlyWrong")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("report", func() {
		Routing(POST("/"))
		Security(JWT)
		Description("Getting information about all orders")
		Payload(func() {
			Member("UID", String, "UID of owner")
			Member("Type", String, "Name of type (actions or fundraising)")
			Member("search", String, "Search string")
			Member("onPage", String, "Items on page")
			Member("currentPage", String, "Current page of list")
			Member("success", Boolean, "Flag for only success orders")
			Member("failed", Boolean, "Flag for only failed orders")
			Member("start", String, "date start of search")
			Member("finish", String, "date start of search")
			Required("Type", "onPage", "currentPage")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

})

// nolint: golint
package design

import (
	. "github.com/goadesign/goa/design" // Use . imports to enable the DSL
	. "github.com/goadesign/goa/design/apidsl"
)

//Token - Structure of JWT
var Token = MediaType("application/vnd.token+json", func() {
	Description("A token")
	Attributes(func() {
		Attribute("token", String, "A JWT token")
	})

	View("default", func() {
		Attribute("token")
	})
})

//RefreshTokenBase - part of answer message from API to APP
var RefreshTokenBase = Type("RefreshTokenBase", func() {
	Attribute("uid", Integer, "UID of user")
	Attribute("jti", String, "JTI of token")
	Attribute("role", String, "Role of user")
	Required("uid", "jti", "role")
})

//AnswerMSG - part of answer message from API to APP
var AnswerMSG = Type("AnswerMSG", func() {
	Attribute("code", Integer, "Code of response")
	Attribute("msg", String, "Message of response")
	Required("code", "msg")
})

// JWT defines a security scheme using JWT.  The scheme uses the "Authorization" header to lookup
// the token.  It also defines then scope "api".
var JWT = JWTSecurity("jwt", func() {
	Header("Authorization")
	Scope("api:access", "API access") // Define "api:access" scope
})

// Resource jwt uses the JWTSecurity security scheme.
var _ = Resource("jwt", func() {
	Description("This resource uses JWT to secure its endpoints")
	DefaultMedia(SuccessMedia)

	Security(JWT, func() { // Use JWT to auth requests to this endpoint
		Scope("api:access") // Enforce presence of "api" scope in JWT claims.
	})

})

var _ = API("auth", func() { // API defines the microservice endpoint and
	Title("Service a pure heart")  // other global properties. There should be one
	Description("The main module") // and exactly one API definition appearing in
	Scheme("http")                 // the design.
	Host("localhost:1200")

	Consumes("application/json")
	Produces("application/json")

	// OAuth2 requires form encoding
	Consumes("application/x-www-form-urlencoded", func() {
		Package("github.com/goadesign/goa/encoding/form")
	})
	Produces("application/x-www-form-urlencoded", func() {
		Package("github.com/goadesign/goa/encoding/form")
	})
})

var _ = Resource("auth", func() {
	BasePath("/auth")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "DELETE", "PUT", "OPTION")
	})

	Action("signin", func() {
		Description("Sign in")
		Routing(POST("/signin"))
		Payload(func() {
			Member("email", String)
			Member("password", String)
			Member("apptype", String)
			Required("email", "password")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(Accepted, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(InternalServerError)
	})

	Action("recover", func() {
		Description("Generate password recovery key")
		Routing(POST("/recover"))
		Payload(func() {
			Member("email", String)
			Required("email")
		})
		Response(OK)
		Response(NotFound)
		Response(InternalServerError)
	})

	Action("change_password", func() {
		Description("Change password")
		Routing(POST("/change_password"))
		Payload(func() {
			Member("key", String)
			Member("password", String)
			Required("key", "password")
		})
		Response(OK)
		Response(NotFound)
		Response(InternalServerError)
	})

	Action("validate", func() {
		Description("Validate JWT")
		Security(JWT)
		Routing(POST("/validate"))
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(Unauthorized)
		Response(InternalServerError)
	})

	Action("checkemailsend", func() {
		Description("Send code to email")
		Security(JWT)
		Routing(POST("/checkemail"))
		Payload(func() {
			Member("email", String)
			Member("UID", String)
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized)
	})

	Action("checkemailcheck", func() {
		Description("Check email and salt")
		Routing(PUT("/checkemail"))
		Payload(func() {
			Member("check", String)
			Member("email", String)
			Required("email", "check")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(InternalServerError)
	})

	Action("getemailstatus", func() {
		Description("Get actual status of users email")
		Security(JWT)
		Routing(GET("/checkemail"))
		Params(func() {
			Param("UID", String, "UID of user")
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(InternalServerError)
	})

	Action("refresh", func() {
		Description("Refresh tokens with refreshToken")
		Security(JWT)
		Routing(GET("/refresh"))
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(InternalServerError)
	})

	Action("register", func() {
		NoSecurity()
		Routing(
			POST("/register"),
		)
		Description("New User Registration")
		Payload(RegisterPayload)

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)
	})

	Action("fromsocnet", func() {
		NoSecurity()
		Routing(
			POST("/fromsocnet"),
		)
		Description("Get auth informations from FB or VK")
		Payload(SocnetLoginPayload)

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
		Response(InternalServerError)
		Response(BadRequest, ErrorMedia)
	})

})

// Tokens ...
var Tokens = MediaType("application/vnd.myserv.auth.token+json", func() {
	ContentType("application/json")
	Attributes(func() {
		Attribute("accessToken", String, "Access JWT")
		Attribute("refreshToken", String, "Refresh JWT")
		Required("accessToken", "refreshToken")
	})
	View("default", func() {
		Attribute("accessToken")
		Attribute("refreshToken")
	})
})

// Datatoapp ...
var Datatoapp = MediaType("application/vnd.myserv.auth.toapp+json", func() {
	ContentType("application/json")
	Attributes(func() {
		Attribute("id", String, "Id of answer")
		Attribute("code", String, "Code of answer")
		Attribute("status", Integer, "status of answer")
		Attribute("details", String, "details of answer")
		Attribute("meta", Any, "Other metadatas")
		Attribute("success", Boolean, "result of operation")
		Required("id", "code", "success", "status", "details", "meta")
	})
	View("default", func() {
		Attribute("id")
		Attribute("success")
		Attribute("code")
		Attribute("status")
		Attribute("details")
		Attribute("meta")

	})
})

// Key ...
var Key = MediaType("application/vnd.myserv.auth.key+json", func() {
	ContentType("application/json")
	Attributes(func() {
		Attribute("key", String, "String key")
		Required("key")
	})
	View("default", func() {
		Attribute("key")
	})
})

// SuccessMedia ...
var SuccessMedia = MediaType("application/vnd.goa.examples.security.success", func() {
	Description("The common media type to all request responses for this example")
	TypeName("Success")
	Attributes(func() {
		Attribute("ok", Boolean, "Always true")
		Required("ok")
	})
	View("default", func() {
		Attribute("ok")
	})
})

// SocnetLoginPayload ...
var SocnetLoginPayload = Type("SocnetLoginPayload", func() {

	Attribute("token", String, func() {
		MinLength(5)
		MaxLength(550)
		Example("EAAPKFD6yUvsBAKF0n4ZCKoKo2d3fILb4pxanNtKho695JQgWV84nKAO67vye1czBwXKZAL4l6NQZCBN5eSf8K50oRu1LcmkdWBT4ZClFd4pK7bbMfq6GeGOJeouCX3IeSZB5Q2nr9qbcmp3VeYlZC2dkAumodkj1UB90ICwJghOHVa4iWcBQBYNJyiZC1ns8EDKz0bi0AfuLwZDZD")
	})

	Attribute("provider", String, func() {
		MinLength(1)
		MaxLength(10)
		Example("FB")
	})

	Attribute("socnetuid", String, func() {
		MinLength(1)
		MaxLength(150)
		Example("EAAPKFD6yUvsBAKF0n4ZCKoKo2d3fILb4pxanNtK")
	})

	Attribute("email", String, func() {
		MinLength(6)
		MaxLength(150)
		Format("email")
		Example("jamesbond@gmail.com")
	})

	Attribute("username", String, func() {
		MinLength(6)
		MaxLength(150)
		Example("Name")
	})

	Attribute("imagelink", String, func() {
		MinLength(6)
		MaxLength(150)
		Example("https://graph.facebook.com/10217621693639545/picture?type=normal")
	})

	Required("provider", "token", "socnetuid")
})

var RegisterPayload = Type("RegisterPayload", func() {

	Attribute("birthday", String, "Birthday")

	Attribute("phone", String, func() {
		MinLength(8)
		MaxLength(18)
		Example("+79512222222")
	})

	Attribute("fbCode", String, func() {
		MinLength(1)
		MaxLength(100)
		Example("79512222222")
	})
	Attribute("vkCode", String, func() {
		MinLength(1)
		MaxLength(100)
		Example("79512222222")
	})

	Attribute("email", String, func() {
		MinLength(6)
		MaxLength(150)
		Format("email")
		Example("jamesbond@gmail.com")
	})

	Attribute("firstName", String, func() {
		MinLength(1)
		MaxLength(200)
		Example("John")
	})

	Attribute("lastName", String, func() {
		MinLength(1)
		MaxLength(200)
		Example("Doe")
	})

	Attribute("password", String, func() {
		MinLength(5)
		MaxLength(100)
		Example("abcd1234")
	})

	Attribute("mainRole", String, func() {
		MinLength(5)
		MaxLength(50)
		Example("user")
	})

	Attribute("apptype", String, func() {
		MinLength(1)
		MaxLength(10)
		Example("usual")
	})

	Required("email", "password", "firstName", "lastName", "birthday")
})

package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("profile", func() {
	BasePath("/api/profile")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "DELETE", "PUT", "OPTION")
	})

	Action("getprofilebyuid", func() {
		Routing(GET("/:UID"))
		Security(JWT)
		Description("Get information from profile")
		Params(func() {
			Param("UID", String, "Users UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("getprofile", func() {
		Routing(GET("/"))
		Security(JWT)
		Description("Get information from profile")
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
	})

	Action("getroles", func() {
		Routing(GET("/roles"))
		Security(JWT)
		Description("Get users roles by")
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
	})

	Action("changerole", func() {
		Routing(POST("/roles"))
		Security(JWT)
		Description("Change user's role request")
		Payload(func() {
			Member("UID", String)
			Member("role", String)
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
	})

	Action("delete", func() {
		Routing(DELETE("/"))
		Security(JWT)
		Description("Delete user from DB")
		Payload(func() {
			Member("UID", String)
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

	Action("changeroleactive", func() {
		Routing(PUT("/roles"))
		Security(JWT)
		Description("Change user's role request")
		Payload(func() {
			Member("UID", String)
			Member("role", String)
			Member("active", Boolean)
			Required("UID", "role", "active")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
	})

	Action("getrolesbyuid", func() {
		Routing(GET("/roles/:UID"))
		Security(JWT)
		Description("Get users roles by UID")
		Params(func() {
			Param("UID", String, "Users UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

	Action("setprofile", func() {
		Routing(PUT("/"))
		Security(JWT)
		Description("Get information from profile")
		Payload(func() {
			Member("firstName", String)
			Member("lastName", String)
			Member("phone", String)
			Member("birthday", String)
			Member("instagram", String)
			Member("description", String)
			Member("UID", String)
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("changepass", func() {
		Routing(POST("/changepass"))

		Description("Set new password")
		Payload(Setnewpassword)

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("recovery", func() {
		Routing(POST("/recovery"))
		Description("Send temp password by email for recovery")
		Payload(func() {
			Member("email", String)
			Required("email")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("recoveryanswer", func() {
		Routing(PUT("/recovery"))
		Description("Set new password from recovery request")
		Payload(Recoverypass)

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("devicetoken", func() {
		Description("Get device token for push notification")
		Routing(PUT("/devicetoken"))
		Security(JWT)
		Payload(DevToken)
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

	Action("changepushstatus", func() {
		Description("Get device token for push notification")
		Routing(PATCH("/pushstatus"))
		Security(JWT)
		Payload(func() {
			Member("UID", String)
			Required("UID")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

})

// DevToken - getting token from device for push notification
var DevToken = Type("DevToken", func() {
	Attribute("token", String, "token", func() {
		MinLength(1)
		MaxLength(500)
		Example("sdfsdfsdf sdfsdfsdf sdf s")
	})
	Attribute("role", String, "role", func() {
		Example("student")
	})
	Attribute("system", String, "System - ios or android", func() {
		MinLength(3)
		MaxLength(8)
		Example("ios")
	})
})

var Setnewpassword = Type("Setnewpassword", func() {
	Attribute("uid", String, "UID of user", func() {
		MinLength(5)
		MaxLength(50)
		Example("123123-123123-123123-12312")
	})

	Attribute("oldpassword", String, "old users password", func() {
		MinLength(5)
		MaxLength(50)
		Example("sdasfw45ferfsdf")
	})

	Attribute("newpassword", String, "new users password", func() {
		MinLength(5)
		MaxLength(50)
		Example("sdasfw45ferfsdf")
	})

	Required("uid", "oldpassword", "newpassword")
})

var Recoverypass = Type("Recoverypass", func() {
	Attribute("email", String, "email of user", func() {
		MinLength(1)
		MaxLength(50)
		Example("123123-123123-123123-12312")
	})

	Attribute("temppassword", String, "Temp users password", func() {
		MinLength(5)
		MaxLength(50)
		Example("sdasfw45ferfsdf")
	})

	Attribute("newpassword", String, "new users password", func() {
		MinLength(5)
		MaxLength(50)
		Example("sdasfw45ferfsdf")
	})

	Required("email", "temppassword", "newpassword")
})

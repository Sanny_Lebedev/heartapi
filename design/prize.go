package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("prize", func() {
	BasePath("/api/prize")

	// Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "PATCH", "DELETE", "PUT", "OPTION")
	})

	Action("item", func() {
		Routing(GET("/item/:UID"))

		Description("Getting information about action")
		Params(func() {
			Param("UID", String, "UID of fundraising")
			Param("usersUID", String, "UID of current user")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("items", func() {
		Routing(GET("/"))

		Description("Getting information about actions if fundraising")
		Params(func() {
			Param("UID", String, "UID of actions")
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
			Param("usersUID", String, "UID of current user")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)

	})

	Action("senditems", func() {
		Description("Send items")
		Routing(PUT("/"))
		Security(JWT)
		Payload(PrizesPayload)
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(Unauthorized, func() {
			Media(Datatoapp)
		})
	})

	Action("sendphoto", func() {
		Description("Send photo for items")
		Routing(PUT("/photo"))
		Security(JWT)
		Params(func() {
			Param("UID", String, "UID of prize")
		})
		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)

	})

	Action("cansend", func() {
		Description("Change active status")
		Security(JWT)
		Routing(PUT("/cansend"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)

	})

	Action("deletephoto", func() {
		Description("Delete photo")
		Security(JWT)
		Routing(DELETE("/photo"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

	Action("deleteitems", func() {
		Description("Delete items")
		Security(JWT)
		Routing(DELETE("/"))
		Payload(func() {
			Member("UID", String)
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(NotFound)
		Response(BadRequest, ErrorMedia)
	})

})

var PrizesPayload = Type("PrizesPayload", func() {

	Attribute("UID", String, "UID")
	Attribute("actionsUID", String, "UID of owner")
	Attribute("maecenasUID", String, "UID of maecenas")
	Attribute("winnerUID", String, "UID of winner")
	Attribute("title", String, "title of prize")
	Attribute("longTitle", String, "longtitle of prize")
	Attribute("description", String, "description of prize")
	Attribute("place", Integer, "place of prize")
	Attribute("active", Boolean, "Active status of prize")
	Required("actionsUID", "maecenasUID", "title")
})

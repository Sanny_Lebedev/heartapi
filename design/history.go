package design

import (
	. "github.com/goadesign/goa/design"
	. "github.com/goadesign/goa/design/apidsl"
)

var _ = Resource("history", func() {
	BasePath("/api/history")

	//Origin("http://localhost:4200", func() {
	Origin("http://maecenas.agency911.org", func() {
		Headers("Authorization", "Content-Type")
		Methods("GET", "POST", "DELETE", "PUT", "OPTION")
	})

	Action("items", func() {
		Routing(GET("/"))
		Security(JWT)
		Description("Getting information about actions and fundraisings for current user")
		Params(func() {
			Param("search", String, "Search string")
			Param("onPage", String, "Items on page")
			Param("currentPage", String, "Current page of list")
			Param("usersUID", String, "UID of current user")
			Param("onlyActive", Boolean, "Flag for only active actions")
			Param("notActive", Boolean, "Flag for only NOT active actions")
			Param("onlyWin", Boolean, "Flag for actions with prize for user")
			Param("onlyFinished", Boolean, "Flag for only finished actions")
		})

		Response(OK, func() {
			Media(Datatoapp)
		})
		Response(BadRequest, ErrorMedia)
		Response(NotFound)
	})

})

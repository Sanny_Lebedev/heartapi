package main

import (
	"time"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/actions"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/jobs"
	mess "bitbucket.org/Sanny_Lebedev/heartapi/utils/message"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/models"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/profile"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/uploads"
	user "bitbucket.org/Sanny_Lebedev/heartapi/utils/users"
	jwtgo "github.com/dgrijalva/jwt-go"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware/security/jwt"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
)

// ProfileController implements the profile resource.
type ProfileController struct {
	*goa.Controller
	DB   *pgx.ConnPool
	log  logger.Logger
	Conf configMain
}

// NewProfileController creates a polls controller.
func NewProfileController(service *goa.Service, pg *pgx.ConnPool, log logger.Logger, configMain configMain) (*ProfileController, error) {
	return &ProfileController{
		Controller: service.NewController("ProfileController"),
		DB:         pg,
		log:        log,
		Conf:       configMain,
	}, nil
}

//Preset - Presets function for controller. Control of correct claims from token
func (c *ProfileController) Preset(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, string) {

	uid := ""
	log := c.log.With().Interface("claims", claims).Logger()

	if role, ok := claims["userID"]; ok {
		switch role.(type) {
		case string:
			uid = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, uid

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, uid
	}
	return true, uid
}

//LoadUserInfo - Presets function for controller. Getting user's info from claims
func (c *ProfileController) LoadUserInfo(MyUser *user.User, uid string) error {

	if MyUser.LoadRolesFromDB(uid) != nil {
		err := errors.New("Token problem")
		return err
	}

	if err := MyUser.FillTheRoles(); err != nil {
		return err
	}

	//Getting main informations about user from DB by UID
	if err := MyUser.GetByUID(uid); err != nil {
		return err
	}
	return nil
}

// Changepass runs the changepass action.
func (c *ProfileController) Changepass(ctx *app.ChangepassProfileContext) error {
	var res app.MyservAuthToapp

	MyUser := user.NewUser(c.DB, c.log)

	isDB, errIsDB := MyUser.CheckUID(ctx.Payload.UID)
	if errIsDB != nil && errIsDB != pgx.ErrNoRows {
		return ctx.BadRequest(goa.ErrBadRequest(errIsDB))
	}

	if !isDB {
		return ctx.NotFound()
	}

	isCorrect := MyUser.CheckOldPass(ctx.Payload.Oldpassword, ctx.Payload.UID)

	if isCorrect {
		isErr := MyUser.CodeThePassword(ctx.Payload.Newpassword, ctx.Payload.UID)
		if isErr == nil {
			answer := new(ActionAnswer)
			answer.Success = true
			answer.Action = "Пароль успешно изменен"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}
	}

	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Ошибка ввода пароля"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
	// }
	// answer := new(ActionAnswer)
	// answer.Success = false
	// answer.Action = "Доступ к разделу запрещен"
	// res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	// return ctx.OK(&res)
}

// Getprofile runs the getprofile action.
func (c *ProfileController) Getprofile(ctx *app.GetprofileProfileContext) error {

	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {
		Profile := profile.New(c.DB, c.log, c.Conf.Picsurl.URL)
		Out, errPR := Profile.Get(uid)
		if errPR != nil && errPR != pgx.ErrNoRows {

			c.log.Error().Err(errPR).Msg("Error with getting users profile")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка доступа к данным"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}
		if !c.Conf.Options.EmailVerify {
			Out.IsVerify = true
		}
		answer := new(profile.Answer)
		answer.Profile = Out
		answer.Action = "Профиль пользователя"
		answer.Success = true
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Доступ к разделу запрещен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

//Getroles - get users roles
func (c *ProfileController) Getroles(ctx *app.GetrolesProfileContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {
		Profile := profile.New(c.DB, c.log, c.Conf.Picsurl.URL)
		Out, errPR := Profile.GetActiveRoles(uid)

		if errPR != nil && errPR != pgx.ErrNoRows {
			c.log.Error().Err(errPR).Msg("Error with getting users profile")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка доступа к данным"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		answer := new(profile.AnswerRoles)
		answer.Roles = Out
		answer.Count = len(Out)
		answer.Action = "Роли пользователя"
		answer.Success = true
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Доступ к разделу запрещен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

//Getrolesbyuid - get users roles by UID
func (c *ProfileController) Getrolesbyuid(ctx *app.GetrolesbyuidProfileContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if (MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser)) && (uid != ctx.UID) && !MyUser.Is(user.RoleAdmin) {
		return ctx.NotFound()
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {
		Profile := profile.New(c.DB, c.log, c.Conf.Picsurl.URL)
		Out, errPR := Profile.GetActiveRoles(ctx.UID)

		if errPR != nil && errPR != pgx.ErrNoRows {
			c.log.Error().Err(errPR).Msg("Error with getting users profile")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка доступа к данным"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		answer := new(profile.AnswerRoles)
		answer.Roles = Out
		answer.Count = len(Out)
		answer.Action = "Роли пользователя"
		answer.Success = true
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Доступ к разделу запрещен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Getprofilebyuid runs the getprofilebyuid action.
func (c *ProfileController) Getprofilebyuid(ctx *app.GetprofilebyuidProfileContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if (MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser)) && (uid != ctx.UID) && !MyUser.Is(user.RoleAdmin) {
		return ctx.NotFound()
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {
		Profile := profile.New(c.DB, c.log, c.Conf.Picsurl.URL)
		Out, errPR := Profile.Get(ctx.UID)
		if errPR != nil && errPR != pgx.ErrNoRows {
			c.log.Error().Err(errPR).Msg("Error with getting users profile")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка доступа к данным"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}
		if !c.Conf.Options.EmailVerify {
			Out.IsVerify = true
		}

		answer := new(profile.Answer)
		answer.Profile = Out
		answer.Action = "Профиль пользователя"
		answer.Success = true
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Доступ к разделу запрещен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// Delete - delete user from system
func (c *ProfileController) Delete(ctx *app.DeleteProfileContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if ctx.Payload.UID == MyUser.UID {
		return ctx.NotFound()
	}

	if MyUser.Is(user.RoleAdmin) {
		answer := new(ActionAnswer)
		answer.Success = false

		err := actions.RemoveMaecenas(c.DB, ctx.Payload.UID)
		if err != nil && err != pgx.ErrNoRows {
			c.log.Error().Err(err).Msg("Problem with removing maecenas from action")
			answer.Action = "Не могу остановить акцию мецената"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		err = uploads.DelProfileFiles(c.DB, ctx.Payload.UID)
		if err != nil && err != pgx.ErrNoRows {
			c.log.Error().Err(err).Msg("Problem with delete users photo from system")

			answer.Action = "Не могу удалить фото"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		err = MyUser.DeleteUsersProfile(ctx.Payload.UID)
		if err != nil {
			c.log.Error().Err(err).Msg("Problem with delete user from system")

			answer.Action = "Не могу удалить данные о пользователе"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		answer.Success = true
		answer.Action = "Пользователь успешно удален"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)

	}
	return ctx.NotFound()
}

// Recovery runs the recovery action.
func (c *ProfileController) Recovery(ctx *app.RecoveryProfileContext) error {
	var res app.MyservAuthToapp
	var tempPass string
	var err error

	MyUser := user.NewUser(c.DB, c.log)

	MyUser.UID, err = MyUser.CheckUIDByEmailNoSocnet(ctx.Payload.Email)
	if err != nil && err != pgx.ErrNoRows {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if !MyUser.CheckNoZeroPass(MyUser.UID) {
		return ctx.NotFound()
	}

	if err == pgx.ErrNoRows {
		return ctx.NotFound()
	}

	tempPass, err = MyUser.MakeTempPass()
	if err != nil {
		c.log.Error().Err(err).Msg("Error with generate temp pass for recover")
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	m := new(jobs.Email)
	m.DB = c.DB
	m.Sender = "system"
	m.Getter = ctx.Payload.Email
	m.UsersUID = MyUser.UID
	m.Body = tempPass
	MailSendResult := m.Send()
	if MailSendResult.Success == false {
		c.log.Error().Err(MailSendResult.Err).Msg("Error with sending temp pass to queue")
		return ctx.BadRequest(goa.ErrBadRequest(MailSendResult.Err))
	}

	answer := new(ActionAnswer)
	answer.Success = true
	answer.Action = "Временный пароль отправлен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
	// ProfileController_Recovery: end_implement
}

// Recoveryanswer runs the recoveryanswer action.
func (c *ProfileController) Recoveryanswer(ctx *app.RecoveryanswerProfileContext) error {
	var res app.MyservAuthToapp
	var err error

	MyUser := user.NewUser(c.DB, c.log)

	MyUser.UID, err = MyUser.CheckUIDByEmail(ctx.Payload.Email)
	if err != nil && err != pgx.ErrNoRows {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if err == pgx.ErrNoRows {
		return ctx.NotFound()
	}

	if !MyUser.Checktemprecover(ctx.Payload.Temppassword, MyUser.UID) {
		return ctx.NotFound()
	}

	isErr := MyUser.CodeThePassword(ctx.Payload.Newpassword, MyUser.UID)
	if isErr == nil {
		answer := new(ActionAnswer)
		answer.Success = true
		answer.Action = "Пароль успешно изменен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	return ctx.NotFound()
}

// Changeroleactive - change roles active status
func (c *ProfileController) Changeroleactive(ctx *app.ChangeroleactiveProfileContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	answer := new(ActionAnswer)
	answer.Success = false

	if (MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser)) && !MyUser.Is(user.RoleAdmin) {
		answer.Action = "Доступ к разделу запрещен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	if ctx.Payload.Role != string(user.RoleSponsor) && ctx.Payload.Role != string(user.RoleAdmin) && ctx.Payload.Role != string(user.RoleUser) {
		answer.Action = "Неверная роль пользователя"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	isExist, err := MyUser.CheckIfUserExist(ctx.Payload.UID)
	if err != nil && err != pgx.ErrNoRows {
		c.log.Error().Err(err).Msg("Error with checking for user exist in DB")
		answer.Action = "Проблемы с изменением роли пользователя"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	if !isExist {
		c.log.Error().Err(err).Msg("Error with checking for user exist in DB")
		answer.Action = "Пользователь не найден"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	err = MyUser.ChangeRoleActivity(ctx.Payload.UID, ctx.Payload.Role, ctx.Payload.Active)
	if err != nil {
		c.log.Error().Err(err).Msg("Error with change role active")
		answer.Action = "Проблемы с изменением роли пользователя"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}
	answer.Success = true
	answer.Action = "Роль успешно изменена"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)

}

// Changerole - request for role change
func (c *ProfileController) Changerole(ctx *app.ChangeroleProfileContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if (MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser)) && ctx.Payload.UID != uid && !MyUser.Is(user.RoleAdmin) {
		answer := new(ActionAnswer)
		answer.Success = false
		answer.Action = "Доступ к разделу запрещен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {

		NewRole := string(user.RoleSponsor)
		if ctx.Payload.Role != nil {
			if *ctx.Payload.Role == string(user.RoleSponsor) || *ctx.Payload.Role == string(user.RoleAdmin) {
				NewRole = *ctx.Payload.Role
			}
		}

		err := MyUser.Setuserrole(ctx.Payload.UID, NewRole)

		// Send notification email
		inDB, err := jobs.IsInDB(c.DB, jobs.Messtr{Qname: models.QnameSend, Getter: models.GetterNameAdmin}, models.ActionMaecenas)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with checking in queue db")
		} else {
			if !inDB {
				// Sending job to DB
				_ = jobs.ActionToDB(c.DB, jobs.Messtr{Qname: models.QnameSend, Getter: models.GetterNameAdmin}, models.ActionMaecenas)
			}
		}

		answer := new(ActionAnswer)
		answer.Success = true
		answer.Action = "Запрос отправлен"

		if err != nil {
			c.log.Error().Err(err).Msg("Error with request for change role")
			answer.Success = false
			answer.Action = "Ошибка формирования запроса"
		}

		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Доступ к разделу запрещен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)

}

// Setprofile runs the setprofile action.
func (c *ProfileController) Setprofile(ctx *app.SetprofileProfileContext) error {
	var res app.MyservAuthToapp
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	result, uid := c.Preset(token, claims)
	if !result {
		res = app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if (MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser)) && ctx.Payload.UID != uid && !MyUser.Is(user.RoleAdmin) {
		return ctx.NotFound()
	}

	if MyUser.Is(user.RoleAdmin) || MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleUser) {
		Profile := profile.New(c.DB, c.log, c.Conf.Picsurl.URL)

		var FirstName, LastName, Phone, Birthday, Instagram, Description string
		FirstName = ""
		LastName = ""
		Phone = ""
		Birthday = ""
		Instagram = ""
		Description = ""

		if ctx.Payload.FirstName != nil {
			FirstName = *ctx.Payload.FirstName
		}
		if ctx.Payload.LastName != nil {
			LastName = *ctx.Payload.LastName
		}
		if ctx.Payload.Phone != nil {
			Phone = *ctx.Payload.Phone
		}
		if ctx.Payload.Birthday != nil {
			Birthday = *ctx.Payload.Birthday
		}
		if ctx.Payload.Instagram != nil {
			Instagram = *ctx.Payload.Instagram
		}
		if ctx.Payload.Description != nil {
			Description = *ctx.Payload.Description
		}

		errPR := Profile.Put(ctx.Payload.UID, FirstName, LastName, Phone, Birthday, Instagram, Description)
		if errPR != nil {
			c.log.Error().Err(errPR).Msg("Error with saving profile")
			answer := new(ActionAnswer)
			answer.Success = false
			answer.Action = "Ошибка сохранения данных"
			res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
			return ctx.OK(&res)
		}

		answer := new(ActionAnswer)
		answer.Success = true
		answer.Action = "Профиль сохранен"
		res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
		return ctx.OK(&res)
	}
	answer := new(ActionAnswer)
	answer.Success = false
	answer.Action = "Доступ к разделу запрещен"
	res = app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Sections", "Status", &answer))
	return ctx.OK(&res)
}

// GetMyRoleFromToken ...
func (c *ProfileController) GetMyRoleFromToken(token *jwtgo.Token, claims jwtgo.MapClaims) (bool, user.RoleName) {
	myrole := ""
	log := c.log.With().Interface("claims", claims).Logger()
	if role, ok := claims["userType"]; ok {
		switch role.(type) {
		case string:
			myrole = role.(string)
		default:
			log.Warn().Msg("userType claim has wrong type")
			return false, user.RoleName(myrole)

		}
	} else {
		log.Warn().Msg("userType claim has wrong type")
		return false, user.RoleName(myrole)
	}
	return true, user.RoleName(myrole)
}

// Changepushstatus ...
func (c ProfileController) Changepushstatus(ctx *app.ChangepushstatusProfileContext) error {
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	res, uid := c.Preset(token, claims)
	resRole, _ := c.GetMyRoleFromToken(token, claims)

	if !res || !resRole {

		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}
	if !MyUser.Is(user.RoleAdmin) && MyUser.UID != ctx.Payload.UID {
		return ctx.BadRequest(goa.ErrBadRequest("access denied"))
	}
	if MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleAdmin) || MyUser.IsnotActive(user.RoleSponsor) || MyUser.Is(user.RoleUser) {

		prof := profile.New(c.DB, c.log, c.Conf.Picsurl.URL)
		err := prof.ChangePushStatus(ctx.Payload.UID)
		Ans := new(ActionAnswer)
		if err != nil {
			c.log.Error().Err(err).Msg("Error with change getpush status")

			Ans.Success = false
			Ans.Action = "Ошибка изменения настройки получения уведомлений пользователем"
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Profiles", "Status", &Ans))
			return ctx.OK(&res)
		}

		Ans.Success = true
		Ans.Action = "Изменения сохранены"
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Profiles", "Status", &Ans))
		return ctx.OK(&res)

	}
	return ctx.BadRequest(goa.ErrBadRequest("access denied"))
}

//Devicetoken - getting token of device for push notification
func (c ProfileController) Devicetoken(ctx *app.DevicetokenProfileContext) error {
	token := jwt.ContextJWT(ctx)
	claims := token.Claims.(jwtgo.MapClaims)
	res, uid := c.Preset(token, claims)
	resRole, _ := c.GetMyRoleFromToken(token, claims)

	CurRole := user.RoleUser

	if ctx.Payload.Role != nil {

		switch *ctx.Payload.Role {
		case string(user.RoleSponsor):
			CurRole = user.RoleSponsor
		case string(user.RoleAdmin):
			CurRole = user.RoleAdmin
		default:
			CurRole = user.RoleUser
		}

	}

	if !res || !resRole {

		res := app.MyservAuthToapp(*(new(mess.Unauthorized)).Prepare("Auth", "Unauthorized", UnauthorizedAnswer{Event: "error", Message: "Wrong token"}))
		return ctx.OK(&res)
	}

	MyUser := user.NewUser(c.DB, c.log)
	if err := c.LoadUserInfo(MyUser, uid); err != nil {
		return ctx.BadRequest(goa.ErrBadRequest(err))
	}

	if MyUser.Is(user.RoleSponsor) || MyUser.Is(user.RoleAdmin) || MyUser.IsnotActive(user.RoleSponsor) || MyUser.Is(user.RoleUser) {

		if *ctx.Payload.System != "ios" && *ctx.Payload.System != "android" {
			Ans := new(ActionAnswer)
			Ans.Success = false
			Ans.Action = "Неверный параметр System (либо ios, либо android)"
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Profiles", "Status", &Ans))
			c.log.Error().Time("Time", time.Now()).Msg("Invalid System parameter")
			return ctx.OK(&res)
		}

		if CurRole != user.RoleSponsor && CurRole != user.RoleUser {
			Ans := new(ActionAnswer)
			Ans.Success = false
			Ans.Action = "Неверная роль пользователя"
			c.log.Error().Time("Time", time.Now()).Msg("Invalid user role")
			res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Profiles", "Status", &Ans))
			return ctx.OK(&res)
		}
		err := MyUser.Devicetoken(*ctx.Payload.Token, *ctx.Payload.System, string(CurRole))
		if err != nil {
			return ctx.BadRequest(goa.ErrBadRequest(err))
		}
		Ans := new(ActionAnswer)
		Ans.Success = true
		Ans.Action = "Токен успешно сохранен"
		c.log.Info().Time("Time", time.Now()).Str("UserRole", string(CurRole)).Str("UserUID", uid).Msg("APP Token successfully saved")
		res := app.MyservAuthToapp(*(new(mess.OK_Auth)).Prepare("Profiles", "Status", &Ans))

		return ctx.OK(&res)
	}

	return ctx.BadRequest(goa.ErrBadRequest("access denied"))
}

//go:generate goagen bootstrap -d bitbucket.org/Sanny_Lebedev/heartapi/design

package main

import (
	"fmt"
	"os"

	"bitbucket.org/Sanny_Lebedev/heartapi/app"
	"bitbucket.org/Sanny_Lebedev/heartapi/logger"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/binpath"
	"bitbucket.org/Sanny_Lebedev/heartapi/utils/migration"
	"github.com/crgimenes/goconfig"
	_ "github.com/crgimenes/goconfig/json"
	"github.com/goadesign/goa"
	"github.com/goadesign/goa/middleware"
	"github.com/jackc/pgx"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/ssh/terminal"
)

// Env is environment configuration
type Env struct {
	myConf *configMain
}

var (
	// ErrUnauthorized is the error returned for unauthorized requests.
	ErrUnauthorized = goa.NewErrorClass("unauthorized", 401)
	config          *configMain
)

func initConfig() (*configMain, error) {
	myconfig := configMain{}
	goconfig.Path = binpath.BinaryPath() + "/"
	goconfig.File = "config.json"
	err := goconfig.Parse(&myconfig)
	if err != nil {

		//panic(err)
		log.Error().Err(err).Msg("failed to parse config file")
		return nil, err
	}
	return &myconfig, nil
}

func pgConnect(cfg *configMain, log logger.Logger) (*pgx.ConnPool, error) {
	pgConfig := new(pgx.ConnConfig)
	pgConfig.TLSConfig = nil
	connPoolConfig := pgx.ConnPoolConfig{
		ConnConfig: pgx.ConnConfig{
			Host:     cfg.Postgres.Host,
			User:     cfg.Postgres.Username,
			Password: cfg.Postgres.Password,
			Database: cfg.Postgres.Database,
			LogLevel: pgx.LogLevelWarn,
			Logger:   log,
		},
		MaxConnections: 50,
	}
	return pgx.NewConnPool(connPoolConfig)
}

func main() {
	// Create service
	service := goa.New("heart")
	var err1 error
	env := Env{}
	env.myConf, err1 = initConfig()

	if err1 != nil {
		// panic(err1)
	}

	var log logger.Logger

	if terminal.IsTerminal(int(os.Stdout.Fd())) || env.myConf.Logparam.Mode != "file" {
		log = logger.Logger{
			Logger: zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger(),
		}
	} else {
		file, err := os.OpenFile(env.myConf.Logparam.MainFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			panic(fmt.Sprintf("can't write log file: %v", err))
		}
		defer file.Close()
		log = logger.Logger{
			Logger: zerolog.New(file).With().Timestamp().Logger(),
		}
	}

	// if terminal.IsTerminal(int(os.Stdout.Fd())) {
	// 	log = logger.Logger{
	// 		Logger: zerolog.New(zerolog.ConsoleWriter{Out: os.Stdout}).With().Timestamp().Logger(),
	// 	}

	// } else {
	// 	file, err := os.OpenFile("/var/log/heart.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	// 	if err != nil {
	// 		panic(fmt.Sprintf("can't write log file: %v", err))
	// 	}
	// 	defer file.Close()
	// 	log = logger.Logger{
	// 		Logger: zerolog.New(file).With().Timestamp().Logger(),
	// 	}

	// }

	pg, err := pgConnect(env.myConf, log)
	if err != nil {
		log.Fatal().Err(err).Msg("failed to connect to db")
	}

	migration.MakeMigration(pg, env.myConf.DBMigration.Current)

	// Mount middleware
	service.Use(middleware.RequestID())
	service.Use(middleware.LogRequest(true))
	service.Use(middleware.ErrorHandler(service, true))
	service.Use(middleware.Recover())
	service.WithLogger(log.GoaLogger())

	jwtMiddleware, err := NewAuthJWTMiddleware()
	exitOnFailure(err)

	app.UseAPIKeyMiddleware(service, NewAPIKeyMiddleware())
	app.UseJWTMiddleware(service, jwtMiddleware)

	// Mount "auth" controller
	c := NewAPIKeyController(service)
	app.MountAPIKeyController(service, c)

	c1, err := NewAuthController(service, pg, log, *env.myConf)
	exitOnFailure(err)
	app.MountAuthController(service, c1)

	// Mount "sections" controller
	c2, err := NewSectionsController(service, pg, log, *env.myConf)
	exitOnFailure(err)
	app.MountSectionsController(service, c2)

	// Mount "fundraising" controller
	c3, err := NewFundraisingController(service, pg, log, *env.myConf)
	exitOnFailure(err)
	app.MountFundraisingController(service, c3)

	// Mount "profile" controller
	c4, err := NewProfileController(service, pg, log, *env.myConf)
	exitOnFailure(err)
	app.MountProfileController(service, c4)

	// Mount "image" controller
	c5, err := NewImageController(service, pg, log, env.myConf.Picsurl.URL)
	exitOnFailure(err)
	app.MountImageController(service, c5)

	// Mount "funds" controller
	c6, err := NewFundsController(service, pg, log, env.myConf.Picsurl.URL)
	exitOnFailure(err)
	app.MountFundsController(service, c6)

	// Mount "actions" controller
	c7, err := NewActionsController(service, pg, log, env.myConf.Picsurl.URL, env.myConf.Options, *env.myConf)
	exitOnFailure(err)
	app.MountActionsController(service, c7)

	// Mount "prize" controller
	c8, err := NewPrizeController(service, pg, log, env.myConf.Picsurl.URL, env.myConf.Options)
	exitOnFailure(err)
	app.MountPrizeController(service, c8)

	// Mount "likes" controller
	c9, err := NewLikesController(service, pg, log, env.myConf.Picsurl.URL, env.myConf.Options)
	exitOnFailure(err)
	app.MountLikesController(service, c9)

	// Mount "history" controller
	c10, err := NewHistoryController(service, pg, log, env.myConf.Picsurl.URL, env.myConf.Options.Domain)
	exitOnFailure(err)
	app.MountHistoryController(service, c10)

	// Mount "Users" controller
	c11, err := NewUsersController(service, pg, log, env.myConf.Picsurl.URL)
	exitOnFailure(err)
	app.MountUsersController(service, c11)

	// Mount "Payment" controller
	c12, err := NewPaymentController(service, pg, log, env.myConf.Sberbank, env.myConf.Options.EmailVerify)
	exitOnFailure(err)
	app.MountPaymentController(service, c12)

	// Mount "bank" controller
	c13, err := NewBankController(service, pg, log, env.myConf.Picsurl.URL, env.myConf.Options)
	exitOnFailure(err)
	app.MountBankController(service, c13)

	// Mount "jwt" controller
	// c2 := NewJWTController(service)
	// app.MountJWTController(service, c2)

	// Start service
	if err := service.ListenAndServe(":1200"); err != nil {
		service.LogError("startup", "err", err)
	}

}

// exitOnFailure prints a fatal error message and exits the process with status 1.
func exitOnFailure(err error) {
	if err == nil {
		return
	}
	fmt.Fprintf(os.Stderr, "[CRIT] %s", err.Error())
	os.Exit(1)
}
